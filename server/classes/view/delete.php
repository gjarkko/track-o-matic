<? $r = RequestHandler::getInstance(); ?>
Are you sure you want to delete <?=$this->values['type'];?> <?=$this->values['id'];?>?

<form method="post" action="<?=$r->getCurrentActionUrl(true, array('delete'));?>">
<p>
	<input type="hidden" name="id" value="<?=$this->values['id'];?>" />
	<input type="submit" name="confirmdelete" class="button" value="<?=ucfirst(Localizer::getText('yes'));?>" />
	<input type="submit" name="cancel" class="button" value="<?=ucfirst(Localizer::getText('no'));?>" />
</p> 
</form>
