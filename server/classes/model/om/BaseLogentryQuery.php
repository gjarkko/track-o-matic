<?php


/**
 * Base class that represents a query for the 'logentry' table.
 *
 * 
 *
 * @method     LogentryQuery orderByLogentryid($order = Criteria::ASC) Order by the logentryid column
 * @method     LogentryQuery orderByLogentrytypeid($order = Criteria::ASC) Order by the logentrytypeid column
 * @method     LogentryQuery orderByUserid($order = Criteria::ASC) Order by the userid column
 * @method     LogentryQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     LogentryQuery orderByTime($order = Criteria::ASC) Order by the time column
 * @method     LogentryQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     LogentryQuery orderBySessionid($order = Criteria::ASC) Order by the sessionid column
 * @method     LogentryQuery orderBySessionage($order = Criteria::ASC) Order by the sessionage column
 * @method     LogentryQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method     LogentryQuery orderByReferencetype($order = Criteria::ASC) Order by the referencetype column
 * @method     LogentryQuery orderByReferenceid($order = Criteria::ASC) Order by the referenceid column
 *
 * @method     LogentryQuery groupByLogentryid() Group by the logentryid column
 * @method     LogentryQuery groupByLogentrytypeid() Group by the logentrytypeid column
 * @method     LogentryQuery groupByUserid() Group by the userid column
 * @method     LogentryQuery groupByCustomerid() Group by the customerid column
 * @method     LogentryQuery groupByTime() Group by the time column
 * @method     LogentryQuery groupByIp() Group by the ip column
 * @method     LogentryQuery groupBySessionid() Group by the sessionid column
 * @method     LogentryQuery groupBySessionage() Group by the sessionage column
 * @method     LogentryQuery groupByMessage() Group by the message column
 * @method     LogentryQuery groupByReferencetype() Group by the referencetype column
 * @method     LogentryQuery groupByReferenceid() Group by the referenceid column
 *
 * @method     LogentryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     LogentryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     LogentryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     LogentryQuery leftJoinLogentrytype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Logentrytype relation
 * @method     LogentryQuery rightJoinLogentrytype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Logentrytype relation
 * @method     LogentryQuery innerJoinLogentrytype($relationAlias = null) Adds a INNER JOIN clause to the query using the Logentrytype relation
 *
 * @method     LogentryQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     LogentryQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     LogentryQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     LogentryQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     LogentryQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     LogentryQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     Logentry findOne(PropelPDO $con = null) Return the first Logentry matching the query
 * @method     Logentry findOneOrCreate(PropelPDO $con = null) Return the first Logentry matching the query, or a new Logentry object populated from the query conditions when no match is found
 *
 * @method     Logentry findOneByLogentryid(int $logentryid) Return the first Logentry filtered by the logentryid column
 * @method     Logentry findOneByLogentrytypeid(int $logentrytypeid) Return the first Logentry filtered by the logentrytypeid column
 * @method     Logentry findOneByUserid(int $userid) Return the first Logentry filtered by the userid column
 * @method     Logentry findOneByCustomerid(int $customerid) Return the first Logentry filtered by the customerid column
 * @method     Logentry findOneByTime(string $time) Return the first Logentry filtered by the time column
 * @method     Logentry findOneByIp(string $ip) Return the first Logentry filtered by the ip column
 * @method     Logentry findOneBySessionid(string $sessionid) Return the first Logentry filtered by the sessionid column
 * @method     Logentry findOneBySessionage(double $sessionage) Return the first Logentry filtered by the sessionage column
 * @method     Logentry findOneByMessage(string $message) Return the first Logentry filtered by the message column
 * @method     Logentry findOneByReferencetype(string $referencetype) Return the first Logentry filtered by the referencetype column
 * @method     Logentry findOneByReferenceid(int $referenceid) Return the first Logentry filtered by the referenceid column
 *
 * @method     array findByLogentryid(int $logentryid) Return Logentry objects filtered by the logentryid column
 * @method     array findByLogentrytypeid(int $logentrytypeid) Return Logentry objects filtered by the logentrytypeid column
 * @method     array findByUserid(int $userid) Return Logentry objects filtered by the userid column
 * @method     array findByCustomerid(int $customerid) Return Logentry objects filtered by the customerid column
 * @method     array findByTime(string $time) Return Logentry objects filtered by the time column
 * @method     array findByIp(string $ip) Return Logentry objects filtered by the ip column
 * @method     array findBySessionid(string $sessionid) Return Logentry objects filtered by the sessionid column
 * @method     array findBySessionage(double $sessionage) Return Logentry objects filtered by the sessionage column
 * @method     array findByMessage(string $message) Return Logentry objects filtered by the message column
 * @method     array findByReferencetype(string $referencetype) Return Logentry objects filtered by the referencetype column
 * @method     array findByReferenceid(int $referenceid) Return Logentry objects filtered by the referenceid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseLogentryQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseLogentryQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Logentry', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new LogentryQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    LogentryQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof LogentryQuery) {
			return $criteria;
		}
		$query = new LogentryQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Logentry|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = LogentryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(LogentryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Logentry A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `LOGENTRYID`, `LOGENTRYTYPEID`, `USERID`, `CUSTOMERID`, `TIME`, `IP`, `SESSIONID`, `SESSIONAGE`, `MESSAGE`, `REFERENCETYPE`, `REFERENCEID` FROM `logentry` WHERE `LOGENTRYID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Logentry();
			$obj->hydrate($row);
			LogentryPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Logentry|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(LogentryPeer::LOGENTRYID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(LogentryPeer::LOGENTRYID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the logentryid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLogentryid(1234); // WHERE logentryid = 1234
	 * $query->filterByLogentryid(array(12, 34)); // WHERE logentryid IN (12, 34)
	 * $query->filterByLogentryid(array('min' => 12)); // WHERE logentryid > 12
	 * </code>
	 *
	 * @param     mixed $logentryid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByLogentryid($logentryid = null, $comparison = null)
	{
		if (is_array($logentryid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(LogentryPeer::LOGENTRYID, $logentryid, $comparison);
	}

	/**
	 * Filter the query on the logentrytypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLogentrytypeid(1234); // WHERE logentrytypeid = 1234
	 * $query->filterByLogentrytypeid(array(12, 34)); // WHERE logentrytypeid IN (12, 34)
	 * $query->filterByLogentrytypeid(array('min' => 12)); // WHERE logentrytypeid > 12
	 * </code>
	 *
	 * @see       filterByLogentrytype()
	 *
	 * @param     mixed $logentrytypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByLogentrytypeid($logentrytypeid = null, $comparison = null)
	{
		if (is_array($logentrytypeid)) {
			$useMinMax = false;
			if (isset($logentrytypeid['min'])) {
				$this->addUsingAlias(LogentryPeer::LOGENTRYTYPEID, $logentrytypeid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($logentrytypeid['max'])) {
				$this->addUsingAlias(LogentryPeer::LOGENTRYTYPEID, $logentrytypeid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::LOGENTRYTYPEID, $logentrytypeid, $comparison);
	}

	/**
	 * Filter the query on the userid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByUserid(1234); // WHERE userid = 1234
	 * $query->filterByUserid(array(12, 34)); // WHERE userid IN (12, 34)
	 * $query->filterByUserid(array('min' => 12)); // WHERE userid > 12
	 * </code>
	 *
	 * @see       filterByUser()
	 *
	 * @param     mixed $userid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByUserid($userid = null, $comparison = null)
	{
		if (is_array($userid)) {
			$useMinMax = false;
			if (isset($userid['min'])) {
				$this->addUsingAlias(LogentryPeer::USERID, $userid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($userid['max'])) {
				$this->addUsingAlias(LogentryPeer::USERID, $userid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::USERID, $userid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(LogentryPeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(LogentryPeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the time column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTime('2011-03-14'); // WHERE time = '2011-03-14'
	 * $query->filterByTime('now'); // WHERE time = '2011-03-14'
	 * $query->filterByTime(array('max' => 'yesterday')); // WHERE time > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $time The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByTime($time = null, $comparison = null)
	{
		if (is_array($time)) {
			$useMinMax = false;
			if (isset($time['min'])) {
				$this->addUsingAlias(LogentryPeer::TIME, $time['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($time['max'])) {
				$this->addUsingAlias(LogentryPeer::TIME, $time['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::TIME, $time, $comparison);
	}

	/**
	 * Filter the query on the ip column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
	 * $query->filterByIp('%fooValue%'); // WHERE ip LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $ip The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByIp($ip = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($ip)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $ip)) {
				$ip = str_replace('*', '%', $ip);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LogentryPeer::IP, $ip, $comparison);
	}

	/**
	 * Filter the query on the sessionid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterBySessionid('fooValue');   // WHERE sessionid = 'fooValue'
	 * $query->filterBySessionid('%fooValue%'); // WHERE sessionid LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $sessionid The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterBySessionid($sessionid = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($sessionid)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $sessionid)) {
				$sessionid = str_replace('*', '%', $sessionid);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LogentryPeer::SESSIONID, $sessionid, $comparison);
	}

	/**
	 * Filter the query on the sessionage column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterBySessionage(1234); // WHERE sessionage = 1234
	 * $query->filterBySessionage(array(12, 34)); // WHERE sessionage IN (12, 34)
	 * $query->filterBySessionage(array('min' => 12)); // WHERE sessionage > 12
	 * </code>
	 *
	 * @param     mixed $sessionage The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterBySessionage($sessionage = null, $comparison = null)
	{
		if (is_array($sessionage)) {
			$useMinMax = false;
			if (isset($sessionage['min'])) {
				$this->addUsingAlias(LogentryPeer::SESSIONAGE, $sessionage['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($sessionage['max'])) {
				$this->addUsingAlias(LogentryPeer::SESSIONAGE, $sessionage['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::SESSIONAGE, $sessionage, $comparison);
	}

	/**
	 * Filter the query on the message column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
	 * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $message The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByMessage($message = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($message)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $message)) {
				$message = str_replace('*', '%', $message);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LogentryPeer::MESSAGE, $message, $comparison);
	}

	/**
	 * Filter the query on the referencetype column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByReferencetype('fooValue');   // WHERE referencetype = 'fooValue'
	 * $query->filterByReferencetype('%fooValue%'); // WHERE referencetype LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $referencetype The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByReferencetype($referencetype = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($referencetype)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $referencetype)) {
				$referencetype = str_replace('*', '%', $referencetype);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LogentryPeer::REFERENCETYPE, $referencetype, $comparison);
	}

	/**
	 * Filter the query on the referenceid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByReferenceid(1234); // WHERE referenceid = 1234
	 * $query->filterByReferenceid(array(12, 34)); // WHERE referenceid IN (12, 34)
	 * $query->filterByReferenceid(array('min' => 12)); // WHERE referenceid > 12
	 * </code>
	 *
	 * @param     mixed $referenceid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByReferenceid($referenceid = null, $comparison = null)
	{
		if (is_array($referenceid)) {
			$useMinMax = false;
			if (isset($referenceid['min'])) {
				$this->addUsingAlias(LogentryPeer::REFERENCEID, $referenceid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($referenceid['max'])) {
				$this->addUsingAlias(LogentryPeer::REFERENCEID, $referenceid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LogentryPeer::REFERENCEID, $referenceid, $comparison);
	}

	/**
	 * Filter the query by a related Logentrytype object
	 *
	 * @param     Logentrytype|PropelCollection $logentrytype The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByLogentrytype($logentrytype, $comparison = null)
	{
		if ($logentrytype instanceof Logentrytype) {
			return $this
				->addUsingAlias(LogentryPeer::LOGENTRYTYPEID, $logentrytype->getLogentrytypeid(), $comparison);
		} elseif ($logentrytype instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(LogentryPeer::LOGENTRYTYPEID, $logentrytype->toKeyValue('PrimaryKey', 'Logentrytypeid'), $comparison);
		} else {
			throw new PropelException('filterByLogentrytype() only accepts arguments of type Logentrytype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Logentrytype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function joinLogentrytype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Logentrytype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Logentrytype');
		}

		return $this;
	}

	/**
	 * Use the Logentrytype relation Logentrytype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentrytypeQuery A secondary query class using the current class as primary query
	 */
	public function useLogentrytypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinLogentrytype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Logentrytype', 'LogentrytypeQuery');
	}

	/**
	 * Filter the query by a related User object
	 *
	 * @param     User|PropelCollection $user The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByUser($user, $comparison = null)
	{
		if ($user instanceof User) {
			return $this
				->addUsingAlias(LogentryPeer::USERID, $user->getUserid(), $comparison);
		} elseif ($user instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(LogentryPeer::USERID, $user->toKeyValue('PrimaryKey', 'Userid'), $comparison);
		} else {
			throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the User relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('User');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'User');
		}

		return $this;
	}

	/**
	 * Use the User relation User object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    UserQuery A secondary query class using the current class as primary query
	 */
	public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinUser($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'User', 'UserQuery');
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(LogentryPeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(LogentryPeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Logentry $logentry Object to remove from the list of results
	 *
	 * @return    LogentryQuery The current query, for fluid interface
	 */
	public function prune($logentry = null)
	{
		if ($logentry) {
			$this->addUsingAlias(LogentryPeer::LOGENTRYID, $logentry->getLogentryid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseLogentryQuery