<?php



/**
 * Skeleton subclass for representing a row from the 'tripvehicle' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Tripvehicle extends BaseTripvehicle {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => false,
			'allowCreate' => true,
			'allowDelete' => false,
		);
	}
	
	public function getTripReference() {
		if (is_object($this->getTrip())) {
			return $this->getTrip()->getTripreference();
		}
		else {
			return NULL;
		}
	}
	
	public function getVehicleReference() {
		if (is_object($this->getVehicle())) {
			return $this->getVehicle()->getVehiclereference();
		}
		else {
			return NULL;
		}
	}
	
	public static function getBrowserColumnMap($user = NULL) {
		return array(
			array(
				'column' => 'trip',
				'label' => ucfirst(Localizer::getText('trip')),
				'foreign' => true,
				'display_method' => 'getTripReference',
				'display_SQL' => 'tripreference',
				
				'join' => array(
					'relation' => 'Trip',
					'table' => 'trip',
					'own_key' => 'tripid',
					'foreign_key' => 'tripid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'vehicle',
				'label' => ucfirst(Localizer::getText('vehicle')),
				'foreign' => true,
				'display_method' => 'getVehicleReference',
				'display_SQL' => 'vehiclereference',
		
				'join' => array(
					'relation' => 'Vehicle',
					'table' => 'vehicle',
					'own_key' => 'vehicleid',
					'foreign_key' => 'vehicleid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
		);
	}
	
		
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'trip',
				'label' => ucfirst(Localizer::getText('trip')),
				'foreign' => true,
				'getMethod' => 'getTripReference',
				'getIdMethod' => 'getTripid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'vehicle',
				'label' => ucfirst(Localizer::getText('vehicle')),
				'foreign' => true,
				'getMethod' => 'getVehicleReference',
				'getIdMethod' => 'getVehicleid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
		);
	}
		
} // Tripvehicle
