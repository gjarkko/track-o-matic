<?php


/**
 * Base class that represents a query for the 'vehicle' table.
 *
 * 
 *
 * @method     VehicleQuery orderByVehicleid($order = Criteria::ASC) Order by the vehicleid column
 * @method     VehicleQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     VehicleQuery orderByVehiclereference($order = Criteria::ASC) Order by the vehiclereference column
 * @method     VehicleQuery orderByApikey($order = Criteria::ASC) Order by the apikey column
 * @method     VehicleQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     VehicleQuery groupByVehicleid() Group by the vehicleid column
 * @method     VehicleQuery groupByCustomerid() Group by the customerid column
 * @method     VehicleQuery groupByVehiclereference() Group by the vehiclereference column
 * @method     VehicleQuery groupByApikey() Group by the apikey column
 * @method     VehicleQuery groupByValid() Group by the valid column
 *
 * @method     VehicleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     VehicleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     VehicleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     VehicleQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     VehicleQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     VehicleQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     VehicleQuery leftJoinVehiclestatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vehiclestatus relation
 * @method     VehicleQuery rightJoinVehiclestatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vehiclestatus relation
 * @method     VehicleQuery innerJoinVehiclestatus($relationAlias = null) Adds a INNER JOIN clause to the query using the Vehiclestatus relation
 *
 * @method     VehicleQuery leftJoinTripvehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tripvehicle relation
 * @method     VehicleQuery rightJoinTripvehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tripvehicle relation
 * @method     VehicleQuery innerJoinTripvehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Tripvehicle relation
 *
 * @method     Vehicle findOne(PropelPDO $con = null) Return the first Vehicle matching the query
 * @method     Vehicle findOneOrCreate(PropelPDO $con = null) Return the first Vehicle matching the query, or a new Vehicle object populated from the query conditions when no match is found
 *
 * @method     Vehicle findOneByVehicleid(int $vehicleid) Return the first Vehicle filtered by the vehicleid column
 * @method     Vehicle findOneByCustomerid(int $customerid) Return the first Vehicle filtered by the customerid column
 * @method     Vehicle findOneByVehiclereference(string $vehiclereference) Return the first Vehicle filtered by the vehiclereference column
 * @method     Vehicle findOneByApikey(string $apikey) Return the first Vehicle filtered by the apikey column
 * @method     Vehicle findOneByValid(boolean $valid) Return the first Vehicle filtered by the valid column
 *
 * @method     array findByVehicleid(int $vehicleid) Return Vehicle objects filtered by the vehicleid column
 * @method     array findByCustomerid(int $customerid) Return Vehicle objects filtered by the customerid column
 * @method     array findByVehiclereference(string $vehiclereference) Return Vehicle objects filtered by the vehiclereference column
 * @method     array findByApikey(string $apikey) Return Vehicle objects filtered by the apikey column
 * @method     array findByValid(boolean $valid) Return Vehicle objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseVehicleQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseVehicleQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Vehicle', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new VehicleQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    VehicleQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof VehicleQuery) {
			return $criteria;
		}
		$query = new VehicleQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Vehicle|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = VehiclePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(VehiclePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Vehicle A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `VEHICLEID`, `CUSTOMERID`, `VEHICLEREFERENCE`, `APIKEY`, `VALID` FROM `vehicle` WHERE `VEHICLEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Vehicle();
			$obj->hydrate($row);
			VehiclePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Vehicle|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(VehiclePeer::VEHICLEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(VehiclePeer::VEHICLEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the vehicleid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByVehicleid(1234); // WHERE vehicleid = 1234
	 * $query->filterByVehicleid(array(12, 34)); // WHERE vehicleid IN (12, 34)
	 * $query->filterByVehicleid(array('min' => 12)); // WHERE vehicleid > 12
	 * </code>
	 *
	 * @param     mixed $vehicleid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByVehicleid($vehicleid = null, $comparison = null)
	{
		if (is_array($vehicleid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(VehiclePeer::VEHICLEID, $vehicleid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(VehiclePeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(VehiclePeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclePeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the vehiclereference column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByVehiclereference('fooValue');   // WHERE vehiclereference = 'fooValue'
	 * $query->filterByVehiclereference('%fooValue%'); // WHERE vehiclereference LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $vehiclereference The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByVehiclereference($vehiclereference = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($vehiclereference)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $vehiclereference)) {
				$vehiclereference = str_replace('*', '%', $vehiclereference);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(VehiclePeer::VEHICLEREFERENCE, $vehiclereference, $comparison);
	}

	/**
	 * Filter the query on the apikey column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByApikey('fooValue');   // WHERE apikey = 'fooValue'
	 * $query->filterByApikey('%fooValue%'); // WHERE apikey LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $apikey The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByApikey($apikey = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($apikey)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $apikey)) {
				$apikey = str_replace('*', '%', $apikey);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(VehiclePeer::APIKEY, $apikey, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(VehiclePeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(VehiclePeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(VehiclePeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Filter the query by a related Vehiclestatus object
	 *
	 * @param     Vehiclestatus $vehiclestatus  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByVehiclestatus($vehiclestatus, $comparison = null)
	{
		if ($vehiclestatus instanceof Vehiclestatus) {
			return $this
				->addUsingAlias(VehiclePeer::VEHICLEID, $vehiclestatus->getVehicleid(), $comparison);
		} elseif ($vehiclestatus instanceof PropelCollection) {
			return $this
				->useVehiclestatusQuery()
				->filterByPrimaryKeys($vehiclestatus->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByVehiclestatus() only accepts arguments of type Vehiclestatus or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Vehiclestatus relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function joinVehiclestatus($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Vehiclestatus');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Vehiclestatus');
		}

		return $this;
	}

	/**
	 * Use the Vehiclestatus relation Vehiclestatus object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehiclestatusQuery A secondary query class using the current class as primary query
	 */
	public function useVehiclestatusQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinVehiclestatus($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Vehiclestatus', 'VehiclestatusQuery');
	}

	/**
	 * Filter the query by a related Tripvehicle object
	 *
	 * @param     Tripvehicle $tripvehicle  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function filterByTripvehicle($tripvehicle, $comparison = null)
	{
		if ($tripvehicle instanceof Tripvehicle) {
			return $this
				->addUsingAlias(VehiclePeer::VEHICLEID, $tripvehicle->getVehicleid(), $comparison);
		} elseif ($tripvehicle instanceof PropelCollection) {
			return $this
				->useTripvehicleQuery()
				->filterByPrimaryKeys($tripvehicle->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTripvehicle() only accepts arguments of type Tripvehicle or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Tripvehicle relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function joinTripvehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tripvehicle');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tripvehicle');
		}

		return $this;
	}

	/**
	 * Use the Tripvehicle relation Tripvehicle object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripvehicleQuery A secondary query class using the current class as primary query
	 */
	public function useTripvehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTripvehicle($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tripvehicle', 'TripvehicleQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Vehicle $vehicle Object to remove from the list of results
	 *
	 * @return    VehicleQuery The current query, for fluid interface
	 */
	public function prune($vehicle = null)
	{
		if ($vehicle) {
			$this->addUsingAlias(VehiclePeer::VEHICLEID, $vehicle->getVehicleid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseVehicleQuery