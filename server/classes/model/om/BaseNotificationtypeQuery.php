<?php


/**
 * Base class that represents a query for the 'notificationtype' table.
 *
 * 
 *
 * @method     NotificationtypeQuery orderByNotificationtypeid($order = Criteria::ASC) Order by the notificationtypeid column
 * @method     NotificationtypeQuery orderByNotificationtype($order = Criteria::ASC) Order by the notificationtype column
 *
 * @method     NotificationtypeQuery groupByNotificationtypeid() Group by the notificationtypeid column
 * @method     NotificationtypeQuery groupByNotificationtype() Group by the notificationtype column
 *
 * @method     NotificationtypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     NotificationtypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     NotificationtypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     NotificationtypeQuery leftJoinNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the Notification relation
 * @method     NotificationtypeQuery rightJoinNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Notification relation
 * @method     NotificationtypeQuery innerJoinNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the Notification relation
 *
 * @method     Notificationtype findOne(PropelPDO $con = null) Return the first Notificationtype matching the query
 * @method     Notificationtype findOneOrCreate(PropelPDO $con = null) Return the first Notificationtype matching the query, or a new Notificationtype object populated from the query conditions when no match is found
 *
 * @method     Notificationtype findOneByNotificationtypeid(int $notificationtypeid) Return the first Notificationtype filtered by the notificationtypeid column
 * @method     Notificationtype findOneByNotificationtype(string $notificationtype) Return the first Notificationtype filtered by the notificationtype column
 *
 * @method     array findByNotificationtypeid(int $notificationtypeid) Return Notificationtype objects filtered by the notificationtypeid column
 * @method     array findByNotificationtype(string $notificationtype) Return Notificationtype objects filtered by the notificationtype column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseNotificationtypeQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseNotificationtypeQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Notificationtype', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new NotificationtypeQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    NotificationtypeQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof NotificationtypeQuery) {
			return $criteria;
		}
		$query = new NotificationtypeQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Notificationtype|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = NotificationtypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(NotificationtypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Notificationtype A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `NOTIFICATIONTYPEID`, `NOTIFICATIONTYPE` FROM `notificationtype` WHERE `NOTIFICATIONTYPEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Notificationtype();
			$obj->hydrate($row);
			NotificationtypePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Notificationtype|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the notificationtypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByNotificationtypeid(1234); // WHERE notificationtypeid = 1234
	 * $query->filterByNotificationtypeid(array(12, 34)); // WHERE notificationtypeid IN (12, 34)
	 * $query->filterByNotificationtypeid(array('min' => 12)); // WHERE notificationtypeid > 12
	 * </code>
	 *
	 * @param     mixed $notificationtypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function filterByNotificationtypeid($notificationtypeid = null, $comparison = null)
	{
		if (is_array($notificationtypeid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPEID, $notificationtypeid, $comparison);
	}

	/**
	 * Filter the query on the notificationtype column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByNotificationtype('fooValue');   // WHERE notificationtype = 'fooValue'
	 * $query->filterByNotificationtype('%fooValue%'); // WHERE notificationtype LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $notificationtype The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function filterByNotificationtype($notificationtype = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($notificationtype)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $notificationtype)) {
				$notificationtype = str_replace('*', '%', $notificationtype);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPE, $notificationtype, $comparison);
	}

	/**
	 * Filter the query by a related Notification object
	 *
	 * @param     Notification $notification  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function filterByNotification($notification, $comparison = null)
	{
		if ($notification instanceof Notification) {
			return $this
				->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPEID, $notification->getNotificationtypeid(), $comparison);
		} elseif ($notification instanceof PropelCollection) {
			return $this
				->useNotificationQuery()
				->filterByPrimaryKeys($notification->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByNotification() only accepts arguments of type Notification or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Notification relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function joinNotification($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Notification');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Notification');
		}

		return $this;
	}

	/**
	 * Use the Notification relation Notification object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery A secondary query class using the current class as primary query
	 */
	public function useNotificationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinNotification($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Notification', 'NotificationQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Notificationtype $notificationtype Object to remove from the list of results
	 *
	 * @return    NotificationtypeQuery The current query, for fluid interface
	 */
	public function prune($notificationtype = null)
	{
		if ($notificationtype) {
			$this->addUsingAlias(NotificationtypePeer::NOTIFICATIONTYPEID, $notificationtype->getNotificationtypeid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseNotificationtypeQuery