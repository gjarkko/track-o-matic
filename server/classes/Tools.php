<?

class Tools {
	
	public static function haversineDistance($lat1, $lng1, $lat2, $lng2) {
		$radius = 6378100; // radius of earth in meters
		$latDist = $lat1 - $lat2;
		$lngDist = $lng1 - $lng2;
		$latDistRad = deg2rad($latDist);
		$lngDistRad = deg2rad($lngDist);
		$sinLatD = sin($latDistRad);
		$sinLngD = sin($lngDistRad);
		$cosLat1 = cos(deg2rad($lat1));
		$cosLat2 = cos(deg2rad($lat2));
		$a = abs(
			$sinLatD * $sinLatD
			 + $cosLat1 * $cosLat2 * $sinLngD * $sinLngD * $sinLngD
		);
		$arcDelta = 2 * atan2(sqrt($a), sqrt(1-$a));
		return $radius*$arcDelta;
	}

	public static function displayError($error) {
		$levels = array(
			1 => 'E_ERROR',
			2 => 'E_WARNING',
			4 => 'E_PARSE',
			8 => 'E_NOTICE',
			16 => 'E_CORE_ERROR',
			32 => 'E_CORE_WARNING',
			64 => 'E_COMPILE_ERROR',
			128 => 'E_COMPILE_WARNING',
			256 => 'E_USER_ERROR',
			512 => 'E_USER_WARNING',
			1024 => 'E_USER_NOTICE',
			2048 => 'E_STRICT',
			4096 => 'E_RECOVERABLE_ERROR',
			8192 => 'E_DEPRECATED',
			16384 => 'E_USER_DEPRECATED',
			32767 => 'E_ALL',
		);
		$error['level'] = $levels[$error['type']];
		
		$errortemplate = '<!DOCTYPEHTML><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><link href="http://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet" type="text/css"><link rel="stylesheet" type="text/css" href="/tom/css/default.css" /></head><body class="gradient"><div id="content"><div class="logo"><img src="/tom/image/trackomatic.png" alt="Trackomatic" title="Trackomatic" /></div><div class="version">Prototype version 0.1</div><div id="userdetails">&nbsp;</div><div id="topmenu"><ul><li class="active"><a href="/tom/main/login">Login</a></li></ul></div><div id="main">%ERROR%</div></div></div></body></html>';
		
		if(Session::getInstance()->usingPhpSessions()) {
			$errorOutput = '
				<h1 style="font-size: 20pt; color: #e05800;">Oops!</h1>
				<div style="font-size: 16pt; color: #333; border-radius: 5px; background-color: #e0e0f0; padding: 5px;">
					Error in file<br />
					<span style="color: #000; font-family: monospace; font-size: 10pt">' . $error['file'] . '</span><br />
					' . $error['level'] . ' on line ' . $error['line'] . ':<br /> 
					<span style="color: #e05800; font-size: 18pt">' . $error['message'] . '<br />
				</div>';
						
			
			$output = str_replace(
				'%ERROR%', 
				$errorOutput, 
				$errortemplate
			);
			echo $output;
		} else {			
			echo json_encode(array('state' => 0, 'message' => 'Internal system error: ' . $error['message'], 'error' => $error));
			exit();
		}
	}
	
	public static function handleError($errno, $errstr, $errfile, $errline) {
		$error = array(
			'type' => $errno,
			'message' => $errstr,
			'file' => $errfile,
			'line' => $errline
		);
		Tools::displayError($error);
		return true;
	}
	
	public static function shutdown() { 
	    $error=error_get_last(); 
	    if($error) {
	    	Tools::displayError($error);
	    } 
	} 
 	
	/**
	 * 
	 * Recursively look for file in given directory.
	 * Doesn't support non-recursive searches, since 
	 * one should simply use file_exists() for that.
	 */
	public static function findFile($file, $path) {

		$directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::FOLLOW_SYMLINKS);
		$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::CHILD_FIRST);
		$regex = new RegexIterator($iterator, '/^.*\\' . DIRECTORY_SEPARATOR . $file . '$/i', RegexIterator::GET_MATCH);
		
		foreach ($regex as $match) {
			return $match[0];
		}
		return false;
	}
		
	/**
	 * Dump a variable/object with PRE tags.
	 */
	public static function dump($obj, $exit = false) {
		echo "<pre>";
		var_dump($obj);
		echo "</pre>";
		if ($exit) {
			exit();
		}
	} 
	
	
	/**
	 * Dig around httpd variables for the most likely client IP
	 */
	public static function get_client_ip() {
		
		$lookup_vars = array(
			"HTTP_CLIENT_IP",
			"HTTP_X_FORWARDED_FOR",
			"HTTP_X_FORWARDED",
			"HTTP_FORWARDED_FOR",
			"HTTP_FORWARDED",
			"REMOTE_ADDR",
		);
		
		foreach ($lookup_vars as $var) {
			if (isset($_SERVER[$var])) {
				return $var;
			}
		}
		
		return NULL;
	}
	
	/**
	* Remove double slashes from path names
	*/
	public static function sanitizePath($path) {
		return preg_replace('/\/{2,}/', '/', $path);
	}
	
	public static function getHttpCode($code) {
		$httpErrorCodes = array(
			'100' => 'Continue', // Only a part of the request has been received by the server, but as long as it has not been rejected, the client should continue with the request
			'101' => 'Switching Protocols', // The server switches protocol
			'200' => 'OK', // The request is OK';
			'201' => 'Created', // The request is complete, and a new resource is created
			'202' => 'Accepted', // The request is accepted for processing, but the processing is not complete
			'203' => 'Non-authoritative Information',
			'204' => 'No Content',
			'205' => 'Reset Content',
			'206' => 'Partial Content',
			'300' => 'Multiple Choices', // A link list. The user can select a link and go to that location. Maximum five addresses
			'301' => 'Moved Permanently', // The requested page has moved to a new url
			'302' => 'Found', // The requested page has moved temporarily to a new url
			'303' => 'See Other', // The requested page can be found under a different url
			'304' => 'Not Modified',
			'305' => 'Use Proxy',
			'306' => 'Unused', // This code was used in a previous version. It is no longer used, but the code is reserved
			'307' => 'Temporary Redirect', // The requested page has moved temporarily to a new url
			'400' => 'Bad Request', // The server did not understand the request
			'401' => 'Unauthorized', // The requested page needs a username and a password
			'402' => 'Payment Required', // You can not use this code yet
			'403' => 'Forbidden', // Access is forbidden to the requested page
			'404' => 'Not Found', // The server can not find the requested page
			'405' => 'Method Not Allowed', // The method specified in the request is not allowed
			'406' => 'Not Acceptable', // The server can only generate a response that is not accepted by the client
			'407' => 'Proxy Authentication Required', // You must authenticate with a proxy server before this request can be served
			'408' => 'Request Timeout', // The request took longer than the server was prepared to wait
			'409' => 'Conflict', // The request could not be completed because of a conflict
			'410' => 'Gone', // The requested page is no longer available
			'411' => 'Length Required', // The "Content-Length" is not defined. The server will not accept the request without it
			'412' => 'Precondition Failed', // The precondition given in the request evaluated to false by the server
			'413' => 'Request Entity Too Large', // The server will not accept the request, because the request entity is too large
			'414' => 'Request-url Too Long', // The server will not accept the request, because the url is too long. Occurs when you convert a "post" request to a "get" request with a long query information
			'415' => 'Unsupported Media Type', // The server will not accept the request, because the media type is not supported
			'416' => 'Requested Range not satisfiable',
			'417' => 'Expectation Failed',
			'500' => 'Internal Server Error', // The request was not completed. The server met an unexpected condition
			'501' => 'Not Implemented', // The request was not completed. The server did not support the functionality required
			'502' => 'Bad Gateway', // The request was not completed. The server received an invalid response from the upstream server
			'503' => 'Service Unavailable', // The request was not completed. The server is temporarily overloading or down
			'504' => 'Gateway Timeout', // The gateway has timed out
			'505' => 'HTTP Version Not Supported', // The server does not support the "http protocol" version
		);
		
		if (!isset($httpErrorCodes[$code])) {
			$code = 500;
		}
		
		return array(
			'code' => $code,
			'description' => $httpErrorCodes[$code],
		);
	}
	
	public static function uniqueValidator($className, $uniques, &$validationFailures) {
		$valid = true;
		$queryClassName = $className . 'Query';
		foreach ($uniques as $unique) {
			$query = $queryClassName::create();
			foreach ($unique as $uniquePart) {
				$query->filterBy($uniquePart['column'], $uniquePart['value']);
			}
			if ($query->count() > 0) {
				$validationFailures[] = new ValidationFailed($unique[0]['column'], sprintf(Localizer::getText('duplicate %s identifier'), $className));
				$valid = false;
			 }
		}
		return $valid;
	}
	
	
	public static function arrayDepth($array) {
		$exploded = explode(',', json_encode($array, JSON_FORCE_OBJECT)."\n\n");
		$longest = 0;
		foreach($exploded as $row){
			$longest = (substr_count($row, ':')>$longest)?
			substr_count($row, ':'):$longest;
		}
		return $longest;
	}
	
	public static function getSQL(ModelCriteria $mc, $params = array()) {
		$sql = BasePeer::createSelectSql($mc, $params);
		return $sql;
	}
	
	// Generate a random, system-widely unique API key
	public static function getApiKey() {
		$initializationVectorSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
			
		// Keep generating keys until a truely unique one has been found
		do {
			// Create a random number
			$initializationVector = mcrypt_create_iv($initializationVectorSize, MCRYPT_RAND);
		
			// Convert to hex string representation
			$initializationVector = bin2hex($initializationVector);
			
			// Similar looking characters (I, 1, L, ...) have been removed
			// from the character set to make API keys more legible.
			$charset = '023456789ABCDEFGHJKLMQRSTUWXYZ';
		
			// Choose characters based on the generated number
			$length = 16;
			$apiKeyBasePart = '';
			for ($i = 0; $i < $length; $i++) {
				// Split to groups of four, separate with dash
				if ($i && $i % 4 == 0) {
					$apiKeyBasePart .= '-';
				}
				$ivIdx = substr($initializationVector, $i*2, 2);
				$char = $charset[round(hexdec($ivIdx)/256*strlen($charset)+0.5)-1];
				$apiKeyBasePart .= $char;
			}
		
			// Get a check digit
			$apiKeyCheckDigit = Tools::getApiKeyCheckDigit($apiKeyBasePart);
			$apiKey = $apiKeyBasePart . $apiKeyCheckDigit;
		}			
		while (	
			!(UserQuery::create()->findByApikey(CryptographyProvider::encrypt($apiKey)))
			&& !(VehicleQuery::create()->findByApikey(CryptographyProvider::encrypt($apiKey)))
		);
		
		return $apiKey;
	}
	
	// Calculate a check digit for the base part of an API key
	public static function getApiKeyCheckDigit($basepart) {
		
		// Simply calculate the CRC from the whole lot, dashes and all
		$checksumCRC = (string)crc32($basepart);
		
		// And return the last character
		return substr($checksumCRC,-1);
	}
	
	// Get all phrases used in system (Localizer calls and Propel schema)
	public static function getPhrases($getAsPairs = false, $withUsageCounts = false) {
		$commands = array(
			"grep -Po '(?<=message\\=\")[^\"]*' " . ROOT_PATH . "database/schema.xml",
			"grep -hPor --include=*php \"(?<=Localizer::getText\\(')[^']*\" " . ROOT_PATH . "*"
		);
		$phrases = array();
		foreach($commands as $command) {
			exec($command, $output, $return);
			$phrases = array_merge($phrases, $output);
		}
		if ($withUsageCounts) {
			$phrasesSeen = array();
			foreach ($phrases as $phrase) {
				if (!isset($phrasesSeen[$phrase])) {
					$phrasesSeen[$phrase] = 1;
				}
				else {
					$phrasesSeen[$phrase]++;
				}
			}
		}
		$phrases = array_unique($phrases);
		sort($phrases);
		if ($getAsPairs) {
			foreach ($phrases as $phrase) {
				if ($withUsageCounts) {
					$pairs[$phrase] = $phrase . ' (' . $phrasesSeen[$phrase] . ')';
				}
				else {
					$pairs[$phrase] = $phrase;
				}
			}
			return $pairs;
		}
		else {
			return $phrases;
		}
	
		return $phrases;
	}
}