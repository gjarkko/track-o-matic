package org.abo.trackomatic.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TrackomaticDropActivity extends Activity {
	private SharedPreferences sharedSettings = null;
	private String            apiKey;
	private String            latitude;
	private String            longitude;
	private String            dropCandidates;
	private AlertDialog       dropDialog;
	private String[]          availableDropCandidates;
	private boolean[]         checkedDropCandidates;
	private String[]          selectedDropCandidates;

	private static final String TAG = "TrackomaticDropActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drop);

		// Get the API key
		sharedSettings = PreferenceManager.getDefaultSharedPreferences(this);
		apiKey         = sharedSettings.getString("settingsApiKey", "");

		// Get saved information
		if (savedInstanceState != null) {
			Log.d(TAG, "onCreate: Restoring state of activity");
			latitude                = savedInstanceState.getString("latitude");
			longitude               = savedInstanceState.getString("longitude");
			availableDropCandidates = savedInstanceState.getStringArray("availableDropCandidates");
			checkedDropCandidates   = savedInstanceState.getBooleanArray("checkedDropCandidates");
			selectedDropCandidates  = savedInstanceState.getStringArray("selectedDropCandidates");

			updateSelectedDropCandidatesInUI();
		} else {
			// Get the information that was passed to this activity from an intent
			Log.d(TAG, "onCreate: Getting extra information");
			Bundle extra = getIntent().getExtras();

			latitude       = extra.getString("org.abo.trackomatic.client.latitude");
			longitude      = extra.getString("org.abo.trackomatic.client.longitude");
			dropCandidates = extra.getString("org.abo.trackomatic.client.dropCandidates");

			if (dropCandidates != null) {
				// Convert the passed drop candidate string to a JSONArray
				JSONArray jsonDropCandidates = null;

				try {
					jsonDropCandidates = new JSONArray(dropCandidates);
				} catch (JSONException e) {
					Log.d(TAG, "Error in parsing the drop candidates");
					e.printStackTrace();
					return;
				}

				// Get the reference from the JSON objects and put them in an
				// array for the alert dialog builder. Make an equally large
				// boolean array to hold the state of the check box
				availableDropCandidates = new String[jsonDropCandidates.length()];
				checkedDropCandidates   = new boolean[jsonDropCandidates.length()];

				for (int i=0; i<jsonDropCandidates.length(); i++) {
					try {
						JSONObject dropCandidate   = new JSONObject(jsonDropCandidates.getString(i));
						availableDropCandidates[i] = dropCandidate.getString("reference");
					} catch (JSONException e) {
						Log.e(TAG, "Error in parsing the drop candidates");
						e.printStackTrace();
						return;
					}
				}
			}
		}

		// Build the drop candidate dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(R.string.dropCandidateDialogTitle);
		builder.setIcon(R.drawable.ic_dialog_menu_generic);
		builder.setCancelable(false);
		builder.setMultiChoiceItems(availableDropCandidates, checkedDropCandidates, new OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
			}
		});
		builder.setPositiveButton("Done", new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				updateSelectedDropCandidates();
				updateSelectedDropCandidatesInUI();
			}
		});
		dropDialog = builder.create();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// Show the dialog if there is no drop candidate selected
		if (selectedDropCandidates == null) {
			dropDialog.show();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Dismiss the alert to avoid window leaking
		dropDialog.dismiss();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "Storing the state of the activity");
		super.onSaveInstanceState(outState);

		outState.putString("latitude", latitude);
		outState.putString("longitude", longitude);
		outState.putStringArray("availableDropCandidates", availableDropCandidates);
		outState.putBooleanArray("checkedDropCandidates", checkedDropCandidates);
		outState.putStringArray("selectedDropCandidates", selectedDropCandidates);
	}

	public void onClickDropHandler(View view) {
		switch(view.getId()) {
		case R.id.dropCandidateButton:
			if (selectedDropCandidates != null) {
				// Register the drops in the background
				for (int i=0; i<selectedDropCandidates.length; i++) {
					// TODO add a small delay between calls?
					new RegisterDropTask().execute(latitude, longitude, selectedDropCandidates[i]);
				}
				// finish();
			} else {
				showMessageInUI("No drop candidate selected. Please select one.");
			}
			break;

		case R.id.dropCandidateTxtVw:
		case R.id.selectedDropCandidateTxtVw:
			// Clear the selection and show the selection dialog again
			selectedDropCandidates = null;
			dropDialog.show();
			break;
		}
	}

	private void showMessageInUI(String message) {
		if (message != null) {
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}

	private void updateSelectedDropCandidatesInUI() {
		TextView txtView = (TextView) findViewById(R.id.selectedDropCandidateTxtVw);
		Button   dropBtn = (Button) findViewById(R.id.dropCandidateButton);
		
		Log.d(TAG, "updateSelectedDropCandidatesInUI");
		
		if (countCheckedItems(checkedDropCandidates) > 0 && selectedDropCandidates != null) {
			// TODO Find a nicer way of showing the selected drop candidates
			String text = "";

			for (int i=0; i<selectedDropCandidates.length; i++) {
				text += "[" + selectedDropCandidates[i] + "]  ";
			}
			txtView.setText(text);
			
			// Enable the drop button
			dropBtn.setEnabled(true);
		} else {
			txtView.setText(getResources().getString(R.string.selectedDropCandidateTxtVw));
			
			// Disable the drop button
			dropBtn.setEnabled(false);
		}
	}

	private void updateSelectedDropCandidates() {
		Log.d(TAG, "updateSelectedDropCandidates: " + countCheckedItems(checkedDropCandidates));
		selectedDropCandidates = new String[countCheckedItems(checkedDropCandidates)];

		for (int i=0, k=0; i<checkedDropCandidates.length; i++) {
			if (checkedDropCandidates[i]) { 
				selectedDropCandidates[k++] = availableDropCandidates[i];
			}
		}
	}

	private int countCheckedItems(boolean[] items) {
		int count = 0;
		if (items != null) {
			for (int i=0; i<items.length; i++) {
				if (items[i]) {
					count++;
				}
			}
		}
		return count;
	}

	private class RegisterDropTask extends AsyncTask<String, String, Void> {
		private HttpClient httpClient;

		@Override
		protected void onPreExecute() {
			// Get the default HTTP client
			httpClient = new DefaultHttpClient();
		}

		@Override
		protected Void doInBackground(String... params) {
			// Must supply latitude, longitude and reference for the drop to be
			// registered
			if (params.length == 3) {
				registerDrop(params[0], params[1], params[2], null);
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			if (values.length > 0) {
				Toast.makeText(TrackomaticDropActivity.this, values[0], Toast.LENGTH_LONG).show();
			}
		}

		private void registerDrop(String latitude, String longitude, String trackableReference, String groupReference) {
			// Check if the API key is set
			if (apiKey.length() > 0) {
				String       server        = sharedSettings.getString("settingsServer", "http://absx.dyndns.org/");
				String       requestParams = getString(R.string.registerDropRequestParameters);
				HttpResponse response      = null;
				StatusLine   statusLine    = null;

				// Construct the GET method for registering the drop
				StringBuilder getString = new StringBuilder(server + requestParams + "?apikey=" + apiKey);

				// A latitude or longitude value of null means they are not defined
				if (latitude != null && longitude != null) {
					getString.append("&lat=" + latitude + "&lng=" + longitude);
				}
				// Add the reference if it is specified
				if (trackableReference != null) {
					getString.append("&reference=" + trackableReference); 
				}
				// Add the group reference if it is specified
				if (groupReference != null) {
					getString.append("&groupreference=" + groupReference);
				}

				// Create the HTTP GET request
				HttpGet getMethod = new HttpGet(getString.toString());
				Log.d(TAG, "Register drop: " + getString.toString());

				// Send the HTTP GET request and catch the response
				try {
					response   = httpClient.execute(getMethod);
					statusLine = response.getStatusLine();
					Log.d(TAG, "registerDrop - Status line: " + statusLine.toString());
				} catch (Exception e) {
					publishProgress("Register drop request failed");
					Log.e(TAG, "registerDrop - Request failed " + e.toString());
					return;
				}

				// Consume the response. If we do not consume the response the
				// client will refuse to reuse the connection
				JSONObject jsonResponse;
				JSONArray  names;

				try {
					jsonResponse = new JSONObject(getContents(response));
					names        = jsonResponse.names();

					for (int i=0; i<names.length(); i++) {
						Log.d(TAG, "registerDrop: " + jsonResponse.getString(names.getString(i)) + " [" + names.getString(i) + "]");
					}
				} catch (JSONException e) {
					if (trackableReference != null) {
						publishProgress("Register drop failed while parsing response for '" + trackableReference + "'");
					} else {
						publishProgress("Register drop failed while parsing response");
					}
					Log.e(TAG, "registerDrop - parsing response failed: " + e.toString());
					e.printStackTrace();
					return;
				}

				// Show status or error message in UI
				try {
					if (jsonResponse.has("status")) {
						if (trackableReference != null) {
							publishProgress("Register drop '" + trackableReference + "': " + jsonResponse.getString("status"));
						} else {
							publishProgress("Register drop: " + jsonResponse.getString("status"));
						}
					} else if (jsonResponse.has("error")) {
						if (trackableReference != null) {
							publishProgress("Register drop '" + trackableReference + "' error: " + jsonResponse.getString("error"));
						} else {
							publishProgress("Register drop error: " + jsonResponse.getString("error"));
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				// Handle the status code
				registerDropStatusHandler(statusLine.getStatusCode());
			} else {
				Log.e(TAG, "registerDrop: No API key specified in the settings");
				publishProgress("No API key specified in the settings");
			}
		}
		
		private void registerDropStatusHandler(int statusCode) {
			switch (statusCode) {
			case 201:
				// Created. Finish the activity.
				finish();
				break;
			case 300: // Multiple choices. No reference or group reference and multiple drop
					  // points found at the address.
			case 401: // Unauthorized. Unrecognized or missing API key
			case 404: // Not found
			case 412: // Precondition failed. Validation error or missing argument.
			case 417: // Expectation failed. Trackable already dropped.
			case 500: // Internal server error.
				break;
			default:
				break;
			}
		}

		// Read the contents of the HTTP response and return it as a string
		private String getContents(HttpResponse response) {
			StringBuilder content = new StringBuilder();

			try {
				InputStream    contentStream = response.getEntity().getContent();
				BufferedReader reader        = new BufferedReader(new InputStreamReader(contentStream));

				String line;
				while ((line = reader.readLine()) != null) {
					content.append(line);
				}

				reader.close();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 

			return content.toString();
		}
	}
}
