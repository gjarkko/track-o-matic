START TRANSACTION;
INSERT INTO language VALUES (1, 'English', 'English');
INSERT INTO language VALUES (2, 'Finnish', 'Suomi');
INSERT INTO language VALUES (3, 'Swedish', 'Svenska');

INSERT INTO country VALUES (1, '', '');
INSERT INTO country VALUES (NULL, 'AD', "Andorra");
INSERT INTO country VALUES (NULL, 'AE', "United Arab Emirates");
INSERT INTO country VALUES (NULL, 'AF', "Afghanistan");
INSERT INTO country VALUES (NULL, 'AG', "Antigua and Barbuda");
INSERT INTO country VALUES (NULL, 'AI', "Anguilla");
INSERT INTO country VALUES (NULL, 'AL', "Albania");
INSERT INTO country VALUES (NULL, 'AM', "Armenia");
INSERT INTO country VALUES (NULL, 'AO', "Angola");
INSERT INTO country VALUES (NULL, 'AQ', "Antarctica");
INSERT INTO country VALUES (NULL, 'AR', "Argentina");
INSERT INTO country VALUES (NULL, 'AS', "American Samoa");
INSERT INTO country VALUES (NULL, 'AT', "Austria");
INSERT INTO country VALUES (NULL, 'AU', "Australia");
INSERT INTO country VALUES (NULL, 'AW', "Aruba");
INSERT INTO country VALUES (NULL, 'AX', "Åland Islands");
INSERT INTO country VALUES (NULL, 'AZ', "Azerbaijan");
INSERT INTO country VALUES (NULL, 'BA', "Bosnia and Herzegovina");
INSERT INTO country VALUES (NULL, 'BB', "Barbados");
INSERT INTO country VALUES (NULL, 'BD', "Bangladesh");
INSERT INTO country VALUES (NULL, 'BE', "Belgium");
INSERT INTO country VALUES (NULL, 'BF', "Burkina Faso");
INSERT INTO country VALUES (NULL, 'BG', "Bulgaria");
INSERT INTO country VALUES (NULL, 'BH', "Bahrain");
INSERT INTO country VALUES (NULL, 'BI', "Burundi");
INSERT INTO country VALUES (NULL, 'BJ', "Benin");
INSERT INTO country VALUES (NULL, 'BL', "Saint Barthélemy");
INSERT INTO country VALUES (NULL, 'BM', "Bermuda");
INSERT INTO country VALUES (NULL, 'BN', "Brunei Darussalam");
INSERT INTO country VALUES (NULL, 'BO', "Bolivia, Plurinational State of");
INSERT INTO country VALUES (NULL, 'BQ', "Bonaire, Sint Eustatius and Saba");
INSERT INTO country VALUES (NULL, 'BR', "Brazil");
INSERT INTO country VALUES (NULL, 'BS', "Bahamas");
INSERT INTO country VALUES (NULL, 'BT', "Bhutan");
INSERT INTO country VALUES (NULL, 'BV', "Bouvet Island");
INSERT INTO country VALUES (NULL, 'BW', "Botswana");
INSERT INTO country VALUES (NULL, 'BY', "Belarus");
INSERT INTO country VALUES (NULL, 'BZ', "Belize");
INSERT INTO country VALUES (NULL, 'CA', "Canada");
INSERT INTO country VALUES (NULL, 'CC', "Cocos (Keeling) Islands");
INSERT INTO country VALUES (NULL, 'CD', "Congo, the Democratic Republic of the");
INSERT INTO country VALUES (NULL, 'CF', "Central African Republic");
INSERT INTO country VALUES (NULL, 'CG', "Congo");
INSERT INTO country VALUES (NULL, 'CH', "Switzerland");
INSERT INTO country VALUES (NULL, 'CI', "Côte d'Ivoire");
INSERT INTO country VALUES (NULL, 'CK', "Cook Islands");
INSERT INTO country VALUES (NULL, 'CL', "Chile");
INSERT INTO country VALUES (NULL, 'CM', "Cameroon");
INSERT INTO country VALUES (NULL, 'CN', "China");
INSERT INTO country VALUES (NULL, 'CO', "Colombia");
INSERT INTO country VALUES (NULL, 'CR', "Costa Rica");
INSERT INTO country VALUES (NULL, 'CU', "Cuba");
INSERT INTO country VALUES (NULL, 'CV', "Cape Verde");
INSERT INTO country VALUES (NULL, 'CW', "Curaçao");
INSERT INTO country VALUES (NULL, 'CX', "Christmas Island");
INSERT INTO country VALUES (NULL, 'CY', "Cyprus");
INSERT INTO country VALUES (NULL, 'CZ', "Czech Republic");
INSERT INTO country VALUES (NULL, 'DE', "Germany");
INSERT INTO country VALUES (NULL, 'DJ', "Djibouti");
INSERT INTO country VALUES (NULL, 'DK', "Denmark");
INSERT INTO country VALUES (NULL, 'DM', "Dominica");
INSERT INTO country VALUES (NULL, 'DO', "Dominican Republic");
INSERT INTO country VALUES (NULL, 'DZ', "Algeria");
INSERT INTO country VALUES (NULL, 'EC', "Ecuador");
INSERT INTO country VALUES (NULL, 'EE', "Estonia");
INSERT INTO country VALUES (NULL, 'EG', "Egypt");
INSERT INTO country VALUES (NULL, 'EH', "Western Sahara");
INSERT INTO country VALUES (NULL, 'ER', "Eritrea");
INSERT INTO country VALUES (NULL, 'ES', "Spain");
INSERT INTO country VALUES (NULL, 'ET', "Ethiopia");
INSERT INTO country VALUES (NULL, 'FI', "Finland");
INSERT INTO country VALUES (NULL, 'FJ', "Fiji");
INSERT INTO country VALUES (NULL, 'FK', "Falkland Islands (Malvinas)");
INSERT INTO country VALUES (NULL, 'FM', "Micronesia, Federated States of");
INSERT INTO country VALUES (NULL, 'FO', "Faroe Islands");
INSERT INTO country VALUES (NULL, 'FR', "France");
INSERT INTO country VALUES (NULL, 'GA', "Gabon");
INSERT INTO country VALUES (NULL, 'GB', "United Kingdom");
INSERT INTO country VALUES (NULL, 'GD', "Grenada");
INSERT INTO country VALUES (NULL, 'GE', "Georgia");
INSERT INTO country VALUES (NULL, 'GF', "French Guiana");
INSERT INTO country VALUES (NULL, 'GG', "Guernsey");
INSERT INTO country VALUES (NULL, 'GH', "Ghana");
INSERT INTO country VALUES (NULL, 'GI', "Gibraltar");
INSERT INTO country VALUES (NULL, 'GL', "Greenland");
INSERT INTO country VALUES (NULL, 'GM', "Gambia");
INSERT INTO country VALUES (NULL, 'GN', "Guinea");
INSERT INTO country VALUES (NULL, 'GP', "Guadeloupe");
INSERT INTO country VALUES (NULL, 'GQ', "Equatorial Guinea");
INSERT INTO country VALUES (NULL, 'GR', "Greece");
INSERT INTO country VALUES (NULL, 'GS', "South Georgia and the South Sandwich Islands");
INSERT INTO country VALUES (NULL, 'GT', "Guatemala");
INSERT INTO country VALUES (NULL, 'GU', "Guam");
INSERT INTO country VALUES (NULL, 'GW', "Guinea-Bissau");
INSERT INTO country VALUES (NULL, 'GY', "Guyana");
INSERT INTO country VALUES (NULL, 'HK', "Hong Kong");
INSERT INTO country VALUES (NULL, 'HM', "Heard Island and McDonald Islands");
INSERT INTO country VALUES (NULL, 'HN', "Honduras");
INSERT INTO country VALUES (NULL, 'HR', "Croatia");
INSERT INTO country VALUES (NULL, 'HT', "Haiti");
INSERT INTO country VALUES (NULL, 'HU', "Hungary");
INSERT INTO country VALUES (NULL, 'ID', "Indonesia");
INSERT INTO country VALUES (NULL, 'IE', "Ireland");
INSERT INTO country VALUES (NULL, 'IL', "Israel");
INSERT INTO country VALUES (NULL, 'IM', "Isle of Man");
INSERT INTO country VALUES (NULL, 'IN', "India");
INSERT INTO country VALUES (NULL, 'IO', "British Indian Ocean Territory");
INSERT INTO country VALUES (NULL, 'IQ', "Iraq");
INSERT INTO country VALUES (NULL, 'IR', "Iran, Islamic Republic of");
INSERT INTO country VALUES (NULL, 'IS', "Iceland");
INSERT INTO country VALUES (NULL, 'IT', "Italy");
INSERT INTO country VALUES (NULL, 'JE', "Jersey");
INSERT INTO country VALUES (NULL, 'JM', "Jamaica");
INSERT INTO country VALUES (NULL, 'JO', "Jordan");
INSERT INTO country VALUES (NULL, 'JP', "Japan");
INSERT INTO country VALUES (NULL, 'KE', "Kenya");
INSERT INTO country VALUES (NULL, 'KG', "Kyrgyzstan");
INSERT INTO country VALUES (NULL, 'KH', "Cambodia");
INSERT INTO country VALUES (NULL, 'KI', "Kiribati");
INSERT INTO country VALUES (NULL, 'KM', "Comoros");
INSERT INTO country VALUES (NULL, 'KN', "Saint Kitts and Nevis");
INSERT INTO country VALUES (NULL, 'KP', "Korea, Democratic People's Republic of");
INSERT INTO country VALUES (NULL, 'KR', "Korea, Republic of");
INSERT INTO country VALUES (NULL, 'KW', "Kuwait");
INSERT INTO country VALUES (NULL, 'KY', "Cayman Islands");
INSERT INTO country VALUES (NULL, 'KZ', "Kazakhstan");
INSERT INTO country VALUES (NULL, 'LA', "Lao People's Democratic Republic");
INSERT INTO country VALUES (NULL, 'LB', "Lebanon");
INSERT INTO country VALUES (NULL, 'LC', "Saint Lucia");
INSERT INTO country VALUES (NULL, 'LI', "Liechtenstein");
INSERT INTO country VALUES (NULL, 'LK', "Sri Lanka");
INSERT INTO country VALUES (NULL, 'LR', "Liberia");
INSERT INTO country VALUES (NULL, 'LS', "Lesotho");
INSERT INTO country VALUES (NULL, 'LT', "Lithuania");
INSERT INTO country VALUES (NULL, 'LU', "Luxembourg");
INSERT INTO country VALUES (NULL, 'LV', "Latvia");
INSERT INTO country VALUES (NULL, 'LY', "Libyan Arab Jamahiriya");
INSERT INTO country VALUES (NULL, 'MA', "Morocco");
INSERT INTO country VALUES (NULL, 'MC', "Monaco");
INSERT INTO country VALUES (NULL, 'MD', "Moldova, Republic of");
INSERT INTO country VALUES (NULL, 'ME', "Montenegro");
INSERT INTO country VALUES (NULL, 'MF', "Saint Martin (French part)");
INSERT INTO country VALUES (NULL, 'MG', "Madagascar");
INSERT INTO country VALUES (NULL, 'MH', "Marshall Islands");
INSERT INTO country VALUES (NULL, 'MK', "Macedonia, the former Yugoslav Republic of");
INSERT INTO country VALUES (NULL, 'ML', "Mali");
INSERT INTO country VALUES (NULL, 'MM', "Myanmar");
INSERT INTO country VALUES (NULL, 'MN', "Mongolia");
INSERT INTO country VALUES (NULL, 'MO', "Macao");
INSERT INTO country VALUES (NULL, 'MP', "Northern Mariana Islands");
INSERT INTO country VALUES (NULL, 'MQ', "Martinique");
INSERT INTO country VALUES (NULL, 'MR', "Mauritania");
INSERT INTO country VALUES (NULL, 'MS', "Montserrat");
INSERT INTO country VALUES (NULL, 'MT', "Malta");
INSERT INTO country VALUES (NULL, 'MU', "Mauritius");
INSERT INTO country VALUES (NULL, 'MV', "Maldives");
INSERT INTO country VALUES (NULL, 'MW', "Malawi");
INSERT INTO country VALUES (NULL, 'MX', "Mexico");
INSERT INTO country VALUES (NULL, 'MY', "Malaysia");
INSERT INTO country VALUES (NULL, 'MZ', "Mozambique");
INSERT INTO country VALUES (NULL, 'NA', "Namibia");
INSERT INTO country VALUES (NULL, 'NC', "New Caledonia");
INSERT INTO country VALUES (NULL, 'NE', "Niger");
INSERT INTO country VALUES (NULL, 'NF', "Norfolk Island");
INSERT INTO country VALUES (NULL, 'NG', "Nigeria");
INSERT INTO country VALUES (NULL, 'NI', "Nicaragua");
INSERT INTO country VALUES (NULL, 'NL', "Netherlands");
INSERT INTO country VALUES (NULL, 'NO', "Norway");
INSERT INTO country VALUES (NULL, 'NP', "Nepal");
INSERT INTO country VALUES (NULL, 'NR', "Nauru");
INSERT INTO country VALUES (NULL, 'NU', "Niue");
INSERT INTO country VALUES (NULL, 'NZ', "New Zealand");
INSERT INTO country VALUES (NULL, 'OM', "Oman");
INSERT INTO country VALUES (NULL, 'PA', "Panama");
INSERT INTO country VALUES (NULL, 'PE', "Peru");
INSERT INTO country VALUES (NULL, 'PF', "French Polynesia");
INSERT INTO country VALUES (NULL, 'PG', "Papua New Guinea");
INSERT INTO country VALUES (NULL, 'PH', "Philippines");
INSERT INTO country VALUES (NULL, 'PK', "Pakistan");
INSERT INTO country VALUES (NULL, 'PL', "Poland");
INSERT INTO country VALUES (NULL, 'PM', "Saint Pierre and Miquelon");
INSERT INTO country VALUES (NULL, 'PN', "Pitcairn");
INSERT INTO country VALUES (NULL, 'PR', "Puerto Rico");
INSERT INTO country VALUES (NULL, 'PS', "Palestinian Territory, Occupied");
INSERT INTO country VALUES (NULL, 'PT', "Portugal");
INSERT INTO country VALUES (NULL, 'PW', "Palau");
INSERT INTO country VALUES (NULL, 'PY', "Paraguay");
INSERT INTO country VALUES (NULL, 'QA', "Qatar");
INSERT INTO country VALUES (NULL, 'RE', "Réunion");
INSERT INTO country VALUES (NULL, 'RO', "Romania");
INSERT INTO country VALUES (NULL, 'RS', "Serbia");
INSERT INTO country VALUES (NULL, 'RU', "Russian Federation");
INSERT INTO country VALUES (NULL, 'RW', "Rwanda");
INSERT INTO country VALUES (NULL, 'SA', "Saudi Arabia");
INSERT INTO country VALUES (NULL, 'SB', "Solomon Islands");
INSERT INTO country VALUES (NULL, 'SC', "Seychelles");
INSERT INTO country VALUES (NULL, 'SD', "Sudan");
INSERT INTO country VALUES (NULL, 'SE', "Sweden");
INSERT INTO country VALUES (NULL, 'SG', "Singapore");
INSERT INTO country VALUES (NULL, 'SH', "Saint Helena, Ascension and Tristan da Cunha");
INSERT INTO country VALUES (NULL, 'SI', "Slovenia");
INSERT INTO country VALUES (NULL, 'SJ', "Svalbard and Jan Mayen");
INSERT INTO country VALUES (NULL, 'SK', "Slovakia");
INSERT INTO country VALUES (NULL, 'SL', "Sierra Leone");
INSERT INTO country VALUES (NULL, 'SM', "San Marino");
INSERT INTO country VALUES (NULL, 'SN', "Senegal");
INSERT INTO country VALUES (NULL, 'SO', "Somalia");
INSERT INTO country VALUES (NULL, 'SR', "Suriname");
INSERT INTO country VALUES (NULL, 'SS', "South Sudan");
INSERT INTO country VALUES (NULL, 'ST', "Sao Tome and Principe");
INSERT INTO country VALUES (NULL, 'SV', "El Salvador");
INSERT INTO country VALUES (NULL, 'SX', "Sint Maarten (Dutch part)");
INSERT INTO country VALUES (NULL, 'SY', "Syrian Arab Republic");
INSERT INTO country VALUES (NULL, 'SZ', "Swaziland");
INSERT INTO country VALUES (NULL, 'TC', "Turks and Caicos Islands");
INSERT INTO country VALUES (NULL, 'TD', "Chad");
INSERT INTO country VALUES (NULL, 'TF', "French Southern Territories");
INSERT INTO country VALUES (NULL, 'TG', "Togo");
INSERT INTO country VALUES (NULL, 'TH', "Thailand");
INSERT INTO country VALUES (NULL, 'TJ', "Tajikistan");
INSERT INTO country VALUES (NULL, 'TK', "Tokelau");
INSERT INTO country VALUES (NULL, 'TL', "Timor-Leste");
INSERT INTO country VALUES (NULL, 'TM', "Turkmenistan");
INSERT INTO country VALUES (NULL, 'TN', "Tunisia");
INSERT INTO country VALUES (NULL, 'TO', "Tonga");
INSERT INTO country VALUES (NULL, 'TR', "Turkey");
INSERT INTO country VALUES (NULL, 'TT', "Trinidad and Tobago");
INSERT INTO country VALUES (NULL, 'TV', "Tuvalu");
INSERT INTO country VALUES (NULL, 'TW', "Taiwan, Province of China");
INSERT INTO country VALUES (NULL, 'TZ', "Tanzania, United Republic of");
INSERT INTO country VALUES (NULL, 'UA', "Ukraine");
INSERT INTO country VALUES (NULL, 'UG', "Uganda");
INSERT INTO country VALUES (NULL, 'UM', "United States Minor Outlying Islands");
INSERT INTO country VALUES (NULL, 'US', "United States");
INSERT INTO country VALUES (NULL, 'UY', "Uruguay");
INSERT INTO country VALUES (NULL, 'UZ', "Uzbekistan");
INSERT INTO country VALUES (NULL, 'VA', "Holy See (Vatican City State)");
INSERT INTO country VALUES (NULL, 'VC', "Saint Vincent and the Grenadines");
INSERT INTO country VALUES (NULL, 'VE', "Venezuela, Bolivarian Republic of");
INSERT INTO country VALUES (NULL, 'VG', "Virgin Islands, British");
INSERT INTO country VALUES (NULL, 'VI', "Virgin Islands, U.S.");
INSERT INTO country VALUES (NULL, 'VN', "Viet Nam");
INSERT INTO country VALUES (NULL, 'VU', "Vanuatu");
INSERT INTO country VALUES (NULL, 'WF', "Wallis and Futuna");
INSERT INTO country VALUES (NULL, 'WS', "Samoa");
INSERT INTO country VALUES (NULL, 'YE', "Yemen");
INSERT INTO country VALUES (NULL, 'YT', "Mayotte");
INSERT INTO country VALUES (NULL, 'ZA', "South Africa");
INSERT INTO country VALUES (NULL, 'ZM', "Zambia");
INSERT INTO country VALUES (NULL, 'ZW', "Zimbabwe");


INSERT INTO logentrytype VALUES (1, 'Info'), (2, 'Warning'), (3, 'Error');
INSERT INTO usertype VALUES (1, 'System (do not use)'), (2, 'Administrator'), (3, 'User'), (4, 'Driver');

INSERT INTO customer VALUES (1, 'System', 0);
INSERT INTO user (userid, usertypeid, languageid, username, firstname, email, password, apikey, valid) VALUES (1,1,1,'system','System','tom@absx.org', '', '', 0);

INSERT INTO address VALUES (1,1,'','',NULL,1,'',1, null, null);

REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('email', 'email');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('password', 'password');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('unknown user or password', 'unknown user or password');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('unknown api key', 'unknown API key');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('logout', 'log out');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('login', 'log in');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('home', 'home');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('localizations', 'home');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('users', 'users');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('customers', 'customers');
REPLACE INTO localization (localizationkey, localizationvalue) VALUES ('vehicles', 'vehicles');
COMMIT;