<?php



/**
 * Skeleton subclass for representing a row from the 'triptrackable' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Triptrackable extends BaseTriptrackable {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => false,
			'allowCreate' => true,
			'allowDelete' => false,
		);
	}
	
	public function getTripReference() {
		if (is_object($this->getTrip())) {
			return $this->getTrip()->getTripreference();
		}
		else {
			return NULL;
		}
	}
	
	public function getTrackableReference() {
		if (is_object($this->getTrackable())) {
			return $this->getTrackable()->getTrackablereference();
		}
		else {
			return NULL;
		}
	}
	
	public static function getBrowserColumnMap($user = NULL) {
		return array(
			array(
				'column' => 'trip',
				'label' => ucfirst(Localizer::getText('trip')),
				'foreign' => true,
				'display_method' => 'getTripReference',
				'display_SQL' => 'tripreference',

				'join' => array(
					'relation' => 'Trip',
					'table' => 'trip',
					'own_key' => 'tripid',
					'foreign_key' => 'tripid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'display_method' => 'getTrackableReference',
				'display_SQL' => 'trackablereference',

				'join' => array(
					'relation' => 'Trackable',
					'table' => 'trackable',
					'own_key' => 'trackableid',
					'foreign_key' => 'trackableid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
		);
	}
	
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'trip',
				'label' => ucfirst(Localizer::getText('trip')),
				'foreign' => true,
				'getMethod' => 'getTripReference',
				'getIdMethod' => 'getTripid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'trackableid',
				'label' => ucfirst(Localizer::getText('trackable')),
				'optionsMethod' => 'getActiveTrackables',
				'getMethod' => 'getTrackableReference',
				'getIdMethod' => 'getTrackableid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
		);
	}
	
	public function getActiveTrackables() {
		
		$trackables = TrackableQuery::create()->findByCustomerid(Session::getInstance()->getUser()->getCustomerid());
		
		$trackableArray = array();

		foreach($trackables as $key => $trackable) {
			if(
				!$trackable->getLastStatus() || 
				(
					!$trackable->getLastStatus()->getStatustype()->getIsfinal() && 
					!$trackable->getLastStatus()->getStatustype()->getIsmoving()
				)
			) {
				$trackableArray[$trackable->getPrimaryKey()] = $trackable->getTrackablereference();
			}
		}
		
		return $trackableArray;
	}
	
} // Triptrackable
