<?php



/**
 * This class defines the structure of the 'countryname' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class CountrynameTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.CountrynameTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('countryname');
		$this->setPhpName('Countryname');
		$this->setClassname('Countryname');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('COUNTRYNAMEID', 'Countrynameid', 'INTEGER', true, 10, null);
		$this->addForeignKey('COUNTRYID', 'Countryid', 'INTEGER', 'country', 'COUNTRYID', true, 10, 1);
		$this->addForeignKey('LANGUAGEID', 'Languageid', 'INTEGER', 'language', 'LANGUAGEID', true, 10, 1);
		$this->addColumn('COUNTRYNAME', 'Countryname', 'VARCHAR', true, 60, null);
		// validators
		$this->addValidator('COUNTRYNAME', 'minLength', 'propel.validator.MinLengthValidator', '1', 'countryname must be at least 1 characters');
		$this->addValidator('COUNTRYNAME', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'countryname must be at most 60 characters');
		$this->addValidator('COUNTRYNAME', 'required', 'propel.validator.RequiredValidator', '', 'countryname is required.');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Country', 'Country', RelationMap::MANY_TO_ONE, array('countryid' => 'countryid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Language', 'Language', RelationMap::MANY_TO_ONE, array('languageid' => 'languageid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // CountrynameTableMap
