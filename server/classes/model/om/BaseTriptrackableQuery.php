<?php


/**
 * Base class that represents a query for the 'triptrackable' table.
 *
 * 
 *
 * @method     TriptrackableQuery orderByTriptrackableid($order = Criteria::ASC) Order by the triptrackableid column
 * @method     TriptrackableQuery orderByTripid($order = Criteria::ASC) Order by the tripid column
 * @method     TriptrackableQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 *
 * @method     TriptrackableQuery groupByTriptrackableid() Group by the triptrackableid column
 * @method     TriptrackableQuery groupByTripid() Group by the tripid column
 * @method     TriptrackableQuery groupByTrackableid() Group by the trackableid column
 *
 * @method     TriptrackableQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TriptrackableQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TriptrackableQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TriptrackableQuery leftJoinTrip($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trip relation
 * @method     TriptrackableQuery rightJoinTrip($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trip relation
 * @method     TriptrackableQuery innerJoinTrip($relationAlias = null) Adds a INNER JOIN clause to the query using the Trip relation
 *
 * @method     TriptrackableQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     TriptrackableQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     TriptrackableQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     Triptrackable findOne(PropelPDO $con = null) Return the first Triptrackable matching the query
 * @method     Triptrackable findOneOrCreate(PropelPDO $con = null) Return the first Triptrackable matching the query, or a new Triptrackable object populated from the query conditions when no match is found
 *
 * @method     Triptrackable findOneByTriptrackableid(int $triptrackableid) Return the first Triptrackable filtered by the triptrackableid column
 * @method     Triptrackable findOneByTripid(int $tripid) Return the first Triptrackable filtered by the tripid column
 * @method     Triptrackable findOneByTrackableid(int $trackableid) Return the first Triptrackable filtered by the trackableid column
 *
 * @method     array findByTriptrackableid(int $triptrackableid) Return Triptrackable objects filtered by the triptrackableid column
 * @method     array findByTripid(int $tripid) Return Triptrackable objects filtered by the tripid column
 * @method     array findByTrackableid(int $trackableid) Return Triptrackable objects filtered by the trackableid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTriptrackableQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseTriptrackableQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Triptrackable', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TriptrackableQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TriptrackableQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TriptrackableQuery) {
			return $criteria;
		}
		$query = new TriptrackableQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Triptrackable|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = TriptrackablePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(TriptrackablePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Triptrackable A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `TRIPTRACKABLEID`, `TRIPID`, `TRACKABLEID` FROM `triptrackable` WHERE `TRIPTRACKABLEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Triptrackable();
			$obj->hydrate($row);
			TriptrackablePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Triptrackable|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TriptrackablePeer::TRIPTRACKABLEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TriptrackablePeer::TRIPTRACKABLEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the triptrackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTriptrackableid(1234); // WHERE triptrackableid = 1234
	 * $query->filterByTriptrackableid(array(12, 34)); // WHERE triptrackableid IN (12, 34)
	 * $query->filterByTriptrackableid(array('min' => 12)); // WHERE triptrackableid > 12
	 * </code>
	 *
	 * @param     mixed $triptrackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByTriptrackableid($triptrackableid = null, $comparison = null)
	{
		if (is_array($triptrackableid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TriptrackablePeer::TRIPTRACKABLEID, $triptrackableid, $comparison);
	}

	/**
	 * Filter the query on the tripid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTripid(1234); // WHERE tripid = 1234
	 * $query->filterByTripid(array(12, 34)); // WHERE tripid IN (12, 34)
	 * $query->filterByTripid(array('min' => 12)); // WHERE tripid > 12
	 * </code>
	 *
	 * @see       filterByTrip()
	 *
	 * @param     mixed $tripid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByTripid($tripid = null, $comparison = null)
	{
		if (is_array($tripid)) {
			$useMinMax = false;
			if (isset($tripid['min'])) {
				$this->addUsingAlias(TriptrackablePeer::TRIPID, $tripid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($tripid['max'])) {
				$this->addUsingAlias(TriptrackablePeer::TRIPID, $tripid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TriptrackablePeer::TRIPID, $tripid, $comparison);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @see       filterByTrackable()
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid)) {
			$useMinMax = false;
			if (isset($trackableid['min'])) {
				$this->addUsingAlias(TriptrackablePeer::TRACKABLEID, $trackableid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($trackableid['max'])) {
				$this->addUsingAlias(TriptrackablePeer::TRACKABLEID, $trackableid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TriptrackablePeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query by a related Trip object
	 *
	 * @param     Trip|PropelCollection $trip The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByTrip($trip, $comparison = null)
	{
		if ($trip instanceof Trip) {
			return $this
				->addUsingAlias(TriptrackablePeer::TRIPID, $trip->getTripid(), $comparison);
		} elseif ($trip instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TriptrackablePeer::TRIPID, $trip->toKeyValue('PrimaryKey', 'Tripid'), $comparison);
		} else {
			throw new PropelException('filterByTrip() only accepts arguments of type Trip or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trip relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function joinTrip($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trip');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trip');
		}

		return $this;
	}

	/**
	 * Use the Trip relation Trip object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery A secondary query class using the current class as primary query
	 */
	public function useTripQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrip($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trip', 'TripQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable|PropelCollection $trackable The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(TriptrackablePeer::TRACKABLEID, $trackable->getTrackableid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TriptrackablePeer::TRACKABLEID, $trackable->toKeyValue('PrimaryKey', 'Trackableid'), $comparison);
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Triptrackable $triptrackable Object to remove from the list of results
	 *
	 * @return    TriptrackableQuery The current query, for fluid interface
	 */
	public function prune($triptrackable = null)
	{
		if ($triptrackable) {
			$this->addUsingAlias(TriptrackablePeer::TRIPTRACKABLEID, $triptrackable->getTriptrackableid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseTriptrackableQuery