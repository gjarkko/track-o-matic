<?php



/**
 * Skeleton subclass for representing a row from the 'address' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Address extends BaseAddress {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'name',
				'label' => ucfirst(Localizer::getText('contact person')),
			),
			array(
				'column' => 'address1',
				'label' => ucfirst(Localizer::getText('address 1')),
			),
			array(
				'column' => 'address2',
				'label' => ucfirst(Localizer::getText('address 2')),
			),
			array(
				'column' => 'postalcode',
				'label' => ucfirst(Localizer::getText('postal code')),
			),
			array(
				'column' => 'city',
				'label' => ucfirst(Localizer::getText('city')),
			),
			array(
				'column' => 'countryName',
				'label' => ucfirst(Localizer::getText('country')),
				'foreign' => true,
				'join' => array(
					'relation' => 'Country',
					'table' => 'country',
					'foreign_key' => 'countryid',
					'own_key' => 'countryid'
				),
				'display_method' => 'getCountryName',
				'display_SQL' => 'Country.countryname'
			),
		);
	}
	
	
	public static function getCrudMap($values = NULL) {
		return array(
			array(
				'column' => 'name',
				'label' => ucfirst(Localizer::getText('contact name')),
				'inputType' => 'text',
			),
			array(
				'column' => 'address1',
				'label' => ucfirst(Localizer::getText('street address (line 1)')),
				'inputType' => 'text',
				'required' => true,
			),
			array(
				'column' => 'address2',
				'label' => ucfirst(Localizer::getText('street address (line 2)')),
				'inputType' => 'text',
			),
			array(
				'column' => 'postalcode',
				'label' => ucfirst(Localizer::getText('postal code')),
				'inputType' => 'text',
				'required' => true,
			),
			array(
				'column' => 'city',
				'label' => ucfirst(Localizer::getText('city')),
				'inputType' => 'text',
				'required' => true,
			),
			array(
				'column' => 'country',
				'getMethod' => 'getCountryName',
				'getIdMethod' => 'getCountryid',
				'label' => ucfirst(Localizer::getText('country')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
		);
	}
	
	
	public function geoCode() {
		$addressFields = array(
			$this->getAddress1(),
			$this->getAddress2(),
			$this->getPostalcode(),
			$this->getCity(),
			$this->getCountry()->getCountryname(),
		);
		try {
			$geocode = Geocoder::getInstance()->geocodeAddress($addressFields);
			$this->setLat($geocode['lat']);
			$this->setLng($geocode['lng']);
		} catch (Exception $e) {}
	}
	
	public function save(PropelPDO $con = null) {
		
		// If creating a new address, use the customer the user has been bound to.
		if (!$this->getPrimaryKey()) {
			if (Session::getInstance()->getUser()) {
				$this->setCustomerId(Session::getInstance()->getUser()->getCustomerid());
			}
		}
				
		if (empty($this->lat) || empty($this->lng)) {
			$this->geoCode();
		}
		parent::save($con);
	}
	
	public function getCountryName() {
		return $this->getCountry()->getCountryname();
	}
	
	public function getAddress() {
		return implode(', ', array(
			$this->getAddress1(),
			$this->getPostalcode(),
			$this->getCity(),
			$this->getCountry()->getCountryname()
		));
	}
	
	public function validate($columns = null) {
		$valid = parent::validate($columns);
		
		if ($valid) {
		
			if ($this->getCountryid() == 1) {
				$this->validationFailures[] = new ValidationFailed('countryid', Localizer::getText('country must be defined'));
				$valid = false;
			}		
		}
		return $valid;
		
	}
} // Address
