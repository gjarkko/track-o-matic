<?php


/**
 * Base class that represents a row from the 'vehiclestatus' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseVehiclestatus extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'VehiclestatusPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        VehiclestatusPeer
	 */
	protected static $peer;

	/**
	 * The value for the vehiclestatusid field.
	 * @var        int
	 */
	protected $vehiclestatusid;

	/**
	 * The value for the statustypeid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $statustypeid;

	/**
	 * The value for the vehicleid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $vehicleid;

	/**
	 * The value for the lat field.
	 * @var        string
	 */
	protected $lat;

	/**
	 * The value for the lng field.
	 * @var        string
	 */
	protected $lng;

	/**
	 * The value for the time field.
	 * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
	 * @var        string
	 */
	protected $time;

	/**
	 * The value for the message field.
	 * @var        string
	 */
	protected $message;

	/**
	 * @var        Statustype
	 */
	protected $aStatustype;

	/**
	 * @var        Vehicle
	 */
	protected $aVehicle;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->statustypeid = 1;
		$this->vehicleid = 1;
	}

	/**
	 * Initializes internal state of BaseVehiclestatus object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [vehiclestatusid] column value.
	 * 
	 * @return     int
	 */
	public function getVehiclestatusid()
	{
		return $this->vehiclestatusid;
	}

	/**
	 * Get the [statustypeid] column value.
	 * 
	 * @return     int
	 */
	public function getStatustypeid()
	{
		return $this->statustypeid;
	}

	/**
	 * Get the [vehicleid] column value.
	 * 
	 * @return     int
	 */
	public function getVehicleid()
	{
		return $this->vehicleid;
	}

	/**
	 * Get the [lat] column value.
	 * 
	 * @return     string
	 */
	public function getLat()
	{
		return $this->lat;
	}

	/**
	 * Get the [lng] column value.
	 * 
	 * @return     string
	 */
	public function getLng()
	{
		return $this->lng;
	}

	/**
	 * Get the [optionally formatted] temporal [time] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getTime($format = 'Y-m-d H:i:s')
	{
		if ($this->time === null) {
			return null;
		}


		if ($this->time === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->time);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->time, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [message] column value.
	 * 
	 * @return     string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Set the value of [vehiclestatusid] column.
	 * 
	 * @param      int $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setVehiclestatusid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->vehiclestatusid !== $v) {
			$this->vehiclestatusid = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::VEHICLESTATUSID;
		}

		return $this;
	} // setVehiclestatusid()

	/**
	 * Set the value of [statustypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setStatustypeid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->statustypeid !== $v) {
			$this->statustypeid = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::STATUSTYPEID;
		}

		if ($this->aStatustype !== null && $this->aStatustype->getStatustypeid() !== $v) {
			$this->aStatustype = null;
		}

		return $this;
	} // setStatustypeid()

	/**
	 * Set the value of [vehicleid] column.
	 * 
	 * @param      int $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setVehicleid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->vehicleid !== $v) {
			$this->vehicleid = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::VEHICLEID;
		}

		if ($this->aVehicle !== null && $this->aVehicle->getVehicleid() !== $v) {
			$this->aVehicle = null;
		}

		return $this;
	} // setVehicleid()

	/**
	 * Set the value of [lat] column.
	 * 
	 * @param      string $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setLat($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lat !== $v) {
			$this->lat = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::LAT;
		}

		return $this;
	} // setLat()

	/**
	 * Set the value of [lng] column.
	 * 
	 * @param      string $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setLng($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lng !== $v) {
			$this->lng = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::LNG;
		}

		return $this;
	} // setLng()

	/**
	 * Sets the value of [time] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setTime($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->time !== null || $dt !== null) {
			$currentDateAsString = ($this->time !== null && $tmpDt = new DateTime($this->time)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->time = $newDateAsString;
				$this->modifiedColumns[] = VehiclestatusPeer::TIME;
			}
		} // if either are not null

		return $this;
	} // setTime()

	/**
	 * Set the value of [message] column.
	 * 
	 * @param      string $v new value
	 * @return     Vehiclestatus The current object (for fluent API support)
	 */
	public function setMessage($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->message !== $v) {
			$this->message = $v;
			$this->modifiedColumns[] = VehiclestatusPeer::MESSAGE;
		}

		return $this;
	} // setMessage()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->statustypeid !== 1) {
				return false;
			}

			if ($this->vehicleid !== 1) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->vehiclestatusid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->statustypeid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->vehicleid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->lat = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->lng = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->time = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->message = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 7; // 7 = VehiclestatusPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Vehiclestatus object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aStatustype !== null && $this->statustypeid !== $this->aStatustype->getStatustypeid()) {
			$this->aStatustype = null;
		}
		if ($this->aVehicle !== null && $this->vehicleid !== $this->aVehicle->getVehicleid()) {
			$this->aVehicle = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(VehiclestatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = VehiclestatusPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aStatustype = null;
			$this->aVehicle = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(VehiclestatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = VehiclestatusQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(VehiclestatusPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				VehiclestatusPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aStatustype !== null) {
				if ($this->aStatustype->isModified() || $this->aStatustype->isNew()) {
					$affectedRows += $this->aStatustype->save($con);
				}
				$this->setStatustype($this->aStatustype);
			}

			if ($this->aVehicle !== null) {
				if ($this->aVehicle->isModified() || $this->aVehicle->isNew()) {
					$affectedRows += $this->aVehicle->save($con);
				}
				$this->setVehicle($this->aVehicle);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = VehiclestatusPeer::VEHICLESTATUSID;
		if (null !== $this->vehiclestatusid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . VehiclestatusPeer::VEHICLESTATUSID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(VehiclestatusPeer::VEHICLESTATUSID)) {
			$modifiedColumns[':p' . $index++]  = '`VEHICLESTATUSID`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::STATUSTYPEID)) {
			$modifiedColumns[':p' . $index++]  = '`STATUSTYPEID`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::VEHICLEID)) {
			$modifiedColumns[':p' . $index++]  = '`VEHICLEID`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::LAT)) {
			$modifiedColumns[':p' . $index++]  = '`LAT`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::LNG)) {
			$modifiedColumns[':p' . $index++]  = '`LNG`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::TIME)) {
			$modifiedColumns[':p' . $index++]  = '`TIME`';
		}
		if ($this->isColumnModified(VehiclestatusPeer::MESSAGE)) {
			$modifiedColumns[':p' . $index++]  = '`MESSAGE`';
		}

		$sql = sprintf(
			'INSERT INTO `vehiclestatus` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`VEHICLESTATUSID`':
						$stmt->bindValue($identifier, $this->vehiclestatusid, PDO::PARAM_INT);
						break;
					case '`STATUSTYPEID`':
						$stmt->bindValue($identifier, $this->statustypeid, PDO::PARAM_INT);
						break;
					case '`VEHICLEID`':
						$stmt->bindValue($identifier, $this->vehicleid, PDO::PARAM_INT);
						break;
					case '`LAT`':
						$stmt->bindValue($identifier, $this->lat, PDO::PARAM_STR);
						break;
					case '`LNG`':
						$stmt->bindValue($identifier, $this->lng, PDO::PARAM_STR);
						break;
					case '`TIME`':
						$stmt->bindValue($identifier, $this->time, PDO::PARAM_STR);
						break;
					case '`MESSAGE`':
						$stmt->bindValue($identifier, $this->message, PDO::PARAM_STR);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setVehiclestatusid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aStatustype !== null) {
				if (!$this->aStatustype->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aStatustype->getValidationFailures());
				}
			}

			if ($this->aVehicle !== null) {
				if (!$this->aVehicle->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aVehicle->getValidationFailures());
				}
			}


			if (($retval = VehiclestatusPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = VehiclestatusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getVehiclestatusid();
				break;
			case 1:
				return $this->getStatustypeid();
				break;
			case 2:
				return $this->getVehicleid();
				break;
			case 3:
				return $this->getLat();
				break;
			case 4:
				return $this->getLng();
				break;
			case 5:
				return $this->getTime();
				break;
			case 6:
				return $this->getMessage();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Vehiclestatus'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Vehiclestatus'][$this->getPrimaryKey()] = true;
		$keys = VehiclestatusPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getVehiclestatusid(),
			$keys[1] => $this->getStatustypeid(),
			$keys[2] => $this->getVehicleid(),
			$keys[3] => $this->getLat(),
			$keys[4] => $this->getLng(),
			$keys[5] => $this->getTime(),
			$keys[6] => $this->getMessage(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aStatustype) {
				$result['Statustype'] = $this->aStatustype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aVehicle) {
				$result['Vehicle'] = $this->aVehicle->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = VehiclestatusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setVehiclestatusid($value);
				break;
			case 1:
				$this->setStatustypeid($value);
				break;
			case 2:
				$this->setVehicleid($value);
				break;
			case 3:
				$this->setLat($value);
				break;
			case 4:
				$this->setLng($value);
				break;
			case 5:
				$this->setTime($value);
				break;
			case 6:
				$this->setMessage($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = VehiclestatusPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setVehiclestatusid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setStatustypeid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setVehicleid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLat($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLng($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTime($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMessage($arr[$keys[6]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(VehiclestatusPeer::DATABASE_NAME);

		if ($this->isColumnModified(VehiclestatusPeer::VEHICLESTATUSID)) $criteria->add(VehiclestatusPeer::VEHICLESTATUSID, $this->vehiclestatusid);
		if ($this->isColumnModified(VehiclestatusPeer::STATUSTYPEID)) $criteria->add(VehiclestatusPeer::STATUSTYPEID, $this->statustypeid);
		if ($this->isColumnModified(VehiclestatusPeer::VEHICLEID)) $criteria->add(VehiclestatusPeer::VEHICLEID, $this->vehicleid);
		if ($this->isColumnModified(VehiclestatusPeer::LAT)) $criteria->add(VehiclestatusPeer::LAT, $this->lat);
		if ($this->isColumnModified(VehiclestatusPeer::LNG)) $criteria->add(VehiclestatusPeer::LNG, $this->lng);
		if ($this->isColumnModified(VehiclestatusPeer::TIME)) $criteria->add(VehiclestatusPeer::TIME, $this->time);
		if ($this->isColumnModified(VehiclestatusPeer::MESSAGE)) $criteria->add(VehiclestatusPeer::MESSAGE, $this->message);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(VehiclestatusPeer::DATABASE_NAME);
		$criteria->add(VehiclestatusPeer::VEHICLESTATUSID, $this->vehiclestatusid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getVehiclestatusid();
	}

	/**
	 * Generic method to set the primary key (vehiclestatusid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setVehiclestatusid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getVehiclestatusid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Vehiclestatus (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setStatustypeid($this->getStatustypeid());
		$copyObj->setVehicleid($this->getVehicleid());
		$copyObj->setLat($this->getLat());
		$copyObj->setLng($this->getLng());
		$copyObj->setTime($this->getTime());
		$copyObj->setMessage($this->getMessage());
		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setVehiclestatusid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Vehiclestatus Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     VehiclestatusPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new VehiclestatusPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Statustype object.
	 *
	 * @param      Statustype $v
	 * @return     Vehiclestatus The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setStatustype(Statustype $v = null)
	{
		if ($v === null) {
			$this->setStatustypeid(1);
		} else {
			$this->setStatustypeid($v->getStatustypeid());
		}

		$this->aStatustype = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Statustype object, it will not be re-added.
		if ($v !== null) {
			$v->addVehiclestatus($this);
		}

		return $this;
	}


	/**
	 * Get the associated Statustype object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Statustype The associated Statustype object.
	 * @throws     PropelException
	 */
	public function getStatustype(PropelPDO $con = null)
	{
		if ($this->aStatustype === null && ($this->statustypeid !== null)) {
			$this->aStatustype = StatustypeQuery::create()->findPk($this->statustypeid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aStatustype->addVehiclestatuss($this);
			 */
		}
		return $this->aStatustype;
	}

	/**
	 * Declares an association between this object and a Vehicle object.
	 *
	 * @param      Vehicle $v
	 * @return     Vehiclestatus The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setVehicle(Vehicle $v = null)
	{
		if ($v === null) {
			$this->setVehicleid(1);
		} else {
			$this->setVehicleid($v->getVehicleid());
		}

		$this->aVehicle = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Vehicle object, it will not be re-added.
		if ($v !== null) {
			$v->addVehiclestatus($this);
		}

		return $this;
	}


	/**
	 * Get the associated Vehicle object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Vehicle The associated Vehicle object.
	 * @throws     PropelException
	 */
	public function getVehicle(PropelPDO $con = null)
	{
		if ($this->aVehicle === null && ($this->vehicleid !== null)) {
			$this->aVehicle = VehicleQuery::create()->findPk($this->vehicleid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aVehicle->addVehiclestatuss($this);
			 */
		}
		return $this->aVehicle;
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->vehiclestatusid = null;
		$this->statustypeid = null;
		$this->vehicleid = null;
		$this->lat = null;
		$this->lng = null;
		$this->time = null;
		$this->message = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

		$this->aStatustype = null;
		$this->aVehicle = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(VehiclestatusPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseVehiclestatus
