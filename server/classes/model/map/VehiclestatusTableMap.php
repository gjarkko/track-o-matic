<?php



/**
 * This class defines the structure of the 'vehiclestatus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class VehiclestatusTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.VehiclestatusTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('vehiclestatus');
		$this->setPhpName('Vehiclestatus');
		$this->setClassname('Vehiclestatus');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('VEHICLESTATUSID', 'Vehiclestatusid', 'INTEGER', true, 10, null);
		$this->addForeignKey('STATUSTYPEID', 'Statustypeid', 'INTEGER', 'statustype', 'STATUSTYPEID', true, 10, 1);
		$this->addForeignKey('VEHICLEID', 'Vehicleid', 'INTEGER', 'vehicle', 'VEHICLEID', true, 10, 1);
		$this->addColumn('LAT', 'Lat', 'DECIMAL', false, 18, null);
		$this->addColumn('LNG', 'Lng', 'DECIMAL', false, 18, null);
		$this->addColumn('TIME', 'Time', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
		$this->addColumn('MESSAGE', 'Message', 'LONGVARCHAR', false, null, null);
		// validators
		$this->addValidator('LAT', 'minValue', 'propel.validator.MinValueValidator', '-90', 'latitude cannot be less than -90 degrees');
		$this->addValidator('LAT', 'maxValue', 'propel.validator.MaxValueValidator', '90', 'latitude cannot be greater than 90 degrees');
		$this->addValidator('LNG', 'minValue', 'propel.validator.MinValueValidator', '-90', 'longitude cannot be less than -90 degrees');
		$this->addValidator('LNG', 'maxValue', 'propel.validator.MaxValueValidator', '90', 'longitude cannot be greater than 90 degrees');
		$this->addValidator('MESSAGE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'message must be at least 1 character');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Statustype', 'Statustype', RelationMap::MANY_TO_ONE, array('statustypeid' => 'statustypeid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Vehicle', 'Vehicle', RelationMap::MANY_TO_ONE, array('vehicleid' => 'vehicleid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // VehiclestatusTableMap
