<?php


/**
 * Base class that represents a row from the 'trackable' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTrackable extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'TrackablePeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        TrackablePeer
	 */
	protected static $peer;

	/**
	 * The value for the trackableid field.
	 * @var        int
	 */
	protected $trackableid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the start_addressid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $start_addressid;

	/**
	 * The value for the end_addressid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $end_addressid;

	/**
	 * The value for the trackablereference field.
	 * @var        string
	 */
	protected $trackablereference;

	/**
	 * The value for the created field.
	 * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
	 * @var        string
	 */
	protected $created;

	/**
	 * The value for the edited field.
	 * @var        string
	 */
	protected $edited;

	/**
	 * The value for the valid field.
	 * Note: this column has a database default value of: true
	 * @var        boolean
	 */
	protected $valid;

	/**
	 * @var        Address
	 */
	protected $aAddressRelatedByStartAddressid;

	/**
	 * @var        Address
	 */
	protected $aAddressRelatedByEndAddressid;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * @var        array Detail[] Collection to store aggregation of Detail objects.
	 */
	protected $collDetails;

	/**
	 * @var        array Groupreference[] Collection to store aggregation of Groupreference objects.
	 */
	protected $collGroupreferences;

	/**
	 * @var        array Notification[] Collection to store aggregation of Notification objects.
	 */
	protected $collNotifications;

	/**
	 * @var        array Trackablestatus[] Collection to store aggregation of Trackablestatus objects.
	 */
	protected $collTrackablestatuss;

	/**
	 * @var        array Triptrackable[] Collection to store aggregation of Triptrackable objects.
	 */
	protected $collTriptrackables;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $detailsScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $groupreferencesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $notificationsScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $trackablestatussScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $triptrackablesScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->customerid = 1;
		$this->start_addressid = 1;
		$this->end_addressid = 1;
		$this->valid = true;
	}

	/**
	 * Initializes internal state of BaseTrackable object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [trackableid] column value.
	 * 
	 * @return     int
	 */
	public function getTrackableid()
	{
		return $this->trackableid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [start_addressid] column value.
	 * 
	 * @return     int
	 */
	public function getStartAddressid()
	{
		return $this->start_addressid;
	}

	/**
	 * Get the [end_addressid] column value.
	 * 
	 * @return     int
	 */
	public function getEndAddressid()
	{
		return $this->end_addressid;
	}

	/**
	 * Get the [trackablereference] column value.
	 * 
	 * @return     string
	 */
	public function getTrackablereference()
	{
		return $this->trackablereference;
	}

	/**
	 * Get the [optionally formatted] temporal [created] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getCreated($format = 'Y-m-d H:i:s')
	{
		if ($this->created === null) {
			return null;
		}


		if ($this->created === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->created);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [edited] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getEdited($format = 'Y-m-d H:i:s')
	{
		if ($this->edited === null) {
			return null;
		}


		if ($this->edited === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->edited);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->edited, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [valid] column value.
	 * 
	 * @return     boolean
	 */
	public function getValid()
	{
		return $this->valid;
	}

	/**
	 * Set the value of [trackableid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setTrackableid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->trackableid !== $v) {
			$this->trackableid = $v;
			$this->modifiedColumns[] = TrackablePeer::TRACKABLEID;
		}

		return $this;
	} // setTrackableid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = TrackablePeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [start_addressid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setStartAddressid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->start_addressid !== $v) {
			$this->start_addressid = $v;
			$this->modifiedColumns[] = TrackablePeer::START_ADDRESSID;
		}

		if ($this->aAddressRelatedByStartAddressid !== null && $this->aAddressRelatedByStartAddressid->getAddressid() !== $v) {
			$this->aAddressRelatedByStartAddressid = null;
		}

		return $this;
	} // setStartAddressid()

	/**
	 * Set the value of [end_addressid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setEndAddressid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->end_addressid !== $v) {
			$this->end_addressid = $v;
			$this->modifiedColumns[] = TrackablePeer::END_ADDRESSID;
		}

		if ($this->aAddressRelatedByEndAddressid !== null && $this->aAddressRelatedByEndAddressid->getAddressid() !== $v) {
			$this->aAddressRelatedByEndAddressid = null;
		}

		return $this;
	} // setEndAddressid()

	/**
	 * Set the value of [trackablereference] column.
	 * 
	 * @param      string $v new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setTrackablereference($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->trackablereference !== $v) {
			$this->trackablereference = $v;
			$this->modifiedColumns[] = TrackablePeer::TRACKABLEREFERENCE;
		}

		return $this;
	} // setTrackablereference()

	/**
	 * Sets the value of [created] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setCreated($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->created !== null || $dt !== null) {
			$currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->created = $newDateAsString;
				$this->modifiedColumns[] = TrackablePeer::CREATED;
			}
		} // if either are not null

		return $this;
	} // setCreated()

	/**
	 * Sets the value of [edited] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setEdited($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->edited !== null || $dt !== null) {
			$currentDateAsString = ($this->edited !== null && $tmpDt = new DateTime($this->edited)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->edited = $newDateAsString;
				$this->modifiedColumns[] = TrackablePeer::EDITED;
			}
		} // if either are not null

		return $this;
	} // setEdited()

	/**
	 * Sets the value of the [valid] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function setValid($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->valid !== $v) {
			$this->valid = $v;
			$this->modifiedColumns[] = TrackablePeer::VALID;
		}

		return $this;
	} // setValid()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->customerid !== 1) {
				return false;
			}

			if ($this->start_addressid !== 1) {
				return false;
			}

			if ($this->end_addressid !== 1) {
				return false;
			}

			if ($this->valid !== true) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->trackableid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->customerid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->start_addressid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->end_addressid = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->trackablereference = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->created = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->edited = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->valid = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 8; // 8 = TrackablePeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Trackable object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
		if ($this->aAddressRelatedByStartAddressid !== null && $this->start_addressid !== $this->aAddressRelatedByStartAddressid->getAddressid()) {
			$this->aAddressRelatedByStartAddressid = null;
		}
		if ($this->aAddressRelatedByEndAddressid !== null && $this->end_addressid !== $this->aAddressRelatedByEndAddressid->getAddressid()) {
			$this->aAddressRelatedByEndAddressid = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TrackablePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = TrackablePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aAddressRelatedByStartAddressid = null;
			$this->aAddressRelatedByEndAddressid = null;
			$this->aCustomer = null;
			$this->collDetails = null;

			$this->collGroupreferences = null;

			$this->collNotifications = null;

			$this->collTrackablestatuss = null;

			$this->collTriptrackables = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TrackablePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = TrackableQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TrackablePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				TrackablePeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aAddressRelatedByStartAddressid !== null) {
				if ($this->aAddressRelatedByStartAddressid->isModified() || $this->aAddressRelatedByStartAddressid->isNew()) {
					$affectedRows += $this->aAddressRelatedByStartAddressid->save($con);
				}
				$this->setAddressRelatedByStartAddressid($this->aAddressRelatedByStartAddressid);
			}

			if ($this->aAddressRelatedByEndAddressid !== null) {
				if ($this->aAddressRelatedByEndAddressid->isModified() || $this->aAddressRelatedByEndAddressid->isNew()) {
					$affectedRows += $this->aAddressRelatedByEndAddressid->save($con);
				}
				$this->setAddressRelatedByEndAddressid($this->aAddressRelatedByEndAddressid);
			}

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->detailsScheduledForDeletion !== null) {
				if (!$this->detailsScheduledForDeletion->isEmpty()) {
					DetailQuery::create()
						->filterByPrimaryKeys($this->detailsScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->detailsScheduledForDeletion = null;
				}
			}

			if ($this->collDetails !== null) {
				foreach ($this->collDetails as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->groupreferencesScheduledForDeletion !== null) {
				if (!$this->groupreferencesScheduledForDeletion->isEmpty()) {
					GroupreferenceQuery::create()
						->filterByPrimaryKeys($this->groupreferencesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->groupreferencesScheduledForDeletion = null;
				}
			}

			if ($this->collGroupreferences !== null) {
				foreach ($this->collGroupreferences as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->notificationsScheduledForDeletion !== null) {
				if (!$this->notificationsScheduledForDeletion->isEmpty()) {
					NotificationQuery::create()
						->filterByPrimaryKeys($this->notificationsScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->notificationsScheduledForDeletion = null;
				}
			}

			if ($this->collNotifications !== null) {
				foreach ($this->collNotifications as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->trackablestatussScheduledForDeletion !== null) {
				if (!$this->trackablestatussScheduledForDeletion->isEmpty()) {
					TrackablestatusQuery::create()
						->filterByPrimaryKeys($this->trackablestatussScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->trackablestatussScheduledForDeletion = null;
				}
			}

			if ($this->collTrackablestatuss !== null) {
				foreach ($this->collTrackablestatuss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->triptrackablesScheduledForDeletion !== null) {
				if (!$this->triptrackablesScheduledForDeletion->isEmpty()) {
					TriptrackableQuery::create()
						->filterByPrimaryKeys($this->triptrackablesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->triptrackablesScheduledForDeletion = null;
				}
			}

			if ($this->collTriptrackables !== null) {
				foreach ($this->collTriptrackables as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = TrackablePeer::TRACKABLEID;
		if (null !== $this->trackableid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . TrackablePeer::TRACKABLEID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(TrackablePeer::TRACKABLEID)) {
			$modifiedColumns[':p' . $index++]  = '`TRACKABLEID`';
		}
		if ($this->isColumnModified(TrackablePeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(TrackablePeer::START_ADDRESSID)) {
			$modifiedColumns[':p' . $index++]  = '`START_ADDRESSID`';
		}
		if ($this->isColumnModified(TrackablePeer::END_ADDRESSID)) {
			$modifiedColumns[':p' . $index++]  = '`END_ADDRESSID`';
		}
		if ($this->isColumnModified(TrackablePeer::TRACKABLEREFERENCE)) {
			$modifiedColumns[':p' . $index++]  = '`TRACKABLEREFERENCE`';
		}
		if ($this->isColumnModified(TrackablePeer::CREATED)) {
			$modifiedColumns[':p' . $index++]  = '`CREATED`';
		}
		if ($this->isColumnModified(TrackablePeer::EDITED)) {
			$modifiedColumns[':p' . $index++]  = '`EDITED`';
		}
		if ($this->isColumnModified(TrackablePeer::VALID)) {
			$modifiedColumns[':p' . $index++]  = '`VALID`';
		}

		$sql = sprintf(
			'INSERT INTO `trackable` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`TRACKABLEID`':
						$stmt->bindValue($identifier, $this->trackableid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`START_ADDRESSID`':
						$stmt->bindValue($identifier, $this->start_addressid, PDO::PARAM_INT);
						break;
					case '`END_ADDRESSID`':
						$stmt->bindValue($identifier, $this->end_addressid, PDO::PARAM_INT);
						break;
					case '`TRACKABLEREFERENCE`':
						$stmt->bindValue($identifier, $this->trackablereference, PDO::PARAM_STR);
						break;
					case '`CREATED`':
						$stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
						break;
					case '`EDITED`':
						$stmt->bindValue($identifier, $this->edited, PDO::PARAM_STR);
						break;
					case '`VALID`':
						$stmt->bindValue($identifier, (int) $this->valid, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setTrackableid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aAddressRelatedByStartAddressid !== null) {
				if (!$this->aAddressRelatedByStartAddressid->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aAddressRelatedByStartAddressid->getValidationFailures());
				}
			}

			if ($this->aAddressRelatedByEndAddressid !== null) {
				if (!$this->aAddressRelatedByEndAddressid->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aAddressRelatedByEndAddressid->getValidationFailures());
				}
			}

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}


			if (($retval = TrackablePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collDetails !== null) {
					foreach ($this->collDetails as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collGroupreferences !== null) {
					foreach ($this->collGroupreferences as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collNotifications !== null) {
					foreach ($this->collNotifications as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTrackablestatuss !== null) {
					foreach ($this->collTrackablestatuss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTriptrackables !== null) {
					foreach ($this->collTriptrackables as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TrackablePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getTrackableid();
				break;
			case 1:
				return $this->getCustomerid();
				break;
			case 2:
				return $this->getStartAddressid();
				break;
			case 3:
				return $this->getEndAddressid();
				break;
			case 4:
				return $this->getTrackablereference();
				break;
			case 5:
				return $this->getCreated();
				break;
			case 6:
				return $this->getEdited();
				break;
			case 7:
				return $this->getValid();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Trackable'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Trackable'][$this->getPrimaryKey()] = true;
		$keys = TrackablePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getTrackableid(),
			$keys[1] => $this->getCustomerid(),
			$keys[2] => $this->getStartAddressid(),
			$keys[3] => $this->getEndAddressid(),
			$keys[4] => $this->getTrackablereference(),
			$keys[5] => $this->getCreated(),
			$keys[6] => $this->getEdited(),
			$keys[7] => $this->getValid(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aAddressRelatedByStartAddressid) {
				$result['AddressRelatedByStartAddressid'] = $this->aAddressRelatedByStartAddressid->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aAddressRelatedByEndAddressid) {
				$result['AddressRelatedByEndAddressid'] = $this->aAddressRelatedByEndAddressid->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->collDetails) {
				$result['Details'] = $this->collDetails->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collGroupreferences) {
				$result['Groupreferences'] = $this->collGroupreferences->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collNotifications) {
				$result['Notifications'] = $this->collNotifications->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTrackablestatuss) {
				$result['Trackablestatuss'] = $this->collTrackablestatuss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTriptrackables) {
				$result['Triptrackables'] = $this->collTriptrackables->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TrackablePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setTrackableid($value);
				break;
			case 1:
				$this->setCustomerid($value);
				break;
			case 2:
				$this->setStartAddressid($value);
				break;
			case 3:
				$this->setEndAddressid($value);
				break;
			case 4:
				$this->setTrackablereference($value);
				break;
			case 5:
				$this->setCreated($value);
				break;
			case 6:
				$this->setEdited($value);
				break;
			case 7:
				$this->setValid($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TrackablePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setTrackableid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCustomerid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStartAddressid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setEndAddressid($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTrackablereference($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCreated($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setEdited($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setValid($arr[$keys[7]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(TrackablePeer::DATABASE_NAME);

		if ($this->isColumnModified(TrackablePeer::TRACKABLEID)) $criteria->add(TrackablePeer::TRACKABLEID, $this->trackableid);
		if ($this->isColumnModified(TrackablePeer::CUSTOMERID)) $criteria->add(TrackablePeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(TrackablePeer::START_ADDRESSID)) $criteria->add(TrackablePeer::START_ADDRESSID, $this->start_addressid);
		if ($this->isColumnModified(TrackablePeer::END_ADDRESSID)) $criteria->add(TrackablePeer::END_ADDRESSID, $this->end_addressid);
		if ($this->isColumnModified(TrackablePeer::TRACKABLEREFERENCE)) $criteria->add(TrackablePeer::TRACKABLEREFERENCE, $this->trackablereference);
		if ($this->isColumnModified(TrackablePeer::CREATED)) $criteria->add(TrackablePeer::CREATED, $this->created);
		if ($this->isColumnModified(TrackablePeer::EDITED)) $criteria->add(TrackablePeer::EDITED, $this->edited);
		if ($this->isColumnModified(TrackablePeer::VALID)) $criteria->add(TrackablePeer::VALID, $this->valid);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TrackablePeer::DATABASE_NAME);
		$criteria->add(TrackablePeer::TRACKABLEID, $this->trackableid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getTrackableid();
	}

	/**
	 * Generic method to set the primary key (trackableid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setTrackableid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getTrackableid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Trackable (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setStartAddressid($this->getStartAddressid());
		$copyObj->setEndAddressid($this->getEndAddressid());
		$copyObj->setTrackablereference($this->getTrackablereference());
		$copyObj->setCreated($this->getCreated());
		$copyObj->setEdited($this->getEdited());
		$copyObj->setValid($this->getValid());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getDetails() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addDetail($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getGroupreferences() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addGroupreference($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getNotifications() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addNotification($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTrackablestatuss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTrackablestatus($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTriptrackables() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTriptrackable($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setTrackableid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Trackable Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     TrackablePeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TrackablePeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Address object.
	 *
	 * @param      Address $v
	 * @return     Trackable The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setAddressRelatedByStartAddressid(Address $v = null)
	{
		if ($v === null) {
			$this->setStartAddressid(1);
		} else {
			$this->setStartAddressid($v->getAddressid());
		}

		$this->aAddressRelatedByStartAddressid = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Address object, it will not be re-added.
		if ($v !== null) {
			$v->addTrackableRelatedByStartAddressid($this);
		}

		return $this;
	}


	/**
	 * Get the associated Address object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Address The associated Address object.
	 * @throws     PropelException
	 */
	public function getAddressRelatedByStartAddressid(PropelPDO $con = null)
	{
		if ($this->aAddressRelatedByStartAddressid === null && ($this->start_addressid !== null)) {
			$this->aAddressRelatedByStartAddressid = AddressQuery::create()->findPk($this->start_addressid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aAddressRelatedByStartAddressid->addTrackablesRelatedByStartAddressid($this);
			 */
		}
		return $this->aAddressRelatedByStartAddressid;
	}

	/**
	 * Declares an association between this object and a Address object.
	 *
	 * @param      Address $v
	 * @return     Trackable The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setAddressRelatedByEndAddressid(Address $v = null)
	{
		if ($v === null) {
			$this->setEndAddressid(1);
		} else {
			$this->setEndAddressid($v->getAddressid());
		}

		$this->aAddressRelatedByEndAddressid = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Address object, it will not be re-added.
		if ($v !== null) {
			$v->addTrackableRelatedByEndAddressid($this);
		}

		return $this;
	}


	/**
	 * Get the associated Address object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Address The associated Address object.
	 * @throws     PropelException
	 */
	public function getAddressRelatedByEndAddressid(PropelPDO $con = null)
	{
		if ($this->aAddressRelatedByEndAddressid === null && ($this->end_addressid !== null)) {
			$this->aAddressRelatedByEndAddressid = AddressQuery::create()->findPk($this->end_addressid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aAddressRelatedByEndAddressid->addTrackablesRelatedByEndAddressid($this);
			 */
		}
		return $this->aAddressRelatedByEndAddressid;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     Trackable The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addTrackable($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addTrackables($this);
			 */
		}
		return $this->aCustomer;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('Detail' == $relationName) {
			return $this->initDetails();
		}
		if ('Groupreference' == $relationName) {
			return $this->initGroupreferences();
		}
		if ('Notification' == $relationName) {
			return $this->initNotifications();
		}
		if ('Trackablestatus' == $relationName) {
			return $this->initTrackablestatuss();
		}
		if ('Triptrackable' == $relationName) {
			return $this->initTriptrackables();
		}
	}

	/**
	 * Clears out the collDetails collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addDetails()
	 */
	public function clearDetails()
	{
		$this->collDetails = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collDetails collection.
	 *
	 * By default this just sets the collDetails collection to an empty array (like clearcollDetails());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initDetails($overrideExisting = true)
	{
		if (null !== $this->collDetails && !$overrideExisting) {
			return;
		}
		$this->collDetails = new PropelObjectCollection();
		$this->collDetails->setModel('Detail');
	}

	/**
	 * Gets an array of Detail objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trackable is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Detail[] List of Detail objects
	 * @throws     PropelException
	 */
	public function getDetails($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collDetails || null !== $criteria) {
			if ($this->isNew() && null === $this->collDetails) {
				// return empty collection
				$this->initDetails();
			} else {
				$collDetails = DetailQuery::create(null, $criteria)
					->filterByTrackable($this)
					->find($con);
				if (null !== $criteria) {
					return $collDetails;
				}
				$this->collDetails = $collDetails;
			}
		}
		return $this->collDetails;
	}

	/**
	 * Sets a collection of Detail objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $details A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setDetails(PropelCollection $details, PropelPDO $con = null)
	{
		$this->detailsScheduledForDeletion = $this->getDetails(new Criteria(), $con)->diff($details);

		foreach ($details as $detail) {
			// Fix issue with collection modified by reference
			if ($detail->isNew()) {
				$detail->setTrackable($this);
			}
			$this->addDetail($detail);
		}

		$this->collDetails = $details;
	}

	/**
	 * Returns the number of related Detail objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Detail objects.
	 * @throws     PropelException
	 */
	public function countDetails(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collDetails || null !== $criteria) {
			if ($this->isNew() && null === $this->collDetails) {
				return 0;
			} else {
				$query = DetailQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrackable($this)
					->count($con);
			}
		} else {
			return count($this->collDetails);
		}
	}

	/**
	 * Method called to associate a Detail object to this object
	 * through the Detail foreign key attribute.
	 *
	 * @param      Detail $l Detail
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function addDetail(Detail $l)
	{
		if ($this->collDetails === null) {
			$this->initDetails();
		}
		if (!$this->collDetails->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddDetail($l);
		}

		return $this;
	}

	/**
	 * @param	Detail $detail The detail object to add.
	 */
	protected function doAddDetail($detail)
	{
		$this->collDetails[]= $detail;
		$detail->setTrackable($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Details from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Detail[] List of Detail objects
	 */
	public function getDetailsJoinDetailtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = DetailQuery::create(null, $criteria);
		$query->joinWith('Detailtype', $join_behavior);

		return $this->getDetails($query, $con);
	}

	/**
	 * Clears out the collGroupreferences collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addGroupreferences()
	 */
	public function clearGroupreferences()
	{
		$this->collGroupreferences = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collGroupreferences collection.
	 *
	 * By default this just sets the collGroupreferences collection to an empty array (like clearcollGroupreferences());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initGroupreferences($overrideExisting = true)
	{
		if (null !== $this->collGroupreferences && !$overrideExisting) {
			return;
		}
		$this->collGroupreferences = new PropelObjectCollection();
		$this->collGroupreferences->setModel('Groupreference');
	}

	/**
	 * Gets an array of Groupreference objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trackable is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Groupreference[] List of Groupreference objects
	 * @throws     PropelException
	 */
	public function getGroupreferences($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collGroupreferences || null !== $criteria) {
			if ($this->isNew() && null === $this->collGroupreferences) {
				// return empty collection
				$this->initGroupreferences();
			} else {
				$collGroupreferences = GroupreferenceQuery::create(null, $criteria)
					->filterByTrackable($this)
					->find($con);
				if (null !== $criteria) {
					return $collGroupreferences;
				}
				$this->collGroupreferences = $collGroupreferences;
			}
		}
		return $this->collGroupreferences;
	}

	/**
	 * Sets a collection of Groupreference objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $groupreferences A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setGroupreferences(PropelCollection $groupreferences, PropelPDO $con = null)
	{
		$this->groupreferencesScheduledForDeletion = $this->getGroupreferences(new Criteria(), $con)->diff($groupreferences);

		foreach ($groupreferences as $groupreference) {
			// Fix issue with collection modified by reference
			if ($groupreference->isNew()) {
				$groupreference->setTrackable($this);
			}
			$this->addGroupreference($groupreference);
		}

		$this->collGroupreferences = $groupreferences;
	}

	/**
	 * Returns the number of related Groupreference objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Groupreference objects.
	 * @throws     PropelException
	 */
	public function countGroupreferences(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collGroupreferences || null !== $criteria) {
			if ($this->isNew() && null === $this->collGroupreferences) {
				return 0;
			} else {
				$query = GroupreferenceQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrackable($this)
					->count($con);
			}
		} else {
			return count($this->collGroupreferences);
		}
	}

	/**
	 * Method called to associate a Groupreference object to this object
	 * through the Groupreference foreign key attribute.
	 *
	 * @param      Groupreference $l Groupreference
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function addGroupreference(Groupreference $l)
	{
		if ($this->collGroupreferences === null) {
			$this->initGroupreferences();
		}
		if (!$this->collGroupreferences->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddGroupreference($l);
		}

		return $this;
	}

	/**
	 * @param	Groupreference $groupreference The groupreference object to add.
	 */
	protected function doAddGroupreference($groupreference)
	{
		$this->collGroupreferences[]= $groupreference;
		$groupreference->setTrackable($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Groupreferences from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Groupreference[] List of Groupreference objects
	 */
	public function getGroupreferencesJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = GroupreferenceQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getGroupreferences($query, $con);
	}

	/**
	 * Clears out the collNotifications collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addNotifications()
	 */
	public function clearNotifications()
	{
		$this->collNotifications = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collNotifications collection.
	 *
	 * By default this just sets the collNotifications collection to an empty array (like clearcollNotifications());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initNotifications($overrideExisting = true)
	{
		if (null !== $this->collNotifications && !$overrideExisting) {
			return;
		}
		$this->collNotifications = new PropelObjectCollection();
		$this->collNotifications->setModel('Notification');
	}

	/**
	 * Gets an array of Notification objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trackable is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 * @throws     PropelException
	 */
	public function getNotifications($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collNotifications || null !== $criteria) {
			if ($this->isNew() && null === $this->collNotifications) {
				// return empty collection
				$this->initNotifications();
			} else {
				$collNotifications = NotificationQuery::create(null, $criteria)
					->filterByTrackable($this)
					->find($con);
				if (null !== $criteria) {
					return $collNotifications;
				}
				$this->collNotifications = $collNotifications;
			}
		}
		return $this->collNotifications;
	}

	/**
	 * Sets a collection of Notification objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $notifications A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setNotifications(PropelCollection $notifications, PropelPDO $con = null)
	{
		$this->notificationsScheduledForDeletion = $this->getNotifications(new Criteria(), $con)->diff($notifications);

		foreach ($notifications as $notification) {
			// Fix issue with collection modified by reference
			if ($notification->isNew()) {
				$notification->setTrackable($this);
			}
			$this->addNotification($notification);
		}

		$this->collNotifications = $notifications;
	}

	/**
	 * Returns the number of related Notification objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Notification objects.
	 * @throws     PropelException
	 */
	public function countNotifications(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collNotifications || null !== $criteria) {
			if ($this->isNew() && null === $this->collNotifications) {
				return 0;
			} else {
				$query = NotificationQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrackable($this)
					->count($con);
			}
		} else {
			return count($this->collNotifications);
		}
	}

	/**
	 * Method called to associate a Notification object to this object
	 * through the Notification foreign key attribute.
	 *
	 * @param      Notification $l Notification
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function addNotification(Notification $l)
	{
		if ($this->collNotifications === null) {
			$this->initNotifications();
		}
		if (!$this->collNotifications->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddNotification($l);
		}

		return $this;
	}

	/**
	 * @param	Notification $notification The notification object to add.
	 */
	protected function doAddNotification($notification)
	{
		$this->collNotifications[]= $notification;
		$notification->setTrackable($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Notifications from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 */
	public function getNotificationsJoinNotificationtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = NotificationQuery::create(null, $criteria);
		$query->joinWith('Notificationtype', $join_behavior);

		return $this->getNotifications($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Notifications from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 */
	public function getNotificationsJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = NotificationQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getNotifications($query, $con);
	}

	/**
	 * Clears out the collTrackablestatuss collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTrackablestatuss()
	 */
	public function clearTrackablestatuss()
	{
		$this->collTrackablestatuss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTrackablestatuss collection.
	 *
	 * By default this just sets the collTrackablestatuss collection to an empty array (like clearcollTrackablestatuss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTrackablestatuss($overrideExisting = true)
	{
		if (null !== $this->collTrackablestatuss && !$overrideExisting) {
			return;
		}
		$this->collTrackablestatuss = new PropelObjectCollection();
		$this->collTrackablestatuss->setModel('Trackablestatus');
	}

	/**
	 * Gets an array of Trackablestatus objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trackable is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Trackablestatus[] List of Trackablestatus objects
	 * @throws     PropelException
	 */
	public function getTrackablestatuss($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTrackablestatuss || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablestatuss) {
				// return empty collection
				$this->initTrackablestatuss();
			} else {
				$collTrackablestatuss = TrackablestatusQuery::create(null, $criteria)
					->filterByTrackable($this)
					->find($con);
				if (null !== $criteria) {
					return $collTrackablestatuss;
				}
				$this->collTrackablestatuss = $collTrackablestatuss;
			}
		}
		return $this->collTrackablestatuss;
	}

	/**
	 * Sets a collection of Trackablestatus objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $trackablestatuss A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTrackablestatuss(PropelCollection $trackablestatuss, PropelPDO $con = null)
	{
		$this->trackablestatussScheduledForDeletion = $this->getTrackablestatuss(new Criteria(), $con)->diff($trackablestatuss);

		foreach ($trackablestatuss as $trackablestatus) {
			// Fix issue with collection modified by reference
			if ($trackablestatus->isNew()) {
				$trackablestatus->setTrackable($this);
			}
			$this->addTrackablestatus($trackablestatus);
		}

		$this->collTrackablestatuss = $trackablestatuss;
	}

	/**
	 * Returns the number of related Trackablestatus objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Trackablestatus objects.
	 * @throws     PropelException
	 */
	public function countTrackablestatuss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTrackablestatuss || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablestatuss) {
				return 0;
			} else {
				$query = TrackablestatusQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrackable($this)
					->count($con);
			}
		} else {
			return count($this->collTrackablestatuss);
		}
	}

	/**
	 * Method called to associate a Trackablestatus object to this object
	 * through the Trackablestatus foreign key attribute.
	 *
	 * @param      Trackablestatus $l Trackablestatus
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function addTrackablestatus(Trackablestatus $l)
	{
		if ($this->collTrackablestatuss === null) {
			$this->initTrackablestatuss();
		}
		if (!$this->collTrackablestatuss->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTrackablestatus($l);
		}

		return $this;
	}

	/**
	 * @param	Trackablestatus $trackablestatus The trackablestatus object to add.
	 */
	protected function doAddTrackablestatus($trackablestatus)
	{
		$this->collTrackablestatuss[]= $trackablestatus;
		$trackablestatus->setTrackable($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Trackablestatuss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Trackablestatus[] List of Trackablestatus objects
	 */
	public function getTrackablestatussJoinStatustype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TrackablestatusQuery::create(null, $criteria);
		$query->joinWith('Statustype', $join_behavior);

		return $this->getTrackablestatuss($query, $con);
	}

	/**
	 * Clears out the collTriptrackables collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTriptrackables()
	 */
	public function clearTriptrackables()
	{
		$this->collTriptrackables = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTriptrackables collection.
	 *
	 * By default this just sets the collTriptrackables collection to an empty array (like clearcollTriptrackables());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTriptrackables($overrideExisting = true)
	{
		if (null !== $this->collTriptrackables && !$overrideExisting) {
			return;
		}
		$this->collTriptrackables = new PropelObjectCollection();
		$this->collTriptrackables->setModel('Triptrackable');
	}

	/**
	 * Gets an array of Triptrackable objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trackable is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Triptrackable[] List of Triptrackable objects
	 * @throws     PropelException
	 */
	public function getTriptrackables($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTriptrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTriptrackables) {
				// return empty collection
				$this->initTriptrackables();
			} else {
				$collTriptrackables = TriptrackableQuery::create(null, $criteria)
					->filterByTrackable($this)
					->find($con);
				if (null !== $criteria) {
					return $collTriptrackables;
				}
				$this->collTriptrackables = $collTriptrackables;
			}
		}
		return $this->collTriptrackables;
	}

	/**
	 * Sets a collection of Triptrackable objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $triptrackables A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTriptrackables(PropelCollection $triptrackables, PropelPDO $con = null)
	{
		$this->triptrackablesScheduledForDeletion = $this->getTriptrackables(new Criteria(), $con)->diff($triptrackables);

		foreach ($triptrackables as $triptrackable) {
			// Fix issue with collection modified by reference
			if ($triptrackable->isNew()) {
				$triptrackable->setTrackable($this);
			}
			$this->addTriptrackable($triptrackable);
		}

		$this->collTriptrackables = $triptrackables;
	}

	/**
	 * Returns the number of related Triptrackable objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Triptrackable objects.
	 * @throws     PropelException
	 */
	public function countTriptrackables(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTriptrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTriptrackables) {
				return 0;
			} else {
				$query = TriptrackableQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrackable($this)
					->count($con);
			}
		} else {
			return count($this->collTriptrackables);
		}
	}

	/**
	 * Method called to associate a Triptrackable object to this object
	 * through the Triptrackable foreign key attribute.
	 *
	 * @param      Triptrackable $l Triptrackable
	 * @return     Trackable The current object (for fluent API support)
	 */
	public function addTriptrackable(Triptrackable $l)
	{
		if ($this->collTriptrackables === null) {
			$this->initTriptrackables();
		}
		if (!$this->collTriptrackables->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTriptrackable($l);
		}

		return $this;
	}

	/**
	 * @param	Triptrackable $triptrackable The triptrackable object to add.
	 */
	protected function doAddTriptrackable($triptrackable)
	{
		$this->collTriptrackables[]= $triptrackable;
		$triptrackable->setTrackable($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trackable is new, it will return
	 * an empty collection; or if this Trackable has previously
	 * been saved, it will retrieve related Triptrackables from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trackable.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Triptrackable[] List of Triptrackable objects
	 */
	public function getTriptrackablesJoinTrip($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TriptrackableQuery::create(null, $criteria);
		$query->joinWith('Trip', $join_behavior);

		return $this->getTriptrackables($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->trackableid = null;
		$this->customerid = null;
		$this->start_addressid = null;
		$this->end_addressid = null;
		$this->trackablereference = null;
		$this->created = null;
		$this->edited = null;
		$this->valid = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collDetails) {
				foreach ($this->collDetails as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collGroupreferences) {
				foreach ($this->collGroupreferences as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collNotifications) {
				foreach ($this->collNotifications as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTrackablestatuss) {
				foreach ($this->collTrackablestatuss as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTriptrackables) {
				foreach ($this->collTriptrackables as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collDetails instanceof PropelCollection) {
			$this->collDetails->clearIterator();
		}
		$this->collDetails = null;
		if ($this->collGroupreferences instanceof PropelCollection) {
			$this->collGroupreferences->clearIterator();
		}
		$this->collGroupreferences = null;
		if ($this->collNotifications instanceof PropelCollection) {
			$this->collNotifications->clearIterator();
		}
		$this->collNotifications = null;
		if ($this->collTrackablestatuss instanceof PropelCollection) {
			$this->collTrackablestatuss->clearIterator();
		}
		$this->collTrackablestatuss = null;
		if ($this->collTriptrackables instanceof PropelCollection) {
			$this->collTriptrackables->clearIterator();
		}
		$this->collTriptrackables = null;
		$this->aAddressRelatedByStartAddressid = null;
		$this->aAddressRelatedByEndAddressid = null;
		$this->aCustomer = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(TrackablePeer::DEFAULT_STRING_FORMAT);
	}

} // BaseTrackable
