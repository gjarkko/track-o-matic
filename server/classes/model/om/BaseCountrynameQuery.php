<?php


/**
 * Base class that represents a query for the 'countryname' table.
 *
 * 
 *
 * @method     CountrynameQuery orderByCountrynameid($order = Criteria::ASC) Order by the countrynameid column
 * @method     CountrynameQuery orderByCountryid($order = Criteria::ASC) Order by the countryid column
 * @method     CountrynameQuery orderByLanguageid($order = Criteria::ASC) Order by the languageid column
 * @method     CountrynameQuery orderByCountryname($order = Criteria::ASC) Order by the countryname column
 *
 * @method     CountrynameQuery groupByCountrynameid() Group by the countrynameid column
 * @method     CountrynameQuery groupByCountryid() Group by the countryid column
 * @method     CountrynameQuery groupByLanguageid() Group by the languageid column
 * @method     CountrynameQuery groupByCountryname() Group by the countryname column
 *
 * @method     CountrynameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     CountrynameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     CountrynameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     CountrynameQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     CountrynameQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     CountrynameQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     CountrynameQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     CountrynameQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     CountrynameQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     Countryname findOne(PropelPDO $con = null) Return the first Countryname matching the query
 * @method     Countryname findOneOrCreate(PropelPDO $con = null) Return the first Countryname matching the query, or a new Countryname object populated from the query conditions when no match is found
 *
 * @method     Countryname findOneByCountrynameid(int $countrynameid) Return the first Countryname filtered by the countrynameid column
 * @method     Countryname findOneByCountryid(int $countryid) Return the first Countryname filtered by the countryid column
 * @method     Countryname findOneByLanguageid(int $languageid) Return the first Countryname filtered by the languageid column
 * @method     Countryname findOneByCountryname(string $countryname) Return the first Countryname filtered by the countryname column
 *
 * @method     array findByCountrynameid(int $countrynameid) Return Countryname objects filtered by the countrynameid column
 * @method     array findByCountryid(int $countryid) Return Countryname objects filtered by the countryid column
 * @method     array findByLanguageid(int $languageid) Return Countryname objects filtered by the languageid column
 * @method     array findByCountryname(string $countryname) Return Countryname objects filtered by the countryname column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseCountrynameQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseCountrynameQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Countryname', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new CountrynameQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    CountrynameQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof CountrynameQuery) {
			return $criteria;
		}
		$query = new CountrynameQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Countryname|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = CountrynamePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(CountrynamePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Countryname A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `COUNTRYNAMEID`, `COUNTRYID`, `LANGUAGEID`, `COUNTRYNAME` FROM `countryname` WHERE `COUNTRYNAMEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Countryname();
			$obj->hydrate($row);
			CountrynamePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Countryname|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(CountrynamePeer::COUNTRYNAMEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(CountrynamePeer::COUNTRYNAMEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the countrynameid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountrynameid(1234); // WHERE countrynameid = 1234
	 * $query->filterByCountrynameid(array(12, 34)); // WHERE countrynameid IN (12, 34)
	 * $query->filterByCountrynameid(array('min' => 12)); // WHERE countrynameid > 12
	 * </code>
	 *
	 * @param     mixed $countrynameid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByCountrynameid($countrynameid = null, $comparison = null)
	{
		if (is_array($countrynameid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(CountrynamePeer::COUNTRYNAMEID, $countrynameid, $comparison);
	}

	/**
	 * Filter the query on the countryid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryid(1234); // WHERE countryid = 1234
	 * $query->filterByCountryid(array(12, 34)); // WHERE countryid IN (12, 34)
	 * $query->filterByCountryid(array('min' => 12)); // WHERE countryid > 12
	 * </code>
	 *
	 * @see       filterByCountry()
	 *
	 * @param     mixed $countryid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByCountryid($countryid = null, $comparison = null)
	{
		if (is_array($countryid)) {
			$useMinMax = false;
			if (isset($countryid['min'])) {
				$this->addUsingAlias(CountrynamePeer::COUNTRYID, $countryid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($countryid['max'])) {
				$this->addUsingAlias(CountrynamePeer::COUNTRYID, $countryid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(CountrynamePeer::COUNTRYID, $countryid, $comparison);
	}

	/**
	 * Filter the query on the languageid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLanguageid(1234); // WHERE languageid = 1234
	 * $query->filterByLanguageid(array(12, 34)); // WHERE languageid IN (12, 34)
	 * $query->filterByLanguageid(array('min' => 12)); // WHERE languageid > 12
	 * </code>
	 *
	 * @see       filterByLanguage()
	 *
	 * @param     mixed $languageid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByLanguageid($languageid = null, $comparison = null)
	{
		if (is_array($languageid)) {
			$useMinMax = false;
			if (isset($languageid['min'])) {
				$this->addUsingAlias(CountrynamePeer::LANGUAGEID, $languageid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($languageid['max'])) {
				$this->addUsingAlias(CountrynamePeer::LANGUAGEID, $languageid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(CountrynamePeer::LANGUAGEID, $languageid, $comparison);
	}

	/**
	 * Filter the query on the countryname column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryname('fooValue');   // WHERE countryname = 'fooValue'
	 * $query->filterByCountryname('%fooValue%'); // WHERE countryname LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $countryname The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByCountryname($countryname = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($countryname)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $countryname)) {
				$countryname = str_replace('*', '%', $countryname);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CountrynamePeer::COUNTRYNAME, $countryname, $comparison);
	}

	/**
	 * Filter the query by a related Country object
	 *
	 * @param     Country|PropelCollection $country The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByCountry($country, $comparison = null)
	{
		if ($country instanceof Country) {
			return $this
				->addUsingAlias(CountrynamePeer::COUNTRYID, $country->getCountryid(), $comparison);
		} elseif ($country instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(CountrynamePeer::COUNTRYID, $country->toKeyValue('PrimaryKey', 'Countryid'), $comparison);
		} else {
			throw new PropelException('filterByCountry() only accepts arguments of type Country or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Country relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function joinCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Country');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Country');
		}

		return $this;
	}

	/**
	 * Use the Country relation Country object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountryQuery A secondary query class using the current class as primary query
	 */
	public function useCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCountry($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Country', 'CountryQuery');
	}

	/**
	 * Filter the query by a related Language object
	 *
	 * @param     Language|PropelCollection $language The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function filterByLanguage($language, $comparison = null)
	{
		if ($language instanceof Language) {
			return $this
				->addUsingAlias(CountrynamePeer::LANGUAGEID, $language->getLanguageid(), $comparison);
		} elseif ($language instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(CountrynamePeer::LANGUAGEID, $language->toKeyValue('PrimaryKey', 'Languageid'), $comparison);
		} else {
			throw new PropelException('filterByLanguage() only accepts arguments of type Language or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Language relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Language');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Language');
		}

		return $this;
	}

	/**
	 * Use the Language relation Language object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LanguageQuery A secondary query class using the current class as primary query
	 */
	public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinLanguage($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Language', 'LanguageQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Countryname $countryname Object to remove from the list of results
	 *
	 * @return    CountrynameQuery The current query, for fluid interface
	 */
	public function prune($countryname = null)
	{
		if ($countryname) {
			$this->addUsingAlias(CountrynamePeer::COUNTRYNAMEID, $countryname->getCountrynameid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseCountrynameQuery