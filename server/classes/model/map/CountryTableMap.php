<?php



/**
 * This class defines the structure of the 'country' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class CountryTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.CountryTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('country');
		$this->setPhpName('Country');
		$this->setClassname('Country');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('COUNTRYID', 'Countryid', 'INTEGER', true, 10, null);
		$this->addColumn('ISOCODE', 'Isocode', 'VARCHAR', true, 2, null);
		$this->addColumn('COUNTRYNAME', 'Countryname', 'VARCHAR', true, 60, null);
		// validators
		$this->addValidator('ISOCODE', 'minLength', 'propel.validator.MinLengthValidator', '2', 'ISO code must be exactly 2 characters');
		$this->addValidator('ISOCODE', 'maxLength', 'propel.validator.MaxLengthValidator', '2', 'ISO code must be exactly 2 characters');
		$this->addValidator('ISOCODE', 'required', 'propel.validator.RequiredValidator', '', 'ISO code is required');
		$this->addValidator('ISOCODE', 'match', 'propel.validator.MatchValidator', '/^([a-zA-Z])+$/', 'ISO code must consist of alphabetical characters');
		$this->addValidator('COUNTRYNAME', 'minLength', 'propel.validator.MinLengthValidator', '1', 'country name must be at least 1 character');
		$this->addValidator('COUNTRYNAME', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'country name must be at most 60 characters');
		$this->addValidator('COUNTRYNAME', 'required', 'propel.validator.RequiredValidator', '', 'country name is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Address', 'Address', RelationMap::ONE_TO_MANY, array('countryid' => 'countryid', ), 'CASCADE', 'CASCADE', 'Addresss');
	} // buildRelations()

} // CountryTableMap
