<?
$config = array(
	'system' => 'tom',
	'system_long' => 'Track-o-Matic',
	'version' => 1,
	'default_language' => 'english',
	'encryption_key' => 'vbRexhC48m0S26sMDlyVhr',
	'session_time_limit' => 15,
	'password_salt' => 'F0WSYum7nFcVeOs04xOfdK',
	'items_per_page' => 20,
	'page_links_shown' => 9,
	'email_sender' => array('tom@absx.org' => 'Trackomatic'),
);