<?php

class NotificationSender {
	
	public function sendCreationNotifications() {
		$notifications = NotificationQuery::create()->findCreationUnsent();
		foreach ($notifications as $notification) {
			echo "Sending creation notification to " . $notification->getDeliveryAddress() . ".\n";
			$notification->sendCreation();
		}
		
	}
	
	public function sendProximityNotifications() {
		$notifications = NotificationQuery::create()->findSendable();
		foreach ($notifications as $notification) {
			echo "Sending notification to " . $notification->getDeliveryAddress() . ".\n";
			$notification->send();
		}
	}
	
} 