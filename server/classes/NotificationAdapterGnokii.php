<?php

class NotificationAdapterGnokii implements NotificationAdapterInterface {
	
	const GNOKII_EXECUTABLE = '/usr/bin/gnokii';
	const GNOKII_CONFIG = '/etc/gnokii/config';
	
	public function sendNotification($receiver, $message, $subject = NULL,  $mime = 'text/plain') {
		$message = mb_substr($message, 0, 160);
		
		// Trim characters other than numbers and leading plus symbol
		$receiver = preg_replace("/(?<!^)[\D]|(?<=^)[^\+\d]/", '', $receiver);
		
		// Accept numbers in format +999234567, +99234567 or 0901234567
		// Probably only works with Finnish numbers.
		if (!preg_match('/^((\+?[1-9][\d]{9,11})|(0[\d]{9}))$/', $receiver)) {
			throw new Exception(ucfirst(Localizer::getText('unrecognized telephone number format')));
		}
		
		// Make extra sure there are no malicious characters going on the command line
		$message = str_replace(array('"', '`', ';'), '', $message);
		
		$command = 'echo "' . $message . '" | '; 
		$command .= self::GNOKII_EXECUTABLE;
		$command .= ' --config ' . self::GNOKII_CONFIG;
		$command .= ' --sendsms ' . $receiver . ' -r | ';
		$command .= 'grep "Send succeeded"';
		
		exec($command, $output, $return);		
		if (1 !== $return) {
			throw new Exception(ucfirst(Localizer::getText('sending sms failed')));
		}
	}
	
	public function isHtml() {
		return false;
	}
}