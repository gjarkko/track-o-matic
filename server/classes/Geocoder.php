<?
//http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true_or_false

class Geocoder {
	public static $instance;
	private $curl;
	
	const BASEURL = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';
	
	private function __construct() {
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
	}
	
	public function __destruct() {
		curl_close($this->curl);
	}
	
	public static function getInstance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function geocodeAddress($address) {
		if (is_array($address)) {
			$address = array_filter($address);
			$address = implode(', ', $address);
		}
		
		$url = self::BASEURL . urlencode($address);
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$response = curl_exec($this->curl);
		$status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if ($status == 200) {
			$geocode = json_decode($response);
			if (isset($geocode->results[0]->geometry->location)) {
				$lat = $geocode->results[0]->geometry->location->lat;
				$lng = $geocode->results[0]->geometry->location->lng;
				return array('lat' => $lat, 'lng' => $lng);
			}
			else {
				throw new Exception('Google Maps API could not find geocode for the address ' . $address);
			}
		}
		else {
			throw new Exception('Google Maps API returned unexpected status ' . $status);
		}
	}
}