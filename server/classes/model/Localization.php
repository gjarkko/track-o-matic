<?php



/**
 * Skeleton subclass for representing a row from the 'localization' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Localization extends BaseLocalization {

	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
		
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language')),
				'foreign' => true,
			),
			array(
				'column' => 'localizationkey',
				'label' => ucfirst(Localizer::getText('key')),
			),
			array(
				'column' => 'localizationvalue',
				'label' => ucfirst(Localizer::getText('value')),
			),
		);
	}
		
		
	public static function getCrudMap() {
		return array(
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'localizationkey',
				'label' => ucfirst(Localizer::getText('key')),
				'required' => true,
				'inputType' => 'select',
				'default' => '',
				'optionsMethod' => 'getLocalizationKeys',
			),
			array(
				'column' => 'localizationvalue',
				'label' => ucfirst(Localizer::getText('value')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
	
	public function getLocalizationKeys() {
		$current = $this->getLocalizationkey();
		$phrases = array($current => $current . ' (0)');
		$systemPhrases = Tools::getPhrases(true, true);
		foreach($systemPhrases as $key => $phrase) {
			$phrases[$key] = $phrase;
		}
		asort($phrases);
		return $phrases; 
	}
} // Localization
