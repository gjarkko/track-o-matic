<?php


/**
 * Base class that represents a row from the 'customer' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseCustomer extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'CustomerPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        CustomerPeer
	 */
	protected static $peer;

	/**
	 * The value for the customerid field.
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the customer field.
	 * @var        string
	 */
	protected $customer;

	/**
	 * The value for the valid field.
	 * Note: this column has a database default value of: true
	 * @var        boolean
	 */
	protected $valid;

	/**
	 * @var        array Address[] Collection to store aggregation of Address objects.
	 */
	protected $collAddresss;

	/**
	 * @var        array Detailtype[] Collection to store aggregation of Detailtype objects.
	 */
	protected $collDetailtypes;

	/**
	 * @var        array Groupreference[] Collection to store aggregation of Groupreference objects.
	 */
	protected $collGroupreferences;

	/**
	 * @var        array Logentry[] Collection to store aggregation of Logentry objects.
	 */
	protected $collLogentrys;

	/**
	 * @var        array Notification[] Collection to store aggregation of Notification objects.
	 */
	protected $collNotifications;

	/**
	 * @var        array Statustype[] Collection to store aggregation of Statustype objects.
	 */
	protected $collStatustypes;

	/**
	 * @var        array Trackable[] Collection to store aggregation of Trackable objects.
	 */
	protected $collTrackables;

	/**
	 * @var        array Trip[] Collection to store aggregation of Trip objects.
	 */
	protected $collTrips;

	/**
	 * @var        array User[] Collection to store aggregation of User objects.
	 */
	protected $collUsers;

	/**
	 * @var        array Vehicle[] Collection to store aggregation of Vehicle objects.
	 */
	protected $collVehicles;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $addresssScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $detailtypesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $groupreferencesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $logentrysScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $notificationsScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $statustypesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $trackablesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $tripsScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $usersScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $vehiclesScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->valid = true;
	}

	/**
	 * Initializes internal state of BaseCustomer object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [customer] column value.
	 * 
	 * @return     string
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * Get the [valid] column value.
	 * 
	 * @return     boolean
	 */
	public function getValid()
	{
		return $this->valid;
	}

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Customer The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = CustomerPeer::CUSTOMERID;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [customer] column.
	 * 
	 * @param      string $v new value
	 * @return     Customer The current object (for fluent API support)
	 */
	public function setCustomer($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->customer !== $v) {
			$this->customer = $v;
			$this->modifiedColumns[] = CustomerPeer::CUSTOMER;
		}

		return $this;
	} // setCustomer()

	/**
	 * Sets the value of the [valid] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     Customer The current object (for fluent API support)
	 */
	public function setValid($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->valid !== $v) {
			$this->valid = $v;
			$this->modifiedColumns[] = CustomerPeer::VALID;
		}

		return $this;
	} // setValid()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->valid !== true) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->customerid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->customer = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->valid = ($row[$startcol + 2] !== null) ? (boolean) $row[$startcol + 2] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 3; // 3 = CustomerPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Customer object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CustomerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = CustomerPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->collAddresss = null;

			$this->collDetailtypes = null;

			$this->collGroupreferences = null;

			$this->collLogentrys = null;

			$this->collNotifications = null;

			$this->collStatustypes = null;

			$this->collTrackables = null;

			$this->collTrips = null;

			$this->collUsers = null;

			$this->collVehicles = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CustomerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = CustomerQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CustomerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				CustomerPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->addresssScheduledForDeletion !== null) {
				if (!$this->addresssScheduledForDeletion->isEmpty()) {
					AddressQuery::create()
						->filterByPrimaryKeys($this->addresssScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->addresssScheduledForDeletion = null;
				}
			}

			if ($this->collAddresss !== null) {
				foreach ($this->collAddresss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->detailtypesScheduledForDeletion !== null) {
				if (!$this->detailtypesScheduledForDeletion->isEmpty()) {
					DetailtypeQuery::create()
						->filterByPrimaryKeys($this->detailtypesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->detailtypesScheduledForDeletion = null;
				}
			}

			if ($this->collDetailtypes !== null) {
				foreach ($this->collDetailtypes as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->groupreferencesScheduledForDeletion !== null) {
				if (!$this->groupreferencesScheduledForDeletion->isEmpty()) {
					GroupreferenceQuery::create()
						->filterByPrimaryKeys($this->groupreferencesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->groupreferencesScheduledForDeletion = null;
				}
			}

			if ($this->collGroupreferences !== null) {
				foreach ($this->collGroupreferences as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->logentrysScheduledForDeletion !== null) {
				if (!$this->logentrysScheduledForDeletion->isEmpty()) {
					LogentryQuery::create()
						->filterByPrimaryKeys($this->logentrysScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->logentrysScheduledForDeletion = null;
				}
			}

			if ($this->collLogentrys !== null) {
				foreach ($this->collLogentrys as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->notificationsScheduledForDeletion !== null) {
				if (!$this->notificationsScheduledForDeletion->isEmpty()) {
					NotificationQuery::create()
						->filterByPrimaryKeys($this->notificationsScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->notificationsScheduledForDeletion = null;
				}
			}

			if ($this->collNotifications !== null) {
				foreach ($this->collNotifications as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->statustypesScheduledForDeletion !== null) {
				if (!$this->statustypesScheduledForDeletion->isEmpty()) {
					StatustypeQuery::create()
						->filterByPrimaryKeys($this->statustypesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->statustypesScheduledForDeletion = null;
				}
			}

			if ($this->collStatustypes !== null) {
				foreach ($this->collStatustypes as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->trackablesScheduledForDeletion !== null) {
				if (!$this->trackablesScheduledForDeletion->isEmpty()) {
					TrackableQuery::create()
						->filterByPrimaryKeys($this->trackablesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->trackablesScheduledForDeletion = null;
				}
			}

			if ($this->collTrackables !== null) {
				foreach ($this->collTrackables as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->tripsScheduledForDeletion !== null) {
				if (!$this->tripsScheduledForDeletion->isEmpty()) {
					TripQuery::create()
						->filterByPrimaryKeys($this->tripsScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->tripsScheduledForDeletion = null;
				}
			}

			if ($this->collTrips !== null) {
				foreach ($this->collTrips as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->usersScheduledForDeletion !== null) {
				if (!$this->usersScheduledForDeletion->isEmpty()) {
					UserQuery::create()
						->filterByPrimaryKeys($this->usersScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->usersScheduledForDeletion = null;
				}
			}

			if ($this->collUsers !== null) {
				foreach ($this->collUsers as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->vehiclesScheduledForDeletion !== null) {
				if (!$this->vehiclesScheduledForDeletion->isEmpty()) {
					VehicleQuery::create()
						->filterByPrimaryKeys($this->vehiclesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->vehiclesScheduledForDeletion = null;
				}
			}

			if ($this->collVehicles !== null) {
				foreach ($this->collVehicles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = CustomerPeer::CUSTOMERID;
		if (null !== $this->customerid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . CustomerPeer::CUSTOMERID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(CustomerPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(CustomerPeer::CUSTOMER)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMER`';
		}
		if ($this->isColumnModified(CustomerPeer::VALID)) {
			$modifiedColumns[':p' . $index++]  = '`VALID`';
		}

		$sql = sprintf(
			'INSERT INTO `customer` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`CUSTOMER`':
						$stmt->bindValue($identifier, $this->customer, PDO::PARAM_STR);
						break;
					case '`VALID`':
						$stmt->bindValue($identifier, (int) $this->valid, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setCustomerid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = CustomerPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collAddresss !== null) {
					foreach ($this->collAddresss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collDetailtypes !== null) {
					foreach ($this->collDetailtypes as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collGroupreferences !== null) {
					foreach ($this->collGroupreferences as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collLogentrys !== null) {
					foreach ($this->collLogentrys as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collNotifications !== null) {
					foreach ($this->collNotifications as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collStatustypes !== null) {
					foreach ($this->collStatustypes as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTrackables !== null) {
					foreach ($this->collTrackables as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTrips !== null) {
					foreach ($this->collTrips as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collUsers !== null) {
					foreach ($this->collUsers as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collVehicles !== null) {
					foreach ($this->collVehicles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CustomerPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCustomerid();
				break;
			case 1:
				return $this->getCustomer();
				break;
			case 2:
				return $this->getValid();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Customer'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Customer'][$this->getPrimaryKey()] = true;
		$keys = CustomerPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCustomerid(),
			$keys[1] => $this->getCustomer(),
			$keys[2] => $this->getValid(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->collAddresss) {
				$result['Addresss'] = $this->collAddresss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collDetailtypes) {
				$result['Detailtypes'] = $this->collDetailtypes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collGroupreferences) {
				$result['Groupreferences'] = $this->collGroupreferences->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collLogentrys) {
				$result['Logentrys'] = $this->collLogentrys->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collNotifications) {
				$result['Notifications'] = $this->collNotifications->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collStatustypes) {
				$result['Statustypes'] = $this->collStatustypes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTrackables) {
				$result['Trackables'] = $this->collTrackables->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTrips) {
				$result['Trips'] = $this->collTrips->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collUsers) {
				$result['Users'] = $this->collUsers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collVehicles) {
				$result['Vehicles'] = $this->collVehicles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CustomerPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCustomerid($value);
				break;
			case 1:
				$this->setCustomer($value);
				break;
			case 2:
				$this->setValid($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CustomerPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCustomerid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCustomer($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setValid($arr[$keys[2]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(CustomerPeer::DATABASE_NAME);

		if ($this->isColumnModified(CustomerPeer::CUSTOMERID)) $criteria->add(CustomerPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(CustomerPeer::CUSTOMER)) $criteria->add(CustomerPeer::CUSTOMER, $this->customer);
		if ($this->isColumnModified(CustomerPeer::VALID)) $criteria->add(CustomerPeer::VALID, $this->valid);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CustomerPeer::DATABASE_NAME);
		$criteria->add(CustomerPeer::CUSTOMERID, $this->customerid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getCustomerid();
	}

	/**
	 * Generic method to set the primary key (customerid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setCustomerid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getCustomerid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Customer (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCustomer($this->getCustomer());
		$copyObj->setValid($this->getValid());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getAddresss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addAddress($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getDetailtypes() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addDetailtype($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getGroupreferences() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addGroupreference($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getLogentrys() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addLogentry($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getNotifications() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addNotification($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getStatustypes() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addStatustype($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTrackables() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTrackable($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTrips() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTrip($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getUsers() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addUser($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getVehicles() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addVehicle($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setCustomerid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Customer Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     CustomerPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CustomerPeer();
		}
		return self::$peer;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('Address' == $relationName) {
			return $this->initAddresss();
		}
		if ('Detailtype' == $relationName) {
			return $this->initDetailtypes();
		}
		if ('Groupreference' == $relationName) {
			return $this->initGroupreferences();
		}
		if ('Logentry' == $relationName) {
			return $this->initLogentrys();
		}
		if ('Notification' == $relationName) {
			return $this->initNotifications();
		}
		if ('Statustype' == $relationName) {
			return $this->initStatustypes();
		}
		if ('Trackable' == $relationName) {
			return $this->initTrackables();
		}
		if ('Trip' == $relationName) {
			return $this->initTrips();
		}
		if ('User' == $relationName) {
			return $this->initUsers();
		}
		if ('Vehicle' == $relationName) {
			return $this->initVehicles();
		}
	}

	/**
	 * Clears out the collAddresss collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addAddresss()
	 */
	public function clearAddresss()
	{
		$this->collAddresss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collAddresss collection.
	 *
	 * By default this just sets the collAddresss collection to an empty array (like clearcollAddresss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initAddresss($overrideExisting = true)
	{
		if (null !== $this->collAddresss && !$overrideExisting) {
			return;
		}
		$this->collAddresss = new PropelObjectCollection();
		$this->collAddresss->setModel('Address');
	}

	/**
	 * Gets an array of Address objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Address[] List of Address objects
	 * @throws     PropelException
	 */
	public function getAddresss($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collAddresss || null !== $criteria) {
			if ($this->isNew() && null === $this->collAddresss) {
				// return empty collection
				$this->initAddresss();
			} else {
				$collAddresss = AddressQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collAddresss;
				}
				$this->collAddresss = $collAddresss;
			}
		}
		return $this->collAddresss;
	}

	/**
	 * Sets a collection of Address objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $addresss A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setAddresss(PropelCollection $addresss, PropelPDO $con = null)
	{
		$this->addresssScheduledForDeletion = $this->getAddresss(new Criteria(), $con)->diff($addresss);

		foreach ($addresss as $address) {
			// Fix issue with collection modified by reference
			if ($address->isNew()) {
				$address->setCustomer($this);
			}
			$this->addAddress($address);
		}

		$this->collAddresss = $addresss;
	}

	/**
	 * Returns the number of related Address objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Address objects.
	 * @throws     PropelException
	 */
	public function countAddresss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collAddresss || null !== $criteria) {
			if ($this->isNew() && null === $this->collAddresss) {
				return 0;
			} else {
				$query = AddressQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collAddresss);
		}
	}

	/**
	 * Method called to associate a Address object to this object
	 * through the Address foreign key attribute.
	 *
	 * @param      Address $l Address
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addAddress(Address $l)
	{
		if ($this->collAddresss === null) {
			$this->initAddresss();
		}
		if (!$this->collAddresss->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddAddress($l);
		}

		return $this;
	}

	/**
	 * @param	Address $address The address object to add.
	 */
	protected function doAddAddress($address)
	{
		$this->collAddresss[]= $address;
		$address->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Addresss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Address[] List of Address objects
	 */
	public function getAddresssJoinCountry($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = AddressQuery::create(null, $criteria);
		$query->joinWith('Country', $join_behavior);

		return $this->getAddresss($query, $con);
	}

	/**
	 * Clears out the collDetailtypes collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addDetailtypes()
	 */
	public function clearDetailtypes()
	{
		$this->collDetailtypes = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collDetailtypes collection.
	 *
	 * By default this just sets the collDetailtypes collection to an empty array (like clearcollDetailtypes());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initDetailtypes($overrideExisting = true)
	{
		if (null !== $this->collDetailtypes && !$overrideExisting) {
			return;
		}
		$this->collDetailtypes = new PropelObjectCollection();
		$this->collDetailtypes->setModel('Detailtype');
	}

	/**
	 * Gets an array of Detailtype objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Detailtype[] List of Detailtype objects
	 * @throws     PropelException
	 */
	public function getDetailtypes($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collDetailtypes || null !== $criteria) {
			if ($this->isNew() && null === $this->collDetailtypes) {
				// return empty collection
				$this->initDetailtypes();
			} else {
				$collDetailtypes = DetailtypeQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collDetailtypes;
				}
				$this->collDetailtypes = $collDetailtypes;
			}
		}
		return $this->collDetailtypes;
	}

	/**
	 * Sets a collection of Detailtype objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $detailtypes A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setDetailtypes(PropelCollection $detailtypes, PropelPDO $con = null)
	{
		$this->detailtypesScheduledForDeletion = $this->getDetailtypes(new Criteria(), $con)->diff($detailtypes);

		foreach ($detailtypes as $detailtype) {
			// Fix issue with collection modified by reference
			if ($detailtype->isNew()) {
				$detailtype->setCustomer($this);
			}
			$this->addDetailtype($detailtype);
		}

		$this->collDetailtypes = $detailtypes;
	}

	/**
	 * Returns the number of related Detailtype objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Detailtype objects.
	 * @throws     PropelException
	 */
	public function countDetailtypes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collDetailtypes || null !== $criteria) {
			if ($this->isNew() && null === $this->collDetailtypes) {
				return 0;
			} else {
				$query = DetailtypeQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collDetailtypes);
		}
	}

	/**
	 * Method called to associate a Detailtype object to this object
	 * through the Detailtype foreign key attribute.
	 *
	 * @param      Detailtype $l Detailtype
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addDetailtype(Detailtype $l)
	{
		if ($this->collDetailtypes === null) {
			$this->initDetailtypes();
		}
		if (!$this->collDetailtypes->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddDetailtype($l);
		}

		return $this;
	}

	/**
	 * @param	Detailtype $detailtype The detailtype object to add.
	 */
	protected function doAddDetailtype($detailtype)
	{
		$this->collDetailtypes[]= $detailtype;
		$detailtype->setCustomer($this);
	}

	/**
	 * Clears out the collGroupreferences collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addGroupreferences()
	 */
	public function clearGroupreferences()
	{
		$this->collGroupreferences = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collGroupreferences collection.
	 *
	 * By default this just sets the collGroupreferences collection to an empty array (like clearcollGroupreferences());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initGroupreferences($overrideExisting = true)
	{
		if (null !== $this->collGroupreferences && !$overrideExisting) {
			return;
		}
		$this->collGroupreferences = new PropelObjectCollection();
		$this->collGroupreferences->setModel('Groupreference');
	}

	/**
	 * Gets an array of Groupreference objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Groupreference[] List of Groupreference objects
	 * @throws     PropelException
	 */
	public function getGroupreferences($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collGroupreferences || null !== $criteria) {
			if ($this->isNew() && null === $this->collGroupreferences) {
				// return empty collection
				$this->initGroupreferences();
			} else {
				$collGroupreferences = GroupreferenceQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collGroupreferences;
				}
				$this->collGroupreferences = $collGroupreferences;
			}
		}
		return $this->collGroupreferences;
	}

	/**
	 * Sets a collection of Groupreference objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $groupreferences A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setGroupreferences(PropelCollection $groupreferences, PropelPDO $con = null)
	{
		$this->groupreferencesScheduledForDeletion = $this->getGroupreferences(new Criteria(), $con)->diff($groupreferences);

		foreach ($groupreferences as $groupreference) {
			// Fix issue with collection modified by reference
			if ($groupreference->isNew()) {
				$groupreference->setCustomer($this);
			}
			$this->addGroupreference($groupreference);
		}

		$this->collGroupreferences = $groupreferences;
	}

	/**
	 * Returns the number of related Groupreference objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Groupreference objects.
	 * @throws     PropelException
	 */
	public function countGroupreferences(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collGroupreferences || null !== $criteria) {
			if ($this->isNew() && null === $this->collGroupreferences) {
				return 0;
			} else {
				$query = GroupreferenceQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collGroupreferences);
		}
	}

	/**
	 * Method called to associate a Groupreference object to this object
	 * through the Groupreference foreign key attribute.
	 *
	 * @param      Groupreference $l Groupreference
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addGroupreference(Groupreference $l)
	{
		if ($this->collGroupreferences === null) {
			$this->initGroupreferences();
		}
		if (!$this->collGroupreferences->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddGroupreference($l);
		}

		return $this;
	}

	/**
	 * @param	Groupreference $groupreference The groupreference object to add.
	 */
	protected function doAddGroupreference($groupreference)
	{
		$this->collGroupreferences[]= $groupreference;
		$groupreference->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Groupreferences from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Groupreference[] List of Groupreference objects
	 */
	public function getGroupreferencesJoinTrackable($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = GroupreferenceQuery::create(null, $criteria);
		$query->joinWith('Trackable', $join_behavior);

		return $this->getGroupreferences($query, $con);
	}

	/**
	 * Clears out the collLogentrys collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addLogentrys()
	 */
	public function clearLogentrys()
	{
		$this->collLogentrys = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collLogentrys collection.
	 *
	 * By default this just sets the collLogentrys collection to an empty array (like clearcollLogentrys());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initLogentrys($overrideExisting = true)
	{
		if (null !== $this->collLogentrys && !$overrideExisting) {
			return;
		}
		$this->collLogentrys = new PropelObjectCollection();
		$this->collLogentrys->setModel('Logentry');
	}

	/**
	 * Gets an array of Logentry objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 * @throws     PropelException
	 */
	public function getLogentrys($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collLogentrys || null !== $criteria) {
			if ($this->isNew() && null === $this->collLogentrys) {
				// return empty collection
				$this->initLogentrys();
			} else {
				$collLogentrys = LogentryQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collLogentrys;
				}
				$this->collLogentrys = $collLogentrys;
			}
		}
		return $this->collLogentrys;
	}

	/**
	 * Sets a collection of Logentry objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $logentrys A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setLogentrys(PropelCollection $logentrys, PropelPDO $con = null)
	{
		$this->logentrysScheduledForDeletion = $this->getLogentrys(new Criteria(), $con)->diff($logentrys);

		foreach ($logentrys as $logentry) {
			// Fix issue with collection modified by reference
			if ($logentry->isNew()) {
				$logentry->setCustomer($this);
			}
			$this->addLogentry($logentry);
		}

		$this->collLogentrys = $logentrys;
	}

	/**
	 * Returns the number of related Logentry objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Logentry objects.
	 * @throws     PropelException
	 */
	public function countLogentrys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collLogentrys || null !== $criteria) {
			if ($this->isNew() && null === $this->collLogentrys) {
				return 0;
			} else {
				$query = LogentryQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collLogentrys);
		}
	}

	/**
	 * Method called to associate a Logentry object to this object
	 * through the Logentry foreign key attribute.
	 *
	 * @param      Logentry $l Logentry
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addLogentry(Logentry $l)
	{
		if ($this->collLogentrys === null) {
			$this->initLogentrys();
		}
		if (!$this->collLogentrys->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddLogentry($l);
		}

		return $this;
	}

	/**
	 * @param	Logentry $logentry The logentry object to add.
	 */
	protected function doAddLogentry($logentry)
	{
		$this->collLogentrys[]= $logentry;
		$logentry->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Logentrys from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 */
	public function getLogentrysJoinLogentrytype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = LogentryQuery::create(null, $criteria);
		$query->joinWith('Logentrytype', $join_behavior);

		return $this->getLogentrys($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Logentrys from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 */
	public function getLogentrysJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = LogentryQuery::create(null, $criteria);
		$query->joinWith('User', $join_behavior);

		return $this->getLogentrys($query, $con);
	}

	/**
	 * Clears out the collNotifications collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addNotifications()
	 */
	public function clearNotifications()
	{
		$this->collNotifications = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collNotifications collection.
	 *
	 * By default this just sets the collNotifications collection to an empty array (like clearcollNotifications());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initNotifications($overrideExisting = true)
	{
		if (null !== $this->collNotifications && !$overrideExisting) {
			return;
		}
		$this->collNotifications = new PropelObjectCollection();
		$this->collNotifications->setModel('Notification');
	}

	/**
	 * Gets an array of Notification objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 * @throws     PropelException
	 */
	public function getNotifications($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collNotifications || null !== $criteria) {
			if ($this->isNew() && null === $this->collNotifications) {
				// return empty collection
				$this->initNotifications();
			} else {
				$collNotifications = NotificationQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collNotifications;
				}
				$this->collNotifications = $collNotifications;
			}
		}
		return $this->collNotifications;
	}

	/**
	 * Sets a collection of Notification objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $notifications A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setNotifications(PropelCollection $notifications, PropelPDO $con = null)
	{
		$this->notificationsScheduledForDeletion = $this->getNotifications(new Criteria(), $con)->diff($notifications);

		foreach ($notifications as $notification) {
			// Fix issue with collection modified by reference
			if ($notification->isNew()) {
				$notification->setCustomer($this);
			}
			$this->addNotification($notification);
		}

		$this->collNotifications = $notifications;
	}

	/**
	 * Returns the number of related Notification objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Notification objects.
	 * @throws     PropelException
	 */
	public function countNotifications(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collNotifications || null !== $criteria) {
			if ($this->isNew() && null === $this->collNotifications) {
				return 0;
			} else {
				$query = NotificationQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collNotifications);
		}
	}

	/**
	 * Method called to associate a Notification object to this object
	 * through the Notification foreign key attribute.
	 *
	 * @param      Notification $l Notification
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addNotification(Notification $l)
	{
		if ($this->collNotifications === null) {
			$this->initNotifications();
		}
		if (!$this->collNotifications->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddNotification($l);
		}

		return $this;
	}

	/**
	 * @param	Notification $notification The notification object to add.
	 */
	protected function doAddNotification($notification)
	{
		$this->collNotifications[]= $notification;
		$notification->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Notifications from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 */
	public function getNotificationsJoinNotificationtype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = NotificationQuery::create(null, $criteria);
		$query->joinWith('Notificationtype', $join_behavior);

		return $this->getNotifications($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Notifications from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Notification[] List of Notification objects
	 */
	public function getNotificationsJoinTrackable($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = NotificationQuery::create(null, $criteria);
		$query->joinWith('Trackable', $join_behavior);

		return $this->getNotifications($query, $con);
	}

	/**
	 * Clears out the collStatustypes collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addStatustypes()
	 */
	public function clearStatustypes()
	{
		$this->collStatustypes = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collStatustypes collection.
	 *
	 * By default this just sets the collStatustypes collection to an empty array (like clearcollStatustypes());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initStatustypes($overrideExisting = true)
	{
		if (null !== $this->collStatustypes && !$overrideExisting) {
			return;
		}
		$this->collStatustypes = new PropelObjectCollection();
		$this->collStatustypes->setModel('Statustype');
	}

	/**
	 * Gets an array of Statustype objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Statustype[] List of Statustype objects
	 * @throws     PropelException
	 */
	public function getStatustypes($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collStatustypes || null !== $criteria) {
			if ($this->isNew() && null === $this->collStatustypes) {
				// return empty collection
				$this->initStatustypes();
			} else {
				$collStatustypes = StatustypeQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collStatustypes;
				}
				$this->collStatustypes = $collStatustypes;
			}
		}
		return $this->collStatustypes;
	}

	/**
	 * Sets a collection of Statustype objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $statustypes A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setStatustypes(PropelCollection $statustypes, PropelPDO $con = null)
	{
		$this->statustypesScheduledForDeletion = $this->getStatustypes(new Criteria(), $con)->diff($statustypes);

		foreach ($statustypes as $statustype) {
			// Fix issue with collection modified by reference
			if ($statustype->isNew()) {
				$statustype->setCustomer($this);
			}
			$this->addStatustype($statustype);
		}

		$this->collStatustypes = $statustypes;
	}

	/**
	 * Returns the number of related Statustype objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Statustype objects.
	 * @throws     PropelException
	 */
	public function countStatustypes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collStatustypes || null !== $criteria) {
			if ($this->isNew() && null === $this->collStatustypes) {
				return 0;
			} else {
				$query = StatustypeQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collStatustypes);
		}
	}

	/**
	 * Method called to associate a Statustype object to this object
	 * through the Statustype foreign key attribute.
	 *
	 * @param      Statustype $l Statustype
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addStatustype(Statustype $l)
	{
		if ($this->collStatustypes === null) {
			$this->initStatustypes();
		}
		if (!$this->collStatustypes->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddStatustype($l);
		}

		return $this;
	}

	/**
	 * @param	Statustype $statustype The statustype object to add.
	 */
	protected function doAddStatustype($statustype)
	{
		$this->collStatustypes[]= $statustype;
		$statustype->setCustomer($this);
	}

	/**
	 * Clears out the collTrackables collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTrackables()
	 */
	public function clearTrackables()
	{
		$this->collTrackables = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTrackables collection.
	 *
	 * By default this just sets the collTrackables collection to an empty array (like clearcollTrackables());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTrackables($overrideExisting = true)
	{
		if (null !== $this->collTrackables && !$overrideExisting) {
			return;
		}
		$this->collTrackables = new PropelObjectCollection();
		$this->collTrackables->setModel('Trackable');
	}

	/**
	 * Gets an array of Trackable objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 * @throws     PropelException
	 */
	public function getTrackables($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackables) {
				// return empty collection
				$this->initTrackables();
			} else {
				$collTrackables = TrackableQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collTrackables;
				}
				$this->collTrackables = $collTrackables;
			}
		}
		return $this->collTrackables;
	}

	/**
	 * Sets a collection of Trackable objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $trackables A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTrackables(PropelCollection $trackables, PropelPDO $con = null)
	{
		$this->trackablesScheduledForDeletion = $this->getTrackables(new Criteria(), $con)->diff($trackables);

		foreach ($trackables as $trackable) {
			// Fix issue with collection modified by reference
			if ($trackable->isNew()) {
				$trackable->setCustomer($this);
			}
			$this->addTrackable($trackable);
		}

		$this->collTrackables = $trackables;
	}

	/**
	 * Returns the number of related Trackable objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Trackable objects.
	 * @throws     PropelException
	 */
	public function countTrackables(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackables) {
				return 0;
			} else {
				$query = TrackableQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collTrackables);
		}
	}

	/**
	 * Method called to associate a Trackable object to this object
	 * through the Trackable foreign key attribute.
	 *
	 * @param      Trackable $l Trackable
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addTrackable(Trackable $l)
	{
		if ($this->collTrackables === null) {
			$this->initTrackables();
		}
		if (!$this->collTrackables->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTrackable($l);
		}

		return $this;
	}

	/**
	 * @param	Trackable $trackable The trackable object to add.
	 */
	protected function doAddTrackable($trackable)
	{
		$this->collTrackables[]= $trackable;
		$trackable->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Trackables from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 */
	public function getTrackablesJoinAddressRelatedByStartAddressid($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TrackableQuery::create(null, $criteria);
		$query->joinWith('AddressRelatedByStartAddressid', $join_behavior);

		return $this->getTrackables($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Trackables from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 */
	public function getTrackablesJoinAddressRelatedByEndAddressid($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TrackableQuery::create(null, $criteria);
		$query->joinWith('AddressRelatedByEndAddressid', $join_behavior);

		return $this->getTrackables($query, $con);
	}

	/**
	 * Clears out the collTrips collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTrips()
	 */
	public function clearTrips()
	{
		$this->collTrips = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTrips collection.
	 *
	 * By default this just sets the collTrips collection to an empty array (like clearcollTrips());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTrips($overrideExisting = true)
	{
		if (null !== $this->collTrips && !$overrideExisting) {
			return;
		}
		$this->collTrips = new PropelObjectCollection();
		$this->collTrips->setModel('Trip');
	}

	/**
	 * Gets an array of Trip objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Trip[] List of Trip objects
	 * @throws     PropelException
	 */
	public function getTrips($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTrips || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrips) {
				// return empty collection
				$this->initTrips();
			} else {
				$collTrips = TripQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collTrips;
				}
				$this->collTrips = $collTrips;
			}
		}
		return $this->collTrips;
	}

	/**
	 * Sets a collection of Trip objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $trips A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTrips(PropelCollection $trips, PropelPDO $con = null)
	{
		$this->tripsScheduledForDeletion = $this->getTrips(new Criteria(), $con)->diff($trips);

		foreach ($trips as $trip) {
			// Fix issue with collection modified by reference
			if ($trip->isNew()) {
				$trip->setCustomer($this);
			}
			$this->addTrip($trip);
		}

		$this->collTrips = $trips;
	}

	/**
	 * Returns the number of related Trip objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Trip objects.
	 * @throws     PropelException
	 */
	public function countTrips(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTrips || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrips) {
				return 0;
			} else {
				$query = TripQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collTrips);
		}
	}

	/**
	 * Method called to associate a Trip object to this object
	 * through the Trip foreign key attribute.
	 *
	 * @param      Trip $l Trip
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addTrip(Trip $l)
	{
		if ($this->collTrips === null) {
			$this->initTrips();
		}
		if (!$this->collTrips->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTrip($l);
		}

		return $this;
	}

	/**
	 * @param	Trip $trip The trip object to add.
	 */
	protected function doAddTrip($trip)
	{
		$this->collTrips[]= $trip;
		$trip->setCustomer($this);
	}

	/**
	 * Clears out the collUsers collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addUsers()
	 */
	public function clearUsers()
	{
		$this->collUsers = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collUsers collection.
	 *
	 * By default this just sets the collUsers collection to an empty array (like clearcollUsers());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initUsers($overrideExisting = true)
	{
		if (null !== $this->collUsers && !$overrideExisting) {
			return;
		}
		$this->collUsers = new PropelObjectCollection();
		$this->collUsers->setModel('User');
	}

	/**
	 * Gets an array of User objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array User[] List of User objects
	 * @throws     PropelException
	 */
	public function getUsers($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collUsers || null !== $criteria) {
			if ($this->isNew() && null === $this->collUsers) {
				// return empty collection
				$this->initUsers();
			} else {
				$collUsers = UserQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collUsers;
				}
				$this->collUsers = $collUsers;
			}
		}
		return $this->collUsers;
	}

	/**
	 * Sets a collection of User objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $users A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setUsers(PropelCollection $users, PropelPDO $con = null)
	{
		$this->usersScheduledForDeletion = $this->getUsers(new Criteria(), $con)->diff($users);

		foreach ($users as $user) {
			// Fix issue with collection modified by reference
			if ($user->isNew()) {
				$user->setCustomer($this);
			}
			$this->addUser($user);
		}

		$this->collUsers = $users;
	}

	/**
	 * Returns the number of related User objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related User objects.
	 * @throws     PropelException
	 */
	public function countUsers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collUsers || null !== $criteria) {
			if ($this->isNew() && null === $this->collUsers) {
				return 0;
			} else {
				$query = UserQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collUsers);
		}
	}

	/**
	 * Method called to associate a User object to this object
	 * through the User foreign key attribute.
	 *
	 * @param      User $l User
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addUser(User $l)
	{
		if ($this->collUsers === null) {
			$this->initUsers();
		}
		if (!$this->collUsers->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddUser($l);
		}

		return $this;
	}

	/**
	 * @param	User $user The user object to add.
	 */
	protected function doAddUser($user)
	{
		$this->collUsers[]= $user;
		$user->setCustomer($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Users from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array User[] List of User objects
	 */
	public function getUsersJoinUsertype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = UserQuery::create(null, $criteria);
		$query->joinWith('Usertype', $join_behavior);

		return $this->getUsers($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Customer is new, it will return
	 * an empty collection; or if this Customer has previously
	 * been saved, it will retrieve related Users from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Customer.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array User[] List of User objects
	 */
	public function getUsersJoinLanguage($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = UserQuery::create(null, $criteria);
		$query->joinWith('Language', $join_behavior);

		return $this->getUsers($query, $con);
	}

	/**
	 * Clears out the collVehicles collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addVehicles()
	 */
	public function clearVehicles()
	{
		$this->collVehicles = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collVehicles collection.
	 *
	 * By default this just sets the collVehicles collection to an empty array (like clearcollVehicles());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initVehicles($overrideExisting = true)
	{
		if (null !== $this->collVehicles && !$overrideExisting) {
			return;
		}
		$this->collVehicles = new PropelObjectCollection();
		$this->collVehicles->setModel('Vehicle');
	}

	/**
	 * Gets an array of Vehicle objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Customer is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Vehicle[] List of Vehicle objects
	 * @throws     PropelException
	 */
	public function getVehicles($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collVehicles || null !== $criteria) {
			if ($this->isNew() && null === $this->collVehicles) {
				// return empty collection
				$this->initVehicles();
			} else {
				$collVehicles = VehicleQuery::create(null, $criteria)
					->filterByCustomer($this)
					->find($con);
				if (null !== $criteria) {
					return $collVehicles;
				}
				$this->collVehicles = $collVehicles;
			}
		}
		return $this->collVehicles;
	}

	/**
	 * Sets a collection of Vehicle objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $vehicles A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setVehicles(PropelCollection $vehicles, PropelPDO $con = null)
	{
		$this->vehiclesScheduledForDeletion = $this->getVehicles(new Criteria(), $con)->diff($vehicles);

		foreach ($vehicles as $vehicle) {
			// Fix issue with collection modified by reference
			if ($vehicle->isNew()) {
				$vehicle->setCustomer($this);
			}
			$this->addVehicle($vehicle);
		}

		$this->collVehicles = $vehicles;
	}

	/**
	 * Returns the number of related Vehicle objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Vehicle objects.
	 * @throws     PropelException
	 */
	public function countVehicles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collVehicles || null !== $criteria) {
			if ($this->isNew() && null === $this->collVehicles) {
				return 0;
			} else {
				$query = VehicleQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByCustomer($this)
					->count($con);
			}
		} else {
			return count($this->collVehicles);
		}
	}

	/**
	 * Method called to associate a Vehicle object to this object
	 * through the Vehicle foreign key attribute.
	 *
	 * @param      Vehicle $l Vehicle
	 * @return     Customer The current object (for fluent API support)
	 */
	public function addVehicle(Vehicle $l)
	{
		if ($this->collVehicles === null) {
			$this->initVehicles();
		}
		if (!$this->collVehicles->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddVehicle($l);
		}

		return $this;
	}

	/**
	 * @param	Vehicle $vehicle The vehicle object to add.
	 */
	protected function doAddVehicle($vehicle)
	{
		$this->collVehicles[]= $vehicle;
		$vehicle->setCustomer($this);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->customerid = null;
		$this->customer = null;
		$this->valid = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collAddresss) {
				foreach ($this->collAddresss as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collDetailtypes) {
				foreach ($this->collDetailtypes as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collGroupreferences) {
				foreach ($this->collGroupreferences as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collLogentrys) {
				foreach ($this->collLogentrys as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collNotifications) {
				foreach ($this->collNotifications as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collStatustypes) {
				foreach ($this->collStatustypes as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTrackables) {
				foreach ($this->collTrackables as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTrips) {
				foreach ($this->collTrips as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collUsers) {
				foreach ($this->collUsers as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collVehicles) {
				foreach ($this->collVehicles as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collAddresss instanceof PropelCollection) {
			$this->collAddresss->clearIterator();
		}
		$this->collAddresss = null;
		if ($this->collDetailtypes instanceof PropelCollection) {
			$this->collDetailtypes->clearIterator();
		}
		$this->collDetailtypes = null;
		if ($this->collGroupreferences instanceof PropelCollection) {
			$this->collGroupreferences->clearIterator();
		}
		$this->collGroupreferences = null;
		if ($this->collLogentrys instanceof PropelCollection) {
			$this->collLogentrys->clearIterator();
		}
		$this->collLogentrys = null;
		if ($this->collNotifications instanceof PropelCollection) {
			$this->collNotifications->clearIterator();
		}
		$this->collNotifications = null;
		if ($this->collStatustypes instanceof PropelCollection) {
			$this->collStatustypes->clearIterator();
		}
		$this->collStatustypes = null;
		if ($this->collTrackables instanceof PropelCollection) {
			$this->collTrackables->clearIterator();
		}
		$this->collTrackables = null;
		if ($this->collTrips instanceof PropelCollection) {
			$this->collTrips->clearIterator();
		}
		$this->collTrips = null;
		if ($this->collUsers instanceof PropelCollection) {
			$this->collUsers->clearIterator();
		}
		$this->collUsers = null;
		if ($this->collVehicles instanceof PropelCollection) {
			$this->collVehicles->clearIterator();
		}
		$this->collVehicles = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(CustomerPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseCustomer
