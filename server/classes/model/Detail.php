<?php



/**
 * Skeleton subclass for representing a row from the 'detail' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Detail extends BaseDetail {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public function getTrackableReference() {
		if (is_object($this->getTrackable())) {
			return $this->getTrackable()->getTrackablereference();
		}
		else {
			return NULL;
		}
	}
	
	public static function getBrowserColumnMap($user = NULL) {
		return array(
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'display_method' => 'getTrackableReference',
				'display_SQL' => 'trackablereference',			
				'join' => array(
					'relation' => 'Trackable',
					'table' => 'trackable',
					'own_key' => 'trackableid',
					'foreign_key' => 'trackableid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'detailtype',
				'label' => ucfirst(Localizer::getText('detail type')),
				'foreign' => true,
				'joinArgs' => array(
					//	array('valid', 1, Criteria::EQUAL)
				),
			),
			array(
				'column' => 'detail',
				'label' => ucfirst(Localizer::getText('value')),
			),
		);
	}
	
		
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'detailtype',
				'label' => ucfirst(Localizer::getText('detail type')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'getMethod' => 'getTrackableReference',
				'getIdMethod' => 'getTrackableid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'detail',
				'label' => ucfirst(Localizer::getText('value')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
	

} // Detail
