<?php



/**
 * Skeleton subclass for representing a row from the 'notification' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Notification extends BaseNotification {
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap($user = NULL) {
		return array(
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'display_method' => 'getTrackableReference',
				'display_SQL' => 'trackablereference',
				
				'join' => array(
					'relation' => 'Trackable',
					'table' => 'trackable',
					'own_key' => 'trackableid',
					'foreign_key' => 'trackableid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'notificationtype',
				'label' => ucfirst(Localizer::getText('notification type')),
				'foreign' => true,
			),
			array(
				'column' => 'deliveryaddress',
				'label' => ucfirst(Localizer::getText('email / mobile')),
			),
			array(
				'column' => 'proximity',
				'label' => ucfirst(Localizer::getText('proximity (m)')),
			),
		
			array(
				'column' => 'timecreated',
				'label' => ucfirst(Localizer::getText('created')),
			),
			array(
				'column' => 'timesent',
				'label' => ucfirst(Localizer::getText('sent')),
			),
		);
	}
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'getMethod' => 'getTrackableReference',
				'getIdMethod' => 'getTrackableid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'notificationtype',
				'label' => ucfirst(Localizer::getText('notification type')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'deliveryaddress',
				'label' => ucfirst(Localizer::getText('email address or mobile number')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'proximity',
				'label' => ucfirst(Localizer::getText('proximity (m)')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
	
	public function getTrackableReference() {
		if (is_object($this->getTrackable())) {
			return $this->getTrackable()->getTrackablereference();
		}
		else {
			return NULL;
		}
	}
	
	public function save(PropelPDO $con = null) {
	
		// If creating a new address, use the customer the user has been bound to.
		if (!$this->getPrimaryKey()) {
			if (Session::getInstance()->getUser()) {
				$this->setCustomerId(Session::getInstance()->getUser()->getCustomerid());
			}
		}
	
		parent::save($con);
	}
	
	protected function getAdapter() {
		if ($this->getNotificationtypeid() == 1) {
			return new NotificationAdapterGnokii();
		}
		elseif ($this->getNotificationtypeid() == 2) {
			return new NotificationAdapterSmtp();
		}
	}
	
	public function send($languageid = 1) {
		$message[] = sprintf(Localizer::getText("Your delivery %s, is now within %d m from destination.", $languageid), $this->getTrackable()->getTrackablereference(), $this->getProximity());
		$message[] = '';
		$message[] = Localizer::getText('Thanks for using Trackomatic!', $languageid);
		$subject = Localizer::getText('Your delivery is near', $languageid);
		
		// $sent could be used to add logging for unsuccessful sending attempts?
		$sent = $this->sendMessage($message, $subject);
		
		$this->setTimesent(date('Y-m-d H:i:s'));
		$this->setNotificationsent(true);
		$this->save();
	}
	
	public function sendCreation($languageid = 1) {
		$message[] = Localizer::getText('Greetings from Trackomatic,', $languageid);
		$message[] = '';
		$message[] = sprintf(Localizer::getText("You will receive a notification when your delivery %s, is within %d m from destination.", $languageid), $this->getTrackable()->getTrackablereference(), $this->getProximity());
		$message[] = '';
		$message[] = Localizer::getText('Stay tuned!', $languageid);
		$subject = Localizer::getText('Proximity notification registered', $languageid);
		
		// $sent could be used to add logging for unsuccessful sending attempts?
		$sent = $this->sendMessage($message, $subject);
		
		$this->setCreationsent(true);
		$this->save();
	}
	
	protected function sendMessage($message, $subject = NULL) {
		
		$adapter = $this->getAdapter();
		if ($adapter->isHtml()) {
			$message = implode("<br />\n", $message);
			$mime = 'text/html';
		}
		else {
			$message = implode("\n", $message);
			$mime = 'text/plain';
		}
				
		$sent = false;
		$tries = 0;
		
		while (!$sent && $tries < 3) {
			try {
				$adapter->sendNotification($this->getDeliveryaddress(), $message, $subject, $mime);
				$sent = true;
			} catch (Exception $e) {
				$tries++;
				sleep(3);
			}
		}
		
		return $sent;
	}

	
} // Notification
