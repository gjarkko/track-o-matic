START TRANSACTION;
REPLACE INTO customer VALUES (NULL, 'Test Company', 1);

-- APIKEY is 12345
REPLACE INTO user (userid, customerid, usertypeid, languageid, username, firstname, email, password, apikey, valid) VALUES (NULL,1,2,1,'admin','Administrator','admin.tom@absx.org', sha('F0WSYum7nFcVeOs04xOfdKadmin'), 'D7B6MbY8jLJGvi7igFcnaujhVrWA+uby7iI4fSrmO04=', 1);


-- APIKEY is 123
REPLACE INTO user (userid, customerid, usertypeid, languageid, username, firstname, email, password, apikey, valid) VALUES (NULL,2,3,1,'customer','Customer user','customer@customer.info', sha('F0WSYum7nFcVeOs04xOfdKcustomer'), '6K7EQ5zEh0GgjvCbv6u/QuLJ9eRflyK3IbSCAXFDBl8=', 1);

REPLACE INTO user (userid, customerid, usertypeid, languageid, username, firstname, email, password, apikey, valid) VALUES (NULL,2,4,1,'customer driver','Customer driver','driver@customer.info', sha('F0WSYum7nFcVeOs04xOfdKdriver'), NULL, 1);


-- APIKEY is 12346
REPLACE into vehicle values (null,2,'VEHICLE 1', 'yD/HjNQe+VXOC5+2jBuakv5se9ahe5mn+mxVuOVxvAI=', 1);

REPLACE INTO `address` VALUES (2,1,'Test sender','Yliopistonkatu 1',NULL,'20100','Turku',71, null, null)
,(3,1,'Test receiver','Puutarhakatu 22',NULL,'20100','Turku',71, null, null);

REPLACE INTO trackable VALUES (2,2,2,3,'T1',CURRENT_TIMESTAMP,NULL,1);
REPLACE INTO trip VALUES (2, 2, 'TRIP 1', 1);
REPLACE INTO triptrackable VALUES (1, 2, 2);
REPLACE INTO tripvehicle VALUES (2,2,1,CURRENT_TIMESTAMP, NULL);

REPLACE INTO statustype VALUES (2, 2, 'En route', 1, 1, 0, 1);
COMMIT;