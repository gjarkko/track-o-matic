<?php



/**
 * Skeleton subclass for representing a row from the 'trackable' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Trackable extends BaseTrackable {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public function getStartAddress() {
		return $this->getAddressRelatedByStartAddressid()->getAddress();
	}
	public function getEndAddress() {
		return $this->getAddressRelatedByEndAddressid()->getAddress();
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'trackablereference',
				'label' => ucfirst(Localizer::getText('reference')),
			),
			array(
				'column' => 'startAddress',
				'label' => ucfirst(Localizer::getText('from address')),
				'foreign' => true,
				'display_method' => 'getStartAddress',
				'display_SQL' => 'concat(AddressRelatedByStartAddressid.address1, ", ", AddressRelatedByStartAddressid.postalcode, AddressRelatedByStartAddressid.city)',
				'join' => array(
					'relation' => 'AddressRelatedByStartAddressid',
					'table' => 'address',
					'own_key' => 'start_addressid',
					'foreign_key' => 'addressid',
				),
			),
			array(
				'column' => 'toAddress',
				'label' => ucfirst(Localizer::getText('to address')),
				'foreign' => true,
				'display_method' => 'getEndAddress',
				'display_SQL' => 'concat(AddressRelatedByEndAddressid.address1, ", ", AddressRelatedByEndAddressid.postalcode, AddressRelatedByEndAddressid.city)',
				'join' => array(
					'relation' => 'AddressRelatedByEndAddressid',
					'table' => 'address',
					'own_key' => 'end_addressid',
					'foreign_key' => 'addressid',
				),
			),
			array(
				'column' => 'created',
				'label' => ucfirst(Localizer::getText('creation date')),
			),
		);
	}
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'trackablereference',
				'label' => ucfirst(Localizer::getText('Reference')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'startaddress',
				'foreignClass' => 'Address',
				'foreignGetMethod' => 'getAddress',
				'getMethod' => 'getStartAddress',
				'getIdMethod' => 'getStartaddressid',
				'label' => ucfirst(Localizer::getText('from address')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'endaddress',
				'foreignClass' => 'Address',
				'foreignGetMethod' => 'getAddress',
				'getMethod' => 'getEndAddress',
				'getIdMethod' => 'getEndaddressid',
				'label' => ucfirst(Localizer::getText('to address')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
		);
	}
	
	
	public function validate($columns = null)
	{
		$valid = parent::validate($columns);
		
		if ($valid) {
			$uniques = array(
				array(
					array(
						'column' => 'Customerid',
						'value' => $this->getCustomerid(),
					),
					array(
						'column' => 'Trackablereference',
						'value' => $this->getTrackablereference(),
					),
				),
			);
			
			$className = 'Trackable';
			$valid = Tools::uniqueValidator($className, $uniques, $this->validationFailures);
			
			if($this->end_addressid == $this->start_addressid &&
			  ($this->end_addressid != 1)) {
				$this->validationFailures[] = new ValidationFailed('start_addressid', Localizer::getText('Start and end address cannot be equal.'));
				$valid = false;
			}
					
		}
		return $valid;
		
	}
	
	public function getLastStatus() {
		$sql= "
			SELECT
				ts.*
			FROM
				trackable t
				JOIN trackablestatus ts ON
					t.trackableid = ts.trackableid
			WHERE
				t.trackableid = :trackableid 
				AND t.customerid = :customerid
			ORDER BY ts.time DESC
			LIMIT 1
		";
		
		$con = Propel::getConnection();
		$stmt = $con->prepare($sql);
		$stmt->bindValue(':trackableid', $this->getPrimaryKey(), PDO::PARAM_INT);
		$stmt->bindValue(':customerid', $this->getCustomerid(), PDO::PARAM_INT);
		$stmt->execute();
		
		$statuses = TrackablestatusPeer::populateObjects($stmt);
		if (count($statuses) == 0) {
			return;
		}
		else {
			return $statuses[0];
		}
	}
	
	
	public function save(PropelPDO $con = null) {
	
		$this->setTrackablereference(strtoupper(($this->getTrackablereference())));
		
		// If creating a new address, use the customer the user has been bound to.
		if (!$this->getPrimaryKey()) {
			if (Session::getInstance()->getUser()) {
				$this->setCustomerId(Session::getInstance()->getUser()->getCustomerid());
			}
		}
	
		parent::save($con);
	}
	
} // Trackable
