package org.abo.trackomatic.client;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TrackomaticTrackingActivity extends Activity {
	private LocationManager   locManager = null;
	private LocationListener  locListener = null;
	private Location          currentLocation = null;
	private Location          previousLocation;
	private SharedPreferences sharedSettings = null;
	private String            apiKey;
	private HttpClient        httpClient;
	private StringBuilder     log = null;
	private StringBuilder     logPath;
	private Date              logStartTime = null;
	private int               timesLocationUpdated = 1;
	private int               updateInterval;
	private boolean           isUpdating = false;
	private Handler           serverUpdateHandler = null;
	private Thread            serverUpdateThread = null;
	private Thread            locationUpdateIntervalThread = null;
	private Handler           messageHandler;
	private int               updateStatus;

	private static final String TAG = "TrackomaticTrackingActivity";
	private static final int    MESSAGE_LOCATIONUPDATE = 0;
	private static final int    MESSAGE_REGISTERDROP   = 1;
	private static final int    UPDATESTATUS_UNKNOWN   = 0;
	private static final int    UPDATESTATUS_SUCCESS   = 1;
	private static final int    UPDATESTATUS_FAIL      = 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tracking);

		// Initialize the location log
		log          = new StringBuilder();
		logPath      = new StringBuilder();
		logStartTime = new Date();

		// Get the default HTTP client
		httpClient = new DefaultHttpClient();
		
		// Get settings manager
		sharedSettings = PreferenceManager.getDefaultSharedPreferences(this);

		// Get the location manager
		locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			((ProgressBar) findViewById(R.id.trackingPrgsBar)).setVisibility(ProgressBar.VISIBLE);
		}

		// Create the location listener that listens to GPS updates
		locListener = new LocationListener() {
			ProgressBar progressBar = (ProgressBar) findViewById(R.id.trackingPrgsBar);
			
			public void onStatusChanged(String provider, int status, Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
				Log.d(TAG, "onProviderEnabled: " + provider);
				
				// Start sending location updates
				isUpdating = true;
				progressBar.setVisibility(ProgressBar.VISIBLE);
			}

			public void onProviderDisabled(String provider) {
				Log.d(TAG, "onProviderDisabled: " + provider);
				
				// Stop sending location updates				
				isUpdating = false;
				progressBar.setVisibility(ProgressBar.INVISIBLE);
				showMessageInUI("Location updating is paused, because GPS provider is disabled. " +
						"Please enable it for the application to function properly.");
			}

			public void onLocationChanged(Location location) {
				// Store the old location and update the current
				previousLocation = currentLocation;
				currentLocation  = location;

				// Update the current location information on the screen
				updateLocationInUI(currentLocation);
				
				// Log the new GPS position if it's enabled
				if (location != null && sharedSettings.getBoolean("settingsLog", false))
					logLocation(location);
				
				Log.d(TAG, "Location fix from provider: " + location.getProvider() + " Accuracy: " + location.getAccuracy() + "m");
			}
		};

		// Handles message sent to the UI thread
		messageHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				Bundle bundle  = msg.getData();
				String message = bundle.getString("message");			
				showMessageInUI(message);
			}
		};

		// Restore the previous state
		if (savedInstanceState != null) {
			Log.d(TAG, "Restoring the state of the activity");
			// Check if the bundle has the tracking information 
			if (savedInstanceState.containsKey("latitude") && savedInstanceState.containsKey("longitude") &&
					savedInstanceState.containsKey("speed") && savedInstanceState.containsKey("updateStatus")) {
				// Create a new location with the saved information
				currentLocation = new Location(LocationManager.GPS_PROVIDER);

				currentLocation.setLatitude(savedInstanceState.getDouble("latitude"));
				currentLocation.setLongitude(savedInstanceState.getDouble("longitude"));
				currentLocation.setSpeed(savedInstanceState.getFloat("speed"));
				
				// Update the location update status
				updateStatus = savedInstanceState.getInt("updateStatus", UPDATESTATUS_UNKNOWN);
				updateLocationStatus(updateStatus);
				
				Log.d(TAG, "updateStatus: " + updateStatus);

				// Update the location
				locListener.onLocationChanged(currentLocation);
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		// Get the value from the settings
		getUserSettings();

		// Register the location listener with the location manager to receive
		// location updates from the GPS provider. There is a 1000 millisecond
		// delay between updates and location is broadcasted if it changes by
		// more than ten meters.
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locListener);
		
		// Listen to changes to the GPS provider and act accordingly
		locManager.addGpsStatusListener(new GpsStatus.Listener() {
			public void onGpsStatusChanged(int event) {
				GpsStatus status = locManager.getGpsStatus(null);
				
				switch (event) {
				case GpsStatus.GPS_EVENT_STARTED:
					Log.d(TAG, "GpsStatus: GPS started");
					break;
				case GpsStatus.GPS_EVENT_STOPPED:
					Log.d(TAG, "GpsStatus: GPS stopped");
					break;
				case GpsStatus.GPS_EVENT_FIRST_FIX:
					Log.d(TAG, "GpsStatus: GPS first fix. Time to first fix: " + status.getTimeToFirstFix() + "ms");
					break;
				case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
					break;
				}
			}
		});

		// Start updating the location
		isUpdating = true;

		// Create the threads that handle the location updates to the server
		createThreads();

		// Start the threads, unless they are already running
		if (!serverUpdateThread.isAlive()) {
			serverUpdateThread.start();
		}
		if (!locationUpdateIntervalThread.isAlive()) {
			locationUpdateIntervalThread.start();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Update the settings after the activity resumes
		getUserSettings();
		Log.d(TAG, "Update interval: " + updateInterval);
	}

	@Override
	protected void onStop() {
		super.onPause();

		// Stop the updating
		isUpdating = false;

		//  Stop the update handler
		serverUpdateHandler.getLooper().quit();
		serverUpdateHandler = null;

		// Stop receiving location updates
		locManager.removeUpdates(locListener);

		// If logging enabled save the locations to a log on the external storage
		if (sharedSettings.getBoolean("settingsLog", false)) {
			if (log.length() > 0) {
				StringBuilder      filename = new StringBuilder(getString(R.string.logFilenamePrefix));
				SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.logFormat));

				// Add a time stamp to the filename and the extension
				filename.append(dateFormat.format(logStartTime));
				filename.append("." + getString(R.string.logFilenameExtension));

				// Save the log as a KML file
				saveLogAsKML(filename.toString());
			}

			// Save the locations to a log on the external storage
			if (logPath.length() > 0) {
				StringBuilder      filename = new StringBuilder(getString(R.string.logFilenamePrefix) + "path_");
				SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.logFormat));

				// Add a time stamp to the filename and the extension
				filename.append(dateFormat.format(logStartTime));
				filename.append("." + getString(R.string.logFilenameExtension));

				// Save the log as a KML file
				saveLogAsKMLPath(filename.toString());
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "Storing the state of the activity");
		super.onSaveInstanceState(outState);

		if (currentLocation != null) {
			// Store tracking information so it can be restored when the activity
			// is restarted.
			outState.putDouble("latitude", currentLocation.getLatitude());
			outState.putDouble("longitude", currentLocation.getLongitude());
			outState.putFloat("speed", currentLocation.getSpeed());
			outState.putInt("updateStatus", updateStatus );
		}
	}

	public void onClickTrackingHandler(View view) {
		switch(view.getId()) {
		case R.id.dropTrackingBtn:
			// Drop trackable
			serverUpdateHandler.sendMessage(serverUpdateHandler.obtainMessage(MESSAGE_REGISTERDROP));
			break;

		case R.id.stopTrackingBtn:
			// Finish the activity. This breaks the normal Android activity
			// life cycle but we don't want to return to this activity again.
			finish();
			break;
		}
	}

	private void updateLocationInUI(Location location) {
		if (location != null) {
			// Get the views for latitude, longitude and speed
			TextView latTxtVw = (TextView) findViewById(R.id.latRowValue);
			TextView lngTxtVw = (TextView) findViewById(R.id.lngRowValue);
			TextView spdTxtVw = (TextView) findViewById(R.id.spdRowValue);

			// Convert from a double to a string and set the latitude,
			// longitude and speed for the views
			latTxtVw.setText(String.format("% .13f", location.getLatitude()));
			lngTxtVw.setText(String.format("% .13f", location.getLongitude()));
			// Convert the speed from m/s to km/h
			spdTxtVw.setText(String.format("% .2f", location.getSpeed()*3.6) + " km/h");
		}
	}
	
	private void updateLocationStatus(int status) {
		ImageView locUpdateStatusImgVw = (ImageView) findViewById(R.id.locUpdateStatusImgVw);
		updateStatus = status;
		
		switch (updateStatus) {
		case UPDATESTATUS_UNKNOWN:
			break;
		case UPDATESTATUS_SUCCESS:
			locUpdateStatusImgVw.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_green));
			break;
		case UPDATESTATUS_FAIL:
			locUpdateStatusImgVw.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_red));
			break;
		}
	}

	private void createThreads() {
		// Create a thread  that passes a message to the location update thread
		// after the specified interval
		locationUpdateIntervalThread = new Thread(new Runnable() {
			public void run() {
				Log.d(TAG, "isUpdating: " + updateInterval);
				while (isUpdating) {
					try {
						Thread.sleep(updateInterval);
						if (serverUpdateHandler != null) {
							serverUpdateHandler.sendMessageDelayed(
									serverUpdateHandler.obtainMessage(MESSAGE_LOCATIONUPDATE),
									updateInterval
									);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		});

		// Create a thread that handles the updates to the server
		serverUpdateThread = new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				// Create a handler for updating the location to the server
				serverUpdateHandler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == MESSAGE_LOCATIONUPDATE) {
							// Update the location if we have one
							if (currentLocation != null) {
								updateLocation(currentLocation.getLatitude(), currentLocation.getLongitude());

								// DEBUG
								Log.d(TAG, 
										currentLocation.getProvider() +
										"(" + currentLocation.getLatitude() + "," + currentLocation.getLongitude() + "," + currentLocation.getAltitude() + ")"
										);

								if (previousLocation != null) {
									Log.d(TAG,
											"Distance to prev. location: " + currentLocation.distanceTo(previousLocation) + "m, " +
											"Accuracy: " + currentLocation.getAccuracy() + "m"
											);
								}
							}
						} else if (msg.what == MESSAGE_REGISTERDROP) {
							if (currentLocation != null) {
								// Drop trackable with latitude and longitude positions
								registerDrop(currentLocation.getLatitude(), currentLocation.getLongitude(), null, null);
							} else {
								// Drop trackable using only the API key
								registerDrop(-1.0, -1.0, null, null);
							}
						}
					}
				};
				// Loop and wait for messages
				Looper.loop();
			}

			private void updateLocation(double latitude, double longitude) {
				// Check if the API key is set
				if (apiKey.length() > 0) {
					String       server        = sharedSettings.getString("settingsServer", "http://absx.dyndns.org/");
					String       requestParams = getString(R.string.updateLocationRequestParameters);
					HttpResponse response      = null;
					StatusLine   statusLine    = null;

					// Construct the GET method for updating the location
					String  getString = server + requestParams + 
							"?apikey=" + apiKey + "&lat=" + Double.valueOf(latitude) + "&lng=" + Double.valueOf(longitude);
					HttpGet getMethod = new HttpGet(getString);
					Log.d(TAG, "Location update: " + getString);

					// Send the HTTP GET request and catch the response
					try {
						response   = httpClient.execute(getMethod);
						statusLine = response.getStatusLine();
						Log.d(TAG, "updateLocation - Status line: " + statusLine.toString());
					} catch (Exception e) {
						Log.e(TAG, "updateLocation - Request failed: " + e.toString());
						updateLocationStatusInUI(UPDATESTATUS_FAIL);
						return;
					}

					// Consume the response. If we do not consume the response the
					// client will refuse to reuse the connection
					JSONObject jsonResponse;
					JSONArray  names;

					try {
						jsonResponse = new JSONObject(getContents(response));
						names        = jsonResponse.names();

						for (int i=0; i<names.length(); i++) {
							Log.d(TAG, "updateLocation: " + jsonResponse.getString(names.getString(i)));
						}
					} catch (JSONException e) {
						Log.e(TAG, "updateLocation - parsing response failed: " + e.toString());
						updateLocationStatusInUI(UPDATESTATUS_FAIL);
						return;
					}

					// Show status or error message in UI
					try {
						if (jsonResponse.has("status")) {
							Log.d(TAG, "Location update: " + jsonResponse.getString("status"));
						} else if (jsonResponse.has("error")) {
							showMessageInUI("Location update error: " + jsonResponse.getString("error"));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

					// Handle the status code
					updateLocationStatusHandler(statusLine.getStatusCode());
				} else {
					Log.e(TAG, "updateLocation: No API key specified in the settings");
					sendMessageToUI("No API key specified in the settings");
				}
			}
			
			private void updateLocationStatusHandler(int statusCode) {
				switch (statusCode) {
				case 201:
					// Created
					updateLocationStatusInUI(UPDATESTATUS_SUCCESS);
					break;
				case 401: // Unauthorized (unrecognized/missing API key)
				case 404: // Not found
				case 412: // Precondition failed (validation error/missing argument)
				case 417: // Expectation failed (customer has no status type for �moving� set)
				case 500: // Internal server error
					updateLocationStatusInUI(UPDATESTATUS_FAIL);
					break;
				default:
					break;
				}
			}
			
			private void updateLocationStatusInUI(final int status) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateLocationStatus(status);
					}
				});
			}

			private void registerDrop(double latitude, double longitude, String trackableReference, String groupReference) {
				// Check if the API key is set 
				if (apiKey.length() > 0) {
					String       server        = sharedSettings.getString("settingsServer", "http://absx.dyndns.org/");
					String       requestParams = getString(R.string.registerDropRequestParameters);
					HttpResponse response      = null;
					StatusLine   statusLine    = null;

					// Construct the GET method for registering the drop
					StringBuilder getString = new StringBuilder(server + requestParams + "?apikey=" + apiKey);

					// A latitude or longitude value of -1 means they are not defined
					if (latitude != -1.0 && longitude != -1.0) {
						getString.append("&lat=" + Double.valueOf(latitude) + "&lng=" + Double.valueOf(longitude));
					}
					// Add the reference if it is specified
					if (trackableReference != null) {
						getString.append("&reference=" + trackableReference); 
					}
					// Add the group reference if it is specified
					if (groupReference != null) {
						getString.append("&groupreference=" + groupReference);
					}

					// Create the HTTP GET request
					HttpGet getMethod = new HttpGet(getString.toString());
					Log.d(TAG, "Register drop: " + getString.toString());

					// Send the HTTP GET request and catch the response
					try {
						response   = httpClient.execute(getMethod);
						statusLine = response.getStatusLine();
						Log.d(TAG, "registerDrop - Status line: " + statusLine.toString());
					} catch (Exception e) {
						sendMessageToUI("Register drop request failed");
						Log.e(TAG, "registerDrop - Request failed " + e.toString());
						return;
					}

					// Consume the response. If we do not consume the response the
					// client will refuse to reuse the connection
					JSONObject jsonResponse;
					JSONArray  names;

					try {
						jsonResponse = new JSONObject(getContents(response));
						names        = jsonResponse.names();

						for (int i=0; i<names.length(); i++) {
							Log.d(TAG, "registerDrop: " + jsonResponse.getString(names.getString(i)));
						}
					} catch (JSONException e) {
						sendMessageToUI("Register drop failed while parsing response");
						Log.e(TAG, "registerDrop - parsing response failed: " + e.toString());
						e.printStackTrace();
						return;
					}

					// Show status or error message in UI
					try {
						if (jsonResponse.has("status")) {
							Log.d(TAG, "Register drop: " + jsonResponse.getString("status"));
						} else if (jsonResponse.has("error")) {
							showMessageInUI("Register drop error: " + jsonResponse.getString("error"));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

					// Handle the status code
					registerDropStatusHandler(statusLine.getStatusCode(), jsonResponse);
				} else {
					Log.e(TAG, "registerDrop: No API key specified in the settings");
					showMessageInUI("No API key specified in the settings");
				}
			}
			
			private void registerDropStatusHandler(int statusCode, JSONObject jsonResponse) {
				switch (statusCode) {
				case 201: // Created.
					break;
				case 300:
					// Multiple choices. No reference or group reference and multiple drop
					// points found at the address.
					Intent i = new Intent(TrackomaticTrackingActivity.this, TrackomaticDropActivity.class);

					if (currentLocation != null) {
						i.putExtra("org.abo.trackomatic.client.longitude", currentLocation.getLongitude());
						i.putExtra("org.abo.trackomatic.client.latitude", currentLocation.getLatitude());
					}

					try {
						i.putExtra("org.abo.trackomatic.client.dropCandidates", jsonResponse.getString("dropCandidates"));
					} catch (JSONException e) {
						Log.e(TAG, "No drop candidates");
						e.printStackTrace();
						return;
					}

					// Start the activity
					startActivity(i);
					break;
				case 401: // Unauthorized. Unrecognized or missing API key
				case 404: // Not found
				case 412: // Precondition failed. Validation error or missing argument.
				case 417: // Expectation failed. Trackable already dropped.
				case 500: // Internal server error
					break;
				default:
					break;
				}
			}

			// Read the contents of the HTTP response and return it as a string
			private String getContents(HttpResponse response) {
				StringBuilder content = new StringBuilder();

				try {
					InputStream    contentStream = response.getEntity().getContent();
					BufferedReader reader        = new BufferedReader(new InputStreamReader(contentStream));

					String line;
					while ((line = reader.readLine()) != null) {
						content.append(line);
					}

					reader.close();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} 

				return content.toString();
			}

			private void sendMessageToUI(String message) {
				if (message != null) {
					// Create a message to update the UI message text view
					Message msg    = messageHandler.obtainMessage();
					Bundle  bundle = new Bundle();

					// Put the message in a bundle and send it to the message handler
					bundle.putString("message", message);
					msg.setData(bundle);
					msg.sendToTarget();
				}
			}
		});
	}

	private void showMessageInUI(String message) {
		if (message != null) {
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}

	private void getUserSettings() {
		// Get the current settings and retrieve the update interval value (and
		// convert it to milliseconds). If no interval is set, the default of
		// five seconds is used.
		sharedSettings = PreferenceManager.getDefaultSharedPreferences(this);
		updateInterval = Integer.valueOf(sharedSettings.getString("settingsInterval", "5")) * 1000;
		apiKey         = sharedSettings.getString("settingsApiKey", "");
	}

	private void logLocation(Location location) {
		if (location != null) {
			// Get the date and time of the location fix
			Date now = new Date(location.getTime());
			SimpleDateFormat timeStampFormat = new SimpleDateFormat(getString(R.string.timeStampFormat));
			SimpleDateFormat dateFormat      = new SimpleDateFormat(getString(R.string.dateFormat));

			// Log the new GPS position
			log.append(
					"\t<Placemark>\n" + 
							"\t\t<name>[" + (timesLocationUpdated++) + "] " + dateFormat.format(now) + "</name>\n" +
							"\t\t<description>Location provider was '" + location.getProvider() + "' with an accuracy of " + location.getAccuracy() + "m.\n" + 
							"\t\tDistance to previous location was " + (previousLocation != null ? location.distanceTo(previousLocation) : "unknown") + ". " +
							"Moving at a speed of " + String.format("% .2f", location.getSpeed()*3.6) + " km/h</description>\n" +  
							"\t\t<TimeStamp><when>" + timeStampFormat.format(now) + "</when></TimeStamp>\n" +
							"\t\t<Point><coordinates>" +
							String.valueOf(location.getLongitude()) + "," +
							String.valueOf(location.getLatitude())  + "," +
							String.valueOf(location.getAltitude())  +
							"</coordinates></Point>\n" +
							"\t</Placemark>\n"
					);
			
			logPath.append(
				"\t\t\t" +
				String.valueOf(location.getLongitude()) + "," +
				String.valueOf(location.getLatitude())  + "," +
				String.valueOf(location.getAltitude())  + "\n"
			);
		}
	}
	
	private void saveLogAsKML(String filename) {
		if (filename != null) {
			// Get the state of the external storage
			String state = Environment.getExternalStorageState();
			Log.d(TAG, "External storage state: " + state);

			// Check if the external storage is mounted
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				// Yes. We can read and write to the external storage. Open the
				// file that we want to write the log to
				File file = new File(getExternalFilesDir(null), filename);
				BufferedOutputStream kmlFile;

				try {
					kmlFile = new BufferedOutputStream(new FileOutputStream(file));
				} catch (FileNotFoundException e) {
					Log.e(TAG, "Error opening " + file, e);
					return;
				}

				String kmlHeader = 
						"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
								"<kml xmlns=\"http://earth.google.com/kml/2.2\">\n" + 
								"<Document>\n\t<name>" + getResources().getString(R.string.appName) + "</name>\n";
				String kmlFooter = "</Document>\n</kml>\n";

				try {
					// Write the KML header
					kmlFile.write(kmlHeader.getBytes());
					// Write the log of the locations to the file
					kmlFile.write(log.toString().getBytes());
					// Write the KML footer
					kmlFile.write(kmlFooter.getBytes());
					// Close the KML log
					kmlFile.close();
				} catch (IOException e) {
					Log.e(TAG, "Error writing log file " + file, e);
				}
			}
		}
	}
	
	private void saveLogAsKMLPath(String filename) {
		if (filename != null) {
			// Get the state of the external storage
			String state = Environment.getExternalStorageState();
			Log.d(TAG, "External storage state: " + state);

			// Check if the external storage is mounted
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				// Yes. We can read and write to the external storage. Open the
				// file that we want to write the log to
				File file = new File(getExternalFilesDir(null), filename);
				BufferedOutputStream kmlFile;

				try {
					kmlFile = new BufferedOutputStream(new FileOutputStream(file));
				} catch (FileNotFoundException e) {
					Log.e(TAG, "Error opening " + file, e);
					return;
				}

				String kmlHeader = 
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"<kml xmlns=\"http://earth.google.com/kml/2.2\">\n" + 
					"<Document>\n\t<name>" + getResources().getString(R.string.appName) + "</name>\n" +
					"\t<Placemark>\n" +
					"\t<LineString>\n" +
					"\t\t<extrude>1</extrude>\n" +
					"\t\t<tessellate>1</tessellate>\n" +
					"\t\t<altitudeMode>absolute</altitudeMode>\n" +
					"\t\t<coordinates>\n";
					
				String kmlFooter =
					"\t\t</coordinates>\n" +
					"\t</LineString>\n" +
					"\t</Placemark>\n" + 
					"</Document>\n</kml>\n";

				try {
					// Write the KML header
					kmlFile.write(kmlHeader.getBytes());
					// Write the log of the locations to the file
					kmlFile.write(logPath.toString().getBytes());
					// Write the KML footer
					kmlFile.write(kmlFooter.getBytes());
					// Close the KML log
					kmlFile.close();
				} catch (IOException e) {
					Log.e(TAG, "Error writing log file " + file, e);
				}
			}
		}
	}
}
