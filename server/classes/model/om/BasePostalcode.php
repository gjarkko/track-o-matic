<?php


/**
 * Base class that represents a row from the 'postalcode' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BasePostalcode extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'PostalcodePeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        PostalcodePeer
	 */
	protected static $peer;

	/**
	 * The value for the postalcodeid field.
	 * @var        int
	 */
	protected $postalcodeid;

	/**
	 * The value for the countryid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $countryid;

	/**
	 * The value for the postalcode field.
	 * @var        string
	 */
	protected $postalcode;

	/**
	 * The value for the postalarea field.
	 * @var        string
	 */
	protected $postalarea;

	/**
	 * @var        Country
	 */
	protected $aCountry;

	/**
	 * @var        array Address[] Collection to store aggregation of Address objects.
	 */
	protected $collAddresss;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $addresssScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->countryid = 1;
	}

	/**
	 * Initializes internal state of BasePostalcode object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [postalcodeid] column value.
	 * 
	 * @return     int
	 */
	public function getPostalcodeid()
	{
		return $this->postalcodeid;
	}

	/**
	 * Get the [countryid] column value.
	 * 
	 * @return     int
	 */
	public function getCountryid()
	{
		return $this->countryid;
	}

	/**
	 * Get the [postalcode] column value.
	 * 
	 * @return     string
	 */
	public function getPostalcode()
	{
		return $this->postalcode;
	}

	/**
	 * Get the [postalarea] column value.
	 * 
	 * @return     string
	 */
	public function getPostalarea()
	{
		return $this->postalarea;
	}

	/**
	 * Set the value of [postalcodeid] column.
	 * 
	 * @param      int $v new value
	 * @return     Postalcode The current object (for fluent API support)
	 */
	public function setPostalcodeid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->postalcodeid !== $v) {
			$this->postalcodeid = $v;
			$this->modifiedColumns[] = PostalcodePeer::POSTALCODEID;
		}

		return $this;
	} // setPostalcodeid()

	/**
	 * Set the value of [countryid] column.
	 * 
	 * @param      int $v new value
	 * @return     Postalcode The current object (for fluent API support)
	 */
	public function setCountryid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->countryid !== $v) {
			$this->countryid = $v;
			$this->modifiedColumns[] = PostalcodePeer::COUNTRYID;
		}

		if ($this->aCountry !== null && $this->aCountry->getCountryid() !== $v) {
			$this->aCountry = null;
		}

		return $this;
	} // setCountryid()

	/**
	 * Set the value of [postalcode] column.
	 * 
	 * @param      string $v new value
	 * @return     Postalcode The current object (for fluent API support)
	 */
	public function setPostalcode($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->postalcode !== $v) {
			$this->postalcode = $v;
			$this->modifiedColumns[] = PostalcodePeer::POSTALCODE;
		}

		return $this;
	} // setPostalcode()

	/**
	 * Set the value of [postalarea] column.
	 * 
	 * @param      string $v new value
	 * @return     Postalcode The current object (for fluent API support)
	 */
	public function setPostalarea($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->postalarea !== $v) {
			$this->postalarea = $v;
			$this->modifiedColumns[] = PostalcodePeer::POSTALAREA;
		}

		return $this;
	} // setPostalarea()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->countryid !== 1) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->postalcodeid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->countryid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->postalcode = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->postalarea = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 4; // 4 = PostalcodePeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Postalcode object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCountry !== null && $this->countryid !== $this->aCountry->getCountryid()) {
			$this->aCountry = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PostalcodePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = PostalcodePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aCountry = null;
			$this->collAddresss = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PostalcodePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = PostalcodeQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PostalcodePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				PostalcodePeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified() || $this->aCountry->isNew()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->addresssScheduledForDeletion !== null) {
				if (!$this->addresssScheduledForDeletion->isEmpty()) {
					AddressQuery::create()
						->filterByPrimaryKeys($this->addresssScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->addresssScheduledForDeletion = null;
				}
			}

			if ($this->collAddresss !== null) {
				foreach ($this->collAddresss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = PostalcodePeer::POSTALCODEID;
		if (null !== $this->postalcodeid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . PostalcodePeer::POSTALCODEID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(PostalcodePeer::POSTALCODEID)) {
			$modifiedColumns[':p' . $index++]  = '`POSTALCODEID`';
		}
		if ($this->isColumnModified(PostalcodePeer::COUNTRYID)) {
			$modifiedColumns[':p' . $index++]  = '`COUNTRYID`';
		}
		if ($this->isColumnModified(PostalcodePeer::POSTALCODE)) {
			$modifiedColumns[':p' . $index++]  = '`POSTALCODE`';
		}
		if ($this->isColumnModified(PostalcodePeer::POSTALAREA)) {
			$modifiedColumns[':p' . $index++]  = '`POSTALAREA`';
		}

		$sql = sprintf(
			'INSERT INTO `postalcode` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`POSTALCODEID`':
						$stmt->bindValue($identifier, $this->postalcodeid, PDO::PARAM_INT);
						break;
					case '`COUNTRYID`':
						$stmt->bindValue($identifier, $this->countryid, PDO::PARAM_INT);
						break;
					case '`POSTALCODE`':
						$stmt->bindValue($identifier, $this->postalcode, PDO::PARAM_STR);
						break;
					case '`POSTALAREA`':
						$stmt->bindValue($identifier, $this->postalarea, PDO::PARAM_STR);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setPostalcodeid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}


			if (($retval = PostalcodePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collAddresss !== null) {
					foreach ($this->collAddresss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PostalcodePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getPostalcodeid();
				break;
			case 1:
				return $this->getCountryid();
				break;
			case 2:
				return $this->getPostalcode();
				break;
			case 3:
				return $this->getPostalarea();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Postalcode'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Postalcode'][$this->getPrimaryKey()] = true;
		$keys = PostalcodePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getPostalcodeid(),
			$keys[1] => $this->getCountryid(),
			$keys[2] => $this->getPostalcode(),
			$keys[3] => $this->getPostalarea(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aCountry) {
				$result['Country'] = $this->aCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->collAddresss) {
				$result['Addresss'] = $this->collAddresss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PostalcodePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setPostalcodeid($value);
				break;
			case 1:
				$this->setCountryid($value);
				break;
			case 2:
				$this->setPostalcode($value);
				break;
			case 3:
				$this->setPostalarea($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PostalcodePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setPostalcodeid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCountryid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPostalcode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPostalarea($arr[$keys[3]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(PostalcodePeer::DATABASE_NAME);

		if ($this->isColumnModified(PostalcodePeer::POSTALCODEID)) $criteria->add(PostalcodePeer::POSTALCODEID, $this->postalcodeid);
		if ($this->isColumnModified(PostalcodePeer::COUNTRYID)) $criteria->add(PostalcodePeer::COUNTRYID, $this->countryid);
		if ($this->isColumnModified(PostalcodePeer::POSTALCODE)) $criteria->add(PostalcodePeer::POSTALCODE, $this->postalcode);
		if ($this->isColumnModified(PostalcodePeer::POSTALAREA)) $criteria->add(PostalcodePeer::POSTALAREA, $this->postalarea);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PostalcodePeer::DATABASE_NAME);
		$criteria->add(PostalcodePeer::POSTALCODEID, $this->postalcodeid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getPostalcodeid();
	}

	/**
	 * Generic method to set the primary key (postalcodeid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setPostalcodeid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getPostalcodeid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Postalcode (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCountryid($this->getCountryid());
		$copyObj->setPostalcode($this->getPostalcode());
		$copyObj->setPostalarea($this->getPostalarea());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getAddresss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addAddress($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setPostalcodeid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Postalcode Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     PostalcodePeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PostalcodePeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Country object.
	 *
	 * @param      Country $v
	 * @return     Postalcode The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCountry(Country $v = null)
	{
		if ($v === null) {
			$this->setCountryid(1);
		} else {
			$this->setCountryid($v->getCountryid());
		}

		$this->aCountry = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Country object, it will not be re-added.
		if ($v !== null) {
			$v->addPostalcode($this);
		}

		return $this;
	}


	/**
	 * Get the associated Country object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Country The associated Country object.
	 * @throws     PropelException
	 */
	public function getCountry(PropelPDO $con = null)
	{
		if ($this->aCountry === null && ($this->countryid !== null)) {
			$this->aCountry = CountryQuery::create()->findPk($this->countryid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCountry->addPostalcodes($this);
			 */
		}
		return $this->aCountry;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('Address' == $relationName) {
			return $this->initAddresss();
		}
	}

	/**
	 * Clears out the collAddresss collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addAddresss()
	 */
	public function clearAddresss()
	{
		$this->collAddresss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collAddresss collection.
	 *
	 * By default this just sets the collAddresss collection to an empty array (like clearcollAddresss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initAddresss($overrideExisting = true)
	{
		if (null !== $this->collAddresss && !$overrideExisting) {
			return;
		}
		$this->collAddresss = new PropelObjectCollection();
		$this->collAddresss->setModel('Address');
	}

	/**
	 * Gets an array of Address objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Postalcode is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Address[] List of Address objects
	 * @throws     PropelException
	 */
	public function getAddresss($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collAddresss || null !== $criteria) {
			if ($this->isNew() && null === $this->collAddresss) {
				// return empty collection
				$this->initAddresss();
			} else {
				$collAddresss = AddressQuery::create(null, $criteria)
					->filterByPostalcode($this)
					->find($con);
				if (null !== $criteria) {
					return $collAddresss;
				}
				$this->collAddresss = $collAddresss;
			}
		}
		return $this->collAddresss;
	}

	/**
	 * Sets a collection of Address objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $addresss A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setAddresss(PropelCollection $addresss, PropelPDO $con = null)
	{
		$this->addresssScheduledForDeletion = $this->getAddresss(new Criteria(), $con)->diff($addresss);

		foreach ($addresss as $address) {
			// Fix issue with collection modified by reference
			if ($address->isNew()) {
				$address->setPostalcode($this);
			}
			$this->addAddress($address);
		}

		$this->collAddresss = $addresss;
	}

	/**
	 * Returns the number of related Address objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Address objects.
	 * @throws     PropelException
	 */
	public function countAddresss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collAddresss || null !== $criteria) {
			if ($this->isNew() && null === $this->collAddresss) {
				return 0;
			} else {
				$query = AddressQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByPostalcode($this)
					->count($con);
			}
		} else {
			return count($this->collAddresss);
		}
	}

	/**
	 * Method called to associate a Address object to this object
	 * through the Address foreign key attribute.
	 *
	 * @param      Address $l Address
	 * @return     Postalcode The current object (for fluent API support)
	 */
	public function addAddress(Address $l)
	{
		if ($this->collAddresss === null) {
			$this->initAddresss();
		}
		if (!$this->collAddresss->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddAddress($l);
		}

		return $this;
	}

	/**
	 * @param	Address $address The address object to add.
	 */
	protected function doAddAddress($address)
	{
		$this->collAddresss[]= $address;
		$address->setPostalcode($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Postalcode is new, it will return
	 * an empty collection; or if this Postalcode has previously
	 * been saved, it will retrieve related Addresss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Postalcode.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Address[] List of Address objects
	 */
	public function getAddresssJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = AddressQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getAddresss($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->postalcodeid = null;
		$this->countryid = null;
		$this->postalcode = null;
		$this->postalarea = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collAddresss) {
				foreach ($this->collAddresss as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collAddresss instanceof PropelCollection) {
			$this->collAddresss->clearIterator();
		}
		$this->collAddresss = null;
		$this->aCountry = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(PostalcodePeer::DEFAULT_STRING_FORMAT);
	}

} // BasePostalcode
