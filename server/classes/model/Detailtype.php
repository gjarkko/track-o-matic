<?php



/**
 * Skeleton subclass for representing a row from the 'detailtype' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Detailtype extends BaseDetailtype {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
			),		
			array(			
				'column' => 'detailtype',
				'label' => ucfirst(Localizer::getText('detail type')),
			),
		);
	}
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),		
			array(
				'column' => 'detailtype',
				'label' => ucfirst(Localizer::getText('detail type')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
		
} // Detailtype
