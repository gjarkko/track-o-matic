<?

/**
 * The Application class
 * - Init & bootstrap application
 * - Move control to HTTP Request Handler
 */

class Application {

	public function __construct($configFile = false) {
		if (isset($configFile)) {
			$this->registerConfig($configFile);
		}
	}

	public function bootstrap() {

		if(!Registry::get('config')) {
			$this->registerConfig();
		}

		return $this;
	}

	public function registerConfig($configFile = false) {
		if (!$configFile || !file_exists($configFile)) {
			$configFile = CONFIG_PATH . 'config.php';
		}
		include $configFile;
			
		if (!isset($config)) {
			throw new Exception('Invalid configuration file ' . $configFile . '.');
		}
			
		foreach ($config as $key => $value) {
			Registry::set($key, $value);
		}
	}
	
	public function run() {
		RequestHandler::flush();
		RequestHandler::getInstance()->handleRequest();
		return $this;
	}

}