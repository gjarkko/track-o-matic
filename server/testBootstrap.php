<?

// Guess the application installation path, set some path constants
define('ROOT_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('CLASS_PATH', ROOT_PATH . 'classes' . DIRECTORY_SEPARATOR);
define('VENDOR_PATH', ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR);
define('CONFIG_PATH', ROOT_PATH . 'config' . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', ROOT_PATH . 'public' . DIRECTORY_SEPARATOR);
define('TESTS_PATH', ROOT_PATH . 'tests' . DIRECTORY_SEPARATOR);
define('BASEURL', '/tom');

// Set include paths
set_include_path(CLASS_PATH . ':' . ROOT_PATH . ':' . get_include_path());

ini_set("display_errors", 1);
error_reporting(E_ALL);

// Include the bare necessities & register autoloader
include_once 'AutoLoader.php';
include_once 'Tools.php';
include_once 'PHPUnit/Autoload.php';
spl_autoload_register('AutoLoader::autoload');

spl_autoload_register('phpunit_autoload');
set_error_handler('Tools::handleError');
register_shutdown_function('Tools::shutdown');

// Init Propel
Propel::init(CONFIG_PATH . "propel-conf-test.php");

// Init & clear test database
include 'populateTestDb.php';