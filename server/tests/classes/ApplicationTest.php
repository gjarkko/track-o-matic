<?

class ApplicationTest extends PHPUnit_Framework_TestCase {

	protected function setUp() {
		require_once implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -2)) . DIRECTORY_SEPARATOR . 'testBootstrap.php';
	}
	
	protected function tearDown() {
		require implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -2)) . DIRECTORY_SEPARATOR . 'populateTestDb.php';
	}
	
	public function testConstructWorks() {
		$app = new Application();
		$this->assertInstanceOf('Application', $app);
	}
	
	
	public function testBootstrapWorks() {
		$app = new Application();
		$app->bootstrap();
		
		// No erros occurred
		$this->assertEquals(true, true);
	}
	
	/**
	* @expectedException PHPUnit_Framework_Error
	*/
	public function testFailingInclude() {
		include 'not_existing_file.php';
	}
}