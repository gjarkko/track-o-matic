<?php


/**
 * Base class that represents a query for the 'tripvehicle' table.
 *
 * 
 *
 * @method     TripvehicleQuery orderByTripvehicleeid($order = Criteria::ASC) Order by the tripvehicleid column
 * @method     TripvehicleQuery orderByTripid($order = Criteria::ASC) Order by the tripid column
 * @method     TripvehicleQuery orderByVehicleid($order = Criteria::ASC) Order by the vehicleid column
 * @method     TripvehicleQuery orderByIntime($order = Criteria::ASC) Order by the intime column
 * @method     TripvehicleQuery orderByOuttime($order = Criteria::ASC) Order by the outtime column
 *
 * @method     TripvehicleQuery groupByTripvehicleeid() Group by the tripvehicleid column
 * @method     TripvehicleQuery groupByTripid() Group by the tripid column
 * @method     TripvehicleQuery groupByVehicleid() Group by the vehicleid column
 * @method     TripvehicleQuery groupByIntime() Group by the intime column
 * @method     TripvehicleQuery groupByOuttime() Group by the outtime column
 *
 * @method     TripvehicleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TripvehicleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TripvehicleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TripvehicleQuery leftJoinTrip($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trip relation
 * @method     TripvehicleQuery rightJoinTrip($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trip relation
 * @method     TripvehicleQuery innerJoinTrip($relationAlias = null) Adds a INNER JOIN clause to the query using the Trip relation
 *
 * @method     TripvehicleQuery leftJoinVehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vehicle relation
 * @method     TripvehicleQuery rightJoinVehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vehicle relation
 * @method     TripvehicleQuery innerJoinVehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Vehicle relation
 *
 * @method     Tripvehicle findOne(PropelPDO $con = null) Return the first Tripvehicle matching the query
 * @method     Tripvehicle findOneOrCreate(PropelPDO $con = null) Return the first Tripvehicle matching the query, or a new Tripvehicle object populated from the query conditions when no match is found
 *
 * @method     Tripvehicle findOneByTripvehicleeid(int $tripvehicleid) Return the first Tripvehicle filtered by the tripvehicleid column
 * @method     Tripvehicle findOneByTripid(int $tripid) Return the first Tripvehicle filtered by the tripid column
 * @method     Tripvehicle findOneByVehicleid(int $vehicleid) Return the first Tripvehicle filtered by the vehicleid column
 * @method     Tripvehicle findOneByIntime(string $intime) Return the first Tripvehicle filtered by the intime column
 * @method     Tripvehicle findOneByOuttime(string $outtime) Return the first Tripvehicle filtered by the outtime column
 *
 * @method     array findByTripvehicleeid(int $tripvehicleid) Return Tripvehicle objects filtered by the tripvehicleid column
 * @method     array findByTripid(int $tripid) Return Tripvehicle objects filtered by the tripid column
 * @method     array findByVehicleid(int $vehicleid) Return Tripvehicle objects filtered by the vehicleid column
 * @method     array findByIntime(string $intime) Return Tripvehicle objects filtered by the intime column
 * @method     array findByOuttime(string $outtime) Return Tripvehicle objects filtered by the outtime column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTripvehicleQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseTripvehicleQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Tripvehicle', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TripvehicleQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TripvehicleQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TripvehicleQuery) {
			return $criteria;
		}
		$query = new TripvehicleQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Tripvehicle|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = TripvehiclePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(TripvehiclePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Tripvehicle A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `TRIPVEHICLEID`, `TRIPID`, `VEHICLEID`, `INTIME`, `OUTTIME` FROM `tripvehicle` WHERE `TRIPVEHICLEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Tripvehicle();
			$obj->hydrate($row);
			TripvehiclePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Tripvehicle|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TripvehiclePeer::TRIPVEHICLEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TripvehiclePeer::TRIPVEHICLEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the tripvehicleid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTripvehicleeid(1234); // WHERE tripvehicleid = 1234
	 * $query->filterByTripvehicleeid(array(12, 34)); // WHERE tripvehicleid IN (12, 34)
	 * $query->filterByTripvehicleeid(array('min' => 12)); // WHERE tripvehicleid > 12
	 * </code>
	 *
	 * @param     mixed $tripvehicleeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByTripvehicleeid($tripvehicleeid = null, $comparison = null)
	{
		if (is_array($tripvehicleeid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TripvehiclePeer::TRIPVEHICLEID, $tripvehicleeid, $comparison);
	}

	/**
	 * Filter the query on the tripid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTripid(1234); // WHERE tripid = 1234
	 * $query->filterByTripid(array(12, 34)); // WHERE tripid IN (12, 34)
	 * $query->filterByTripid(array('min' => 12)); // WHERE tripid > 12
	 * </code>
	 *
	 * @see       filterByTrip()
	 *
	 * @param     mixed $tripid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByTripid($tripid = null, $comparison = null)
	{
		if (is_array($tripid)) {
			$useMinMax = false;
			if (isset($tripid['min'])) {
				$this->addUsingAlias(TripvehiclePeer::TRIPID, $tripid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($tripid['max'])) {
				$this->addUsingAlias(TripvehiclePeer::TRIPID, $tripid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TripvehiclePeer::TRIPID, $tripid, $comparison);
	}

	/**
	 * Filter the query on the vehicleid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByVehicleid(1234); // WHERE vehicleid = 1234
	 * $query->filterByVehicleid(array(12, 34)); // WHERE vehicleid IN (12, 34)
	 * $query->filterByVehicleid(array('min' => 12)); // WHERE vehicleid > 12
	 * </code>
	 *
	 * @see       filterByVehicle()
	 *
	 * @param     mixed $vehicleid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByVehicleid($vehicleid = null, $comparison = null)
	{
		if (is_array($vehicleid)) {
			$useMinMax = false;
			if (isset($vehicleid['min'])) {
				$this->addUsingAlias(TripvehiclePeer::VEHICLEID, $vehicleid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($vehicleid['max'])) {
				$this->addUsingAlias(TripvehiclePeer::VEHICLEID, $vehicleid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TripvehiclePeer::VEHICLEID, $vehicleid, $comparison);
	}

	/**
	 * Filter the query on the intime column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByIntime('2011-03-14'); // WHERE intime = '2011-03-14'
	 * $query->filterByIntime('now'); // WHERE intime = '2011-03-14'
	 * $query->filterByIntime(array('max' => 'yesterday')); // WHERE intime > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $intime The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByIntime($intime = null, $comparison = null)
	{
		if (is_array($intime)) {
			$useMinMax = false;
			if (isset($intime['min'])) {
				$this->addUsingAlias(TripvehiclePeer::INTIME, $intime['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($intime['max'])) {
				$this->addUsingAlias(TripvehiclePeer::INTIME, $intime['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TripvehiclePeer::INTIME, $intime, $comparison);
	}

	/**
	 * Filter the query on the outtime column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByOuttime('2011-03-14'); // WHERE outtime = '2011-03-14'
	 * $query->filterByOuttime('now'); // WHERE outtime = '2011-03-14'
	 * $query->filterByOuttime(array('max' => 'yesterday')); // WHERE outtime > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $outtime The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByOuttime($outtime = null, $comparison = null)
	{
		if (is_array($outtime)) {
			$useMinMax = false;
			if (isset($outtime['min'])) {
				$this->addUsingAlias(TripvehiclePeer::OUTTIME, $outtime['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($outtime['max'])) {
				$this->addUsingAlias(TripvehiclePeer::OUTTIME, $outtime['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TripvehiclePeer::OUTTIME, $outtime, $comparison);
	}

	/**
	 * Filter the query by a related Trip object
	 *
	 * @param     Trip|PropelCollection $trip The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByTrip($trip, $comparison = null)
	{
		if ($trip instanceof Trip) {
			return $this
				->addUsingAlias(TripvehiclePeer::TRIPID, $trip->getTripid(), $comparison);
		} elseif ($trip instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TripvehiclePeer::TRIPID, $trip->toKeyValue('PrimaryKey', 'Tripid'), $comparison);
		} else {
			throw new PropelException('filterByTrip() only accepts arguments of type Trip or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trip relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function joinTrip($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trip');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trip');
		}

		return $this;
	}

	/**
	 * Use the Trip relation Trip object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery A secondary query class using the current class as primary query
	 */
	public function useTripQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrip($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trip', 'TripQuery');
	}

	/**
	 * Filter the query by a related Vehicle object
	 *
	 * @param     Vehicle|PropelCollection $vehicle The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function filterByVehicle($vehicle, $comparison = null)
	{
		if ($vehicle instanceof Vehicle) {
			return $this
				->addUsingAlias(TripvehiclePeer::VEHICLEID, $vehicle->getVehicleid(), $comparison);
		} elseif ($vehicle instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TripvehiclePeer::VEHICLEID, $vehicle->toKeyValue('PrimaryKey', 'Vehicleid'), $comparison);
		} else {
			throw new PropelException('filterByVehicle() only accepts arguments of type Vehicle or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Vehicle relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function joinVehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Vehicle');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Vehicle');
		}

		return $this;
	}

	/**
	 * Use the Vehicle relation Vehicle object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery A secondary query class using the current class as primary query
	 */
	public function useVehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinVehicle($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Vehicle', 'VehicleQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Tripvehicle $tripvehicle Object to remove from the list of results
	 *
	 * @return    TripvehicleQuery The current query, for fluid interface
	 */
	public function prune($tripvehicle = null)
	{
		if ($tripvehicle) {
			$this->addUsingAlias(TripvehiclePeer::TRIPVEHICLEID, $tripvehicle->getTripvehicleeid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseTripvehicleQuery