<?php


/**
 * Base class that represents a query for the 'notification' table.
 *
 * 
 *
 * @method     NotificationQuery orderByNotificationid($order = Criteria::ASC) Order by the notificationid column
 * @method     NotificationQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     NotificationQuery orderByNotificationtypeid($order = Criteria::ASC) Order by the notificationtypeid column
 * @method     NotificationQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 * @method     NotificationQuery orderByDeliveryaddress($order = Criteria::ASC) Order by the deliveryaddress column
 * @method     NotificationQuery orderByProximity($order = Criteria::ASC) Order by the proximity column
 * @method     NotificationQuery orderByTimecreated($order = Criteria::ASC) Order by the timecreated column
 * @method     NotificationQuery orderByTimesent($order = Criteria::ASC) Order by the timesent column
 * @method     NotificationQuery orderByCreationsent($order = Criteria::ASC) Order by the creationsent column
 * @method     NotificationQuery orderByNotificationsent($order = Criteria::ASC) Order by the notificationsent column
 *
 * @method     NotificationQuery groupByNotificationid() Group by the notificationid column
 * @method     NotificationQuery groupByCustomerid() Group by the customerid column
 * @method     NotificationQuery groupByNotificationtypeid() Group by the notificationtypeid column
 * @method     NotificationQuery groupByTrackableid() Group by the trackableid column
 * @method     NotificationQuery groupByDeliveryaddress() Group by the deliveryaddress column
 * @method     NotificationQuery groupByProximity() Group by the proximity column
 * @method     NotificationQuery groupByTimecreated() Group by the timecreated column
 * @method     NotificationQuery groupByTimesent() Group by the timesent column
 * @method     NotificationQuery groupByCreationsent() Group by the creationsent column
 * @method     NotificationQuery groupByNotificationsent() Group by the notificationsent column
 *
 * @method     NotificationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     NotificationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     NotificationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     NotificationQuery leftJoinNotificationtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Notificationtype relation
 * @method     NotificationQuery rightJoinNotificationtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Notificationtype relation
 * @method     NotificationQuery innerJoinNotificationtype($relationAlias = null) Adds a INNER JOIN clause to the query using the Notificationtype relation
 *
 * @method     NotificationQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     NotificationQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     NotificationQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     NotificationQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     NotificationQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     NotificationQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     Notification findOne(PropelPDO $con = null) Return the first Notification matching the query
 * @method     Notification findOneOrCreate(PropelPDO $con = null) Return the first Notification matching the query, or a new Notification object populated from the query conditions when no match is found
 *
 * @method     Notification findOneByNotificationid(int $notificationid) Return the first Notification filtered by the notificationid column
 * @method     Notification findOneByCustomerid(int $customerid) Return the first Notification filtered by the customerid column
 * @method     Notification findOneByNotificationtypeid(int $notificationtypeid) Return the first Notification filtered by the notificationtypeid column
 * @method     Notification findOneByTrackableid(int $trackableid) Return the first Notification filtered by the trackableid column
 * @method     Notification findOneByDeliveryaddress(string $deliveryaddress) Return the first Notification filtered by the deliveryaddress column
 * @method     Notification findOneByProximity(int $proximity) Return the first Notification filtered by the proximity column
 * @method     Notification findOneByTimecreated(string $timecreated) Return the first Notification filtered by the timecreated column
 * @method     Notification findOneByTimesent(string $timesent) Return the first Notification filtered by the timesent column
 * @method     Notification findOneByCreationsent(boolean $creationsent) Return the first Notification filtered by the creationsent column
 * @method     Notification findOneByNotificationsent(boolean $notificationsent) Return the first Notification filtered by the notificationsent column
 *
 * @method     array findByNotificationid(int $notificationid) Return Notification objects filtered by the notificationid column
 * @method     array findByCustomerid(int $customerid) Return Notification objects filtered by the customerid column
 * @method     array findByNotificationtypeid(int $notificationtypeid) Return Notification objects filtered by the notificationtypeid column
 * @method     array findByTrackableid(int $trackableid) Return Notification objects filtered by the trackableid column
 * @method     array findByDeliveryaddress(string $deliveryaddress) Return Notification objects filtered by the deliveryaddress column
 * @method     array findByProximity(int $proximity) Return Notification objects filtered by the proximity column
 * @method     array findByTimecreated(string $timecreated) Return Notification objects filtered by the timecreated column
 * @method     array findByTimesent(string $timesent) Return Notification objects filtered by the timesent column
 * @method     array findByCreationsent(boolean $creationsent) Return Notification objects filtered by the creationsent column
 * @method     array findByNotificationsent(boolean $notificationsent) Return Notification objects filtered by the notificationsent column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseNotificationQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseNotificationQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Notification', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new NotificationQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    NotificationQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof NotificationQuery) {
			return $criteria;
		}
		$query = new NotificationQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Notification|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = NotificationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(NotificationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Notification A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `NOTIFICATIONID`, `CUSTOMERID`, `NOTIFICATIONTYPEID`, `TRACKABLEID`, `DELIVERYADDRESS`, `PROXIMITY`, `TIMECREATED`, `TIMESENT`, `CREATIONSENT`, `NOTIFICATIONSENT` FROM `notification` WHERE `NOTIFICATIONID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Notification();
			$obj->hydrate($row);
			NotificationPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Notification|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(NotificationPeer::NOTIFICATIONID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(NotificationPeer::NOTIFICATIONID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the notificationid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByNotificationid(1234); // WHERE notificationid = 1234
	 * $query->filterByNotificationid(array(12, 34)); // WHERE notificationid IN (12, 34)
	 * $query->filterByNotificationid(array('min' => 12)); // WHERE notificationid > 12
	 * </code>
	 *
	 * @param     mixed $notificationid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByNotificationid($notificationid = null, $comparison = null)
	{
		if (is_array($notificationid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(NotificationPeer::NOTIFICATIONID, $notificationid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(NotificationPeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(NotificationPeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the notificationtypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByNotificationtypeid(1234); // WHERE notificationtypeid = 1234
	 * $query->filterByNotificationtypeid(array(12, 34)); // WHERE notificationtypeid IN (12, 34)
	 * $query->filterByNotificationtypeid(array('min' => 12)); // WHERE notificationtypeid > 12
	 * </code>
	 *
	 * @see       filterByNotificationtype()
	 *
	 * @param     mixed $notificationtypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByNotificationtypeid($notificationtypeid = null, $comparison = null)
	{
		if (is_array($notificationtypeid)) {
			$useMinMax = false;
			if (isset($notificationtypeid['min'])) {
				$this->addUsingAlias(NotificationPeer::NOTIFICATIONTYPEID, $notificationtypeid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($notificationtypeid['max'])) {
				$this->addUsingAlias(NotificationPeer::NOTIFICATIONTYPEID, $notificationtypeid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::NOTIFICATIONTYPEID, $notificationtypeid, $comparison);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @see       filterByTrackable()
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid)) {
			$useMinMax = false;
			if (isset($trackableid['min'])) {
				$this->addUsingAlias(NotificationPeer::TRACKABLEID, $trackableid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($trackableid['max'])) {
				$this->addUsingAlias(NotificationPeer::TRACKABLEID, $trackableid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query on the deliveryaddress column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByDeliveryaddress('fooValue');   // WHERE deliveryaddress = 'fooValue'
	 * $query->filterByDeliveryaddress('%fooValue%'); // WHERE deliveryaddress LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $deliveryaddress The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByDeliveryaddress($deliveryaddress = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($deliveryaddress)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $deliveryaddress)) {
				$deliveryaddress = str_replace('*', '%', $deliveryaddress);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(NotificationPeer::DELIVERYADDRESS, $deliveryaddress, $comparison);
	}

	/**
	 * Filter the query on the proximity column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByProximity(1234); // WHERE proximity = 1234
	 * $query->filterByProximity(array(12, 34)); // WHERE proximity IN (12, 34)
	 * $query->filterByProximity(array('min' => 12)); // WHERE proximity > 12
	 * </code>
	 *
	 * @param     mixed $proximity The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByProximity($proximity = null, $comparison = null)
	{
		if (is_array($proximity)) {
			$useMinMax = false;
			if (isset($proximity['min'])) {
				$this->addUsingAlias(NotificationPeer::PROXIMITY, $proximity['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($proximity['max'])) {
				$this->addUsingAlias(NotificationPeer::PROXIMITY, $proximity['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::PROXIMITY, $proximity, $comparison);
	}

	/**
	 * Filter the query on the timecreated column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTimecreated('2011-03-14'); // WHERE timecreated = '2011-03-14'
	 * $query->filterByTimecreated('now'); // WHERE timecreated = '2011-03-14'
	 * $query->filterByTimecreated(array('max' => 'yesterday')); // WHERE timecreated > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $timecreated The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByTimecreated($timecreated = null, $comparison = null)
	{
		if (is_array($timecreated)) {
			$useMinMax = false;
			if (isset($timecreated['min'])) {
				$this->addUsingAlias(NotificationPeer::TIMECREATED, $timecreated['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($timecreated['max'])) {
				$this->addUsingAlias(NotificationPeer::TIMECREATED, $timecreated['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::TIMECREATED, $timecreated, $comparison);
	}

	/**
	 * Filter the query on the timesent column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTimesent('2011-03-14'); // WHERE timesent = '2011-03-14'
	 * $query->filterByTimesent('now'); // WHERE timesent = '2011-03-14'
	 * $query->filterByTimesent(array('max' => 'yesterday')); // WHERE timesent > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $timesent The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByTimesent($timesent = null, $comparison = null)
	{
		if (is_array($timesent)) {
			$useMinMax = false;
			if (isset($timesent['min'])) {
				$this->addUsingAlias(NotificationPeer::TIMESENT, $timesent['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($timesent['max'])) {
				$this->addUsingAlias(NotificationPeer::TIMESENT, $timesent['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(NotificationPeer::TIMESENT, $timesent, $comparison);
	}

	/**
	 * Filter the query on the creationsent column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCreationsent(true); // WHERE creationsent = true
	 * $query->filterByCreationsent('yes'); // WHERE creationsent = true
	 * </code>
	 *
	 * @param     boolean|string $creationsent The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByCreationsent($creationsent = null, $comparison = null)
	{
		if (is_string($creationsent)) {
			$creationsent = in_array(strtolower($creationsent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(NotificationPeer::CREATIONSENT, $creationsent, $comparison);
	}

	/**
	 * Filter the query on the notificationsent column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByNotificationsent(true); // WHERE notificationsent = true
	 * $query->filterByNotificationsent('yes'); // WHERE notificationsent = true
	 * </code>
	 *
	 * @param     boolean|string $notificationsent The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByNotificationsent($notificationsent = null, $comparison = null)
	{
		if (is_string($notificationsent)) {
			$notificationsent = in_array(strtolower($notificationsent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(NotificationPeer::NOTIFICATIONSENT, $notificationsent, $comparison);
	}

	/**
	 * Filter the query by a related Notificationtype object
	 *
	 * @param     Notificationtype|PropelCollection $notificationtype The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByNotificationtype($notificationtype, $comparison = null)
	{
		if ($notificationtype instanceof Notificationtype) {
			return $this
				->addUsingAlias(NotificationPeer::NOTIFICATIONTYPEID, $notificationtype->getNotificationtypeid(), $comparison);
		} elseif ($notificationtype instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(NotificationPeer::NOTIFICATIONTYPEID, $notificationtype->toKeyValue('PrimaryKey', 'Notificationtypeid'), $comparison);
		} else {
			throw new PropelException('filterByNotificationtype() only accepts arguments of type Notificationtype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Notificationtype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function joinNotificationtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Notificationtype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Notificationtype');
		}

		return $this;
	}

	/**
	 * Use the Notificationtype relation Notificationtype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationtypeQuery A secondary query class using the current class as primary query
	 */
	public function useNotificationtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinNotificationtype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Notificationtype', 'NotificationtypeQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable|PropelCollection $trackable The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(NotificationPeer::TRACKABLEID, $trackable->getTrackableid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(NotificationPeer::TRACKABLEID, $trackable->toKeyValue('PrimaryKey', 'Trackableid'), $comparison);
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(NotificationPeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(NotificationPeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Notification $notification Object to remove from the list of results
	 *
	 * @return    NotificationQuery The current query, for fluid interface
	 */
	public function prune($notification = null)
	{
		if ($notification) {
			$this->addUsingAlias(NotificationPeer::NOTIFICATIONID, $notification->getNotificationid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseNotificationQuery