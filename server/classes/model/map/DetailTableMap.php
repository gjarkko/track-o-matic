<?php



/**
 * This class defines the structure of the 'detail' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class DetailTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.DetailTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('detail');
		$this->setPhpName('Detail');
		$this->setClassname('Detail');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('DETAILID', 'Detailid', 'INTEGER', true, 10, null);
		$this->addForeignKey('DETAILTYPEID', 'Detailtypeid', 'INTEGER', 'detailtype', 'DETAILTYPEID', true, 10, 1);
		$this->addForeignKey('TRACKABLEID', 'Trackableid', 'INTEGER', 'trackable', 'TRACKABLEID', true, 10, 1);
		$this->addColumn('DETAIL', 'Detail', 'LONGVARCHAR', true, null, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('DETAIL', 'minLength', 'propel.validator.MinLengthValidator', '1', 'detail must be at least 1 character');
		$this->addValidator('DETAIL', 'required', 'propel.validator.RequiredValidator', '', 'detail is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Detailtype', 'Detailtype', RelationMap::MANY_TO_ONE, array('detailtypeid' => 'detailtypeid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Trackable', 'Trackable', RelationMap::MANY_TO_ONE, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // DetailTableMap
