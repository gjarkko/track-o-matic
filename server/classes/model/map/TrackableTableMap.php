<?php



/**
 * This class defines the structure of the 'trackable' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class TrackableTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.TrackableTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('trackable');
		$this->setPhpName('Trackable');
		$this->setClassname('Trackable');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('TRACKABLEID', 'Trackableid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addForeignKey('START_ADDRESSID', 'StartAddressid', 'INTEGER', 'address', 'ADDRESSID', true, 10, 1);
		$this->addForeignKey('END_ADDRESSID', 'EndAddressid', 'INTEGER', 'address', 'ADDRESSID', true, 10, 1);
		$this->addColumn('TRACKABLEREFERENCE', 'Trackablereference', 'VARCHAR', true, 120, null);
		$this->addColumn('CREATED', 'Created', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
		$this->addColumn('EDITED', 'Edited', 'TIMESTAMP', false, null, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('TRACKABLEREFERENCE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'trackable reference must be at least 1 character');
		$this->addValidator('TRACKABLEREFERENCE', 'maxLength', 'propel.validator.MaxLengthValidator', '120', 'trackable reference must be at most 120 characters');
		$this->addValidator('TRACKABLEREFERENCE', 'required', 'propel.validator.RequiredValidator', '', 'trackable reference is required');
		$this->addValidator('TRACKABLEREFERENCE', 'match', 'propel.validator.MatchValidator', '/^([a-zA-Z0-9-\/])+$/', 'trackable reference must consist of letters, numbers or characters \'-\' and \'/\'');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('AddressRelatedByStartAddressid', 'Address', RelationMap::MANY_TO_ONE, array('start_addressid' => 'addressid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('AddressRelatedByEndAddressid', 'Address', RelationMap::MANY_TO_ONE, array('end_addressid' => 'addressid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Detail', 'Detail', RelationMap::ONE_TO_MANY, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE', 'Details');
		$this->addRelation('Groupreference', 'Groupreference', RelationMap::ONE_TO_MANY, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE', 'Groupreferences');
		$this->addRelation('Notification', 'Notification', RelationMap::ONE_TO_MANY, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE', 'Notifications');
		$this->addRelation('Trackablestatus', 'Trackablestatus', RelationMap::ONE_TO_MANY, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE', 'Trackablestatuss');
		$this->addRelation('Triptrackable', 'Triptrackable', RelationMap::ONE_TO_MANY, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE', 'Triptrackables');
	} // buildRelations()

} // TrackableTableMap
