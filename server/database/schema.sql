
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- address
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address`
(
	`addressid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`name` VARCHAR(120) NOT NULL,
	`address1` VARCHAR(60) NOT NULL,
	`address2` VARCHAR(60),
	`postalcode` VARCHAR(10) NOT NULL,
	`city` VARCHAR(60) DEFAULT '' NOT NULL,
	`countryid` int(10) unsigned DEFAULT 1 NOT NULL,
	`lat` decimal(18,12) signed,
	`lng` decimal(18,12) signed,
	PRIMARY KEY (`addressid`),
	UNIQUE INDEX `unique1` (`customerid`, `name`, `address1`, `postalcode`, `city`, `countryid`),
	INDEX `FI_talcode_ibfk_1` (`countryid`),
	CONSTRAINT `address_ibfk_1`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `postalcode_ibfk_1`
		FOREIGN KEY (`countryid`)
		REFERENCES `country` (`countryid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- country
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country`
(
	`countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`isocode` VARCHAR(2) NOT NULL,
	`countryname` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`countryid`),
	UNIQUE INDEX `isocode` (`isocode`),
	UNIQUE INDEX `countryname` (`countryname`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- customer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer`
(
	`customerid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customer` VARCHAR(60) NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`customerid`),
	UNIQUE INDEX `customer` (`customer`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- detail
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `detail`;

CREATE TABLE `detail`
(
	`detailid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`detailtypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`trackableid` int(10) unsigned DEFAULT 1 NOT NULL,
	`detail` TEXT NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`detailid`),
	INDEX `detailtypeid` (`detailtypeid`),
	INDEX `trackableid` (`trackableid`),
	CONSTRAINT `detail_ibfk_1`
		FOREIGN KEY (`detailtypeid`)
		REFERENCES `detailtype` (`detailtypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `detail_ibfk_2`
		FOREIGN KEY (`trackableid`)
		REFERENCES `trackable` (`trackableid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- detailtype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `detailtype`;

CREATE TABLE `detailtype`
(
	`detailtypeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`detailtype` VARCHAR(60) NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`detailtypeid`),
	UNIQUE INDEX `unique1` (`customerid`, `detailtype`),
	CONSTRAINT `detailtype_ibfk_1`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- groupreference
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `groupreference`;

CREATE TABLE `groupreference`
(
	`groupreferenceid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`trackableid` int(10) unsigned DEFAULT 1 NOT NULL,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`groupreference` VARCHAR(120) NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`groupreferenceid`),
	UNIQUE INDEX `unique1` (`customerid`, `trackableid`, `groupreference`),
	INDEX `trackableid` (`trackableid`),
	CONSTRAINT `groupreference_ibfk_1`
		FOREIGN KEY (`trackableid`)
		REFERENCES `trackable` (`trackableid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `groupreference_ibfk_2`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- language
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language`
(
	`languageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`language` VARCHAR(20) NOT NULL,
	`languagelocal` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`languageid`),
	UNIQUE INDEX `language` (`language`),
	UNIQUE INDEX `languagelocal` (`languagelocal`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- localization
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `localization`;

CREATE TABLE `localization`
(
	`localizationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`languageid` int(10) unsigned DEFAULT 1 NOT NULL,
	`localizationkey` VARCHAR(60) NOT NULL,
	`localizationvalue` VARCHAR(120) NOT NULL,
	PRIMARY KEY (`localizationid`),
	UNIQUE INDEX `unique1` (`languageid`, `localizationkey`),
	CONSTRAINT `localization_ibfk_1`
		FOREIGN KEY (`languageid`)
		REFERENCES `language` (`languageid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- logentry
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `logentry`;

CREATE TABLE `logentry`
(
	`logentryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`logentrytypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`userid` int(10) unsigned DEFAULT 1 NOT NULL,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`ip` VARCHAR(15),
	`sessionid` VARCHAR(30),
	`sessionage` FLOAT,
	`message` VARCHAR(128) NOT NULL,
	`referencetype` VARCHAR(128),
	`referenceid` int(10) unsigned,
	PRIMARY KEY (`logentryid`),
	INDEX `ip` (`ip`),
	INDEX `sessionid` (`sessionid`),
	INDEX `logentrytypeid` (`logentrytypeid`),
	INDEX `userid` (`userid`),
	INDEX `customerid` (`customerid`),
	CONSTRAINT `logentry_ibfk_1`
		FOREIGN KEY (`logentrytypeid`)
		REFERENCES `logentrytype` (`logentrytypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `logentry_ibfk_2`
		FOREIGN KEY (`userid`)
		REFERENCES `user` (`userid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `logentry_ibfk_3`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- logentrytype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `logentrytype`;

CREATE TABLE `logentrytype`
(
	`logentrytypeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`logentrytype` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`logentrytypeid`),
	UNIQUE INDEX `logentrytype` (`logentrytype`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- notification
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification`
(
	`notificationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`notificationtypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`trackableid` int(10) unsigned DEFAULT 1 NOT NULL,
	`deliveryaddress` VARCHAR(60) NOT NULL,
	`proximity` INTEGER(10) NOT NULL,
	`timecreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`timesent` DATETIME,
	`creationsent` tinyint(1) unsigned DEFAULT 0 NOT NULL,
	`notificationsent` tinyint(1) unsigned DEFAULT 0 NOT NULL,
	PRIMARY KEY (`notificationid`),
	UNIQUE INDEX `unique1` (`trackableid`, `deliveryaddress`),
	INDEX `FI_ification_ibfk_1` (`notificationtypeid`),
	INDEX `FI_ification_ibfk_3` (`customerid`),
	CONSTRAINT `notification_ibfk_1`
		FOREIGN KEY (`notificationtypeid`)
		REFERENCES `notificationtype` (`notificationtypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `notification_ibfk_2`
		FOREIGN KEY (`trackableid`)
		REFERENCES `trackable` (`trackableid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `notification_ibfk_3`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- notificationtype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `notificationtype`;

CREATE TABLE `notificationtype`
(
	`notificationtypeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`notificationtype` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`notificationtypeid`),
	UNIQUE INDEX `unique1` (`notificationtype`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- vehiclestatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vehiclestatus`;

CREATE TABLE `vehiclestatus`
(
	`vehiclestatusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`statustypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`vehicleid` int(10) unsigned DEFAULT 1 NOT NULL,
	`lat` decimal(18,12) signed,
	`lng` decimal(18,12) signed,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`message` TEXT,
	PRIMARY KEY (`vehiclestatusid`),
	INDEX `statustypeid` (`statustypeid`),
	INDEX `vehicleid` (`vehicleid`),
	CONSTRAINT `vehiclestatus_ibfk_1`
		FOREIGN KEY (`statustypeid`)
		REFERENCES `statustype` (`statustypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `vehiclestatus_ibfk_2`
		FOREIGN KEY (`vehicleid`)
		REFERENCES `vehicle` (`vehicleid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- trackablestatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `trackablestatus`;

CREATE TABLE `trackablestatus`
(
	`trackablestatusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`statustypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`trackableid` int(10) unsigned DEFAULT 1 NOT NULL,
	`lat` decimal(18,12) signed,
	`lng` decimal(18,12) signed,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`message` TEXT,
	PRIMARY KEY (`trackablestatusid`),
	INDEX `statustypeid` (`statustypeid`),
	INDEX `trackableid` (`trackableid`),
	CONSTRAINT `trackablestatus_ibfk_1`
		FOREIGN KEY (`statustypeid`)
		REFERENCES `statustype` (`statustypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `trackablestatus_ibfk_2`
		FOREIGN KEY (`trackableid`)
		REFERENCES `trackable` (`trackableid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- statustype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `statustype`;

CREATE TABLE `statustype`
(
	`statustypeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`statustype` VARCHAR(60) NOT NULL,
	`ismoving` tinyint(1) unsigned DEFAULT 0 NOT NULL,
	`haslocation` tinyint(1) unsigned DEFAULT 0 NOT NULL,
	`isfinal` tinyint(1) unsigned DEFAULT 0 NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`statustypeid`),
	UNIQUE INDEX `unique1` (`customerid`, `statustype`),
	CONSTRAINT `statustype_ibfk_1`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- trackable
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `trackable`;

CREATE TABLE `trackable`
(
	`trackableid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`start_addressid` int(10) unsigned DEFAULT 1 NOT NULL,
	`end_addressid` int(10) unsigned DEFAULT 1 NOT NULL,
	`trackablereference` VARCHAR(120) NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`edited` DATETIME,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`trackableid`),
	UNIQUE INDEX `unique1` (`customerid`, `trackablereference`),
	INDEX `start_addressid` (`start_addressid`),
	INDEX `end_addressid` (`end_addressid`),
	CONSTRAINT `trackable_ibfk_1`
		FOREIGN KEY (`start_addressid`)
		REFERENCES `address` (`addressid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `trackable_ibfk_2`
		FOREIGN KEY (`end_addressid`)
		REFERENCES `address` (`addressid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `trackable_ibfk_3`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- trip
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `trip`;

CREATE TABLE `trip`
(
	`tripid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`tripreference` VARCHAR(60) NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`tripid`),
	UNIQUE INDEX `unique1` (`customerid`, `tripreference`),
	CONSTRAINT `trip_ibfk_1`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- triptrackable
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `triptrackable`;

CREATE TABLE `triptrackable`
(
	`triptrackableid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`tripid` int(10) unsigned DEFAULT 1 NOT NULL,
	`trackableid` int(10) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`triptrackableid`),
	UNIQUE INDEX `unique1` (`tripid`, `trackableid`),
	INDEX `trackableid` (`trackableid`),
	CONSTRAINT `triptrackable_ibfk_1`
		FOREIGN KEY (`tripid`)
		REFERENCES `trip` (`tripid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `triptrackable_ibfk_2`
		FOREIGN KEY (`trackableid`)
		REFERENCES `trackable` (`trackableid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tripvehicle
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tripvehicle`;

CREATE TABLE `tripvehicle`
(
	`tripvehicleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`tripid` int(10) unsigned DEFAULT 1 NOT NULL,
	`vehicleid` int(10) unsigned DEFAULT 1 NOT NULL,
	`intime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`outtime` DATETIME,
	PRIMARY KEY (`tripvehicleid`),
	UNIQUE INDEX `unique1` (`tripid`, `vehicleid`, `intime`, `outtime`),
	INDEX `vehicleid` (`vehicleid`),
	CONSTRAINT `tripvehicle_ibfk_1`
		FOREIGN KEY (`tripid`)
		REFERENCES `trip` (`tripid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `tripvehicle_ibfk_2`
		FOREIGN KEY (`vehicleid`)
		REFERENCES `vehicle` (`vehicleid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
	`userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`usertypeid` int(10) unsigned DEFAULT 1 NOT NULL,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`languageid` int(10) unsigned DEFAULT 1 NOT NULL,
	`username` VARCHAR(80) NOT NULL,
	`firstname` VARCHAR(60) NOT NULL,
	`lastname` VARCHAR(60),
	`email` VARCHAR(80) NOT NULL,
	`password` VARCHAR(160),
	`apikey` VARCHAR(44),
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`userid`),
	UNIQUE INDEX `username` (`username`),
	UNIQUE INDEX `email` (`email`),
	UNIQUE INDEX `apikey` (`apikey`),
	INDEX `usertypeid` (`usertypeid`),
	INDEX `customerid` (`customerid`),
	INDEX `languageid` (`languageid`),
	CONSTRAINT `user_ibfk_1`
		FOREIGN KEY (`usertypeid`)
		REFERENCES `usertype` (`usertypeid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `user_ibfk_2`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `user_ibfk_3`
		FOREIGN KEY (`languageid`)
		REFERENCES `language` (`languageid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usertype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usertype`;

CREATE TABLE `usertype`
(
	`usertypeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`usertype` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`usertypeid`),
	UNIQUE INDEX `usertype` (`usertype`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- vehicle
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vehicle`;

CREATE TABLE `vehicle`
(
	`vehicleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`customerid` int(10) unsigned DEFAULT 1 NOT NULL,
	`vehiclereference` VARCHAR(30) NOT NULL,
	`apikey` VARCHAR(44) NOT NULL,
	`valid` tinyint(1) unsigned DEFAULT 1 NOT NULL,
	PRIMARY KEY (`vehicleid`),
	UNIQUE INDEX `unique1` (`customerid`, `vehiclereference`),
	UNIQUE INDEX `apikey` (`apikey`),
	CONSTRAINT `vehicle_ibfk_1`
		FOREIGN KEY (`customerid`)
		REFERENCES `customer` (`customerid`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
