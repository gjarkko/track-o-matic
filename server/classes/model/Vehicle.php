<?php



/**
 * Skeleton subclass for representing a row from the 'vehicle' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Vehicle extends BaseVehicle {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
		
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
			),
			array(
				'column' => 'vehiclereference',
				'label' => ucfirst(Localizer::getText('reference')),
			),
		);
	}
	
		
	public static function getCrudMap($user = NULL) {
		return array(
		
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'vehiclereference',
				'label' => ucfirst(Localizer::getText('vehicle reference')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'apikey',
				'label' => ucfirst(Localizer::getText('API key')),
				'inputType' => 'show',
				'default' => '',
				'getMethod' => 'getDecryptedApikey',
			),
		);
	}
	
	private function setDecryptedApikey($value) {
		$value = trim($value);
		if(strlen($value) > 0) {
			$this->setApikey(CryptographyProvider::encrypt($value));
		}
		else {
			$this->setApikey('');
		}
	}
	
	public function getDecryptedApikey() {
		if (NULL === $this->getApikey()) {
			$this->setApikey(CryptographyProvider::encrypt(Tools::getApiKey())); 
		}
		return CryptographyProvider::decrypt($this->getApikey());
	}
		
	public function removeTripvehicles() {
		$sql= "
			UPDATE 
				tripvehicle
			SET
				outtime = CURRENT_TIMESTAMP
			WHERE
				vehicleid = :vehicleid
				AND outtime IS NULL
		";
		
		$con = Propel::getConnection();
		$stmt = $con->prepare($sql);
		$stmt->bindValue(':vehicleid', $this->getPrimaryKey(), PDO::PARAM_INT);
		$stmt->execute();
	}
	
	public function getCurrentTrackables() {
		
		// Find trackables currently on the vehicle's active trip.
		// Exclude dropped trackables.
		$sql= "
		 SELECT
		  t.*
		 FROM
		  tripvehicle tv
		  JOIN triptrackable tt ON
		   tv.tripid = tt.tripid
		  JOIN trackable t
		   ON tt.trackableid = t.trackableid
		  LEFT JOIN trackablestatus ts ON
		   t.trackableid = ts.trackableid
		  JOIN statustype st ON
		   st.statustypeid = ts.statustypeid
		 WHERE
		  tv.vehicleid = :vehicleid
		  AND tv.outtime IS NULL
		  AND (
		   ts.trackablestatusid IS NULL
		   OR st.isfinal = 0
		  )
		  AND ts.trackablestatusid = (
		   SELECT
		     max(ts.trackablestatusid)
		   FROM
		    tripvehicle tv
		    JOIN triptrackable tt ON
		     tv.tripid = tt.tripid
		    LEFT JOIN trackablestatus ts ON
		     tt.trackableid = ts.trackableid
		    LEFT JOIN statustype st ON
		     st.statustypeid = ts.statustypeid AND st.isfinal = 0
		   WHERE
		    tv.vehicleid = :vehicleid
		    AND tv.outtime IS NULL
		    AND (
		     ts.trackablestatusid IS NULL
		     OR st.statustypeid IS NOT NULL
		    )
		  );
		";
		
		$con = Propel::getConnection();
		$stmt = $con->prepare($sql);
		$stmt->bindValue(':vehicleid', $this->getPrimaryKey(), PDO::PARAM_INT);
		$stmt->execute();
		
		$trackables = TrackablePeer::populateObjects($stmt);
		if (count($trackables) == 0) {
			return array();
		}
		else {
			return $trackables;
		}
	}
	
	public function getLastStatus() {
		$sql= "
		  SELECT
	        vs.*
	      FROM
	        vehiclestatus vs 
	      WHERE
	        vs.vehicleid = :vehicleid
	      ORDER BY
	        vs.time DESC
	      LIMIT 1
		";
				
		$con = Propel::getConnection();
		$stmt = $con->prepare($sql);
		$stmt->bindValue(':vehicleid', $this->getPrimaryKey(), PDO::PARAM_INT);
		$stmt->execute();
		
		$statuses = VehiclestatusPeer::populateObjects($stmt);
		if (count($statuses) == 0) {
			return;
		}
		else {
			return $statuses[0];
		}
	}
	
} // Vehicle
