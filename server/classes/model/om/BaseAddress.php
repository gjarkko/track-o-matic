<?php


/**
 * Base class that represents a row from the 'address' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseAddress extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'AddressPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        AddressPeer
	 */
	protected static $peer;

	/**
	 * The value for the addressid field.
	 * @var        int
	 */
	protected $addressid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the name field.
	 * @var        string
	 */
	protected $name;

	/**
	 * The value for the address1 field.
	 * @var        string
	 */
	protected $address1;

	/**
	 * The value for the address2 field.
	 * @var        string
	 */
	protected $address2;

	/**
	 * The value for the postalcode field.
	 * @var        string
	 */
	protected $postalcode;

	/**
	 * The value for the city field.
	 * Note: this column has a database default value of: ''
	 * @var        string
	 */
	protected $city;

	/**
	 * The value for the countryid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $countryid;

	/**
	 * The value for the lat field.
	 * @var        string
	 */
	protected $lat;

	/**
	 * The value for the lng field.
	 * @var        string
	 */
	protected $lng;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * @var        Country
	 */
	protected $aCountry;

	/**
	 * @var        array Trackable[] Collection to store aggregation of Trackable objects.
	 */
	protected $collTrackablesRelatedByStartAddressid;

	/**
	 * @var        array Trackable[] Collection to store aggregation of Trackable objects.
	 */
	protected $collTrackablesRelatedByEndAddressid;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $trackablesRelatedByStartAddressidScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $trackablesRelatedByEndAddressidScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->customerid = 1;
		$this->city = '';
		$this->countryid = 1;
	}

	/**
	 * Initializes internal state of BaseAddress object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [addressid] column value.
	 * 
	 * @return     int
	 */
	public function getAddressid()
	{
		return $this->addressid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [name] column value.
	 * 
	 * @return     string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get the [address1] column value.
	 * 
	 * @return     string
	 */
	public function getAddress1()
	{
		return $this->address1;
	}

	/**
	 * Get the [address2] column value.
	 * 
	 * @return     string
	 */
	public function getAddress2()
	{
		return $this->address2;
	}

	/**
	 * Get the [postalcode] column value.
	 * 
	 * @return     string
	 */
	public function getPostalcode()
	{
		return $this->postalcode;
	}

	/**
	 * Get the [city] column value.
	 * 
	 * @return     string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Get the [countryid] column value.
	 * 
	 * @return     int
	 */
	public function getCountryid()
	{
		return $this->countryid;
	}

	/**
	 * Get the [lat] column value.
	 * 
	 * @return     string
	 */
	public function getLat()
	{
		return $this->lat;
	}

	/**
	 * Get the [lng] column value.
	 * 
	 * @return     string
	 */
	public function getLng()
	{
		return $this->lng;
	}

	/**
	 * Set the value of [addressid] column.
	 * 
	 * @param      int $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setAddressid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->addressid !== $v) {
			$this->addressid = $v;
			$this->modifiedColumns[] = AddressPeer::ADDRESSID;
		}

		return $this;
	} // setAddressid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = AddressPeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [name] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = AddressPeer::NAME;
		}

		return $this;
	} // setName()

	/**
	 * Set the value of [address1] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setAddress1($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->address1 !== $v) {
			$this->address1 = $v;
			$this->modifiedColumns[] = AddressPeer::ADDRESS1;
		}

		return $this;
	} // setAddress1()

	/**
	 * Set the value of [address2] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setAddress2($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->address2 !== $v) {
			$this->address2 = $v;
			$this->modifiedColumns[] = AddressPeer::ADDRESS2;
		}

		return $this;
	} // setAddress2()

	/**
	 * Set the value of [postalcode] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setPostalcode($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->postalcode !== $v) {
			$this->postalcode = $v;
			$this->modifiedColumns[] = AddressPeer::POSTALCODE;
		}

		return $this;
	} // setPostalcode()

	/**
	 * Set the value of [city] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setCity($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->city !== $v) {
			$this->city = $v;
			$this->modifiedColumns[] = AddressPeer::CITY;
		}

		return $this;
	} // setCity()

	/**
	 * Set the value of [countryid] column.
	 * 
	 * @param      int $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setCountryid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->countryid !== $v) {
			$this->countryid = $v;
			$this->modifiedColumns[] = AddressPeer::COUNTRYID;
		}

		if ($this->aCountry !== null && $this->aCountry->getCountryid() !== $v) {
			$this->aCountry = null;
		}

		return $this;
	} // setCountryid()

	/**
	 * Set the value of [lat] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setLat($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lat !== $v) {
			$this->lat = $v;
			$this->modifiedColumns[] = AddressPeer::LAT;
		}

		return $this;
	} // setLat()

	/**
	 * Set the value of [lng] column.
	 * 
	 * @param      string $v new value
	 * @return     Address The current object (for fluent API support)
	 */
	public function setLng($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lng !== $v) {
			$this->lng = $v;
			$this->modifiedColumns[] = AddressPeer::LNG;
		}

		return $this;
	} // setLng()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->customerid !== 1) {
				return false;
			}

			if ($this->city !== '') {
				return false;
			}

			if ($this->countryid !== 1) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->addressid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->customerid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->address1 = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->address2 = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->postalcode = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->city = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->countryid = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
			$this->lat = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->lng = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 10; // 10 = AddressPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Address object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
		if ($this->aCountry !== null && $this->countryid !== $this->aCountry->getCountryid()) {
			$this->aCountry = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AddressPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = AddressPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aCustomer = null;
			$this->aCountry = null;
			$this->collTrackablesRelatedByStartAddressid = null;

			$this->collTrackablesRelatedByEndAddressid = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AddressPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = AddressQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AddressPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				AddressPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified() || $this->aCountry->isNew()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->trackablesRelatedByStartAddressidScheduledForDeletion !== null) {
				if (!$this->trackablesRelatedByStartAddressidScheduledForDeletion->isEmpty()) {
					TrackableQuery::create()
						->filterByPrimaryKeys($this->trackablesRelatedByStartAddressidScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->trackablesRelatedByStartAddressidScheduledForDeletion = null;
				}
			}

			if ($this->collTrackablesRelatedByStartAddressid !== null) {
				foreach ($this->collTrackablesRelatedByStartAddressid as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->trackablesRelatedByEndAddressidScheduledForDeletion !== null) {
				if (!$this->trackablesRelatedByEndAddressidScheduledForDeletion->isEmpty()) {
					TrackableQuery::create()
						->filterByPrimaryKeys($this->trackablesRelatedByEndAddressidScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->trackablesRelatedByEndAddressidScheduledForDeletion = null;
				}
			}

			if ($this->collTrackablesRelatedByEndAddressid !== null) {
				foreach ($this->collTrackablesRelatedByEndAddressid as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = AddressPeer::ADDRESSID;
		if (null !== $this->addressid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . AddressPeer::ADDRESSID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(AddressPeer::ADDRESSID)) {
			$modifiedColumns[':p' . $index++]  = '`ADDRESSID`';
		}
		if ($this->isColumnModified(AddressPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(AddressPeer::NAME)) {
			$modifiedColumns[':p' . $index++]  = '`NAME`';
		}
		if ($this->isColumnModified(AddressPeer::ADDRESS1)) {
			$modifiedColumns[':p' . $index++]  = '`ADDRESS1`';
		}
		if ($this->isColumnModified(AddressPeer::ADDRESS2)) {
			$modifiedColumns[':p' . $index++]  = '`ADDRESS2`';
		}
		if ($this->isColumnModified(AddressPeer::POSTALCODE)) {
			$modifiedColumns[':p' . $index++]  = '`POSTALCODE`';
		}
		if ($this->isColumnModified(AddressPeer::CITY)) {
			$modifiedColumns[':p' . $index++]  = '`CITY`';
		}
		if ($this->isColumnModified(AddressPeer::COUNTRYID)) {
			$modifiedColumns[':p' . $index++]  = '`COUNTRYID`';
		}
		if ($this->isColumnModified(AddressPeer::LAT)) {
			$modifiedColumns[':p' . $index++]  = '`LAT`';
		}
		if ($this->isColumnModified(AddressPeer::LNG)) {
			$modifiedColumns[':p' . $index++]  = '`LNG`';
		}

		$sql = sprintf(
			'INSERT INTO `address` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`ADDRESSID`':
						$stmt->bindValue($identifier, $this->addressid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`NAME`':
						$stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
						break;
					case '`ADDRESS1`':
						$stmt->bindValue($identifier, $this->address1, PDO::PARAM_STR);
						break;
					case '`ADDRESS2`':
						$stmt->bindValue($identifier, $this->address2, PDO::PARAM_STR);
						break;
					case '`POSTALCODE`':
						$stmt->bindValue($identifier, $this->postalcode, PDO::PARAM_STR);
						break;
					case '`CITY`':
						$stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
						break;
					case '`COUNTRYID`':
						$stmt->bindValue($identifier, $this->countryid, PDO::PARAM_INT);
						break;
					case '`LAT`':
						$stmt->bindValue($identifier, $this->lat, PDO::PARAM_STR);
						break;
					case '`LNG`':
						$stmt->bindValue($identifier, $this->lng, PDO::PARAM_STR);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setAddressid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}

			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}


			if (($retval = AddressPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTrackablesRelatedByStartAddressid !== null) {
					foreach ($this->collTrackablesRelatedByStartAddressid as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTrackablesRelatedByEndAddressid !== null) {
					foreach ($this->collTrackablesRelatedByEndAddressid as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AddressPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getAddressid();
				break;
			case 1:
				return $this->getCustomerid();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getAddress1();
				break;
			case 4:
				return $this->getAddress2();
				break;
			case 5:
				return $this->getPostalcode();
				break;
			case 6:
				return $this->getCity();
				break;
			case 7:
				return $this->getCountryid();
				break;
			case 8:
				return $this->getLat();
				break;
			case 9:
				return $this->getLng();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Address'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Address'][$this->getPrimaryKey()] = true;
		$keys = AddressPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getAddressid(),
			$keys[1] => $this->getCustomerid(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getAddress1(),
			$keys[4] => $this->getAddress2(),
			$keys[5] => $this->getPostalcode(),
			$keys[6] => $this->getCity(),
			$keys[7] => $this->getCountryid(),
			$keys[8] => $this->getLat(),
			$keys[9] => $this->getLng(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aCountry) {
				$result['Country'] = $this->aCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->collTrackablesRelatedByStartAddressid) {
				$result['TrackablesRelatedByStartAddressid'] = $this->collTrackablesRelatedByStartAddressid->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTrackablesRelatedByEndAddressid) {
				$result['TrackablesRelatedByEndAddressid'] = $this->collTrackablesRelatedByEndAddressid->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AddressPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setAddressid($value);
				break;
			case 1:
				$this->setCustomerid($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setAddress1($value);
				break;
			case 4:
				$this->setAddress2($value);
				break;
			case 5:
				$this->setPostalcode($value);
				break;
			case 6:
				$this->setCity($value);
				break;
			case 7:
				$this->setCountryid($value);
				break;
			case 8:
				$this->setLat($value);
				break;
			case 9:
				$this->setLng($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AddressPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setAddressid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCustomerid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setAddress1($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setAddress2($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPostalcode($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCity($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCountryid($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setLat($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setLng($arr[$keys[9]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(AddressPeer::DATABASE_NAME);

		if ($this->isColumnModified(AddressPeer::ADDRESSID)) $criteria->add(AddressPeer::ADDRESSID, $this->addressid);
		if ($this->isColumnModified(AddressPeer::CUSTOMERID)) $criteria->add(AddressPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(AddressPeer::NAME)) $criteria->add(AddressPeer::NAME, $this->name);
		if ($this->isColumnModified(AddressPeer::ADDRESS1)) $criteria->add(AddressPeer::ADDRESS1, $this->address1);
		if ($this->isColumnModified(AddressPeer::ADDRESS2)) $criteria->add(AddressPeer::ADDRESS2, $this->address2);
		if ($this->isColumnModified(AddressPeer::POSTALCODE)) $criteria->add(AddressPeer::POSTALCODE, $this->postalcode);
		if ($this->isColumnModified(AddressPeer::CITY)) $criteria->add(AddressPeer::CITY, $this->city);
		if ($this->isColumnModified(AddressPeer::COUNTRYID)) $criteria->add(AddressPeer::COUNTRYID, $this->countryid);
		if ($this->isColumnModified(AddressPeer::LAT)) $criteria->add(AddressPeer::LAT, $this->lat);
		if ($this->isColumnModified(AddressPeer::LNG)) $criteria->add(AddressPeer::LNG, $this->lng);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AddressPeer::DATABASE_NAME);
		$criteria->add(AddressPeer::ADDRESSID, $this->addressid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getAddressid();
	}

	/**
	 * Generic method to set the primary key (addressid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setAddressid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getAddressid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Address (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setName($this->getName());
		$copyObj->setAddress1($this->getAddress1());
		$copyObj->setAddress2($this->getAddress2());
		$copyObj->setPostalcode($this->getPostalcode());
		$copyObj->setCity($this->getCity());
		$copyObj->setCountryid($this->getCountryid());
		$copyObj->setLat($this->getLat());
		$copyObj->setLng($this->getLng());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getTrackablesRelatedByStartAddressid() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTrackableRelatedByStartAddressid($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTrackablesRelatedByEndAddressid() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTrackableRelatedByEndAddressid($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setAddressid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Address Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     AddressPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AddressPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     Address The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addAddress($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addAddresss($this);
			 */
		}
		return $this->aCustomer;
	}

	/**
	 * Declares an association between this object and a Country object.
	 *
	 * @param      Country $v
	 * @return     Address The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCountry(Country $v = null)
	{
		if ($v === null) {
			$this->setCountryid(1);
		} else {
			$this->setCountryid($v->getCountryid());
		}

		$this->aCountry = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Country object, it will not be re-added.
		if ($v !== null) {
			$v->addAddress($this);
		}

		return $this;
	}


	/**
	 * Get the associated Country object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Country The associated Country object.
	 * @throws     PropelException
	 */
	public function getCountry(PropelPDO $con = null)
	{
		if ($this->aCountry === null && ($this->countryid !== null)) {
			$this->aCountry = CountryQuery::create()->findPk($this->countryid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCountry->addAddresss($this);
			 */
		}
		return $this->aCountry;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('TrackableRelatedByStartAddressid' == $relationName) {
			return $this->initTrackablesRelatedByStartAddressid();
		}
		if ('TrackableRelatedByEndAddressid' == $relationName) {
			return $this->initTrackablesRelatedByEndAddressid();
		}
	}

	/**
	 * Clears out the collTrackablesRelatedByStartAddressid collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTrackablesRelatedByStartAddressid()
	 */
	public function clearTrackablesRelatedByStartAddressid()
	{
		$this->collTrackablesRelatedByStartAddressid = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTrackablesRelatedByStartAddressid collection.
	 *
	 * By default this just sets the collTrackablesRelatedByStartAddressid collection to an empty array (like clearcollTrackablesRelatedByStartAddressid());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTrackablesRelatedByStartAddressid($overrideExisting = true)
	{
		if (null !== $this->collTrackablesRelatedByStartAddressid && !$overrideExisting) {
			return;
		}
		$this->collTrackablesRelatedByStartAddressid = new PropelObjectCollection();
		$this->collTrackablesRelatedByStartAddressid->setModel('Trackable');
	}

	/**
	 * Gets an array of Trackable objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Address is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 * @throws     PropelException
	 */
	public function getTrackablesRelatedByStartAddressid($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTrackablesRelatedByStartAddressid || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablesRelatedByStartAddressid) {
				// return empty collection
				$this->initTrackablesRelatedByStartAddressid();
			} else {
				$collTrackablesRelatedByStartAddressid = TrackableQuery::create(null, $criteria)
					->filterByAddressRelatedByStartAddressid($this)
					->find($con);
				if (null !== $criteria) {
					return $collTrackablesRelatedByStartAddressid;
				}
				$this->collTrackablesRelatedByStartAddressid = $collTrackablesRelatedByStartAddressid;
			}
		}
		return $this->collTrackablesRelatedByStartAddressid;
	}

	/**
	 * Sets a collection of TrackableRelatedByStartAddressid objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $trackablesRelatedByStartAddressid A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTrackablesRelatedByStartAddressid(PropelCollection $trackablesRelatedByStartAddressid, PropelPDO $con = null)
	{
		$this->trackablesRelatedByStartAddressidScheduledForDeletion = $this->getTrackablesRelatedByStartAddressid(new Criteria(), $con)->diff($trackablesRelatedByStartAddressid);

		foreach ($trackablesRelatedByStartAddressid as $trackableRelatedByStartAddressid) {
			// Fix issue with collection modified by reference
			if ($trackableRelatedByStartAddressid->isNew()) {
				$trackableRelatedByStartAddressid->setAddressRelatedByStartAddressid($this);
			}
			$this->addTrackableRelatedByStartAddressid($trackableRelatedByStartAddressid);
		}

		$this->collTrackablesRelatedByStartAddressid = $trackablesRelatedByStartAddressid;
	}

	/**
	 * Returns the number of related Trackable objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Trackable objects.
	 * @throws     PropelException
	 */
	public function countTrackablesRelatedByStartAddressid(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTrackablesRelatedByStartAddressid || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablesRelatedByStartAddressid) {
				return 0;
			} else {
				$query = TrackableQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByAddressRelatedByStartAddressid($this)
					->count($con);
			}
		} else {
			return count($this->collTrackablesRelatedByStartAddressid);
		}
	}

	/**
	 * Method called to associate a Trackable object to this object
	 * through the Trackable foreign key attribute.
	 *
	 * @param      Trackable $l Trackable
	 * @return     Address The current object (for fluent API support)
	 */
	public function addTrackableRelatedByStartAddressid(Trackable $l)
	{
		if ($this->collTrackablesRelatedByStartAddressid === null) {
			$this->initTrackablesRelatedByStartAddressid();
		}
		if (!$this->collTrackablesRelatedByStartAddressid->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTrackableRelatedByStartAddressid($l);
		}

		return $this;
	}

	/**
	 * @param	TrackableRelatedByStartAddressid $trackableRelatedByStartAddressid The trackableRelatedByStartAddressid object to add.
	 */
	protected function doAddTrackableRelatedByStartAddressid($trackableRelatedByStartAddressid)
	{
		$this->collTrackablesRelatedByStartAddressid[]= $trackableRelatedByStartAddressid;
		$trackableRelatedByStartAddressid->setAddressRelatedByStartAddressid($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Address is new, it will return
	 * an empty collection; or if this Address has previously
	 * been saved, it will retrieve related TrackablesRelatedByStartAddressid from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Address.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 */
	public function getTrackablesRelatedByStartAddressidJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TrackableQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getTrackablesRelatedByStartAddressid($query, $con);
	}

	/**
	 * Clears out the collTrackablesRelatedByEndAddressid collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTrackablesRelatedByEndAddressid()
	 */
	public function clearTrackablesRelatedByEndAddressid()
	{
		$this->collTrackablesRelatedByEndAddressid = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTrackablesRelatedByEndAddressid collection.
	 *
	 * By default this just sets the collTrackablesRelatedByEndAddressid collection to an empty array (like clearcollTrackablesRelatedByEndAddressid());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTrackablesRelatedByEndAddressid($overrideExisting = true)
	{
		if (null !== $this->collTrackablesRelatedByEndAddressid && !$overrideExisting) {
			return;
		}
		$this->collTrackablesRelatedByEndAddressid = new PropelObjectCollection();
		$this->collTrackablesRelatedByEndAddressid->setModel('Trackable');
	}

	/**
	 * Gets an array of Trackable objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Address is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 * @throws     PropelException
	 */
	public function getTrackablesRelatedByEndAddressid($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTrackablesRelatedByEndAddressid || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablesRelatedByEndAddressid) {
				// return empty collection
				$this->initTrackablesRelatedByEndAddressid();
			} else {
				$collTrackablesRelatedByEndAddressid = TrackableQuery::create(null, $criteria)
					->filterByAddressRelatedByEndAddressid($this)
					->find($con);
				if (null !== $criteria) {
					return $collTrackablesRelatedByEndAddressid;
				}
				$this->collTrackablesRelatedByEndAddressid = $collTrackablesRelatedByEndAddressid;
			}
		}
		return $this->collTrackablesRelatedByEndAddressid;
	}

	/**
	 * Sets a collection of TrackableRelatedByEndAddressid objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $trackablesRelatedByEndAddressid A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTrackablesRelatedByEndAddressid(PropelCollection $trackablesRelatedByEndAddressid, PropelPDO $con = null)
	{
		$this->trackablesRelatedByEndAddressidScheduledForDeletion = $this->getTrackablesRelatedByEndAddressid(new Criteria(), $con)->diff($trackablesRelatedByEndAddressid);

		foreach ($trackablesRelatedByEndAddressid as $trackableRelatedByEndAddressid) {
			// Fix issue with collection modified by reference
			if ($trackableRelatedByEndAddressid->isNew()) {
				$trackableRelatedByEndAddressid->setAddressRelatedByEndAddressid($this);
			}
			$this->addTrackableRelatedByEndAddressid($trackableRelatedByEndAddressid);
		}

		$this->collTrackablesRelatedByEndAddressid = $trackablesRelatedByEndAddressid;
	}

	/**
	 * Returns the number of related Trackable objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Trackable objects.
	 * @throws     PropelException
	 */
	public function countTrackablesRelatedByEndAddressid(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTrackablesRelatedByEndAddressid || null !== $criteria) {
			if ($this->isNew() && null === $this->collTrackablesRelatedByEndAddressid) {
				return 0;
			} else {
				$query = TrackableQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByAddressRelatedByEndAddressid($this)
					->count($con);
			}
		} else {
			return count($this->collTrackablesRelatedByEndAddressid);
		}
	}

	/**
	 * Method called to associate a Trackable object to this object
	 * through the Trackable foreign key attribute.
	 *
	 * @param      Trackable $l Trackable
	 * @return     Address The current object (for fluent API support)
	 */
	public function addTrackableRelatedByEndAddressid(Trackable $l)
	{
		if ($this->collTrackablesRelatedByEndAddressid === null) {
			$this->initTrackablesRelatedByEndAddressid();
		}
		if (!$this->collTrackablesRelatedByEndAddressid->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTrackableRelatedByEndAddressid($l);
		}

		return $this;
	}

	/**
	 * @param	TrackableRelatedByEndAddressid $trackableRelatedByEndAddressid The trackableRelatedByEndAddressid object to add.
	 */
	protected function doAddTrackableRelatedByEndAddressid($trackableRelatedByEndAddressid)
	{
		$this->collTrackablesRelatedByEndAddressid[]= $trackableRelatedByEndAddressid;
		$trackableRelatedByEndAddressid->setAddressRelatedByEndAddressid($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Address is new, it will return
	 * an empty collection; or if this Address has previously
	 * been saved, it will retrieve related TrackablesRelatedByEndAddressid from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Address.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Trackable[] List of Trackable objects
	 */
	public function getTrackablesRelatedByEndAddressidJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TrackableQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getTrackablesRelatedByEndAddressid($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->addressid = null;
		$this->customerid = null;
		$this->name = null;
		$this->address1 = null;
		$this->address2 = null;
		$this->postalcode = null;
		$this->city = null;
		$this->countryid = null;
		$this->lat = null;
		$this->lng = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collTrackablesRelatedByStartAddressid) {
				foreach ($this->collTrackablesRelatedByStartAddressid as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTrackablesRelatedByEndAddressid) {
				foreach ($this->collTrackablesRelatedByEndAddressid as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collTrackablesRelatedByStartAddressid instanceof PropelCollection) {
			$this->collTrackablesRelatedByStartAddressid->clearIterator();
		}
		$this->collTrackablesRelatedByStartAddressid = null;
		if ($this->collTrackablesRelatedByEndAddressid instanceof PropelCollection) {
			$this->collTrackablesRelatedByEndAddressid->clearIterator();
		}
		$this->collTrackablesRelatedByEndAddressid = null;
		$this->aCustomer = null;
		$this->aCountry = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(AddressPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseAddress
