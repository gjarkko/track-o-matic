<?php



/**
 * This class defines the structure of the 'vehicle' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class VehicleTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.VehicleTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('vehicle');
		$this->setPhpName('Vehicle');
		$this->setClassname('Vehicle');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('VEHICLEID', 'Vehicleid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('VEHICLEREFERENCE', 'Vehiclereference', 'VARCHAR', true, 30, null);
		$this->addColumn('APIKEY', 'Apikey', 'VARCHAR', true, 44, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('VEHICLEREFERENCE', 'minLength', 'propel.validator.MinLengthValidator', '3', 'vehicle reference must be at least 3 characters');
		$this->addValidator('VEHICLEREFERENCE', 'maxLength', 'propel.validator.MaxLengthValidator', '30', 'vehicle reference must be at most 30 characters');
		$this->addValidator('VEHICLEREFERENCE', 'required', 'propel.validator.RequiredValidator', '', 'vehicle reference is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Vehiclestatus', 'Vehiclestatus', RelationMap::ONE_TO_MANY, array('vehicleid' => 'vehicleid', ), 'CASCADE', 'CASCADE', 'Vehiclestatuss');
		$this->addRelation('Tripvehicle', 'Tripvehicle', RelationMap::ONE_TO_MANY, array('vehicleid' => 'vehicleid', ), 'CASCADE', 'CASCADE', 'Tripvehicles');
	} // buildRelations()

} // VehicleTableMap
