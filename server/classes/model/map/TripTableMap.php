<?php



/**
 * This class defines the structure of the 'trip' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class TripTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.TripTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('trip');
		$this->setPhpName('Trip');
		$this->setClassname('Trip');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('TRIPID', 'Tripid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('TRIPREFERENCE', 'Tripreference', 'VARCHAR', true, 60, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('TRIPREFERENCE', 'minLength', 'propel.validator.MinLengthValidator', '5', 'trip reference must be at least 5 characters');
		$this->addValidator('TRIPREFERENCE', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'trip reference must be at most 60 characters');
		$this->addValidator('TRIPREFERENCE', 'required', 'propel.validator.RequiredValidator', '', 'trip reference is required');
		$this->addValidator('TRIPREFERENCE', 'match', 'propel.validator.MatchValidator', '/^([a-zA-Z0-9-\/])+$/', 'trip reference must consist of letters, numbers or characters \'-\' and \'/\'');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Triptrackable', 'Triptrackable', RelationMap::ONE_TO_MANY, array('tripid' => 'tripid', ), 'CASCADE', 'CASCADE', 'Triptrackables');
		$this->addRelation('Tripvehicle', 'Tripvehicle', RelationMap::ONE_TO_MANY, array('tripid' => 'tripid', ), 'CASCADE', 'CASCADE', 'Tripvehicles');
	} // buildRelations()

} // TripTableMap
