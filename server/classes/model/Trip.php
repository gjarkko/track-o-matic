<?php



/**
 * Skeleton subclass for representing a row from the 'trip' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Trip extends BaseTrip {

	public static function getBrowserConfig() {
		return array(
				'allowView' => true,
				'allowEdit' => false,
				'allowCreate' => true,
				'allowDelete' => false,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'tripreference',
				'label' => ucfirst(Localizer::getText('reference')),
			),
		);
	}
	
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'tripreference',
				'label' => ucfirst(Localizer::getText('trip reference')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
	
	public function save(PropelPDO $con = null) {
	
		// If creating a new address, use the customer the user has been bound to.
		if (!$this->getPrimaryKey()) {
			if (Session::getInstance()->getUser()) {
				$this->setCustomerId(Session::getInstance()->getUser()->getCustomerid());
			}
		}
	
		parent::save($con);
	}
	
} // Trip
