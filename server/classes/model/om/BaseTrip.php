<?php


/**
 * Base class that represents a row from the 'trip' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTrip extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'TripPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        TripPeer
	 */
	protected static $peer;

	/**
	 * The value for the tripid field.
	 * @var        int
	 */
	protected $tripid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the tripreference field.
	 * @var        string
	 */
	protected $tripreference;

	/**
	 * The value for the valid field.
	 * Note: this column has a database default value of: true
	 * @var        boolean
	 */
	protected $valid;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * @var        array Triptrackable[] Collection to store aggregation of Triptrackable objects.
	 */
	protected $collTriptrackables;

	/**
	 * @var        array Tripvehicle[] Collection to store aggregation of Tripvehicle objects.
	 */
	protected $collTripvehicles;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $triptrackablesScheduledForDeletion = null;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $tripvehiclesScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->customerid = 1;
		$this->valid = true;
	}

	/**
	 * Initializes internal state of BaseTrip object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [tripid] column value.
	 * 
	 * @return     int
	 */
	public function getTripid()
	{
		return $this->tripid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [tripreference] column value.
	 * 
	 * @return     string
	 */
	public function getTripreference()
	{
		return $this->tripreference;
	}

	/**
	 * Get the [valid] column value.
	 * 
	 * @return     boolean
	 */
	public function getValid()
	{
		return $this->valid;
	}

	/**
	 * Set the value of [tripid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trip The current object (for fluent API support)
	 */
	public function setTripid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->tripid !== $v) {
			$this->tripid = $v;
			$this->modifiedColumns[] = TripPeer::TRIPID;
		}

		return $this;
	} // setTripid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Trip The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = TripPeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [tripreference] column.
	 * 
	 * @param      string $v new value
	 * @return     Trip The current object (for fluent API support)
	 */
	public function setTripreference($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tripreference !== $v) {
			$this->tripreference = $v;
			$this->modifiedColumns[] = TripPeer::TRIPREFERENCE;
		}

		return $this;
	} // setTripreference()

	/**
	 * Sets the value of the [valid] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     Trip The current object (for fluent API support)
	 */
	public function setValid($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->valid !== $v) {
			$this->valid = $v;
			$this->modifiedColumns[] = TripPeer::VALID;
		}

		return $this;
	} // setValid()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->customerid !== 1) {
				return false;
			}

			if ($this->valid !== true) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->tripid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->customerid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->tripreference = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->valid = ($row[$startcol + 3] !== null) ? (boolean) $row[$startcol + 3] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 4; // 4 = TripPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Trip object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TripPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = TripPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aCustomer = null;
			$this->collTriptrackables = null;

			$this->collTripvehicles = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TripPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = TripQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TripPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				TripPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->triptrackablesScheduledForDeletion !== null) {
				if (!$this->triptrackablesScheduledForDeletion->isEmpty()) {
					TriptrackableQuery::create()
						->filterByPrimaryKeys($this->triptrackablesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->triptrackablesScheduledForDeletion = null;
				}
			}

			if ($this->collTriptrackables !== null) {
				foreach ($this->collTriptrackables as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->tripvehiclesScheduledForDeletion !== null) {
				if (!$this->tripvehiclesScheduledForDeletion->isEmpty()) {
					TripvehicleQuery::create()
						->filterByPrimaryKeys($this->tripvehiclesScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->tripvehiclesScheduledForDeletion = null;
				}
			}

			if ($this->collTripvehicles !== null) {
				foreach ($this->collTripvehicles as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = TripPeer::TRIPID;
		if (null !== $this->tripid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . TripPeer::TRIPID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(TripPeer::TRIPID)) {
			$modifiedColumns[':p' . $index++]  = '`TRIPID`';
		}
		if ($this->isColumnModified(TripPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(TripPeer::TRIPREFERENCE)) {
			$modifiedColumns[':p' . $index++]  = '`TRIPREFERENCE`';
		}
		if ($this->isColumnModified(TripPeer::VALID)) {
			$modifiedColumns[':p' . $index++]  = '`VALID`';
		}

		$sql = sprintf(
			'INSERT INTO `trip` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`TRIPID`':
						$stmt->bindValue($identifier, $this->tripid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`TRIPREFERENCE`':
						$stmt->bindValue($identifier, $this->tripreference, PDO::PARAM_STR);
						break;
					case '`VALID`':
						$stmt->bindValue($identifier, (int) $this->valid, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setTripid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}


			if (($retval = TripPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTriptrackables !== null) {
					foreach ($this->collTriptrackables as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collTripvehicles !== null) {
					foreach ($this->collTripvehicles as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TripPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getTripid();
				break;
			case 1:
				return $this->getCustomerid();
				break;
			case 2:
				return $this->getTripreference();
				break;
			case 3:
				return $this->getValid();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Trip'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Trip'][$this->getPrimaryKey()] = true;
		$keys = TripPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getTripid(),
			$keys[1] => $this->getCustomerid(),
			$keys[2] => $this->getTripreference(),
			$keys[3] => $this->getValid(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->collTriptrackables) {
				$result['Triptrackables'] = $this->collTriptrackables->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
			if (null !== $this->collTripvehicles) {
				$result['Tripvehicles'] = $this->collTripvehicles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TripPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setTripid($value);
				break;
			case 1:
				$this->setCustomerid($value);
				break;
			case 2:
				$this->setTripreference($value);
				break;
			case 3:
				$this->setValid($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TripPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setTripid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCustomerid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTripreference($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setValid($arr[$keys[3]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(TripPeer::DATABASE_NAME);

		if ($this->isColumnModified(TripPeer::TRIPID)) $criteria->add(TripPeer::TRIPID, $this->tripid);
		if ($this->isColumnModified(TripPeer::CUSTOMERID)) $criteria->add(TripPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(TripPeer::TRIPREFERENCE)) $criteria->add(TripPeer::TRIPREFERENCE, $this->tripreference);
		if ($this->isColumnModified(TripPeer::VALID)) $criteria->add(TripPeer::VALID, $this->valid);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TripPeer::DATABASE_NAME);
		$criteria->add(TripPeer::TRIPID, $this->tripid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getTripid();
	}

	/**
	 * Generic method to set the primary key (tripid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setTripid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getTripid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Trip (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setTripreference($this->getTripreference());
		$copyObj->setValid($this->getValid());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getTriptrackables() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTriptrackable($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getTripvehicles() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTripvehicle($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setTripid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Trip Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     TripPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TripPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     Trip The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addTrip($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addTrips($this);
			 */
		}
		return $this->aCustomer;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('Triptrackable' == $relationName) {
			return $this->initTriptrackables();
		}
		if ('Tripvehicle' == $relationName) {
			return $this->initTripvehicles();
		}
	}

	/**
	 * Clears out the collTriptrackables collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTriptrackables()
	 */
	public function clearTriptrackables()
	{
		$this->collTriptrackables = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTriptrackables collection.
	 *
	 * By default this just sets the collTriptrackables collection to an empty array (like clearcollTriptrackables());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTriptrackables($overrideExisting = true)
	{
		if (null !== $this->collTriptrackables && !$overrideExisting) {
			return;
		}
		$this->collTriptrackables = new PropelObjectCollection();
		$this->collTriptrackables->setModel('Triptrackable');
	}

	/**
	 * Gets an array of Triptrackable objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trip is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Triptrackable[] List of Triptrackable objects
	 * @throws     PropelException
	 */
	public function getTriptrackables($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTriptrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTriptrackables) {
				// return empty collection
				$this->initTriptrackables();
			} else {
				$collTriptrackables = TriptrackableQuery::create(null, $criteria)
					->filterByTrip($this)
					->find($con);
				if (null !== $criteria) {
					return $collTriptrackables;
				}
				$this->collTriptrackables = $collTriptrackables;
			}
		}
		return $this->collTriptrackables;
	}

	/**
	 * Sets a collection of Triptrackable objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $triptrackables A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTriptrackables(PropelCollection $triptrackables, PropelPDO $con = null)
	{
		$this->triptrackablesScheduledForDeletion = $this->getTriptrackables(new Criteria(), $con)->diff($triptrackables);

		foreach ($triptrackables as $triptrackable) {
			// Fix issue with collection modified by reference
			if ($triptrackable->isNew()) {
				$triptrackable->setTrip($this);
			}
			$this->addTriptrackable($triptrackable);
		}

		$this->collTriptrackables = $triptrackables;
	}

	/**
	 * Returns the number of related Triptrackable objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Triptrackable objects.
	 * @throws     PropelException
	 */
	public function countTriptrackables(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTriptrackables || null !== $criteria) {
			if ($this->isNew() && null === $this->collTriptrackables) {
				return 0;
			} else {
				$query = TriptrackableQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrip($this)
					->count($con);
			}
		} else {
			return count($this->collTriptrackables);
		}
	}

	/**
	 * Method called to associate a Triptrackable object to this object
	 * through the Triptrackable foreign key attribute.
	 *
	 * @param      Triptrackable $l Triptrackable
	 * @return     Trip The current object (for fluent API support)
	 */
	public function addTriptrackable(Triptrackable $l)
	{
		if ($this->collTriptrackables === null) {
			$this->initTriptrackables();
		}
		if (!$this->collTriptrackables->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTriptrackable($l);
		}

		return $this;
	}

	/**
	 * @param	Triptrackable $triptrackable The triptrackable object to add.
	 */
	protected function doAddTriptrackable($triptrackable)
	{
		$this->collTriptrackables[]= $triptrackable;
		$triptrackable->setTrip($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trip is new, it will return
	 * an empty collection; or if this Trip has previously
	 * been saved, it will retrieve related Triptrackables from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trip.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Triptrackable[] List of Triptrackable objects
	 */
	public function getTriptrackablesJoinTrackable($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TriptrackableQuery::create(null, $criteria);
		$query->joinWith('Trackable', $join_behavior);

		return $this->getTriptrackables($query, $con);
	}

	/**
	 * Clears out the collTripvehicles collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTripvehicles()
	 */
	public function clearTripvehicles()
	{
		$this->collTripvehicles = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTripvehicles collection.
	 *
	 * By default this just sets the collTripvehicles collection to an empty array (like clearcollTripvehicles());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initTripvehicles($overrideExisting = true)
	{
		if (null !== $this->collTripvehicles && !$overrideExisting) {
			return;
		}
		$this->collTripvehicles = new PropelObjectCollection();
		$this->collTripvehicles->setModel('Tripvehicle');
	}

	/**
	 * Gets an array of Tripvehicle objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Trip is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Tripvehicle[] List of Tripvehicle objects
	 * @throws     PropelException
	 */
	public function getTripvehicles($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTripvehicles || null !== $criteria) {
			if ($this->isNew() && null === $this->collTripvehicles) {
				// return empty collection
				$this->initTripvehicles();
			} else {
				$collTripvehicles = TripvehicleQuery::create(null, $criteria)
					->filterByTrip($this)
					->find($con);
				if (null !== $criteria) {
					return $collTripvehicles;
				}
				$this->collTripvehicles = $collTripvehicles;
			}
		}
		return $this->collTripvehicles;
	}

	/**
	 * Sets a collection of Tripvehicle objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $tripvehicles A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setTripvehicles(PropelCollection $tripvehicles, PropelPDO $con = null)
	{
		$this->tripvehiclesScheduledForDeletion = $this->getTripvehicles(new Criteria(), $con)->diff($tripvehicles);

		foreach ($tripvehicles as $tripvehicle) {
			// Fix issue with collection modified by reference
			if ($tripvehicle->isNew()) {
				$tripvehicle->setTrip($this);
			}
			$this->addTripvehicle($tripvehicle);
		}

		$this->collTripvehicles = $tripvehicles;
	}

	/**
	 * Returns the number of related Tripvehicle objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Tripvehicle objects.
	 * @throws     PropelException
	 */
	public function countTripvehicles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTripvehicles || null !== $criteria) {
			if ($this->isNew() && null === $this->collTripvehicles) {
				return 0;
			} else {
				$query = TripvehicleQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByTrip($this)
					->count($con);
			}
		} else {
			return count($this->collTripvehicles);
		}
	}

	/**
	 * Method called to associate a Tripvehicle object to this object
	 * through the Tripvehicle foreign key attribute.
	 *
	 * @param      Tripvehicle $l Tripvehicle
	 * @return     Trip The current object (for fluent API support)
	 */
	public function addTripvehicle(Tripvehicle $l)
	{
		if ($this->collTripvehicles === null) {
			$this->initTripvehicles();
		}
		if (!$this->collTripvehicles->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddTripvehicle($l);
		}

		return $this;
	}

	/**
	 * @param	Tripvehicle $tripvehicle The tripvehicle object to add.
	 */
	protected function doAddTripvehicle($tripvehicle)
	{
		$this->collTripvehicles[]= $tripvehicle;
		$tripvehicle->setTrip($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Trip is new, it will return
	 * an empty collection; or if this Trip has previously
	 * been saved, it will retrieve related Tripvehicles from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Trip.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tripvehicle[] List of Tripvehicle objects
	 */
	public function getTripvehiclesJoinVehicle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TripvehicleQuery::create(null, $criteria);
		$query->joinWith('Vehicle', $join_behavior);

		return $this->getTripvehicles($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->tripid = null;
		$this->customerid = null;
		$this->tripreference = null;
		$this->valid = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collTriptrackables) {
				foreach ($this->collTriptrackables as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collTripvehicles) {
				foreach ($this->collTripvehicles as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collTriptrackables instanceof PropelCollection) {
			$this->collTriptrackables->clearIterator();
		}
		$this->collTriptrackables = null;
		if ($this->collTripvehicles instanceof PropelCollection) {
			$this->collTripvehicles->clearIterator();
		}
		$this->collTripvehicles = null;
		$this->aCustomer = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(TripPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseTrip
