<?php



/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class User extends BaseUser {

	public function getFullName() {
		$fullname = $this->getFirstname(); 
		if ($this->getLastname()) {
			$fullname .= ' ' . $this->getLastname();	
		} 
		return $fullname;
	}
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'usertype',
				'label' => ucfirst(Localizer::getText('user type')),
				'foreign' => true,
			),
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
				'joinArgs' => array(
					//	array('valid', 1, Criteria::EQUAL)
				),
			),
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language')),
				'foreign' => true,
			),
			array(
				'column' => 'username',
				'label' => ucfirst(Localizer::getText('user name')),
			),
			array(
				'column' => 'firstname',
				'label' => ucfirst(Localizer::getText('firstname')),
			),
			array(
				'column' => 'lastname',
				'label' => ucfirst(Localizer::getText('lastname')),
			),
			array(
				'column' => 'email',
				'label' => ucfirst(Localizer::getText('email')),
			),
		);
	}
	
	
	public static function getCrudMap() {
		return array(
			array(
				'column' => 'usertype',
				'label' => ucfirst(Localizer::getText('user type')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),
			array(
				'column' => 'username',
				'label' => ucfirst(Localizer::getText('user name')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'firstname',
				'label' => ucfirst(Localizer::getText('first name')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'lastname',
				'label' => ucfirst(Localizer::getText('last name')),
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'email',
				'label' => ucfirst(Localizer::getText('email')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'password',
				'label' => ucfirst(Localizer::getText('password')),
				'inputType' => 'password',
				'default' => '',
				'hideValue' => true,
				'setMethod' => 'setNewPassword',
			),
			array(
				'column' => 'apikey',
				'label' => ucfirst(Localizer::getText('API key')),
				'inputType' => 'show',
				'default' => '',
				'getMethod' => 'getDecryptedApikey',
			),
		);
	}
	
	public function setNewPassword($value) {
		$value = trim($value);
		if(strlen($value) > 0) {
			$this->setPassword(sha1(Registry::get('password_salt') . $value));
		}
	}
	
	private function setDecryptedApikey($value) {
		$value = trim($value);
		if(strlen($value) > 0) {
			$this->setApikey(CryptographyProvider::encrypt($value));
		}
		else {
			$this->setApikey('');
		}
	}
	
	public function getDecryptedApikey() {
		if (NULL === $this->getApikey()) {
			$this->setApikey(CryptographyProvider::encrypt(Tools::getApiKey())); 
		}
		return CryptographyProvider::decrypt($this->getApikey());
	}
} // User
