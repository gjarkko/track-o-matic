<?php



/**
 * This class defines the structure of the 'customer' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class CustomerTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.CustomerTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('customer');
		$this->setPhpName('Customer');
		$this->setClassname('Customer');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('CUSTOMERID', 'Customerid', 'INTEGER', true, 10, null);
		$this->addColumn('CUSTOMER', 'Customer', 'VARCHAR', true, 60, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('CUSTOMER', 'minLength', 'propel.validator.MinLengthValidator', '5', 'customer name must be at least 5 characters');
		$this->addValidator('CUSTOMER', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'customer name must be at most 60 characters');
		$this->addValidator('CUSTOMER', 'required', 'propel.validator.RequiredValidator', '', 'customer name is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Address', 'Address', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Addresss');
		$this->addRelation('Detailtype', 'Detailtype', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Detailtypes');
		$this->addRelation('Groupreference', 'Groupreference', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Groupreferences');
		$this->addRelation('Logentry', 'Logentry', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Logentrys');
		$this->addRelation('Notification', 'Notification', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Notifications');
		$this->addRelation('Statustype', 'Statustype', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Statustypes');
		$this->addRelation('Trackable', 'Trackable', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Trackables');
		$this->addRelation('Trip', 'Trip', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Trips');
		$this->addRelation('User', 'User', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Users');
		$this->addRelation('Vehicle', 'Vehicle', RelationMap::ONE_TO_MANY, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE', 'Vehicles');
	} // buildRelations()

} // CustomerTableMap
