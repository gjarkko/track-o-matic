<?php


/**
 * Base class that represents a row from the 'user' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseUser extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'UserPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        UserPeer
	 */
	protected static $peer;

	/**
	 * The value for the userid field.
	 * @var        int
	 */
	protected $userid;

	/**
	 * The value for the usertypeid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $usertypeid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the languageid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $languageid;

	/**
	 * The value for the username field.
	 * @var        string
	 */
	protected $username;

	/**
	 * The value for the firstname field.
	 * @var        string
	 */
	protected $firstname;

	/**
	 * The value for the lastname field.
	 * @var        string
	 */
	protected $lastname;

	/**
	 * The value for the email field.
	 * @var        string
	 */
	protected $email;

	/**
	 * The value for the password field.
	 * @var        string
	 */
	protected $password;

	/**
	 * The value for the apikey field.
	 * @var        string
	 */
	protected $apikey;

	/**
	 * The value for the valid field.
	 * Note: this column has a database default value of: true
	 * @var        boolean
	 */
	protected $valid;

	/**
	 * @var        Usertype
	 */
	protected $aUsertype;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * @var        Language
	 */
	protected $aLanguage;

	/**
	 * @var        array Logentry[] Collection to store aggregation of Logentry objects.
	 */
	protected $collLogentrys;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * An array of objects scheduled for deletion.
	 * @var		array
	 */
	protected $logentrysScheduledForDeletion = null;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->usertypeid = 1;
		$this->customerid = 1;
		$this->languageid = 1;
		$this->valid = true;
	}

	/**
	 * Initializes internal state of BaseUser object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [userid] column value.
	 * 
	 * @return     int
	 */
	public function getUserid()
	{
		return $this->userid;
	}

	/**
	 * Get the [usertypeid] column value.
	 * 
	 * @return     int
	 */
	public function getUsertypeid()
	{
		return $this->usertypeid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [languageid] column value.
	 * 
	 * @return     int
	 */
	public function getLanguageid()
	{
		return $this->languageid;
	}

	/**
	 * Get the [username] column value.
	 * 
	 * @return     string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * Get the [firstname] column value.
	 * 
	 * @return     string
	 */
	public function getFirstname()
	{
		return $this->firstname;
	}

	/**
	 * Get the [lastname] column value.
	 * 
	 * @return     string
	 */
	public function getLastname()
	{
		return $this->lastname;
	}

	/**
	 * Get the [email] column value.
	 * 
	 * @return     string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Get the [password] column value.
	 * 
	 * @return     string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * Get the [apikey] column value.
	 * 
	 * @return     string
	 */
	public function getApikey()
	{
		return $this->apikey;
	}

	/**
	 * Get the [valid] column value.
	 * 
	 * @return     boolean
	 */
	public function getValid()
	{
		return $this->valid;
	}

	/**
	 * Set the value of [userid] column.
	 * 
	 * @param      int $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setUserid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->userid !== $v) {
			$this->userid = $v;
			$this->modifiedColumns[] = UserPeer::USERID;
		}

		return $this;
	} // setUserid()

	/**
	 * Set the value of [usertypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setUsertypeid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->usertypeid !== $v) {
			$this->usertypeid = $v;
			$this->modifiedColumns[] = UserPeer::USERTYPEID;
		}

		if ($this->aUsertype !== null && $this->aUsertype->getUsertypeid() !== $v) {
			$this->aUsertype = null;
		}

		return $this;
	} // setUsertypeid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = UserPeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [languageid] column.
	 * 
	 * @param      int $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setLanguageid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->languageid !== $v) {
			$this->languageid = $v;
			$this->modifiedColumns[] = UserPeer::LANGUAGEID;
		}

		if ($this->aLanguage !== null && $this->aLanguage->getLanguageid() !== $v) {
			$this->aLanguage = null;
		}

		return $this;
	} // setLanguageid()

	/**
	 * Set the value of [username] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setUsername($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = UserPeer::USERNAME;
		}

		return $this;
	} // setUsername()

	/**
	 * Set the value of [firstname] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setFirstname($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->firstname !== $v) {
			$this->firstname = $v;
			$this->modifiedColumns[] = UserPeer::FIRSTNAME;
		}

		return $this;
	} // setFirstname()

	/**
	 * Set the value of [lastname] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setLastname($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lastname !== $v) {
			$this->lastname = $v;
			$this->modifiedColumns[] = UserPeer::LASTNAME;
		}

		return $this;
	} // setLastname()

	/**
	 * Set the value of [email] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setEmail($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = UserPeer::EMAIL;
		}

		return $this;
	} // setEmail()

	/**
	 * Set the value of [password] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setPassword($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = UserPeer::PASSWORD;
		}

		return $this;
	} // setPassword()

	/**
	 * Set the value of [apikey] column.
	 * 
	 * @param      string $v new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setApikey($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->apikey !== $v) {
			$this->apikey = $v;
			$this->modifiedColumns[] = UserPeer::APIKEY;
		}

		return $this;
	} // setApikey()

	/**
	 * Sets the value of the [valid] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     User The current object (for fluent API support)
	 */
	public function setValid($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->valid !== $v) {
			$this->valid = $v;
			$this->modifiedColumns[] = UserPeer::VALID;
		}

		return $this;
	} // setValid()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->usertypeid !== 1) {
				return false;
			}

			if ($this->customerid !== 1) {
				return false;
			}

			if ($this->languageid !== 1) {
				return false;
			}

			if ($this->valid !== true) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->userid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->usertypeid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->customerid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->languageid = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->username = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->firstname = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->lastname = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->email = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->password = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->apikey = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->valid = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 11; // 11 = UserPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating User object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aUsertype !== null && $this->usertypeid !== $this->aUsertype->getUsertypeid()) {
			$this->aUsertype = null;
		}
		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
		if ($this->aLanguage !== null && $this->languageid !== $this->aLanguage->getLanguageid()) {
			$this->aLanguage = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = UserPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aUsertype = null;
			$this->aCustomer = null;
			$this->aLanguage = null;
			$this->collLogentrys = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = UserQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				UserPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aUsertype !== null) {
				if ($this->aUsertype->isModified() || $this->aUsertype->isNew()) {
					$affectedRows += $this->aUsertype->save($con);
				}
				$this->setUsertype($this->aUsertype);
			}

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->aLanguage !== null) {
				if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
					$affectedRows += $this->aLanguage->save($con);
				}
				$this->setLanguage($this->aLanguage);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			if ($this->logentrysScheduledForDeletion !== null) {
				if (!$this->logentrysScheduledForDeletion->isEmpty()) {
					LogentryQuery::create()
						->filterByPrimaryKeys($this->logentrysScheduledForDeletion->getPrimaryKeys(false))
						->delete($con);
					$this->logentrysScheduledForDeletion = null;
				}
			}

			if ($this->collLogentrys !== null) {
				foreach ($this->collLogentrys as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = UserPeer::USERID;
		if (null !== $this->userid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserPeer::USERID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(UserPeer::USERID)) {
			$modifiedColumns[':p' . $index++]  = '`USERID`';
		}
		if ($this->isColumnModified(UserPeer::USERTYPEID)) {
			$modifiedColumns[':p' . $index++]  = '`USERTYPEID`';
		}
		if ($this->isColumnModified(UserPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(UserPeer::LANGUAGEID)) {
			$modifiedColumns[':p' . $index++]  = '`LANGUAGEID`';
		}
		if ($this->isColumnModified(UserPeer::USERNAME)) {
			$modifiedColumns[':p' . $index++]  = '`USERNAME`';
		}
		if ($this->isColumnModified(UserPeer::FIRSTNAME)) {
			$modifiedColumns[':p' . $index++]  = '`FIRSTNAME`';
		}
		if ($this->isColumnModified(UserPeer::LASTNAME)) {
			$modifiedColumns[':p' . $index++]  = '`LASTNAME`';
		}
		if ($this->isColumnModified(UserPeer::EMAIL)) {
			$modifiedColumns[':p' . $index++]  = '`EMAIL`';
		}
		if ($this->isColumnModified(UserPeer::PASSWORD)) {
			$modifiedColumns[':p' . $index++]  = '`PASSWORD`';
		}
		if ($this->isColumnModified(UserPeer::APIKEY)) {
			$modifiedColumns[':p' . $index++]  = '`APIKEY`';
		}
		if ($this->isColumnModified(UserPeer::VALID)) {
			$modifiedColumns[':p' . $index++]  = '`VALID`';
		}

		$sql = sprintf(
			'INSERT INTO `user` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`USERID`':
						$stmt->bindValue($identifier, $this->userid, PDO::PARAM_INT);
						break;
					case '`USERTYPEID`':
						$stmt->bindValue($identifier, $this->usertypeid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`LANGUAGEID`':
						$stmt->bindValue($identifier, $this->languageid, PDO::PARAM_INT);
						break;
					case '`USERNAME`':
						$stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
						break;
					case '`FIRSTNAME`':
						$stmt->bindValue($identifier, $this->firstname, PDO::PARAM_STR);
						break;
					case '`LASTNAME`':
						$stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
						break;
					case '`EMAIL`':
						$stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
						break;
					case '`PASSWORD`':
						$stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
						break;
					case '`APIKEY`':
						$stmt->bindValue($identifier, $this->apikey, PDO::PARAM_STR);
						break;
					case '`VALID`':
						$stmt->bindValue($identifier, (int) $this->valid, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setUserid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aUsertype !== null) {
				if (!$this->aUsertype->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aUsertype->getValidationFailures());
				}
			}

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}

			if ($this->aLanguage !== null) {
				if (!$this->aLanguage->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
				}
			}


			if (($retval = UserPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collLogentrys !== null) {
					foreach ($this->collLogentrys as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserid();
				break;
			case 1:
				return $this->getUsertypeid();
				break;
			case 2:
				return $this->getCustomerid();
				break;
			case 3:
				return $this->getLanguageid();
				break;
			case 4:
				return $this->getUsername();
				break;
			case 5:
				return $this->getFirstname();
				break;
			case 6:
				return $this->getLastname();
				break;
			case 7:
				return $this->getEmail();
				break;
			case 8:
				return $this->getPassword();
				break;
			case 9:
				return $this->getApikey();
				break;
			case 10:
				return $this->getValid();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['User'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['User'][$this->getPrimaryKey()] = true;
		$keys = UserPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserid(),
			$keys[1] => $this->getUsertypeid(),
			$keys[2] => $this->getCustomerid(),
			$keys[3] => $this->getLanguageid(),
			$keys[4] => $this->getUsername(),
			$keys[5] => $this->getFirstname(),
			$keys[6] => $this->getLastname(),
			$keys[7] => $this->getEmail(),
			$keys[8] => $this->getPassword(),
			$keys[9] => $this->getApikey(),
			$keys[10] => $this->getValid(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aUsertype) {
				$result['Usertype'] = $this->aUsertype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aLanguage) {
				$result['Language'] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->collLogentrys) {
				$result['Logentrys'] = $this->collLogentrys->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserid($value);
				break;
			case 1:
				$this->setUsertypeid($value);
				break;
			case 2:
				$this->setCustomerid($value);
				break;
			case 3:
				$this->setLanguageid($value);
				break;
			case 4:
				$this->setUsername($value);
				break;
			case 5:
				$this->setFirstname($value);
				break;
			case 6:
				$this->setLastname($value);
				break;
			case 7:
				$this->setEmail($value);
				break;
			case 8:
				$this->setPassword($value);
				break;
			case 9:
				$this->setApikey($value);
				break;
			case 10:
				$this->setValid($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UserPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUsertypeid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCustomerid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLanguageid($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setUsername($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFirstname($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLastname($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setEmail($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPassword($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setApikey($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setValid($arr[$keys[10]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(UserPeer::DATABASE_NAME);

		if ($this->isColumnModified(UserPeer::USERID)) $criteria->add(UserPeer::USERID, $this->userid);
		if ($this->isColumnModified(UserPeer::USERTYPEID)) $criteria->add(UserPeer::USERTYPEID, $this->usertypeid);
		if ($this->isColumnModified(UserPeer::CUSTOMERID)) $criteria->add(UserPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(UserPeer::LANGUAGEID)) $criteria->add(UserPeer::LANGUAGEID, $this->languageid);
		if ($this->isColumnModified(UserPeer::USERNAME)) $criteria->add(UserPeer::USERNAME, $this->username);
		if ($this->isColumnModified(UserPeer::FIRSTNAME)) $criteria->add(UserPeer::FIRSTNAME, $this->firstname);
		if ($this->isColumnModified(UserPeer::LASTNAME)) $criteria->add(UserPeer::LASTNAME, $this->lastname);
		if ($this->isColumnModified(UserPeer::EMAIL)) $criteria->add(UserPeer::EMAIL, $this->email);
		if ($this->isColumnModified(UserPeer::PASSWORD)) $criteria->add(UserPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(UserPeer::APIKEY)) $criteria->add(UserPeer::APIKEY, $this->apikey);
		if ($this->isColumnModified(UserPeer::VALID)) $criteria->add(UserPeer::VALID, $this->valid);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UserPeer::DATABASE_NAME);
		$criteria->add(UserPeer::USERID, $this->userid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getUserid();
	}

	/**
	 * Generic method to set the primary key (userid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setUserid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getUserid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of User (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setUsertypeid($this->getUsertypeid());
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setLanguageid($this->getLanguageid());
		$copyObj->setUsername($this->getUsername());
		$copyObj->setFirstname($this->getFirstname());
		$copyObj->setLastname($this->getLastname());
		$copyObj->setEmail($this->getEmail());
		$copyObj->setPassword($this->getPassword());
		$copyObj->setApikey($this->getApikey());
		$copyObj->setValid($this->getValid());

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getLogentrys() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addLogentry($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)

		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setUserid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     User Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     UserPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UserPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Usertype object.
	 *
	 * @param      Usertype $v
	 * @return     User The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setUsertype(Usertype $v = null)
	{
		if ($v === null) {
			$this->setUsertypeid(1);
		} else {
			$this->setUsertypeid($v->getUsertypeid());
		}

		$this->aUsertype = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Usertype object, it will not be re-added.
		if ($v !== null) {
			$v->addUser($this);
		}

		return $this;
	}


	/**
	 * Get the associated Usertype object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Usertype The associated Usertype object.
	 * @throws     PropelException
	 */
	public function getUsertype(PropelPDO $con = null)
	{
		if ($this->aUsertype === null && ($this->usertypeid !== null)) {
			$this->aUsertype = UsertypeQuery::create()->findPk($this->usertypeid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aUsertype->addUsers($this);
			 */
		}
		return $this->aUsertype;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     User The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addUser($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addUsers($this);
			 */
		}
		return $this->aCustomer;
	}

	/**
	 * Declares an association between this object and a Language object.
	 *
	 * @param      Language $v
	 * @return     User The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setLanguage(Language $v = null)
	{
		if ($v === null) {
			$this->setLanguageid(1);
		} else {
			$this->setLanguageid($v->getLanguageid());
		}

		$this->aLanguage = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Language object, it will not be re-added.
		if ($v !== null) {
			$v->addUser($this);
		}

		return $this;
	}


	/**
	 * Get the associated Language object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Language The associated Language object.
	 * @throws     PropelException
	 */
	public function getLanguage(PropelPDO $con = null)
	{
		if ($this->aLanguage === null && ($this->languageid !== null)) {
			$this->aLanguage = LanguageQuery::create()->findPk($this->languageid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aLanguage->addUsers($this);
			 */
		}
		return $this->aLanguage;
	}


	/**
	 * Initializes a collection based on the name of a relation.
	 * Avoids crafting an 'init[$relationName]s' method name
	 * that wouldn't work when StandardEnglishPluralizer is used.
	 *
	 * @param      string $relationName The name of the relation to initialize
	 * @return     void
	 */
	public function initRelation($relationName)
	{
		if ('Logentry' == $relationName) {
			return $this->initLogentrys();
		}
	}

	/**
	 * Clears out the collLogentrys collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addLogentrys()
	 */
	public function clearLogentrys()
	{
		$this->collLogentrys = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collLogentrys collection.
	 *
	 * By default this just sets the collLogentrys collection to an empty array (like clearcollLogentrys());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @param      boolean $overrideExisting If set to true, the method call initializes
	 *                                        the collection even if it is not empty
	 *
	 * @return     void
	 */
	public function initLogentrys($overrideExisting = true)
	{
		if (null !== $this->collLogentrys && !$overrideExisting) {
			return;
		}
		$this->collLogentrys = new PropelObjectCollection();
		$this->collLogentrys->setModel('Logentry');
	}

	/**
	 * Gets an array of Logentry objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this User is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 * @throws     PropelException
	 */
	public function getLogentrys($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collLogentrys || null !== $criteria) {
			if ($this->isNew() && null === $this->collLogentrys) {
				// return empty collection
				$this->initLogentrys();
			} else {
				$collLogentrys = LogentryQuery::create(null, $criteria)
					->filterByUser($this)
					->find($con);
				if (null !== $criteria) {
					return $collLogentrys;
				}
				$this->collLogentrys = $collLogentrys;
			}
		}
		return $this->collLogentrys;
	}

	/**
	 * Sets a collection of Logentry objects related by a one-to-many relationship
	 * to the current object.
	 * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
	 * and new objects from the given Propel collection.
	 *
	 * @param      PropelCollection $logentrys A Propel collection.
	 * @param      PropelPDO $con Optional connection object
	 */
	public function setLogentrys(PropelCollection $logentrys, PropelPDO $con = null)
	{
		$this->logentrysScheduledForDeletion = $this->getLogentrys(new Criteria(), $con)->diff($logentrys);

		foreach ($logentrys as $logentry) {
			// Fix issue with collection modified by reference
			if ($logentry->isNew()) {
				$logentry->setUser($this);
			}
			$this->addLogentry($logentry);
		}

		$this->collLogentrys = $logentrys;
	}

	/**
	 * Returns the number of related Logentry objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Logentry objects.
	 * @throws     PropelException
	 */
	public function countLogentrys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collLogentrys || null !== $criteria) {
			if ($this->isNew() && null === $this->collLogentrys) {
				return 0;
			} else {
				$query = LogentryQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByUser($this)
					->count($con);
			}
		} else {
			return count($this->collLogentrys);
		}
	}

	/**
	 * Method called to associate a Logentry object to this object
	 * through the Logentry foreign key attribute.
	 *
	 * @param      Logentry $l Logentry
	 * @return     User The current object (for fluent API support)
	 */
	public function addLogentry(Logentry $l)
	{
		if ($this->collLogentrys === null) {
			$this->initLogentrys();
		}
		if (!$this->collLogentrys->contains($l)) { // only add it if the **same** object is not already associated
			$this->doAddLogentry($l);
		}

		return $this;
	}

	/**
	 * @param	Logentry $logentry The logentry object to add.
	 */
	protected function doAddLogentry($logentry)
	{
		$this->collLogentrys[]= $logentry;
		$logentry->setUser($this);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this User is new, it will return
	 * an empty collection; or if this User has previously
	 * been saved, it will retrieve related Logentrys from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in User.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 */
	public function getLogentrysJoinLogentrytype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = LogentryQuery::create(null, $criteria);
		$query->joinWith('Logentrytype', $join_behavior);

		return $this->getLogentrys($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this User is new, it will return
	 * an empty collection; or if this User has previously
	 * been saved, it will retrieve related Logentrys from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in User.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Logentry[] List of Logentry objects
	 */
	public function getLogentrysJoinCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = LogentryQuery::create(null, $criteria);
		$query->joinWith('Customer', $join_behavior);

		return $this->getLogentrys($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->userid = null;
		$this->usertypeid = null;
		$this->customerid = null;
		$this->languageid = null;
		$this->username = null;
		$this->firstname = null;
		$this->lastname = null;
		$this->email = null;
		$this->password = null;
		$this->apikey = null;
		$this->valid = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collLogentrys) {
				foreach ($this->collLogentrys as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		if ($this->collLogentrys instanceof PropelCollection) {
			$this->collLogentrys->clearIterator();
		}
		$this->collLogentrys = null;
		$this->aUsertype = null;
		$this->aCustomer = null;
		$this->aLanguage = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(UserPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseUser
