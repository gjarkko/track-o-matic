<?

class RequestHandler {

	const API_CONTROLLER_PREFIX = 'api';
	const DEFAULT_CONTROLLER = 'main';
	const DEFAULT_ACTION = 'index';
	const LOGIN_CONTROLLER = 'main';
	const LOGIN_ACTION = 'login';
	const CLASS_POSTFIX = 'Controller';
	const CLASS_FILE_EXTENSION = '.php';
	const ACTION_POSTFIX = 'Action';

	public static $instance;
	private $params;
	private $actionQueue;
	private $continueExecution;
	
	private $originalControllerName;
	private $originalActionName;
	
	private $currentActionIndex;
	private $currentController;
	private $currentControllerName;
	private $currentActionName;
	
	private function __construct() {
		
		$this->actionQueue = array();
		$this->params = array();
		$this->actionQueueIndex = 0;
		$this->continueExecution = true;
		
		if (isset($_REQUEST['request'])) {
			foreach(explode('/', $_REQUEST['request']) as $i => $part) {
				switch($i) {
					case 0: $controller = $part; break;
					case 1: $action = $part; break;
					case 2: $this->setParam('id', $part); break;
					case 3: break(2);
				}
			}
		}

		// Move request variables to $this->params
		foreach($_REQUEST as $key => $value) {
			if ($key == 'controller') {
				$controller = $value;
			}
			elseif ($key == 'action') {
				$action = $value;
			}
			elseif ($key != 'request' && $key[0] != '_') {
				$this->setParam($key, $value);
			}
		}

		if(isset($controller) && $controller == self::API_CONTROLLER_PREFIX) {
			Session::getInstance(false);
		}
		
		if(!isset($controller)) {
			$controller = self::DEFAULT_CONTROLLER; 
		}
		if (!isset($action)) {
			$action = self::DEFAULT_ACTION;
		}
		
		// Queue initial action
		$this->addAction($controller, $action);
		$this->originalControllerName = $controller;
		$this->originalActionName = $action;
	}
	
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public static function flush() {
		self::$instance = new self();
	}
	
	public function handleRequest() {
		
		while (NULL !== (list($controller, $action) = $this->getNextAction())) {
			$this->setContinueExecution(true);
			$this->currentControllerName = $controller;
			$this->currentActionName = $action;
			$controllerObject = new $controller();
			$this->currentController = $controllerObject;
			if($this->continueExecution) {
				$controllerObject->$action();
			} else {
				continue;
			}
		}
		
		// Render last controller
		if(is_object($controllerObject) && !$controllerObject->getRenderStarted()) {
			$controllerObject->render();
		}
		
		return $this;
	}

	public function getParam($key) {
		$key = strtolower($key);
		if(isset($this->params[$key])) {
			return $this->params[$key];
		}
		return NULL;
	}
	
	public function getParams() {
		return $this->params;
	}

	public function setParam($key, $value) {
		$key = strtolower($key);
		$this->params[$key] = $value;
	}
	
	public function setParams($params) {
		$this->params = $params;
	}
	
	public function hasParam($key) {
		return isset($this->params[$key]);
	}
	
	public function hasParams($keys) {
		$return = true;
		foreach ($keys as $key) {
			$return = $return && $this->hasParam(strtolower($key));
		}
		return $return;
	}
	
	public function hasParamMap($paramMap) {
		$params = array();
		foreach($paramMap as $key => $paramInfo) {
			if (!empty($paramInfo['param']) && empty($paramInfo['optional'])) {
				$params[] = $paramInfo['param'];
			}
			elseif (!empty($paramInfo['constraintEqualsParam']) && empty($paramInfo['optional'])) {
				$params[] = $paramInfo['constraintEqualsParam'];
			}
		}
		return $this->hasParams($params);
	}
	
	public function addAction($controllerName, $actionName) {
		try {
			$controllerClass = $this->getControllerClass($controllerName);
			$actionMethod = $this->getActionMethod($controllerClass, $actionName);
		} catch (Exception $e) {
			$controllerClass = 'MainController';
			$actionMethod = $this->getActionMethod($controllerClass, 'unsupportedaction');
			$this->setParam('unsupportedActionName', $actionName);
		}
		
		$this->actionQueue[] = array($controllerClass, $actionMethod);
	}
	
	private function getNextAction() {
		
		if (isset($this->actionQueue[$this->actionQueueIndex])) {
			return $this->actionQueue[$this->actionQueueIndex++];
		}
	}
	
	private function clearQueue() {
		$this->currentActionIndex = count($this->actionQueue);
	}
	
	public function getCurrentControllerName() {
		if (isset($this->currentControllerName)) {
			return $this->currentControllerName;
		}
	
	}
	
	public function getCurrentActionName() {
		if (isset($this->currentActionName)) {
			return $this->currentActionName;
		}
	}
	
	public function getOriginalControllerName() {
		if (isset($this->originalControllerName)) {
			return $this->originalControllerName;
		}
	
	}
	
	public function getOriginalActionName() {
		if (isset($this->originalActionName)) {
			return $this->originalActionName;
		}
	}
	
	public function getCurrentController() {
		if (isset($this->currentController) && is_object($this->currentController)) {
			return $this->currentController;			
		}
	}
		
	public function getControllerClass($controllerName) {
		$class = ucfirst($controllerName) . self::CLASS_POSTFIX;
		if(!class_exists($class, true)) {
			throw new Exception($class);
		}
	
		return $class;
	}
	
	public function getActionMethod($controllerClass, $actionName) {
		$method = ucfirst($actionName) . self::ACTION_POSTFIX;
		if(!method_exists($controllerClass, $method)) {
			throw new Exception('Action not found: ' . $controllerClass . '::' . $method);
		}
		
		return $method;
	}
	
	public function getActionUrl($controllerClass, $actionName, $includeCurrentParams = false, $exclude = array()) {
		if (is_object($controllerClass)) {
			$className = get_class($controllerClass);
		}
		else {
			$className = $controllerClass;
		}
		$url = BASEURL . '/' . str_replace('controller', '', strtolower($className)) . '/' . str_replace('action', '', strtolower($actionName));
		
		$defaultExcludes = array(
			'zdedebuggerpresent',
			Registry::get('system'),
		);
		if ($includeCurrentParams) {
			if (count($this->getParams())) {
				$url .= '?';
			}
			
			$params = $this->getParams();
			if (!is_array($exclude)) {
				$exclude = array($exclude);
			}
			$exclude = array_merge($defaultExcludes, $exclude);
			foreach ($exclude as $excludedParam) {
				if (isset($params[$excludedParam])) {
					unset($params[$excludedParam]);
				}
			}
			$url .= http_build_query($params);
			/*
			foreach ($this->getParams() as $key => $paramValue) {
				if (!in_array($key, $defaultExcludes) && !in_array($key, $exclude)) {
					if (is_array($paramValue)) {
						$paramValue = Tools::urlencodeArray($key, $paramValue);
					}
					$url .= $key . '=' . urlencode($paramValue) . '&';
				}
			}
			if (count($this->getParams())) {
				$url = rtrim($url, '&');
			}
			*/
		}
		return $url;
	}
	
	public function getCurrentActionUrl($includeCurrentParams = false, $exclude = array()) {
		return $this->getActionUrl($this->getCurrentController(), $this->getCurrentActionName(), $includeCurrentParams, $exclude);
	}
	
	public function redirect() {
		$args = func_get_args();
		if (func_num_args() == 2) {
			$url = BASEURL . '/' . strtolower($args[0]) . '/' . strtolower($args[1]);
		}
		elseif (func_num_args() == 1 && substr($args[0],0,4) != substr(BASEURL,0,4) && substr($args[0],0,4) != 'http') {
			$url = BASEURL . '/' . strtolower($args[0]);
		}
		else {
			$url = $args[0];
		}		
		
		header('Location: ' . $url);
		die();
	}
	
	public function setContinueExecution($continueExecution) {
		$this->continueExecution = $continueExecution;
	}
	
	public function getContinueExecution() {
		return $this->continueExecution;
	}
}
