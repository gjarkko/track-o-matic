<?php


/**
 * Base class that represents a row from the 'logentry' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseLogentry extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'LogentryPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        LogentryPeer
	 */
	protected static $peer;

	/**
	 * The value for the logentryid field.
	 * @var        int
	 */
	protected $logentryid;

	/**
	 * The value for the logentrytypeid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $logentrytypeid;

	/**
	 * The value for the userid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $userid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the time field.
	 * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
	 * @var        string
	 */
	protected $time;

	/**
	 * The value for the ip field.
	 * @var        string
	 */
	protected $ip;

	/**
	 * The value for the sessionid field.
	 * @var        string
	 */
	protected $sessionid;

	/**
	 * The value for the sessionage field.
	 * @var        double
	 */
	protected $sessionage;

	/**
	 * The value for the message field.
	 * @var        string
	 */
	protected $message;

	/**
	 * The value for the referencetype field.
	 * @var        string
	 */
	protected $referencetype;

	/**
	 * The value for the referenceid field.
	 * @var        int
	 */
	protected $referenceid;

	/**
	 * @var        Logentrytype
	 */
	protected $aLogentrytype;

	/**
	 * @var        User
	 */
	protected $aUser;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->logentrytypeid = 1;
		$this->userid = 1;
		$this->customerid = 1;
	}

	/**
	 * Initializes internal state of BaseLogentry object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [logentryid] column value.
	 * 
	 * @return     int
	 */
	public function getLogentryid()
	{
		return $this->logentryid;
	}

	/**
	 * Get the [logentrytypeid] column value.
	 * 
	 * @return     int
	 */
	public function getLogentrytypeid()
	{
		return $this->logentrytypeid;
	}

	/**
	 * Get the [userid] column value.
	 * 
	 * @return     int
	 */
	public function getUserid()
	{
		return $this->userid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [optionally formatted] temporal [time] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getTime($format = 'Y-m-d H:i:s')
	{
		if ($this->time === null) {
			return null;
		}


		if ($this->time === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->time);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->time, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [ip] column value.
	 * 
	 * @return     string
	 */
	public function getIp()
	{
		return $this->ip;
	}

	/**
	 * Get the [sessionid] column value.
	 * 
	 * @return     string
	 */
	public function getSessionid()
	{
		return $this->sessionid;
	}

	/**
	 * Get the [sessionage] column value.
	 * 
	 * @return     double
	 */
	public function getSessionage()
	{
		return $this->sessionage;
	}

	/**
	 * Get the [message] column value.
	 * 
	 * @return     string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Get the [referencetype] column value.
	 * 
	 * @return     string
	 */
	public function getReferencetype()
	{
		return $this->referencetype;
	}

	/**
	 * Get the [referenceid] column value.
	 * 
	 * @return     int
	 */
	public function getReferenceid()
	{
		return $this->referenceid;
	}

	/**
	 * Set the value of [logentryid] column.
	 * 
	 * @param      int $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setLogentryid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->logentryid !== $v) {
			$this->logentryid = $v;
			$this->modifiedColumns[] = LogentryPeer::LOGENTRYID;
		}

		return $this;
	} // setLogentryid()

	/**
	 * Set the value of [logentrytypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setLogentrytypeid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->logentrytypeid !== $v) {
			$this->logentrytypeid = $v;
			$this->modifiedColumns[] = LogentryPeer::LOGENTRYTYPEID;
		}

		if ($this->aLogentrytype !== null && $this->aLogentrytype->getLogentrytypeid() !== $v) {
			$this->aLogentrytype = null;
		}

		return $this;
	} // setLogentrytypeid()

	/**
	 * Set the value of [userid] column.
	 * 
	 * @param      int $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setUserid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->userid !== $v) {
			$this->userid = $v;
			$this->modifiedColumns[] = LogentryPeer::USERID;
		}

		if ($this->aUser !== null && $this->aUser->getUserid() !== $v) {
			$this->aUser = null;
		}

		return $this;
	} // setUserid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = LogentryPeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Sets the value of [time] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setTime($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->time !== null || $dt !== null) {
			$currentDateAsString = ($this->time !== null && $tmpDt = new DateTime($this->time)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->time = $newDateAsString;
				$this->modifiedColumns[] = LogentryPeer::TIME;
			}
		} // if either are not null

		return $this;
	} // setTime()

	/**
	 * Set the value of [ip] column.
	 * 
	 * @param      string $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setIp($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->ip !== $v) {
			$this->ip = $v;
			$this->modifiedColumns[] = LogentryPeer::IP;
		}

		return $this;
	} // setIp()

	/**
	 * Set the value of [sessionid] column.
	 * 
	 * @param      string $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setSessionid($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->sessionid !== $v) {
			$this->sessionid = $v;
			$this->modifiedColumns[] = LogentryPeer::SESSIONID;
		}

		return $this;
	} // setSessionid()

	/**
	 * Set the value of [sessionage] column.
	 * 
	 * @param      double $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setSessionage($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->sessionage !== $v) {
			$this->sessionage = $v;
			$this->modifiedColumns[] = LogentryPeer::SESSIONAGE;
		}

		return $this;
	} // setSessionage()

	/**
	 * Set the value of [message] column.
	 * 
	 * @param      string $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setMessage($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->message !== $v) {
			$this->message = $v;
			$this->modifiedColumns[] = LogentryPeer::MESSAGE;
		}

		return $this;
	} // setMessage()

	/**
	 * Set the value of [referencetype] column.
	 * 
	 * @param      string $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setReferencetype($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->referencetype !== $v) {
			$this->referencetype = $v;
			$this->modifiedColumns[] = LogentryPeer::REFERENCETYPE;
		}

		return $this;
	} // setReferencetype()

	/**
	 * Set the value of [referenceid] column.
	 * 
	 * @param      int $v new value
	 * @return     Logentry The current object (for fluent API support)
	 */
	public function setReferenceid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->referenceid !== $v) {
			$this->referenceid = $v;
			$this->modifiedColumns[] = LogentryPeer::REFERENCEID;
		}

		return $this;
	} // setReferenceid()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->logentrytypeid !== 1) {
				return false;
			}

			if ($this->userid !== 1) {
				return false;
			}

			if ($this->customerid !== 1) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->logentryid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->logentrytypeid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->userid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->customerid = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->time = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->ip = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->sessionid = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->sessionage = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
			$this->message = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->referencetype = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->referenceid = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 11; // 11 = LogentryPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Logentry object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aLogentrytype !== null && $this->logentrytypeid !== $this->aLogentrytype->getLogentrytypeid()) {
			$this->aLogentrytype = null;
		}
		if ($this->aUser !== null && $this->userid !== $this->aUser->getUserid()) {
			$this->aUser = null;
		}
		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogentryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = LogentryPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aLogentrytype = null;
			$this->aUser = null;
			$this->aCustomer = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogentryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = LogentryQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogentryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				LogentryPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aLogentrytype !== null) {
				if ($this->aLogentrytype->isModified() || $this->aLogentrytype->isNew()) {
					$affectedRows += $this->aLogentrytype->save($con);
				}
				$this->setLogentrytype($this->aLogentrytype);
			}

			if ($this->aUser !== null) {
				if ($this->aUser->isModified() || $this->aUser->isNew()) {
					$affectedRows += $this->aUser->save($con);
				}
				$this->setUser($this->aUser);
			}

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = LogentryPeer::LOGENTRYID;
		if (null !== $this->logentryid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . LogentryPeer::LOGENTRYID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(LogentryPeer::LOGENTRYID)) {
			$modifiedColumns[':p' . $index++]  = '`LOGENTRYID`';
		}
		if ($this->isColumnModified(LogentryPeer::LOGENTRYTYPEID)) {
			$modifiedColumns[':p' . $index++]  = '`LOGENTRYTYPEID`';
		}
		if ($this->isColumnModified(LogentryPeer::USERID)) {
			$modifiedColumns[':p' . $index++]  = '`USERID`';
		}
		if ($this->isColumnModified(LogentryPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(LogentryPeer::TIME)) {
			$modifiedColumns[':p' . $index++]  = '`TIME`';
		}
		if ($this->isColumnModified(LogentryPeer::IP)) {
			$modifiedColumns[':p' . $index++]  = '`IP`';
		}
		if ($this->isColumnModified(LogentryPeer::SESSIONID)) {
			$modifiedColumns[':p' . $index++]  = '`SESSIONID`';
		}
		if ($this->isColumnModified(LogentryPeer::SESSIONAGE)) {
			$modifiedColumns[':p' . $index++]  = '`SESSIONAGE`';
		}
		if ($this->isColumnModified(LogentryPeer::MESSAGE)) {
			$modifiedColumns[':p' . $index++]  = '`MESSAGE`';
		}
		if ($this->isColumnModified(LogentryPeer::REFERENCETYPE)) {
			$modifiedColumns[':p' . $index++]  = '`REFERENCETYPE`';
		}
		if ($this->isColumnModified(LogentryPeer::REFERENCEID)) {
			$modifiedColumns[':p' . $index++]  = '`REFERENCEID`';
		}

		$sql = sprintf(
			'INSERT INTO `logentry` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`LOGENTRYID`':
						$stmt->bindValue($identifier, $this->logentryid, PDO::PARAM_INT);
						break;
					case '`LOGENTRYTYPEID`':
						$stmt->bindValue($identifier, $this->logentrytypeid, PDO::PARAM_INT);
						break;
					case '`USERID`':
						$stmt->bindValue($identifier, $this->userid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`TIME`':
						$stmt->bindValue($identifier, $this->time, PDO::PARAM_STR);
						break;
					case '`IP`':
						$stmt->bindValue($identifier, $this->ip, PDO::PARAM_STR);
						break;
					case '`SESSIONID`':
						$stmt->bindValue($identifier, $this->sessionid, PDO::PARAM_STR);
						break;
					case '`SESSIONAGE`':
						$stmt->bindValue($identifier, $this->sessionage, PDO::PARAM_STR);
						break;
					case '`MESSAGE`':
						$stmt->bindValue($identifier, $this->message, PDO::PARAM_STR);
						break;
					case '`REFERENCETYPE`':
						$stmt->bindValue($identifier, $this->referencetype, PDO::PARAM_STR);
						break;
					case '`REFERENCEID`':
						$stmt->bindValue($identifier, $this->referenceid, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setLogentryid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aLogentrytype !== null) {
				if (!$this->aLogentrytype->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLogentrytype->getValidationFailures());
				}
			}

			if ($this->aUser !== null) {
				if (!$this->aUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aUser->getValidationFailures());
				}
			}

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}


			if (($retval = LogentryPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogentryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getLogentryid();
				break;
			case 1:
				return $this->getLogentrytypeid();
				break;
			case 2:
				return $this->getUserid();
				break;
			case 3:
				return $this->getCustomerid();
				break;
			case 4:
				return $this->getTime();
				break;
			case 5:
				return $this->getIp();
				break;
			case 6:
				return $this->getSessionid();
				break;
			case 7:
				return $this->getSessionage();
				break;
			case 8:
				return $this->getMessage();
				break;
			case 9:
				return $this->getReferencetype();
				break;
			case 10:
				return $this->getReferenceid();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Logentry'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Logentry'][$this->getPrimaryKey()] = true;
		$keys = LogentryPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getLogentryid(),
			$keys[1] => $this->getLogentrytypeid(),
			$keys[2] => $this->getUserid(),
			$keys[3] => $this->getCustomerid(),
			$keys[4] => $this->getTime(),
			$keys[5] => $this->getIp(),
			$keys[6] => $this->getSessionid(),
			$keys[7] => $this->getSessionage(),
			$keys[8] => $this->getMessage(),
			$keys[9] => $this->getReferencetype(),
			$keys[10] => $this->getReferenceid(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aLogentrytype) {
				$result['Logentrytype'] = $this->aLogentrytype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aUser) {
				$result['User'] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogentryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setLogentryid($value);
				break;
			case 1:
				$this->setLogentrytypeid($value);
				break;
			case 2:
				$this->setUserid($value);
				break;
			case 3:
				$this->setCustomerid($value);
				break;
			case 4:
				$this->setTime($value);
				break;
			case 5:
				$this->setIp($value);
				break;
			case 6:
				$this->setSessionid($value);
				break;
			case 7:
				$this->setSessionage($value);
				break;
			case 8:
				$this->setMessage($value);
				break;
			case 9:
				$this->setReferencetype($value);
				break;
			case 10:
				$this->setReferenceid($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogentryPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setLogentryid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setLogentrytypeid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUserid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCustomerid($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTime($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIp($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSessionid($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSessionage($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setMessage($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setReferencetype($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setReferenceid($arr[$keys[10]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(LogentryPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogentryPeer::LOGENTRYID)) $criteria->add(LogentryPeer::LOGENTRYID, $this->logentryid);
		if ($this->isColumnModified(LogentryPeer::LOGENTRYTYPEID)) $criteria->add(LogentryPeer::LOGENTRYTYPEID, $this->logentrytypeid);
		if ($this->isColumnModified(LogentryPeer::USERID)) $criteria->add(LogentryPeer::USERID, $this->userid);
		if ($this->isColumnModified(LogentryPeer::CUSTOMERID)) $criteria->add(LogentryPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(LogentryPeer::TIME)) $criteria->add(LogentryPeer::TIME, $this->time);
		if ($this->isColumnModified(LogentryPeer::IP)) $criteria->add(LogentryPeer::IP, $this->ip);
		if ($this->isColumnModified(LogentryPeer::SESSIONID)) $criteria->add(LogentryPeer::SESSIONID, $this->sessionid);
		if ($this->isColumnModified(LogentryPeer::SESSIONAGE)) $criteria->add(LogentryPeer::SESSIONAGE, $this->sessionage);
		if ($this->isColumnModified(LogentryPeer::MESSAGE)) $criteria->add(LogentryPeer::MESSAGE, $this->message);
		if ($this->isColumnModified(LogentryPeer::REFERENCETYPE)) $criteria->add(LogentryPeer::REFERENCETYPE, $this->referencetype);
		if ($this->isColumnModified(LogentryPeer::REFERENCEID)) $criteria->add(LogentryPeer::REFERENCEID, $this->referenceid);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogentryPeer::DATABASE_NAME);
		$criteria->add(LogentryPeer::LOGENTRYID, $this->logentryid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getLogentryid();
	}

	/**
	 * Generic method to set the primary key (logentryid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setLogentryid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getLogentryid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Logentry (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setLogentrytypeid($this->getLogentrytypeid());
		$copyObj->setUserid($this->getUserid());
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setTime($this->getTime());
		$copyObj->setIp($this->getIp());
		$copyObj->setSessionid($this->getSessionid());
		$copyObj->setSessionage($this->getSessionage());
		$copyObj->setMessage($this->getMessage());
		$copyObj->setReferencetype($this->getReferencetype());
		$copyObj->setReferenceid($this->getReferenceid());
		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setLogentryid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Logentry Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     LogentryPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogentryPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Logentrytype object.
	 *
	 * @param      Logentrytype $v
	 * @return     Logentry The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setLogentrytype(Logentrytype $v = null)
	{
		if ($v === null) {
			$this->setLogentrytypeid(1);
		} else {
			$this->setLogentrytypeid($v->getLogentrytypeid());
		}

		$this->aLogentrytype = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Logentrytype object, it will not be re-added.
		if ($v !== null) {
			$v->addLogentry($this);
		}

		return $this;
	}


	/**
	 * Get the associated Logentrytype object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Logentrytype The associated Logentrytype object.
	 * @throws     PropelException
	 */
	public function getLogentrytype(PropelPDO $con = null)
	{
		if ($this->aLogentrytype === null && ($this->logentrytypeid !== null)) {
			$this->aLogentrytype = LogentrytypeQuery::create()->findPk($this->logentrytypeid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aLogentrytype->addLogentrys($this);
			 */
		}
		return $this->aLogentrytype;
	}

	/**
	 * Declares an association between this object and a User object.
	 *
	 * @param      User $v
	 * @return     Logentry The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setUser(User $v = null)
	{
		if ($v === null) {
			$this->setUserid(1);
		} else {
			$this->setUserid($v->getUserid());
		}

		$this->aUser = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the User object, it will not be re-added.
		if ($v !== null) {
			$v->addLogentry($this);
		}

		return $this;
	}


	/**
	 * Get the associated User object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     User The associated User object.
	 * @throws     PropelException
	 */
	public function getUser(PropelPDO $con = null)
	{
		if ($this->aUser === null && ($this->userid !== null)) {
			$this->aUser = UserQuery::create()->findPk($this->userid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aUser->addLogentrys($this);
			 */
		}
		return $this->aUser;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     Logentry The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addLogentry($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addLogentrys($this);
			 */
		}
		return $this->aCustomer;
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->logentryid = null;
		$this->logentrytypeid = null;
		$this->userid = null;
		$this->customerid = null;
		$this->time = null;
		$this->ip = null;
		$this->sessionid = null;
		$this->sessionage = null;
		$this->message = null;
		$this->referencetype = null;
		$this->referenceid = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

		$this->aLogentrytype = null;
		$this->aUser = null;
		$this->aCustomer = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(LogentryPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseLogentry
