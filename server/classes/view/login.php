			<div class="loginform">
				<form method="post" action="<?=BASEURL?>/">
					<fieldset class="info_fieldset">
						<div>
							<label><?=ucfirst(Localizer::getText('username or email'));?></label>
							<input class="textbox" id="email" type="text" name="email" value="" /><br />
						</div>
						<div>
							<label><?=ucfirst(Localizer::getText('password'));?></label>
							<input class="textbox" type="password" name="password" value="" /><br />
						</div>
						<input class="button" type="submit" name="submit" value="<?=ucfirst(Localizer::getText('login'));?>">
					</fieldset>
				</form>
			</div>
			<script type="text/javascript">
				$(function(){
					$("#email").focus();
				});
			</script>