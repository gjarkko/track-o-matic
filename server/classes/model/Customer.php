<?php



/**
 * Skeleton subclass for representing a row from the 'customer' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Customer extends BaseCustomer {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
			),
		);
	}
	
	public static function getCrudMap() {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
		
		
	public static function getEditorColumnMap() {
		return $this->getBrowserColumnMap();
	}
} // Customer
