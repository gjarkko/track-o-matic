#!/usr/bin/php -q
<?php

$interval = 2;

// Guess the application installation path, set some path constants
define('ROOT_PATH', implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -1)) . DIRECTORY_SEPARATOR);
define('CLASS_PATH', ROOT_PATH . 'classes' . DIRECTORY_SEPARATOR);
define('VENDOR_PATH', ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR);
define('CONFIG_PATH', ROOT_PATH . 'config' . DIRECTORY_SEPARATOR);
define('TESTS_PATH', ROOT_PATH . 'tests' . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', ROOT_PATH . 'public' . DIRECTORY_SEPARATOR);
define('BASEURL', '/tom');

// Set include paths
set_include_path(CLASS_PATH . ':' . ROOT_PATH . ':' . VENDOR_PATH . ':' . get_include_path());

ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);


// Include the bare necessities & register autoloader
include 'AutoLoader.php';
include 'Tools.php';
spl_autoload_register('AutoLoader::autoload');
register_shutdown_function('Tools::shutdown');

// Init Propel
Propel::init(CONFIG_PATH . "propel-conf.php");

// Bootstrap & run
$app = new Application();
$app->bootstrap();

$notificationSender = new NotificationSender();
while (true) {
	try {
		$notificationSender->sendCreationNotifications();
		$notificationSender->sendProximityNotifications();
		echo ".";
	} catch (Exception $e) {
		echo "x";
	}
	usleep($interval * 1000000);
}
