<?php


/**
 * Base class that represents a query for the 'logentrytype' table.
 *
 * 
 *
 * @method     LogentrytypeQuery orderByLogentrytypeid($order = Criteria::ASC) Order by the logentrytypeid column
 * @method     LogentrytypeQuery orderByLogentrytype($order = Criteria::ASC) Order by the logentrytype column
 *
 * @method     LogentrytypeQuery groupByLogentrytypeid() Group by the logentrytypeid column
 * @method     LogentrytypeQuery groupByLogentrytype() Group by the logentrytype column
 *
 * @method     LogentrytypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     LogentrytypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     LogentrytypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     LogentrytypeQuery leftJoinLogentry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Logentry relation
 * @method     LogentrytypeQuery rightJoinLogentry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Logentry relation
 * @method     LogentrytypeQuery innerJoinLogentry($relationAlias = null) Adds a INNER JOIN clause to the query using the Logentry relation
 *
 * @method     Logentrytype findOne(PropelPDO $con = null) Return the first Logentrytype matching the query
 * @method     Logentrytype findOneOrCreate(PropelPDO $con = null) Return the first Logentrytype matching the query, or a new Logentrytype object populated from the query conditions when no match is found
 *
 * @method     Logentrytype findOneByLogentrytypeid(int $logentrytypeid) Return the first Logentrytype filtered by the logentrytypeid column
 * @method     Logentrytype findOneByLogentrytype(string $logentrytype) Return the first Logentrytype filtered by the logentrytype column
 *
 * @method     array findByLogentrytypeid(int $logentrytypeid) Return Logentrytype objects filtered by the logentrytypeid column
 * @method     array findByLogentrytype(string $logentrytype) Return Logentrytype objects filtered by the logentrytype column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseLogentrytypeQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseLogentrytypeQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Logentrytype', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new LogentrytypeQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    LogentrytypeQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof LogentrytypeQuery) {
			return $criteria;
		}
		$query = new LogentrytypeQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Logentrytype|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = LogentrytypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(LogentrytypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Logentrytype A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `LOGENTRYTYPEID`, `LOGENTRYTYPE` FROM `logentrytype` WHERE `LOGENTRYTYPEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Logentrytype();
			$obj->hydrate($row);
			LogentrytypePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Logentrytype|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(LogentrytypePeer::LOGENTRYTYPEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(LogentrytypePeer::LOGENTRYTYPEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the logentrytypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLogentrytypeid(1234); // WHERE logentrytypeid = 1234
	 * $query->filterByLogentrytypeid(array(12, 34)); // WHERE logentrytypeid IN (12, 34)
	 * $query->filterByLogentrytypeid(array('min' => 12)); // WHERE logentrytypeid > 12
	 * </code>
	 *
	 * @param     mixed $logentrytypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function filterByLogentrytypeid($logentrytypeid = null, $comparison = null)
	{
		if (is_array($logentrytypeid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(LogentrytypePeer::LOGENTRYTYPEID, $logentrytypeid, $comparison);
	}

	/**
	 * Filter the query on the logentrytype column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLogentrytype('fooValue');   // WHERE logentrytype = 'fooValue'
	 * $query->filterByLogentrytype('%fooValue%'); // WHERE logentrytype LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $logentrytype The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function filterByLogentrytype($logentrytype = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($logentrytype)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $logentrytype)) {
				$logentrytype = str_replace('*', '%', $logentrytype);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LogentrytypePeer::LOGENTRYTYPE, $logentrytype, $comparison);
	}

	/**
	 * Filter the query by a related Logentry object
	 *
	 * @param     Logentry $logentry  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function filterByLogentry($logentry, $comparison = null)
	{
		if ($logentry instanceof Logentry) {
			return $this
				->addUsingAlias(LogentrytypePeer::LOGENTRYTYPEID, $logentry->getLogentrytypeid(), $comparison);
		} elseif ($logentry instanceof PropelCollection) {
			return $this
				->useLogentryQuery()
				->filterByPrimaryKeys($logentry->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByLogentry() only accepts arguments of type Logentry or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Logentry relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function joinLogentry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Logentry');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Logentry');
		}

		return $this;
	}

	/**
	 * Use the Logentry relation Logentry object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentryQuery A secondary query class using the current class as primary query
	 */
	public function useLogentryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinLogentry($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Logentry', 'LogentryQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Logentrytype $logentrytype Object to remove from the list of results
	 *
	 * @return    LogentrytypeQuery The current query, for fluid interface
	 */
	public function prune($logentrytype = null)
	{
		if ($logentrytype) {
			$this->addUsingAlias(LogentrytypePeer::LOGENTRYTYPEID, $logentrytype->getLogentrytypeid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseLogentrytypeQuery