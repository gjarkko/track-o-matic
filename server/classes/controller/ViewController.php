<?php

class ViewController extends CrudController {
	
	protected $className;
	
	public function __construct() {
		parent::__construct();
		$this->view->setViewName('view');
	}
	
	protected function buildViewData(BaseObject $object) {
		
		$this->view->setValue('className', $this->className);
		$this->view->setValue('object', $object);
		
		// Fetch ordered data
		$class = $this->className;
				
		if (!is_callable($class . '::getCrudMap')) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'undefinedobject'));
			return;
		}
		
		$objectCrudMap = $class::getCrudMap();
		$viewValues = array();
		foreach ($objectCrudMap as $field) {
			
			if (isset($field['getMethod'])) {
				$methodName = $field['getMethod'];
			}
			else {
				$methodName = 'get' . ucfirst(strtolower($field['column']));
			}
			
			
			if (!empty($field['hideValue'])) {
				continue;
			}
			if (!empty($field['foreign'])) {
				if (empty($field['getMethod'])) {
					$foreignMethodName = $methodName;
					$viewValues[$field['label']] = $object->$methodName()->$foreignMethodName();
				}
				else {
					$methodName = $field['getMethod'];
					$viewValues[$field['label']] = $object->$methodName();
				}
				
			}
			else {
				$viewValues[$field['label']] = $object->$methodName();
			}
			
		}		
		$this->view->setValue('viewValues', $viewValues);
		$this->view->setValue('objectCrudMap', $objectCrudMap);
	}
	
	protected function viewAction() {
		
		if (!$this->requestHandler->getParam('id')) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('browser', strtolower($className)));
			return;
		}
		if (!$this->isAuthorized()) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
			return;
		}
		
		$queryClassName = $this->className . 'Query';
		$query = $queryClassName::create();
		if (is_callable($this->className . '::getValid')) {
			$query->filterByValid(1);
		}
		
		$object = $query->findPk($this->requestHandler->getParam('id'));
		if (!is_object($object)) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unknownobject'));
			return;
		}
		$this->buildViewData($object);
	}
	
	public function userAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view user';
		$this->className = 'User';
		$this->viewAction();
	}
	
	public function customerAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view customer';
		$this->className = 'Customer';
		$this->viewAction();
	}
	
	
	public function languageAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view language';
		$this->className = 'Language';
		$this->viewAction();
	}
	
	public function localizationAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view translation';
		$this->className = 'Localization';
		$this->viewAction();
	}
	
	public function countryAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view country';
		$this->className = 'Country';
		$this->viewAction();
	}
	
	public function addressAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view address';
		$this->className = 'Address';
		$this->viewAction();
	}
	
	public function trackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view trackable';
		$this->className = 'Trackable';
		$this->viewAction();
	}
	
	public function groupreferenceAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view group reference';
		$this->className = 'Groupreference';
		$this->viewAction();
	}
	
	public function tripAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view trip';
		$this->className = 'Trip';
		$this->viewAction();
	}
	
	public function vehicleAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view vehicle';
		$this->className = 'Vehicle';
		$this->viewAction();
	}
	
	public function tripvehicleAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view trip on vehicle';
		$this->className = 'Tripvehicle';
		$this->viewAction();
	}

	public function detailtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view detail type';
		$this->className = 'Detailtype';
		$this->viewAction();
	}
	
	public function detailAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'view detail';
		$this->className = 'Detail';
		$this->viewAction();
	}

	public function triptrackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view trackable on trip';
		$this->className = 'Triptrackable';
		$this->viewAction();
	}
	
	public function statustypeAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view status type';
		$this->className = 'Statustype';
		$this->viewAction();
	}	
	
	public function vehiclestatusAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'view status';
		$this->className = 'Vehiclestatus';
		$this->viewAction();
	}
	
	public function notificationAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit notification';
		$this->className = 'Notification';
		$this->viewAction();
	}
	
	public function notificationtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit notification type';
		$this->className = 'Notificationtype';
		$this->viewAction();
	}
}