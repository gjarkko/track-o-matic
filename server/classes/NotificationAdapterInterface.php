<?php
interface NotificationAdapterInterface {
	public function sendNotification($receiver, $message, $subject = NULL,  $mime = 'text/plain');
	public function isHtml();
}