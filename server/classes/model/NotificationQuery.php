<?php



/**
 * Skeleton subclass for performing query and update operations on the 'notification' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class NotificationQuery extends BaseNotificationQuery {

	public function findCreationUnsent() {
		return NotificationQuery::create()->findByCreationsent(false);
	}
	
	public function findUnsent() {
		$con = Propel::getConnection();
		$sql = "
			SELECT 
                n.*
            FROM
                notification n
            WHERE
                n.creationsent = 1
                AND n.notificationsent = 0
			
		";
		$stmt = $con->query($sql);
		$array = NotificationPeer::populateObjects($stmt);
		
		$formatter = new PropelObjectFormatter();
		$formatter->setClass('Notification');
		$formatter->setPeer('NotificationPeer');
		$formatter->setDbName(Propel::getDefaultDB());
		$collection = new PropelObjectCollection();
		$collection->setModel('Notification');
		$collection->setFormatter($formatter);
		if (!empty($array)) {
			foreach($array as $notification) {
				$collection->append($notification);
			}
		}
		return $collection;
	}
	
	public function findSendable() {
		$notifications = $this->findUnsent();
		if (count($notifications) > 0) {
			foreach ($notifications as $key => $notification) {
				$targetLat = $notification->getTrackable()->getAddressRelatedByEndAddressid()->getLat();
				$targetLng = $notification->getTrackable()->getAddressRelatedByEndAddressid()->getLng();
				$currentStatus = $notification->getTrackable()->getLastStatus();
				if (!is_object($currentStatus)) {
					$notifications->remove($key);
					continue;
				}
				$currentLat = $currentStatus->getLat();
				$currentLng = $currentStatus->getLng();
				$tolerance = $notification->getProximity();
		
				$distanceToTarget = Tools::haversineDistance($targetLat, $targetLng, $currentLat, $currentLng);
				if ($distanceToTarget > $tolerance) {
					$notifications->remove($key);
					continue;
				}
			}
		}
		return $notifications;
	}
	
} // NotificationQuery
