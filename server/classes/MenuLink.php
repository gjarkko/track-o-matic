<?

class MenuLink {
	private $url;
	private $text;
	private $controller;
	private $action;
	private $active;
	
	public function __construct($controller, $action, $text = false) {
		$this->controller = $controller;
		$this->action = $action;
		
		if ($text) {
			$this->text = $text;
		}
		else {
			$this->text = $action;
		}
	}
	
	public function getText() {
		return $this->text;
	}
	
	public function getUrl() {
		if (!isset($this->url)) {
			$this->url = BASEURL . '/' . $this->controller . '/' . $this->action;
		}
		return $this->url;
	}
	
	public function getActive() {
		if (!isset($this->active)) {
			
			$controllerClass = RequestHandler::getInstance()->getControllerClass($this->controller);
			$actionMethod = RequestHandler::getInstance()->getActionMethod($controllerClass, $this->action);
			
			$this->active = 
				$controllerClass == RequestHandler::getInstance()->getCurrentControllerName()
				&& $actionMethod == RequestHandler::getInstance()->getCurrentActionName();
		}
		return $this->active;
	}
}