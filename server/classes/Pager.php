<?

class Pager {
	public static function getPagingLinks($page, $pages) {
		$pagingLinks = array(
			'backward' => array(), 
			'pages' => array(), 
			'forward' => array(),
		);
		$pageLinksShown = Registry::get('page_links_shown');
		
		if ($page > 1) {
			$pagingLinks['backward'][] = array(
				'link' => true,
				'text' => ucfirst(Localizer::getText('first')),
			    'image' => 'first.png',
				'pageNo' => 1,
				'current' => NULL,
			);
			$pagingLinks['backward'][] = array(
				'link' => true,
				'text' => ucfirst(Localizer::getText('previous')),
				'image' => 'previous.png',
				'pageNo' => $page - 1,
				'current' => NULL,
			);
		}
		else {
			$pagingLinks['backward'][] = array(
				'link' => false,
				'text' => ucfirst(Localizer::getText('next')),
				'image' => 'next.png',
				'pageNo' => 1,
				'current' => NULL,
			);
			$pagingLinks['backward'][] = array(
				'link' => false,
				'text' => ucfirst(Localizer::getText('last')),
			    'image' => 'last.png',
				'pageNo' => $page - 1,
				'current' => NULL,
			);
		}
		
		$hasLesser = false;
		$hasGreater = false;
		$pagingLinksCommon = array();
		foreach (range(1, $pages) as $i) {
			if (abs($i - $page) <= floor($pageLinksShown/2)) {
				if ($i == $page) {
					$pagingLinksCommon[] = array(
						'link' => false,
						'text' =>  (string)$i,
						'pageNo' => $i,
						'current' => true,
					);
				}
				else {
					$pagingLinksCommon[] = array(
						'link' => true,
						'text' =>  (string)$i,
						'pageNo' => $i,
						'current' => false,
					);
				}
			}
			elseif ($i - $page < -floor($pageLinksShown/2)) {
				$hasLesser = true;
			}
			else {
				$hasGreater = true;
			}
		}
		if ($hasLesser) {
			$pagingLinks['pages'][] = array(
				'link' => false,
				'text' =>  '...',
				'pageNo' => NULL,
				'current' => NULL,
			);
		}
		$pagingLinks['pages'] = array_merge($pagingLinks['pages'], $pagingLinksCommon);
		if ($hasGreater) {
			$pagingLinks['pages'][] = array(
				'link' => false,
				'text' =>  '...',
				'pageNo' => NULL,
				'current' => NULL,
			);
		}
		if ($page < $pages) {
			$pagingLinks['forward'][] = array(
				'link' => true,
				'text' => ucfirst(Localizer::getText('next')),
			    'image' => 'next.png',
				'pageNo' => $page+1,
				'current' => NULL,
			);
			$pagingLinks['forward'][] = array(
				'link' => true,
				'text' => ucfirst(Localizer::getText('last')),
			    'image' => 'last.png',
				'pageNo' => $pages,
				'current' => NULL,
			);
		}
		else {
			$pagingLinks['forward'][] = array(
				'link' => false,
				'text' => ucfirst(Localizer::getText('first')),
			    'image' => 'first.png',
				'pageNo' => $page+1,
				'current' => NULL,
			);
			$pagingLinks['forward'][] = array(
				'link' => false,
				'text' => ucfirst(Localizer::getText('previous')),
			    'image' => 'previous.png',
				'pageNo' => $pages,
				'current' => NULL,
			);
		}
		return $pagingLinks;	
	}
	
}