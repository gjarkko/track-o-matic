<?php



/**
 * Skeleton subclass for representing a row from the 'statustype' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Statustype extends BaseStatustype {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
			),		
			array(
				'column' => 'statustype',
				'label' => ucfirst(Localizer::getText('status type')),
			),
			array(
				'column' => 'ismoving',
				'label' => ucfirst(Localizer::getText('moving')),
			),
			array(
				'column' => 'haslocation',
				'label' => ucfirst(Localizer::getText('has location')),
			),
			array(
				'column' => 'isfinal',
				'label' => ucfirst(Localizer::getText('final')),
			),
		);
	}
		
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'customer',
				'label' => ucfirst(Localizer::getText('customer')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
			),		
			array(
				'column' => 'statustype',
				'label' => ucfirst(Localizer::getText('status type')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'ismoving',
				'label' => ucfirst(Localizer::getText('moving')),
				'required' => false,
				'inputType' => 'checkbox',
				'default' => true,
			),
			array(
				'column' => 'haslocation',
				'label' => ucfirst(Localizer::getText('has location')),
				'required' => false,
				'inputType' => 'checkbox',
				'default' => true,
			),
			array(
				'column' => 'isfinal',
				'label' => ucfirst(Localizer::getText('is final')),
				'required' => false,
				'inputType' => 'checkbox',
				'default' => false,
			),
		);
	}

} // Statustype
