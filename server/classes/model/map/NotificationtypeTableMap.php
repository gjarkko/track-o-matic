<?php



/**
 * This class defines the structure of the 'notificationtype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class NotificationtypeTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.NotificationtypeTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('notificationtype');
		$this->setPhpName('Notificationtype');
		$this->setClassname('Notificationtype');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('NOTIFICATIONTYPEID', 'Notificationtypeid', 'INTEGER', true, 10, null);
		$this->addColumn('NOTIFICATIONTYPE', 'Notificationtype', 'VARCHAR', true, 60, null);
		// validators
		$this->addValidator('NOTIFICATIONTYPE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'notification type must be at least 1 character');
		$this->addValidator('NOTIFICATIONTYPE', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'notification type must be at most 60 characters');
		$this->addValidator('NOTIFICATIONTYPE', 'required', 'propel.validator.RequiredValidator', '', 'notification type is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Notification', 'Notification', RelationMap::ONE_TO_MANY, array('notificationtypeid' => 'notificationtypeid', ), 'CASCADE', 'CASCADE', 'Notifications');
	} // buildRelations()

} // NotificationtypeTableMap
