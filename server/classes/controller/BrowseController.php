<?php
/**
 *
 * Generic object browser
 * Makes HTML views of objects
 * Supports ordering, grouping, paging
 */
class BrowseController extends XmlController {
	
	protected $className;
	protected $keys;
	
	public function __construct() {
		parent::__construct();
		
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->view->setViewName('browse');
		
		// Set default order
		if (!$this->requestHandler->getParam('order')) {
			$this->view->setValue('order', 0);
		}
		else {
			$this->view->setValue('order', (int)$this->requestHandler->getParam('order'));
		}

		// Set default direction
		if (!$this->requestHandler->getParam('direction') || !in_array($this->requestHandler->getParam('direction'), array('ASC', 'DESC'))) {
			$this->view->setValue('direction', 'ASC');
		}
		else {
			$this->view->setValue('direction', $this->requestHandler->getParam('direction'));
		}
		
		// Set default page
		if (!$this->requestHandler->getParam('page') || (int)$this->requestHandler->getParam('page') < 0) {
			$this->view->setValue('page', 1);
		}
		else {
			$this->view->setValue('page', $this->requestHandler->getParam('page'));
		}
	}

	protected function buildBrowserData() {
		
		$this->view->setValue('className', $this->className);
		
		// Fetch ordered data
		$class = $this->className;
		$queryClass = $class . 'Query';
		$query = $queryClass::create();
		/* @var $query ModelCriteria */
		
		if (is_callable($this->className . '::getValid')) {
			$query->filterByValid(1);
		}
		
		
		if (
			Session::getInstance()->getUser()->getUsertypeid() > Controller::USERTYPE_ADMIN 
			&& is_callable($this->className . '::getCustomer')
		) {
			$query->filterByCustomer(Session::getInstance()->getUser()->getCustomer());
		}
		
		// Ordering
		$objectBrowserColumnMap = NULL;
		if (is_callable($class . '::getBrowserColumnMap')) {
			$user = Session::getInstance()->getUser();
			$objectBrowserColumnMap = $class::getBrowserColumnMap($user);
			if (isset($objectBrowserColumnMap[$this->view->getValue('order')])) {
				
				
				// Get the column definition for the column used for ordering
				$orderColumn = $objectBrowserColumnMap[$this->view->getValue('order')];
				
				$direction = ($this->view->getValue('direction') == 'ASC') ? ModelCriteria::ASC : ModelCriteria::DESC;
				if (!empty($orderColumn['foreign']) || !empty($orderColumn['foreignChild'])) {

					// Join to foreign table for ordering
					if (empty($orderColumn['join'])) {
						$foreignClass = ucfirst(strtolower($orderColumn['column']));
						$table = $orderColumn['column'];
						$relation = $foreignClass;
						$foreignClassPeer = $foreignClass . 'Peer';
						if (!empty($orderColumn['foreign'])) {
							$foreignClassKey = $foreignClass . 'id';
						}
						else {
							$foreignClassKey = $class . 'id';
						}	
						$localClassKey = $foreignClassKey;
					} else {
						$foreignClass = ucfirst(strtolower($orderColumn['join']['table']));
						$table = $orderColumn['join']['table'];
						if (isset($orderColumn['join']['relation'])) {
							$relation = $orderColumn['join']['relation'];
						}
						else {
							$relation = $foreignClass;
						}
						$foreignClassPeer = $foreignClass . 'Peer';
						$foreignClassKey = $orderColumn['join']['foreign_key'];
						$localClassKey = $orderColumn['join']['own_key'];
					}
					
					if (isset($orderColumn['display_SQL'])) {
						$displaySql = $orderColumn['display_SQL'];
					}
					else {
						$displaySql = $foreignClass;
					}
					
					//Tools::dump($foreignClass);
					
					$foreignClassRelation = $foreignClass . ' ' . $relation; 
					
					$query->join($relation . ' ' . $relation);
					$conditions = array();
					$conditions[] = $relation . '.' . $foreignClassKey . '=' . strtolower($class) . '.' . $localClassKey;
					
					// This section was used with the foreignChild column type with countries and country names. Currently obsolete.
					
					if (!empty($orderColumn['joinArgs'])) {
						foreach ($orderColumn['joinArgs'] as $arg) {
							$conditions[] = $relation . '.' . $arg[0] . $arg[2] . $arg[1];
						}
					}
			
					$conditionIndex = 1;
					//Tools::dump($conditions);
					foreach($conditions as $condition) {
						$query->condition('cond' . $conditionIndex, $condition);
						if ($conditionIndex > 1) {
							$query->combine(array('cond' . ($conditionIndex-1),'cond' .  ($conditionIndex)), 'and', 'cond' . ($conditionIndex+1));
							$conditionIndex++;
						}
						$conditionIndex++;
					}
					
					$query->setJoinCondition($relation, 'cond' . ($conditionIndex-1));
					if ($direction == 'ASC') {
						$query->addAscendingOrderByColumn($displaySql);
					}
					elseif ($direction == 'DESC') {
						$query->addDescendingOrderByColumn($displaySql);
					}
					//$query->orderBy($relation . '.' . $displaySql, $direction);
				}
				else {
					$query->orderBy(ucfirst(strtolower($orderColumn['column'])), $direction);
				}
			}
		}
		
		
		// Paging: count pages and set current page on query
		//Tools::dump(Tools::getSQL($query), true);
		$objectCount = $query->count();
		$this->view->setValue('pages', max(array(1,ceil($objectCount / Registry::get('items_per_page')))));
		
		// If requested a page greater than max page, set view to max page
		if ($this->view->getValue('page') > $this->view->getValue('pages')) {
			$this->view->setValue('page', $this->view->getValue('pages'));
		}
		if ($objectCount > Registry::get('items_per_page')) {
			$query->setOffset(
				($this->view->getValue('page') - 1) * Registry::get('items_per_page')
			);
			$query->setLimit(Registry::get('items_per_page'));
		}
		
		//Tools::dump(Tools::getSQL($query));
		$browserData = $query->find();
	
		if (is_object($browserData) && get_class($browserData) == 'PropelObjectCollection') {
			$this->view->setValue('browserData', $browserData);
		}
		
		if (is_callable($class . '::getCrudMap')) {
			$objectCrudMap = $class::getCrudMap();
			$this->view->setValue('objectCrudMap', $objectCrudMap);
		}
	}
	
	public function customerAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'customers';
		$this->className = 'Customer';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function languageAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'languages';
		$this->className = 'Language';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function localizationAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'localizations';
		$this->className = 'Localization';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function userAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'users';
		$this->className = 'User';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function countryAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'countries';
		$this->className = 'Country';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function notificationtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'notification types';
		$this->className = 'Notificationtype';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function groupreferenceAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'trackablegroups';
		$this->className = 'Groupreference';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function tripAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'trips';
		$this->className = 'Trip';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function vehicleAction() {
		
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'vehicles';
		$this->className = 'Vehicle';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function tripvehicleAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'vehicle trips';
		$this->className = 'Tripvehicle';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function detailtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'detail types';
		$this->className = 'Detailtype';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function detailAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'details';
		$this->className = 'Detail';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function statustypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'status types';
		$this->className = 'Statustype';
		if ($this->isAuthorized()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function vehiclestatusAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'statuses';
		$this->className = 'Vehiclestatus';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function triptrackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'Trackables on trip';
		$this->className = 'Triptrackable';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function addressAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'Addresses';
		$this->className = 'Address';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function trackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'trackables';
		$this->className = 'Trackable';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
	public function notificationAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'notifications';
		$this->className = 'Notification';
		if ($this->isAuthorized() && $this->hasCustomerRelation()) {
			$this->buildBrowserData();
		}
		else {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
		}
	}
}
