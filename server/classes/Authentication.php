<?

class Authentication {
		
	public static function Authenticate() {
		$args = func_get_args();
		switch (func_num_args()) {
			case 1:
				
					$conditions = array(
						'apikey' => CryptographyProvider::encrypt($args[0]),
						'valid' => 1,
					);
					
					// Authentication by user API key.
					if ($user = UserQuery::create()->findOneByArray($conditions)) {
						Session::getInstance(false)->setUser($user);
						return true;
					}				

					// Authenticate by vehicle API key
					if ($vehicle = VehicleQuery::create()->findOneByArray($conditions)) {

						Registry::set('vehicleid', $vehicle->getPrimaryKey());
						$conditions = array(
							'customerid' => $vehicle->getCustomerId(),
							'usertypeid' => UsertypeQuery::create()->findOneByUsertype('Driver')->getUsertypeid(),
							'valid' => 1,
						);
						
						if ($user = UserQuery::create()->findOneByArray($conditions)) {
							Session::getInstance(false)->setUser($user);		
							return true;
						}
					}
					break;
			case 2:
					// Authentication by either user-password or email-password
					$shapass = sha1(Registry::get('password_salt') . $args[1]); // Emulate MySQL SHA() function
					$conditions = array(
						'username' => $args[0],
						'password' => $shapass,
						'valid' => 1,
					);
					if ($user = UserQuery::create()->findOneByArray($conditions)) {
						Session::getInstance()->setUser($user);
						return true;
					}
					$conditions = array(
						'email' => $args[0],
						'password' => $shapass,
						'valid' => 1,
					);
					if ($user = UserQuery::create()->findOneByArray($conditions)) {
						Session::getInstance()->setUser($user);
						return true;
					}
					
					Session::getInstance()->addFlashMessage(new FlashMessage(Localizer::getText('unknown user or password'), FlashMessage::WARNING));
					break;
						
		}
		return false;
	}
	
	
	
}