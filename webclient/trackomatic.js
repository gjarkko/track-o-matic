/**
 * JSON Java Script request object.
 * @param fullUrl
 * @returns {JSONJScriptRequest}
 */
function JSONJScriptRequest(fullUrl) {
    this.fullUrl = fullUrl; 
    this.noCacheIE = "&timestamp=" + (new Date()).getTime();
    this.headLoc = document.getElementsByTagName("head").item(0);
    this.scriptId = "JscriptId" + JSONJScriptRequest.scriptCounter++;
}

JSONJScriptRequest.scriptCounter = 1;

JSONJScriptRequest.prototype.buildScriptTag = function () {
    this.scriptObj = document.createElement("script");
    this.scriptObj.setAttribute("type", "text/javascript");
    this.scriptObj.setAttribute("charset", "utf-8");
    this.scriptObj.setAttribute("src", this.fullUrl + this.noCacheIE);
    this.scriptObj.setAttribute("id", this.scriptId);
};
 
JSONJScriptRequest.prototype.removeScriptTag = function () {
    this.headLoc.removeChild(this.scriptObj);  
};

JSONJScriptRequest.prototype.addScriptTag = function () {
    this.headLoc.appendChild(this.scriptObj);
};

var serverurl = "http://absx.dyndns.org/tom/api/getTrackableStatus";
var timeout = 0;
var delay = 1000;
var geocoder = null;
var map = null;
var mapOptions = null;
var overlays = [];
var lastGeocode = (new Date()).getTime();

/**
 * Calls the getTrackableStatus service as
 * a cross-domain HTTP request and passes 
 * the given reference to the service. The
 * service returns a Javascript callback function
 * with a JSON encoded string as the value
 * of the function argument.
 * 
 * @param reference
 */
function search(reference) {
	clearTimeout(timeout);
    $("#status").empty();
    $("#map_canvas").remove();
    $("#map_info").remove();
    $("#error").remove();
    geocoder = null;
    infowindow = null;
    map = null;
    mapOptions = null;
	document.getElementById("reference").style.backgroundImage = "url(indicator.gif)";
	reference = reference.toUpperCase();
	document.getElementById("reference").value = reference;
	var url = serverurl + "?apikey=M4YL-UQS3-W7QG-J6F61&reference=" + reference + "&callback=true"; 
	JSONJavaScriptRequest = new JSONJScriptRequest(url); 
	JSONJavaScriptRequest.buildScriptTag(); 
	JSONJavaScriptRequest.addScriptTag();
	document.getElementById("reference").style.backgroundImage = "url(magnifier.png)";
	
}

/**
 * Fetches the latest status from the defined server
 * for the given reference and and calls getTrackableStatus(reference)
 * @param reference
 */
function refresh(reference) {
	var url = serverurl + "?apikey=M4YL-UQS3-W7QG-J6F61&reference=" + reference + "&callback=true"; 
	JSONJavaScriptRequest = new JSONJScriptRequest(url); 
	JSONJavaScriptRequest.buildScriptTag(); 
	JSONJavaScriptRequest.addScriptTag();
}
/**
 * Extracts the latest status from the 
 * response and displays it. The status 
 * is displayed in a table and the 
 * geo-coordinates are plotted on a map.
 * The the status table and map are only
 * rendered if they have not been rendered
 * previously, otherwise the status
 * data is only refreshed, the marker is moved
 * on the map and the map is re-centered.
 * 
 * The call back function of search(reference) 
 * and refresh(reference) calls this function.
 * 
 * @param response - A JSON encoded string.
 */
function getTrackableStatus(response) {  
  
  var reference = document.getElementById("reference").value;
  document.getElementById("reference").value = reference;
  
  if(response.statuses != null && response.statuses != undefined) {
	  if(response.statuses[reference][0] != null && response.statuses[reference][0] != undefined) {
		  
		  var lat = response.statuses[reference][0].lat;
		  var lng = response.statuses[reference][0].lng;
		  var latlng = new google.maps.LatLng(lat, lng);
		  
		  if(geocoder == null) {
			  geocoder = new google.maps.Geocoder();
		  }
		  
		  if(document.getElementById("status").innerHTML == "") {
			  $("#status").append("<p><table style=\"width: 500px;\"><tr><td class=\"tom_label\">Timestamp</td><td class=\"tom_value\" id=\"timestamp\">" + response.statuses[reference][0].time + "</td>" + 
					              "<tr><td class=\"tom_label\">Status</td><td class=\"tom_value\" id=\"statustext\">" + response.statuses[reference][0].status + "</td></tr>" +
					              "<tr><td class=\"tom_label\">Approximate Address</td><td class=\"tom_value\" id=\"approxaddress\">Unknown</td></tr></table></p>");
			  $("#trackomatic").append("<div id=\"map_canvas\" name=\"map_canvas\" " +
			  						   "style=\"width:500px; height:500px; border: 1px solid rgb(50,50,100); " +
			  						   "-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;\"></div>");
		  } else {
			  document.getElementById("timestamp").innerHTML = response.statuses[reference][0].time;
			  document.getElementById("statustext").innerHTML = response.statuses[reference][0].status;
		  }
		  
		  mapOptions = {zoom: 13, center: latlng, mapTypeId: google.maps.MapTypeId.TERRAIN};
		  
		  if(map == null) {
			  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		  }
		  
		  map.setCenter(latlng);
		  if (!overlays[0]) {
			  overlays[0] = new google.maps.Marker({
				  map: map,
			  });
	      }
	      overlays[0].setPosition(latlng);
		  
	      if (Math.abs(lastGeocode - (new Date()).getTime()) > 5000) {
				geocoder.geocode({'latLng': latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					for(i = 0;i < results.length;i++) {
						if (results[i]) {
							overlays[0].setTitle(results[i].formatted_address)
							document.getElementById("approxaddress").innerHTML = results[i].formatted_address;
							break;
						} 
					}
				} else {
					console.log("Geocoder failed due to: " + status);
				}
			});
				lastGeocode = (new Date()).getTime();
	      }
			    
		    timeout = setTimeout("refresh('" + reference + "')", delay);
			
		    if(document.getElementById("map_info") == null) {
		    	$("#trackomatic").append("<div class=\"tom_text\" id=\"map_info\">The map will automatically refresh in " + (delay / 1000) + " seconds.</div>");
		    }
		    
	  } else {
		  $("#trackomatic").append("<p><div id=\"error\" name=\"error\" class=\"tom_error\"></div></p>");
		  $("#error").append("The latest status of reference '" + reference + "' is still unknown.");
		  clearTimeout(timeout);
	  }
    } else {
    	$("#trackomatic").append("<p><div id=\"error\" name=\"error\" class=\"tom_error\"></div></p>");
    	if(response.error != null && response.error != undefined) {
    		$("#error").append(response.error + ".");
    	} else {
    		$("#error").append("Unknown error");
    	}
    	clearTimeout(timeout);
    }
    JSONJavaScriptRequest.removeScriptTag();
	document.getElementById("reference").style.backgroundImage = "url(magnifier.png)";
}

var mapsDone = false;
var mapsok = function() {
	mapsDone = true;
}

/**
 * Lazy load JQuery if not already loaded
 */
load = {
	tryReady: function(time_elapsed) {
		if (typeof $ == "undefined" || !mapsDone) {
			if (time_elapsed <= 50000) {
				setTimeout("load.tryReady(" + (time_elapsed + 10) + ")", 10); // set a timer to check again in 200 ms.
			} else {
				console.log("Timed out while loading jQuery.")
			}
		}
		else {
			this.main();
		}
		return this;
	},
	getScript: function(filename) {
		
		var maps = document.createElement('script');
		maps.setAttribute("type", "text/javascript");
		maps.setAttribute("src", "http://maps.googleapis.com/maps/api/js?sensor=false&callback=mapsok");
		document.getElementsByTagName("head")[0].appendChild(maps);
		
		var script = document.createElement('script');
		script.setAttribute("type","text/javascript");
		script.setAttribute("src", filename);
		document.getElementsByTagName("head")[0].appendChild(script);
		
		
		return this;
	},
	main: function() {
		/**
		 * Generates search UI dynamically around the script element's parent.
		 */
		var container = $('script[src*="trackomatic.js"]').parent();
		container.attr('id', 'trackomatic');
		
		$("body").append('<link href="trackomatic.css" rel="stylesheet" type="text/css" />'); 
		$("#trackomatic").append("<input type=\"text\" id=\"reference\" class=\"tom_search_field\" />&nbsp;");
		$("#trackomatic").append("<input type=\"button\" class=\"tom_button\" id=\"search\" value=\"search\" />");
		$("#trackomatic").append("<div id=\"status\"></div>");
		$("#reference").keyup(function(event) {
			if(event.keyCode == 13){
				$("#search").click();
			}
		});
	    $("#reference").focus();
		$("#search").click(function () {
			search($("#reference").val());
			return false;
		});
	}
}
load.getScript("http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js").tryReady(0);