<?php



/**
 * Skeleton subclass for representing a row from the 'language' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Language extends BaseLanguage {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language in English')),
			),
			array(
				'column' => 'languagelocal',
				'label' => ucfirst(Localizer::getText('language in local')),
			),
		);
	}
		
	
	public static function getCrudMap() {
		return array(
			array(
				'column' => 'language',
				'label' => ucfirst(Localizer::getText('language')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array (
				'column' => 'languagelocal',
				'label' => ucfirst(Localizer::getText('local language')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			)
		);
	}
} // Language
