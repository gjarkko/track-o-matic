<?php


/**
 * Base class that represents a query for the 'trackablestatus' table.
 *
 * 
 *
 * @method     TrackablestatusQuery orderByTrackablestatusid($order = Criteria::ASC) Order by the trackablestatusid column
 * @method     TrackablestatusQuery orderByStatustypeid($order = Criteria::ASC) Order by the statustypeid column
 * @method     TrackablestatusQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 * @method     TrackablestatusQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     TrackablestatusQuery orderByLng($order = Criteria::ASC) Order by the lng column
 * @method     TrackablestatusQuery orderByTime($order = Criteria::ASC) Order by the time column
 * @method     TrackablestatusQuery orderByMessage($order = Criteria::ASC) Order by the message column
 *
 * @method     TrackablestatusQuery groupByTrackablestatusid() Group by the trackablestatusid column
 * @method     TrackablestatusQuery groupByStatustypeid() Group by the statustypeid column
 * @method     TrackablestatusQuery groupByTrackableid() Group by the trackableid column
 * @method     TrackablestatusQuery groupByLat() Group by the lat column
 * @method     TrackablestatusQuery groupByLng() Group by the lng column
 * @method     TrackablestatusQuery groupByTime() Group by the time column
 * @method     TrackablestatusQuery groupByMessage() Group by the message column
 *
 * @method     TrackablestatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TrackablestatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TrackablestatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TrackablestatusQuery leftJoinStatustype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Statustype relation
 * @method     TrackablestatusQuery rightJoinStatustype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Statustype relation
 * @method     TrackablestatusQuery innerJoinStatustype($relationAlias = null) Adds a INNER JOIN clause to the query using the Statustype relation
 *
 * @method     TrackablestatusQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     TrackablestatusQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     TrackablestatusQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     Trackablestatus findOne(PropelPDO $con = null) Return the first Trackablestatus matching the query
 * @method     Trackablestatus findOneOrCreate(PropelPDO $con = null) Return the first Trackablestatus matching the query, or a new Trackablestatus object populated from the query conditions when no match is found
 *
 * @method     Trackablestatus findOneByTrackablestatusid(int $trackablestatusid) Return the first Trackablestatus filtered by the trackablestatusid column
 * @method     Trackablestatus findOneByStatustypeid(int $statustypeid) Return the first Trackablestatus filtered by the statustypeid column
 * @method     Trackablestatus findOneByTrackableid(int $trackableid) Return the first Trackablestatus filtered by the trackableid column
 * @method     Trackablestatus findOneByLat(string $lat) Return the first Trackablestatus filtered by the lat column
 * @method     Trackablestatus findOneByLng(string $lng) Return the first Trackablestatus filtered by the lng column
 * @method     Trackablestatus findOneByTime(string $time) Return the first Trackablestatus filtered by the time column
 * @method     Trackablestatus findOneByMessage(string $message) Return the first Trackablestatus filtered by the message column
 *
 * @method     array findByTrackablestatusid(int $trackablestatusid) Return Trackablestatus objects filtered by the trackablestatusid column
 * @method     array findByStatustypeid(int $statustypeid) Return Trackablestatus objects filtered by the statustypeid column
 * @method     array findByTrackableid(int $trackableid) Return Trackablestatus objects filtered by the trackableid column
 * @method     array findByLat(string $lat) Return Trackablestatus objects filtered by the lat column
 * @method     array findByLng(string $lng) Return Trackablestatus objects filtered by the lng column
 * @method     array findByTime(string $time) Return Trackablestatus objects filtered by the time column
 * @method     array findByMessage(string $message) Return Trackablestatus objects filtered by the message column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTrackablestatusQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseTrackablestatusQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Trackablestatus', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TrackablestatusQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TrackablestatusQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TrackablestatusQuery) {
			return $criteria;
		}
		$query = new TrackablestatusQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Trackablestatus|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = TrackablestatusPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(TrackablestatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trackablestatus A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `TRACKABLESTATUSID`, `STATUSTYPEID`, `TRACKABLEID`, `LAT`, `LNG`, `TIME`, `MESSAGE` FROM `trackablestatus` WHERE `TRACKABLESTATUSID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Trackablestatus();
			$obj->hydrate($row);
			TrackablestatusPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trackablestatus|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TrackablestatusPeer::TRACKABLESTATUSID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TrackablestatusPeer::TRACKABLESTATUSID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the trackablestatusid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackablestatusid(1234); // WHERE trackablestatusid = 1234
	 * $query->filterByTrackablestatusid(array(12, 34)); // WHERE trackablestatusid IN (12, 34)
	 * $query->filterByTrackablestatusid(array('min' => 12)); // WHERE trackablestatusid > 12
	 * </code>
	 *
	 * @param     mixed $trackablestatusid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByTrackablestatusid($trackablestatusid = null, $comparison = null)
	{
		if (is_array($trackablestatusid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TrackablestatusPeer::TRACKABLESTATUSID, $trackablestatusid, $comparison);
	}

	/**
	 * Filter the query on the statustypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByStatustypeid(1234); // WHERE statustypeid = 1234
	 * $query->filterByStatustypeid(array(12, 34)); // WHERE statustypeid IN (12, 34)
	 * $query->filterByStatustypeid(array('min' => 12)); // WHERE statustypeid > 12
	 * </code>
	 *
	 * @see       filterByStatustype()
	 *
	 * @param     mixed $statustypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByStatustypeid($statustypeid = null, $comparison = null)
	{
		if (is_array($statustypeid)) {
			$useMinMax = false;
			if (isset($statustypeid['min'])) {
				$this->addUsingAlias(TrackablestatusPeer::STATUSTYPEID, $statustypeid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($statustypeid['max'])) {
				$this->addUsingAlias(TrackablestatusPeer::STATUSTYPEID, $statustypeid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::STATUSTYPEID, $statustypeid, $comparison);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @see       filterByTrackable()
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid)) {
			$useMinMax = false;
			if (isset($trackableid['min'])) {
				$this->addUsingAlias(TrackablestatusPeer::TRACKABLEID, $trackableid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($trackableid['max'])) {
				$this->addUsingAlias(TrackablestatusPeer::TRACKABLEID, $trackableid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query on the lat column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLat(1234); // WHERE lat = 1234
	 * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
	 * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
	 * </code>
	 *
	 * @param     mixed $lat The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByLat($lat = null, $comparison = null)
	{
		if (is_array($lat)) {
			$useMinMax = false;
			if (isset($lat['min'])) {
				$this->addUsingAlias(TrackablestatusPeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lat['max'])) {
				$this->addUsingAlias(TrackablestatusPeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::LAT, $lat, $comparison);
	}

	/**
	 * Filter the query on the lng column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLng(1234); // WHERE lng = 1234
	 * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
	 * $query->filterByLng(array('min' => 12)); // WHERE lng > 12
	 * </code>
	 *
	 * @param     mixed $lng The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByLng($lng = null, $comparison = null)
	{
		if (is_array($lng)) {
			$useMinMax = false;
			if (isset($lng['min'])) {
				$this->addUsingAlias(TrackablestatusPeer::LNG, $lng['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lng['max'])) {
				$this->addUsingAlias(TrackablestatusPeer::LNG, $lng['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::LNG, $lng, $comparison);
	}

	/**
	 * Filter the query on the time column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTime('2011-03-14'); // WHERE time = '2011-03-14'
	 * $query->filterByTime('now'); // WHERE time = '2011-03-14'
	 * $query->filterByTime(array('max' => 'yesterday')); // WHERE time > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $time The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByTime($time = null, $comparison = null)
	{
		if (is_array($time)) {
			$useMinMax = false;
			if (isset($time['min'])) {
				$this->addUsingAlias(TrackablestatusPeer::TIME, $time['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($time['max'])) {
				$this->addUsingAlias(TrackablestatusPeer::TIME, $time['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::TIME, $time, $comparison);
	}

	/**
	 * Filter the query on the message column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
	 * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $message The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByMessage($message = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($message)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $message)) {
				$message = str_replace('*', '%', $message);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TrackablestatusPeer::MESSAGE, $message, $comparison);
	}

	/**
	 * Filter the query by a related Statustype object
	 *
	 * @param     Statustype|PropelCollection $statustype The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByStatustype($statustype, $comparison = null)
	{
		if ($statustype instanceof Statustype) {
			return $this
				->addUsingAlias(TrackablestatusPeer::STATUSTYPEID, $statustype->getStatustypeid(), $comparison);
		} elseif ($statustype instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TrackablestatusPeer::STATUSTYPEID, $statustype->toKeyValue('PrimaryKey', 'Statustypeid'), $comparison);
		} else {
			throw new PropelException('filterByStatustype() only accepts arguments of type Statustype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Statustype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function joinStatustype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Statustype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Statustype');
		}

		return $this;
	}

	/**
	 * Use the Statustype relation Statustype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    StatustypeQuery A secondary query class using the current class as primary query
	 */
	public function useStatustypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinStatustype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Statustype', 'StatustypeQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable|PropelCollection $trackable The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(TrackablestatusPeer::TRACKABLEID, $trackable->getTrackableid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TrackablestatusPeer::TRACKABLEID, $trackable->toKeyValue('PrimaryKey', 'Trackableid'), $comparison);
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Trackablestatus $trackablestatus Object to remove from the list of results
	 *
	 * @return    TrackablestatusQuery The current query, for fluid interface
	 */
	public function prune($trackablestatus = null)
	{
		if ($trackablestatus) {
			$this->addUsingAlias(TrackablestatusPeer::TRACKABLESTATUSID, $trackablestatus->getTrackablestatusid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseTrackablestatusQuery