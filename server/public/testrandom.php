<?php
function getApiKey() {
	
	// Create a random number
	$initializationVectorSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$initializationVector = mcrypt_create_iv($initializationVectorSize, MCRYPT_RAND);
	
	// Similar looking characters (I, 1, L, ...) have been removed
	// from the character set to make API keys more legible.
	$charset = '023456789ABCDEFGHJKLMQRSTUWXYZ';
	
	// Choose characters based on the generated number
	$length = 16;
	$apiKeyBasePart = '';
	for ($i = 0; $i < $length; $i++) {
		
		if ($i && $i % 4 == 0) {
			$apiKeyBasePart .= '-';
		}
		$ivIdx = substr($initializationVector, $i*2, 2);
		$char = $charset[round(hexdec($ivIdx)/256*strlen($charset)+0.5)-1];
		$apiKeyBasePart .= $char;
	}
	
	// Get a check digit
	$apiKeyCheckDigit = getApiKeyCheckDigit($apiKeyBasePart);
	
	return $apiKeyBasePart . $apiKeyCheckDigit;
}

function getApiKeyCheckDigit($basepart) {
	$checksumCRC = (string)crc32($basepart);
	echo $checksumCRC . "<br>";
	$checkCharacter = substr($checksumCRC,-1);
	return $checkCharacter;
}

echo getApiKey();