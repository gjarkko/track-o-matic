<?php



/**
 * This class defines the structure of the 'detailtype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class DetailtypeTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.DetailtypeTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('detailtype');
		$this->setPhpName('Detailtype');
		$this->setClassname('Detailtype');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('DETAILTYPEID', 'Detailtypeid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('DETAILTYPE', 'Detailtype', 'VARCHAR', true, 60, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('DETAILTYPE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'detail type must be at least 1 character');
		$this->addValidator('DETAILTYPE', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'detail type must be at most 60 characters');
		$this->addValidator('DETAILTYPE', 'required', 'propel.validator.RequiredValidator', '', 'detail type is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Detail', 'Detail', RelationMap::ONE_TO_MANY, array('detailtypeid' => 'detailtypeid', ), 'CASCADE', 'CASCADE', 'Details');
	} // buildRelations()

} // DetailtypeTableMap
