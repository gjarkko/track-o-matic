<?php



/**
 * This class defines the structure of the 'usertype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class UsertypeTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.UsertypeTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('usertype');
		$this->setPhpName('Usertype');
		$this->setClassname('Usertype');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('USERTYPEID', 'Usertypeid', 'INTEGER', true, 10, null);
		$this->addColumn('USERTYPE', 'Usertype', 'VARCHAR', true, 20, null);
		// validators
		$this->addValidator('USERTYPE', 'minLength', 'propel.validator.MinLengthValidator', '3', 'user type must be at least 3 characters');
		$this->addValidator('USERTYPE', 'maxLength', 'propel.validator.MaxLengthValidator', '20', 'user type must be at most 20 characters');
		$this->addValidator('USERTYPE', 'required', 'propel.validator.RequiredValidator', '', 'user type is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('User', 'User', RelationMap::ONE_TO_MANY, array('usertypeid' => 'usertypeid', ), 'CASCADE', 'CASCADE', 'Users');
	} // buildRelations()

} // UsertypeTableMap
