package org.abo.trackomatic.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class TrackomaticClientActivity extends Activity {
	private SharedPreferences sharedSettings;
	private String            apiKey;
	private boolean           dialogDismissed = false;
	private AlertDialog       dialog;

	private static final String TAG = "TrackomaticClientActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		// Restore the state of the activity
		if (savedInstanceState != null) {
			Log.d(TAG, "onCreate: Restoring state of activity");
			dialogDismissed = savedInstanceState.getBoolean("dialogDismissed", false);
		}

		// Get the settings
		sharedSettings = PreferenceManager.getDefaultSharedPreferences(this);

		// Unless the dialog has been dismissed create the dialog and show it
		if (!dialogDismissed) {
			// Check if the API key is set in an URI
			Uri data = getIntent().getData();
			if (data != null) {
				// Check if there is an API key set in the URI
				if (!data.getPathSegments().isEmpty()) {
					// Get old and new API key
					String oldApiKey = sharedSettings.getString("settingsApiKey", "<empty>");
					apiKey = data.getPathSegments().get(0);

					// Do not continue if the new and the old API key are the same
					if (apiKey.compareTo(oldApiKey) == 0) {
						Log.d(TAG, "onCreate: The new and old API key are the same");
						return;
					}

					// Show a dialog and ask if the user wants to replace the old
					// API key with the new one.  
					String message = "Replace <b>" + oldApiKey + "</b> with <b>" + apiKey + "</b>";
					AlertDialog.Builder builder;
					builder = new AlertDialog.Builder(this);
				
					builder.setTitle("Replace API key?");
					builder.setIcon(R.drawable.ic_dialog_info);
					builder.setMessage(Html.fromHtml(message));
					builder.setCancelable(false);
					builder.setPositiveButton("Yes", new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Set the API key
							SharedPreferences.Editor editor = sharedSettings.edit();
							editor.putString("settingsApiKey", apiKey.trim());
							// Make the changes permanent
							editor.commit();

							Log.d(TAG, "New API key: " + sharedSettings.getString("settingsApiKey", "<empty>"));
						}
					});
					builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Log.d("AlertDialogActivity", "onCreate: Not replacing API key");
							dialogDismissed = true;
						}
					});
					builder.create().show();
				}
			}
		}
		
		// Get the location manager
		LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		// Check that the GPS provider is enabled. If not enable show the user
		// a warning message.
		if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			
			builder.setTitle("No GPS provider");
			builder.setIcon(R.drawable.ic_dialog_alert);
			builder.setMessage("Please enable the GPS provider for the application to function properly.");
			builder.setCancelable(false);
			builder.setPositiveButton("Dismiss", new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			dialog = builder.create();
			dialog.show();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		// Read the API key from a file on the external storage if it exists
		loadApiKeyFromFile("apikey.txt");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		// Dismiss the dialog to avoid window leaking
		if (dialog != null) {
			dialog.dismiss();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "Storing the state of the activity");
		outState.putBoolean("dialogDismissed", dialogDismissed);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		// Add options to the menu
		MenuItem menuItem = menu.add("Settings");
		menuItem.setIcon(R.drawable.ic_menu_preferences);

		// Method must return true or else the menu will not show
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		if (item.getTitle() == "Settings") {
			// Call the settings activity
			Intent i = new Intent(this, TrackomaticSettingsActivity.class);
			startActivity(i);
		}
		// By returning true we do not allow further menu processing
		return true;
	}

	public void onClickClientHandler(View view) {
		Intent i;

		switch(view.getId()) {
		case R.id.startTrackingBtn:
			// Call the tracking activity
			i = new Intent(this, TrackomaticTrackingActivity.class);
			startActivity(i);
			break;
		}
	}

	private void loadApiKeyFromFile(String filename) {
		if (filename != null) {
			String state = Environment.getExternalStorageState();
			Log.d(TAG, "External storage state: " + state);

			// Check if the external storage is mounted
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				File file;

				// Open the file and check if the file exists
				if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.ECLAIR_MR1) {
					// API level 7 or lower
					file = new File(Environment.getExternalStorageDirectory(), filename);
				} else {
					file = new File(getExternalFilesDir(null), filename);
				}

				if (file.exists()) {
					BufferedReader apiKeyFile = null;
					try {
						apiKeyFile = new BufferedReader(new FileReader(file));
					} catch (FileNotFoundException e) {
						Log.e(TAG, "Could not open " + file);
					}

					// Read the API key from the file
					String apiKey = null;
					try {
						apiKey = apiKeyFile.readLine();
					} catch (IOException e) {
						Log.e(TAG, "Error reading " + file);
					}

					if (apiKey != null) {
						// Set the API key
						SharedPreferences.Editor editor = sharedSettings.edit();
						editor.putString("settingsApiKey", apiKey.trim());
						// Make the changes permanent
						editor.commit();

						Log.d(TAG, "API key: " + sharedSettings.getString("settingsApiKey", "<empty>"));
					} else {
						Log.e(TAG, "No API key found in the file");
					}

					// Delete the file as it is not needed anymore
					if (file.delete()) {
						Log.d(TAG, "API key file deleted");
					}
				}
			}
		}
	}
}
