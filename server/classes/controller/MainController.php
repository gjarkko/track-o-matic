<?

class MainController extends XmlController {

	
	public function loginAction() {
		
		if($this->requestHandler->getParam('email') && $this->requestHandler->getParam('password')) {
			if (Authentication::Authenticate($this->requestHandler->getParam('email'), $this->requestHandler->getParam('password'))) {
				$this->requestHandler->addAction(
					$this->requestHandler->getOriginalControllerName(),
					$this->requestHandler->getOriginalActionName()
				);
				return;
			}
		}
		
		$this->values['title'] = 'login';
		$this->view->setViewName('login');
		
		
	}
	
	public function logoutAction() {
		Session::getInstance()->destroy();
		$this->requestHandler->redirect(BASEURL);
	}
	
	public function indexAction() {
		
		if(Session::getInstance()->getUser()->getUsertypeid() == Controller::USERTYPE_USER) {
			$vehicles = array();
			$trackables = array();
			
			$this->view->setViewName('home');
			
			//Get the vehicles
			$vehicle = VehicleQuery::create()->findByCustomerid(Session::getInstance()->getUser()->getCustomerid());
			
			//Collect the references, trackables and 
			//latest coordinates of vechicles that 
			//contain trackables.
			foreach ($vehicle as $v) {
				
				$trackables = $v->getCurrentTrackables();
				
				if(count($trackables) > 0) {
					
					$status = $trackables[0]->getLastStatus();
					
					if($status != null) {
						 array_push($vehicles,
							array(
								'reference' => $v->getVehiclereference(),
								'trackables' => $trackables,
								'lng' => $status->getLng(),
								'lat' => $status->getLat()
							)
						);		
					}
				}
			}
			
			//Pass to the view for rendering
			$this->view->setValue('vehicles', $vehicles);
			
			//Pass the API key to the presentation layer. Needed for AJAX calls.
			$this->view->setValue('apikey', CryptographyProvider::decrypt(Session::getInstance()->getUser()->getApiKey()));
		}
		else {
			$this->requestHandler->addAction(
					'Browse',
					'customer'
				);
		}
	}
	
}
