<?php


/**
 * Base class that represents a query for the 'detail' table.
 *
 * 
 *
 * @method     DetailQuery orderByDetailid($order = Criteria::ASC) Order by the detailid column
 * @method     DetailQuery orderByDetailtypeid($order = Criteria::ASC) Order by the detailtypeid column
 * @method     DetailQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 * @method     DetailQuery orderByDetail($order = Criteria::ASC) Order by the detail column
 * @method     DetailQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     DetailQuery groupByDetailid() Group by the detailid column
 * @method     DetailQuery groupByDetailtypeid() Group by the detailtypeid column
 * @method     DetailQuery groupByTrackableid() Group by the trackableid column
 * @method     DetailQuery groupByDetail() Group by the detail column
 * @method     DetailQuery groupByValid() Group by the valid column
 *
 * @method     DetailQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     DetailQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     DetailQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     DetailQuery leftJoinDetailtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Detailtype relation
 * @method     DetailQuery rightJoinDetailtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Detailtype relation
 * @method     DetailQuery innerJoinDetailtype($relationAlias = null) Adds a INNER JOIN clause to the query using the Detailtype relation
 *
 * @method     DetailQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     DetailQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     DetailQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     Detail findOne(PropelPDO $con = null) Return the first Detail matching the query
 * @method     Detail findOneOrCreate(PropelPDO $con = null) Return the first Detail matching the query, or a new Detail object populated from the query conditions when no match is found
 *
 * @method     Detail findOneByDetailid(int $detailid) Return the first Detail filtered by the detailid column
 * @method     Detail findOneByDetailtypeid(int $detailtypeid) Return the first Detail filtered by the detailtypeid column
 * @method     Detail findOneByTrackableid(int $trackableid) Return the first Detail filtered by the trackableid column
 * @method     Detail findOneByDetail(string $detail) Return the first Detail filtered by the detail column
 * @method     Detail findOneByValid(boolean $valid) Return the first Detail filtered by the valid column
 *
 * @method     array findByDetailid(int $detailid) Return Detail objects filtered by the detailid column
 * @method     array findByDetailtypeid(int $detailtypeid) Return Detail objects filtered by the detailtypeid column
 * @method     array findByTrackableid(int $trackableid) Return Detail objects filtered by the trackableid column
 * @method     array findByDetail(string $detail) Return Detail objects filtered by the detail column
 * @method     array findByValid(boolean $valid) Return Detail objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseDetailQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseDetailQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Detail', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new DetailQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    DetailQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof DetailQuery) {
			return $criteria;
		}
		$query = new DetailQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Detail|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = DetailPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(DetailPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Detail A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `DETAILID`, `DETAILTYPEID`, `TRACKABLEID`, `DETAIL`, `VALID` FROM `detail` WHERE `DETAILID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Detail();
			$obj->hydrate($row);
			DetailPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Detail|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(DetailPeer::DETAILID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(DetailPeer::DETAILID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the detailid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByDetailid(1234); // WHERE detailid = 1234
	 * $query->filterByDetailid(array(12, 34)); // WHERE detailid IN (12, 34)
	 * $query->filterByDetailid(array('min' => 12)); // WHERE detailid > 12
	 * </code>
	 *
	 * @param     mixed $detailid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByDetailid($detailid = null, $comparison = null)
	{
		if (is_array($detailid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(DetailPeer::DETAILID, $detailid, $comparison);
	}

	/**
	 * Filter the query on the detailtypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByDetailtypeid(1234); // WHERE detailtypeid = 1234
	 * $query->filterByDetailtypeid(array(12, 34)); // WHERE detailtypeid IN (12, 34)
	 * $query->filterByDetailtypeid(array('min' => 12)); // WHERE detailtypeid > 12
	 * </code>
	 *
	 * @see       filterByDetailtype()
	 *
	 * @param     mixed $detailtypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByDetailtypeid($detailtypeid = null, $comparison = null)
	{
		if (is_array($detailtypeid)) {
			$useMinMax = false;
			if (isset($detailtypeid['min'])) {
				$this->addUsingAlias(DetailPeer::DETAILTYPEID, $detailtypeid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($detailtypeid['max'])) {
				$this->addUsingAlias(DetailPeer::DETAILTYPEID, $detailtypeid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(DetailPeer::DETAILTYPEID, $detailtypeid, $comparison);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @see       filterByTrackable()
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid)) {
			$useMinMax = false;
			if (isset($trackableid['min'])) {
				$this->addUsingAlias(DetailPeer::TRACKABLEID, $trackableid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($trackableid['max'])) {
				$this->addUsingAlias(DetailPeer::TRACKABLEID, $trackableid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(DetailPeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query on the detail column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByDetail('fooValue');   // WHERE detail = 'fooValue'
	 * $query->filterByDetail('%fooValue%'); // WHERE detail LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $detail The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByDetail($detail = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($detail)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $detail)) {
				$detail = str_replace('*', '%', $detail);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(DetailPeer::DETAIL, $detail, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(DetailPeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Detailtype object
	 *
	 * @param     Detailtype|PropelCollection $detailtype The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByDetailtype($detailtype, $comparison = null)
	{
		if ($detailtype instanceof Detailtype) {
			return $this
				->addUsingAlias(DetailPeer::DETAILTYPEID, $detailtype->getDetailtypeid(), $comparison);
		} elseif ($detailtype instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(DetailPeer::DETAILTYPEID, $detailtype->toKeyValue('PrimaryKey', 'Detailtypeid'), $comparison);
		} else {
			throw new PropelException('filterByDetailtype() only accepts arguments of type Detailtype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Detailtype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function joinDetailtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Detailtype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Detailtype');
		}

		return $this;
	}

	/**
	 * Use the Detailtype relation Detailtype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DetailtypeQuery A secondary query class using the current class as primary query
	 */
	public function useDetailtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDetailtype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Detailtype', 'DetailtypeQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable|PropelCollection $trackable The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(DetailPeer::TRACKABLEID, $trackable->getTrackableid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(DetailPeer::TRACKABLEID, $trackable->toKeyValue('PrimaryKey', 'Trackableid'), $comparison);
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Detail $detail Object to remove from the list of results
	 *
	 * @return    DetailQuery The current query, for fluid interface
	 */
	public function prune($detail = null)
	{
		if ($detail) {
			$this->addUsingAlias(DetailPeer::DETAILID, $detail->getDetailid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseDetailQuery