<?php


/**
 * Base class that represents a query for the 'localization' table.
 *
 * 
 *
 * @method     LocalizationQuery orderByLocalizationid($order = Criteria::ASC) Order by the localizationid column
 * @method     LocalizationQuery orderByLanguageid($order = Criteria::ASC) Order by the languageid column
 * @method     LocalizationQuery orderByLocalizationkey($order = Criteria::ASC) Order by the localizationkey column
 * @method     LocalizationQuery orderByLocalizationvalue($order = Criteria::ASC) Order by the localizationvalue column
 *
 * @method     LocalizationQuery groupByLocalizationid() Group by the localizationid column
 * @method     LocalizationQuery groupByLanguageid() Group by the languageid column
 * @method     LocalizationQuery groupByLocalizationkey() Group by the localizationkey column
 * @method     LocalizationQuery groupByLocalizationvalue() Group by the localizationvalue column
 *
 * @method     LocalizationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     LocalizationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     LocalizationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     LocalizationQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     LocalizationQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     LocalizationQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     Localization findOne(PropelPDO $con = null) Return the first Localization matching the query
 * @method     Localization findOneOrCreate(PropelPDO $con = null) Return the first Localization matching the query, or a new Localization object populated from the query conditions when no match is found
 *
 * @method     Localization findOneByLocalizationid(int $localizationid) Return the first Localization filtered by the localizationid column
 * @method     Localization findOneByLanguageid(int $languageid) Return the first Localization filtered by the languageid column
 * @method     Localization findOneByLocalizationkey(string $localizationkey) Return the first Localization filtered by the localizationkey column
 * @method     Localization findOneByLocalizationvalue(string $localizationvalue) Return the first Localization filtered by the localizationvalue column
 *
 * @method     array findByLocalizationid(int $localizationid) Return Localization objects filtered by the localizationid column
 * @method     array findByLanguageid(int $languageid) Return Localization objects filtered by the languageid column
 * @method     array findByLocalizationkey(string $localizationkey) Return Localization objects filtered by the localizationkey column
 * @method     array findByLocalizationvalue(string $localizationvalue) Return Localization objects filtered by the localizationvalue column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseLocalizationQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseLocalizationQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Localization', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new LocalizationQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    LocalizationQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof LocalizationQuery) {
			return $criteria;
		}
		$query = new LocalizationQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Localization|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = LocalizationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(LocalizationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Localization A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `LOCALIZATIONID`, `LANGUAGEID`, `LOCALIZATIONKEY`, `LOCALIZATIONVALUE` FROM `localization` WHERE `LOCALIZATIONID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Localization();
			$obj->hydrate($row);
			LocalizationPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Localization|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(LocalizationPeer::LOCALIZATIONID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(LocalizationPeer::LOCALIZATIONID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the localizationid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLocalizationid(1234); // WHERE localizationid = 1234
	 * $query->filterByLocalizationid(array(12, 34)); // WHERE localizationid IN (12, 34)
	 * $query->filterByLocalizationid(array('min' => 12)); // WHERE localizationid > 12
	 * </code>
	 *
	 * @param     mixed $localizationid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByLocalizationid($localizationid = null, $comparison = null)
	{
		if (is_array($localizationid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(LocalizationPeer::LOCALIZATIONID, $localizationid, $comparison);
	}

	/**
	 * Filter the query on the languageid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLanguageid(1234); // WHERE languageid = 1234
	 * $query->filterByLanguageid(array(12, 34)); // WHERE languageid IN (12, 34)
	 * $query->filterByLanguageid(array('min' => 12)); // WHERE languageid > 12
	 * </code>
	 *
	 * @see       filterByLanguage()
	 *
	 * @param     mixed $languageid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByLanguageid($languageid = null, $comparison = null)
	{
		if (is_array($languageid)) {
			$useMinMax = false;
			if (isset($languageid['min'])) {
				$this->addUsingAlias(LocalizationPeer::LANGUAGEID, $languageid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($languageid['max'])) {
				$this->addUsingAlias(LocalizationPeer::LANGUAGEID, $languageid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(LocalizationPeer::LANGUAGEID, $languageid, $comparison);
	}

	/**
	 * Filter the query on the localizationkey column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLocalizationkey('fooValue');   // WHERE localizationkey = 'fooValue'
	 * $query->filterByLocalizationkey('%fooValue%'); // WHERE localizationkey LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $localizationkey The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByLocalizationkey($localizationkey = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($localizationkey)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $localizationkey)) {
				$localizationkey = str_replace('*', '%', $localizationkey);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LocalizationPeer::LOCALIZATIONKEY, $localizationkey, $comparison);
	}

	/**
	 * Filter the query on the localizationvalue column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLocalizationvalue('fooValue');   // WHERE localizationvalue = 'fooValue'
	 * $query->filterByLocalizationvalue('%fooValue%'); // WHERE localizationvalue LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $localizationvalue The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByLocalizationvalue($localizationvalue = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($localizationvalue)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $localizationvalue)) {
				$localizationvalue = str_replace('*', '%', $localizationvalue);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(LocalizationPeer::LOCALIZATIONVALUE, $localizationvalue, $comparison);
	}

	/**
	 * Filter the query by a related Language object
	 *
	 * @param     Language|PropelCollection $language The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function filterByLanguage($language, $comparison = null)
	{
		if ($language instanceof Language) {
			return $this
				->addUsingAlias(LocalizationPeer::LANGUAGEID, $language->getLanguageid(), $comparison);
		} elseif ($language instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(LocalizationPeer::LANGUAGEID, $language->toKeyValue('PrimaryKey', 'Languageid'), $comparison);
		} else {
			throw new PropelException('filterByLanguage() only accepts arguments of type Language or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Language relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Language');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Language');
		}

		return $this;
	}

	/**
	 * Use the Language relation Language object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LanguageQuery A secondary query class using the current class as primary query
	 */
	public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinLanguage($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Language', 'LanguageQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Localization $localization Object to remove from the list of results
	 *
	 * @return    LocalizationQuery The current query, for fluid interface
	 */
	public function prune($localization = null)
	{
		if ($localization) {
			$this->addUsingAlias(LocalizationPeer::LOCALIZATIONID, $localization->getLocalizationid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseLocalizationQuery