<?

$baseDataPath = ROOT_PATH . 'database/basedata.sql';

// Fetch config
$conf = Propel::getConfiguration(PropelConfiguration::TYPE_ARRAY_FLAT);
$host = preg_replace('/(.*host\=)|(\;.*)/', '', $conf['datasources.server.connection.dsn']);
$dbname = preg_replace('/^(.*dbname\=)/', '', $conf['datasources.server.connection.dsn']);
$user = $conf['datasources.server.connection.user'];
$pass = $conf['datasources.server.connection.password'];

// Clear database
$con = Propel::getConnection();
$con->beginTransaction();
$stmt = $con->prepare('
	SELECT TABLE_NAME 
	FROM information_schema.TABLES 
	WHERE TABLE_SCHEMA = "' . $dbname . '"
');
$stmt->execute();
$tables = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

foreach($tables as $table) {
	if ($con->query('SELECT count(*) FROM ' . $table . ';')->fetchColumn(0)) {
		$con->query('DELETE FROM ' . $table . ';');
	}
}
$con->commit();


foreach($tables as $table) {
	$con->query('ALTER TABLE ' . $table . ' auto_increment=1;');
}
$con->commit();


// Re-import from base data
$command = trim(`which mysql`) . ' -u' . $user . ' -p' . $pass . ' -h ' . $host . ' ' . $dbname . ' < ' . $baseDataPath;
exec($command);
