<?php


/**
 * Base class that represents a query for the 'address' table.
 *
 * 
 *
 * @method     AddressQuery orderByAddressid($order = Criteria::ASC) Order by the addressid column
 * @method     AddressQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     AddressQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     AddressQuery orderByAddress1($order = Criteria::ASC) Order by the address1 column
 * @method     AddressQuery orderByAddress2($order = Criteria::ASC) Order by the address2 column
 * @method     AddressQuery orderByPostalcode($order = Criteria::ASC) Order by the postalcode column
 * @method     AddressQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     AddressQuery orderByCountryid($order = Criteria::ASC) Order by the countryid column
 * @method     AddressQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     AddressQuery orderByLng($order = Criteria::ASC) Order by the lng column
 *
 * @method     AddressQuery groupByAddressid() Group by the addressid column
 * @method     AddressQuery groupByCustomerid() Group by the customerid column
 * @method     AddressQuery groupByName() Group by the name column
 * @method     AddressQuery groupByAddress1() Group by the address1 column
 * @method     AddressQuery groupByAddress2() Group by the address2 column
 * @method     AddressQuery groupByPostalcode() Group by the postalcode column
 * @method     AddressQuery groupByCity() Group by the city column
 * @method     AddressQuery groupByCountryid() Group by the countryid column
 * @method     AddressQuery groupByLat() Group by the lat column
 * @method     AddressQuery groupByLng() Group by the lng column
 *
 * @method     AddressQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     AddressQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     AddressQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     AddressQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     AddressQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     AddressQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     AddressQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     AddressQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     AddressQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     AddressQuery leftJoinTrackableRelatedByStartAddressid($relationAlias = null) Adds a LEFT JOIN clause to the query using the TrackableRelatedByStartAddressid relation
 * @method     AddressQuery rightJoinTrackableRelatedByStartAddressid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TrackableRelatedByStartAddressid relation
 * @method     AddressQuery innerJoinTrackableRelatedByStartAddressid($relationAlias = null) Adds a INNER JOIN clause to the query using the TrackableRelatedByStartAddressid relation
 *
 * @method     AddressQuery leftJoinTrackableRelatedByEndAddressid($relationAlias = null) Adds a LEFT JOIN clause to the query using the TrackableRelatedByEndAddressid relation
 * @method     AddressQuery rightJoinTrackableRelatedByEndAddressid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TrackableRelatedByEndAddressid relation
 * @method     AddressQuery innerJoinTrackableRelatedByEndAddressid($relationAlias = null) Adds a INNER JOIN clause to the query using the TrackableRelatedByEndAddressid relation
 *
 * @method     Address findOne(PropelPDO $con = null) Return the first Address matching the query
 * @method     Address findOneOrCreate(PropelPDO $con = null) Return the first Address matching the query, or a new Address object populated from the query conditions when no match is found
 *
 * @method     Address findOneByAddressid(int $addressid) Return the first Address filtered by the addressid column
 * @method     Address findOneByCustomerid(int $customerid) Return the first Address filtered by the customerid column
 * @method     Address findOneByName(string $name) Return the first Address filtered by the name column
 * @method     Address findOneByAddress1(string $address1) Return the first Address filtered by the address1 column
 * @method     Address findOneByAddress2(string $address2) Return the first Address filtered by the address2 column
 * @method     Address findOneByPostalcode(string $postalcode) Return the first Address filtered by the postalcode column
 * @method     Address findOneByCity(string $city) Return the first Address filtered by the city column
 * @method     Address findOneByCountryid(int $countryid) Return the first Address filtered by the countryid column
 * @method     Address findOneByLat(string $lat) Return the first Address filtered by the lat column
 * @method     Address findOneByLng(string $lng) Return the first Address filtered by the lng column
 *
 * @method     array findByAddressid(int $addressid) Return Address objects filtered by the addressid column
 * @method     array findByCustomerid(int $customerid) Return Address objects filtered by the customerid column
 * @method     array findByName(string $name) Return Address objects filtered by the name column
 * @method     array findByAddress1(string $address1) Return Address objects filtered by the address1 column
 * @method     array findByAddress2(string $address2) Return Address objects filtered by the address2 column
 * @method     array findByPostalcode(string $postalcode) Return Address objects filtered by the postalcode column
 * @method     array findByCity(string $city) Return Address objects filtered by the city column
 * @method     array findByCountryid(int $countryid) Return Address objects filtered by the countryid column
 * @method     array findByLat(string $lat) Return Address objects filtered by the lat column
 * @method     array findByLng(string $lng) Return Address objects filtered by the lng column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseAddressQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseAddressQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Address', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new AddressQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    AddressQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof AddressQuery) {
			return $criteria;
		}
		$query = new AddressQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Address|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = AddressPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(AddressPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Address A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `ADDRESSID`, `CUSTOMERID`, `NAME`, `ADDRESS1`, `ADDRESS2`, `POSTALCODE`, `CITY`, `COUNTRYID`, `LAT`, `LNG` FROM `address` WHERE `ADDRESSID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Address();
			$obj->hydrate($row);
			AddressPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Address|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(AddressPeer::ADDRESSID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(AddressPeer::ADDRESSID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the addressid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByAddressid(1234); // WHERE addressid = 1234
	 * $query->filterByAddressid(array(12, 34)); // WHERE addressid IN (12, 34)
	 * $query->filterByAddressid(array('min' => 12)); // WHERE addressid > 12
	 * </code>
	 *
	 * @param     mixed $addressid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByAddressid($addressid = null, $comparison = null)
	{
		if (is_array($addressid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(AddressPeer::ADDRESSID, $addressid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(AddressPeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(AddressPeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AddressPeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the name column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
	 * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $name The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByName($name = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($name)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $name)) {
				$name = str_replace('*', '%', $name);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AddressPeer::NAME, $name, $comparison);
	}

	/**
	 * Filter the query on the address1 column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByAddress1('fooValue');   // WHERE address1 = 'fooValue'
	 * $query->filterByAddress1('%fooValue%'); // WHERE address1 LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $address1 The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByAddress1($address1 = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($address1)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $address1)) {
				$address1 = str_replace('*', '%', $address1);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AddressPeer::ADDRESS1, $address1, $comparison);
	}

	/**
	 * Filter the query on the address2 column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByAddress2('fooValue');   // WHERE address2 = 'fooValue'
	 * $query->filterByAddress2('%fooValue%'); // WHERE address2 LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $address2 The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByAddress2($address2 = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($address2)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $address2)) {
				$address2 = str_replace('*', '%', $address2);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AddressPeer::ADDRESS2, $address2, $comparison);
	}

	/**
	 * Filter the query on the postalcode column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByPostalcode('fooValue');   // WHERE postalcode = 'fooValue'
	 * $query->filterByPostalcode('%fooValue%'); // WHERE postalcode LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $postalcode The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByPostalcode($postalcode = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($postalcode)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $postalcode)) {
				$postalcode = str_replace('*', '%', $postalcode);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AddressPeer::POSTALCODE, $postalcode, $comparison);
	}

	/**
	 * Filter the query on the city column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
	 * $query->filterByCity('%fooValue%'); // WHERE city LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $city The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByCity($city = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($city)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $city)) {
				$city = str_replace('*', '%', $city);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AddressPeer::CITY, $city, $comparison);
	}

	/**
	 * Filter the query on the countryid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryid(1234); // WHERE countryid = 1234
	 * $query->filterByCountryid(array(12, 34)); // WHERE countryid IN (12, 34)
	 * $query->filterByCountryid(array('min' => 12)); // WHERE countryid > 12
	 * </code>
	 *
	 * @see       filterByCountry()
	 *
	 * @param     mixed $countryid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByCountryid($countryid = null, $comparison = null)
	{
		if (is_array($countryid)) {
			$useMinMax = false;
			if (isset($countryid['min'])) {
				$this->addUsingAlias(AddressPeer::COUNTRYID, $countryid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($countryid['max'])) {
				$this->addUsingAlias(AddressPeer::COUNTRYID, $countryid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AddressPeer::COUNTRYID, $countryid, $comparison);
	}

	/**
	 * Filter the query on the lat column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLat(1234); // WHERE lat = 1234
	 * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
	 * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
	 * </code>
	 *
	 * @param     mixed $lat The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByLat($lat = null, $comparison = null)
	{
		if (is_array($lat)) {
			$useMinMax = false;
			if (isset($lat['min'])) {
				$this->addUsingAlias(AddressPeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lat['max'])) {
				$this->addUsingAlias(AddressPeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AddressPeer::LAT, $lat, $comparison);
	}

	/**
	 * Filter the query on the lng column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLng(1234); // WHERE lng = 1234
	 * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
	 * $query->filterByLng(array('min' => 12)); // WHERE lng > 12
	 * </code>
	 *
	 * @param     mixed $lng The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByLng($lng = null, $comparison = null)
	{
		if (is_array($lng)) {
			$useMinMax = false;
			if (isset($lng['min'])) {
				$this->addUsingAlias(AddressPeer::LNG, $lng['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lng['max'])) {
				$this->addUsingAlias(AddressPeer::LNG, $lng['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AddressPeer::LNG, $lng, $comparison);
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(AddressPeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(AddressPeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Filter the query by a related Country object
	 *
	 * @param     Country|PropelCollection $country The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByCountry($country, $comparison = null)
	{
		if ($country instanceof Country) {
			return $this
				->addUsingAlias(AddressPeer::COUNTRYID, $country->getCountryid(), $comparison);
		} elseif ($country instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(AddressPeer::COUNTRYID, $country->toKeyValue('PrimaryKey', 'Countryid'), $comparison);
		} else {
			throw new PropelException('filterByCountry() only accepts arguments of type Country or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Country relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function joinCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Country');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Country');
		}

		return $this;
	}

	/**
	 * Use the Country relation Country object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountryQuery A secondary query class using the current class as primary query
	 */
	public function useCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCountry($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Country', 'CountryQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable $trackable  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByTrackableRelatedByStartAddressid($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(AddressPeer::ADDRESSID, $trackable->getStartAddressid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			return $this
				->useTrackableRelatedByStartAddressidQuery()
				->filterByPrimaryKeys($trackable->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTrackableRelatedByStartAddressid() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the TrackableRelatedByStartAddressid relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function joinTrackableRelatedByStartAddressid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('TrackableRelatedByStartAddressid');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'TrackableRelatedByStartAddressid');
		}

		return $this;
	}

	/**
	 * Use the TrackableRelatedByStartAddressid relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableRelatedByStartAddressidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackableRelatedByStartAddressid($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'TrackableRelatedByStartAddressid', 'TrackableQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable $trackable  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function filterByTrackableRelatedByEndAddressid($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(AddressPeer::ADDRESSID, $trackable->getEndAddressid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			return $this
				->useTrackableRelatedByEndAddressidQuery()
				->filterByPrimaryKeys($trackable->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTrackableRelatedByEndAddressid() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the TrackableRelatedByEndAddressid relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function joinTrackableRelatedByEndAddressid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('TrackableRelatedByEndAddressid');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'TrackableRelatedByEndAddressid');
		}

		return $this;
	}

	/**
	 * Use the TrackableRelatedByEndAddressid relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableRelatedByEndAddressidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackableRelatedByEndAddressid($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'TrackableRelatedByEndAddressid', 'TrackableQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Address $address Object to remove from the list of results
	 *
	 * @return    AddressQuery The current query, for fluid interface
	 */
	public function prune($address = null)
	{
		if ($address) {
			$this->addUsingAlias(AddressPeer::ADDRESSID, $address->getAddressid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseAddressQuery