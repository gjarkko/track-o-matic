<?php


/**
 * Base class that represents a query for the 'postalcode' table.
 *
 * 
 *
 * @method     PostalcodeQuery orderByPostalcodeid($order = Criteria::ASC) Order by the postalcodeid column
 * @method     PostalcodeQuery orderByCountryid($order = Criteria::ASC) Order by the countryid column
 * @method     PostalcodeQuery orderByPostalcode($order = Criteria::ASC) Order by the postalcode column
 * @method     PostalcodeQuery orderByPostalarea($order = Criteria::ASC) Order by the postalarea column
 *
 * @method     PostalcodeQuery groupByPostalcodeid() Group by the postalcodeid column
 * @method     PostalcodeQuery groupByCountryid() Group by the countryid column
 * @method     PostalcodeQuery groupByPostalcode() Group by the postalcode column
 * @method     PostalcodeQuery groupByPostalarea() Group by the postalarea column
 *
 * @method     PostalcodeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     PostalcodeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     PostalcodeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     PostalcodeQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     PostalcodeQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     PostalcodeQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     PostalcodeQuery leftJoinAddress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Address relation
 * @method     PostalcodeQuery rightJoinAddress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Address relation
 * @method     PostalcodeQuery innerJoinAddress($relationAlias = null) Adds a INNER JOIN clause to the query using the Address relation
 *
 * @method     Postalcode findOne(PropelPDO $con = null) Return the first Postalcode matching the query
 * @method     Postalcode findOneOrCreate(PropelPDO $con = null) Return the first Postalcode matching the query, or a new Postalcode object populated from the query conditions when no match is found
 *
 * @method     Postalcode findOneByPostalcodeid(int $postalcodeid) Return the first Postalcode filtered by the postalcodeid column
 * @method     Postalcode findOneByCountryid(int $countryid) Return the first Postalcode filtered by the countryid column
 * @method     Postalcode findOneByPostalcode(string $postalcode) Return the first Postalcode filtered by the postalcode column
 * @method     Postalcode findOneByPostalarea(string $postalarea) Return the first Postalcode filtered by the postalarea column
 *
 * @method     array findByPostalcodeid(int $postalcodeid) Return Postalcode objects filtered by the postalcodeid column
 * @method     array findByCountryid(int $countryid) Return Postalcode objects filtered by the countryid column
 * @method     array findByPostalcode(string $postalcode) Return Postalcode objects filtered by the postalcode column
 * @method     array findByPostalarea(string $postalarea) Return Postalcode objects filtered by the postalarea column
 *
 * @package    propel.generator.model.om
 */
abstract class BasePostalcodeQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BasePostalcodeQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Postalcode', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new PostalcodeQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    PostalcodeQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof PostalcodeQuery) {
			return $criteria;
		}
		$query = new PostalcodeQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Postalcode|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = PostalcodePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(PostalcodePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Postalcode A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `POSTALCODEID`, `COUNTRYID`, `POSTALCODE`, `POSTALAREA` FROM `postalcode` WHERE `POSTALCODEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Postalcode();
			$obj->hydrate($row);
			PostalcodePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Postalcode|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(PostalcodePeer::POSTALCODEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(PostalcodePeer::POSTALCODEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the postalcodeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByPostalcodeid(1234); // WHERE postalcodeid = 1234
	 * $query->filterByPostalcodeid(array(12, 34)); // WHERE postalcodeid IN (12, 34)
	 * $query->filterByPostalcodeid(array('min' => 12)); // WHERE postalcodeid > 12
	 * </code>
	 *
	 * @param     mixed $postalcodeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByPostalcodeid($postalcodeid = null, $comparison = null)
	{
		if (is_array($postalcodeid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(PostalcodePeer::POSTALCODEID, $postalcodeid, $comparison);
	}

	/**
	 * Filter the query on the countryid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryid(1234); // WHERE countryid = 1234
	 * $query->filterByCountryid(array(12, 34)); // WHERE countryid IN (12, 34)
	 * $query->filterByCountryid(array('min' => 12)); // WHERE countryid > 12
	 * </code>
	 *
	 * @see       filterByCountry()
	 *
	 * @param     mixed $countryid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByCountryid($countryid = null, $comparison = null)
	{
		if (is_array($countryid)) {
			$useMinMax = false;
			if (isset($countryid['min'])) {
				$this->addUsingAlias(PostalcodePeer::COUNTRYID, $countryid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($countryid['max'])) {
				$this->addUsingAlias(PostalcodePeer::COUNTRYID, $countryid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(PostalcodePeer::COUNTRYID, $countryid, $comparison);
	}

	/**
	 * Filter the query on the postalcode column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByPostalcode('fooValue');   // WHERE postalcode = 'fooValue'
	 * $query->filterByPostalcode('%fooValue%'); // WHERE postalcode LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $postalcode The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByPostalcode($postalcode = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($postalcode)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $postalcode)) {
				$postalcode = str_replace('*', '%', $postalcode);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PostalcodePeer::POSTALCODE, $postalcode, $comparison);
	}

	/**
	 * Filter the query on the postalarea column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByPostalarea('fooValue');   // WHERE postalarea = 'fooValue'
	 * $query->filterByPostalarea('%fooValue%'); // WHERE postalarea LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $postalarea The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByPostalarea($postalarea = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($postalarea)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $postalarea)) {
				$postalarea = str_replace('*', '%', $postalarea);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PostalcodePeer::POSTALAREA, $postalarea, $comparison);
	}

	/**
	 * Filter the query by a related Country object
	 *
	 * @param     Country|PropelCollection $country The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByCountry($country, $comparison = null)
	{
		if ($country instanceof Country) {
			return $this
				->addUsingAlias(PostalcodePeer::COUNTRYID, $country->getCountryid(), $comparison);
		} elseif ($country instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(PostalcodePeer::COUNTRYID, $country->toKeyValue('PrimaryKey', 'Countryid'), $comparison);
		} else {
			throw new PropelException('filterByCountry() only accepts arguments of type Country or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Country relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function joinCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Country');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Country');
		}

		return $this;
	}

	/**
	 * Use the Country relation Country object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountryQuery A secondary query class using the current class as primary query
	 */
	public function useCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCountry($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Country', 'CountryQuery');
	}

	/**
	 * Filter the query by a related Address object
	 *
	 * @param     Address $address  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function filterByAddress($address, $comparison = null)
	{
		if ($address instanceof Address) {
			return $this
				->addUsingAlias(PostalcodePeer::POSTALCODEID, $address->getPostalcodeid(), $comparison);
		} elseif ($address instanceof PropelCollection) {
			return $this
				->useAddressQuery()
				->filterByPrimaryKeys($address->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByAddress() only accepts arguments of type Address or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Address relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function joinAddress($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Address');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Address');
		}

		return $this;
	}

	/**
	 * Use the Address relation Address object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery A secondary query class using the current class as primary query
	 */
	public function useAddressQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAddress($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Address', 'AddressQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Postalcode $postalcode Object to remove from the list of results
	 *
	 * @return    PostalcodeQuery The current query, for fluid interface
	 */
	public function prune($postalcode = null)
	{
		if ($postalcode) {
			$this->addUsingAlias(PostalcodePeer::POSTALCODEID, $postalcode->getPostalcodeid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BasePostalcodeQuery