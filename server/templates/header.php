<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
<? foreach ($this->includes as $include): ?>
	<?=$include;?>
<? endforeach; ?>
	<title><?= Localizer::getText($this->getValue('title')); ?></title>
</head>
<body class="gradient">
	<div id="content">
	
		<div class="logo">
	    	<img src="<?=BASEURL;?>/image/trackomatic.png" alt="Trackomatic" title="Trackomatic" />
	    </div>
	    <div class="version">
	    	Prototype version 0.1
	    </div>
	    <div id="userdetails">
	    <?
		$user = Session::getInstance()->getUser();
	    if ($user): ?>
	    Hello <?=$user->getFullname();?>
	    <? else: ?>
	    &nbsp;
	    <? endif; ?>
		</div>
		<div id="topmenu">
			<ul>
<? foreach ($this->getMenuLinks() as $menuLink): ?>
	<? if ($menuLink->getActive()): ?>
				<li class="active"><a href="<?=$menuLink->getUrl();?>"><?=$menuLink->getText();?></a></li>
	<? else: ?>
				<li><a href="<?=$menuLink->getUrl();?>"><?=$menuLink->getText();?></a></li>	
	<? endif; ?>
<? endforeach; ?>

			</ul>
		</div>
<? if (count(Session::getInstance(true)->getFlashMessages())): ?>
			<div id="messages"> 
	<? foreach (Session::getInstance(true)->getFlashMessages() as $message): ?>
				<div class="<?=$message->getClassName();?>"><?=$message->getText();?></div>
	<? endforeach; ?>
			</div>
	<? Session::getInstance()->clearFlashMessages(); ?>
<? endif; ?>
		<div id="main">
