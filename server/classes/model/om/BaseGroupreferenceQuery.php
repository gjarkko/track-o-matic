<?php


/**
 * Base class that represents a query for the 'groupreference' table.
 *
 * 
 *
 * @method     GroupreferenceQuery orderByGroupreferenceid($order = Criteria::ASC) Order by the groupreferenceid column
 * @method     GroupreferenceQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 * @method     GroupreferenceQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     GroupreferenceQuery orderByGroupreference($order = Criteria::ASC) Order by the groupreference column
 * @method     GroupreferenceQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     GroupreferenceQuery groupByGroupreferenceid() Group by the groupreferenceid column
 * @method     GroupreferenceQuery groupByTrackableid() Group by the trackableid column
 * @method     GroupreferenceQuery groupByCustomerid() Group by the customerid column
 * @method     GroupreferenceQuery groupByGroupreference() Group by the groupreference column
 * @method     GroupreferenceQuery groupByValid() Group by the valid column
 *
 * @method     GroupreferenceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     GroupreferenceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     GroupreferenceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     GroupreferenceQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     GroupreferenceQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     GroupreferenceQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     GroupreferenceQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     GroupreferenceQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     GroupreferenceQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     Groupreference findOne(PropelPDO $con = null) Return the first Groupreference matching the query
 * @method     Groupreference findOneOrCreate(PropelPDO $con = null) Return the first Groupreference matching the query, or a new Groupreference object populated from the query conditions when no match is found
 *
 * @method     Groupreference findOneByGroupreferenceid(int $groupreferenceid) Return the first Groupreference filtered by the groupreferenceid column
 * @method     Groupreference findOneByTrackableid(int $trackableid) Return the first Groupreference filtered by the trackableid column
 * @method     Groupreference findOneByCustomerid(int $customerid) Return the first Groupreference filtered by the customerid column
 * @method     Groupreference findOneByGroupreference(string $groupreference) Return the first Groupreference filtered by the groupreference column
 * @method     Groupreference findOneByValid(boolean $valid) Return the first Groupreference filtered by the valid column
 *
 * @method     array findByGroupreferenceid(int $groupreferenceid) Return Groupreference objects filtered by the groupreferenceid column
 * @method     array findByTrackableid(int $trackableid) Return Groupreference objects filtered by the trackableid column
 * @method     array findByCustomerid(int $customerid) Return Groupreference objects filtered by the customerid column
 * @method     array findByGroupreference(string $groupreference) Return Groupreference objects filtered by the groupreference column
 * @method     array findByValid(boolean $valid) Return Groupreference objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseGroupreferenceQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseGroupreferenceQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Groupreference', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new GroupreferenceQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    GroupreferenceQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof GroupreferenceQuery) {
			return $criteria;
		}
		$query = new GroupreferenceQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Groupreference|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = GroupreferencePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(GroupreferencePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Groupreference A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `GROUPREFERENCEID`, `TRACKABLEID`, `CUSTOMERID`, `GROUPREFERENCE`, `VALID` FROM `groupreference` WHERE `GROUPREFERENCEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Groupreference();
			$obj->hydrate($row);
			GroupreferencePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Groupreference|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(GroupreferencePeer::GROUPREFERENCEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(GroupreferencePeer::GROUPREFERENCEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the groupreferenceid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByGroupreferenceid(1234); // WHERE groupreferenceid = 1234
	 * $query->filterByGroupreferenceid(array(12, 34)); // WHERE groupreferenceid IN (12, 34)
	 * $query->filterByGroupreferenceid(array('min' => 12)); // WHERE groupreferenceid > 12
	 * </code>
	 *
	 * @param     mixed $groupreferenceid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByGroupreferenceid($groupreferenceid = null, $comparison = null)
	{
		if (is_array($groupreferenceid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(GroupreferencePeer::GROUPREFERENCEID, $groupreferenceid, $comparison);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @see       filterByTrackable()
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid)) {
			$useMinMax = false;
			if (isset($trackableid['min'])) {
				$this->addUsingAlias(GroupreferencePeer::TRACKABLEID, $trackableid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($trackableid['max'])) {
				$this->addUsingAlias(GroupreferencePeer::TRACKABLEID, $trackableid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(GroupreferencePeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(GroupreferencePeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(GroupreferencePeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(GroupreferencePeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the groupreference column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByGroupreference('fooValue');   // WHERE groupreference = 'fooValue'
	 * $query->filterByGroupreference('%fooValue%'); // WHERE groupreference LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $groupreference The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByGroupreference($groupreference = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($groupreference)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $groupreference)) {
				$groupreference = str_replace('*', '%', $groupreference);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(GroupreferencePeer::GROUPREFERENCE, $groupreference, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(GroupreferencePeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable|PropelCollection $trackable The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(GroupreferencePeer::TRACKABLEID, $trackable->getTrackableid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(GroupreferencePeer::TRACKABLEID, $trackable->toKeyValue('PrimaryKey', 'Trackableid'), $comparison);
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(GroupreferencePeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(GroupreferencePeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Groupreference $groupreference Object to remove from the list of results
	 *
	 * @return    GroupreferenceQuery The current query, for fluid interface
	 */
	public function prune($groupreference = null)
	{
		if ($groupreference) {
			$this->addUsingAlias(GroupreferencePeer::GROUPREFERENCEID, $groupreference->getGroupreferenceid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseGroupreferenceQuery