<?
$r = RequestHandler::getInstance();
$object = $this->getValue('trackable');
$addresses = $this->getValue('addresses');
$browserConfig = array(
	'allowView' => false,
	'allowEdit' => false,
	'allowCreate' => false,
	'allowDelete' => false,
);

$browserConfig = array_merge($browserConfig, Trackable::getBrowserConfig());
?>
<div class="objectForm">
<form method="post" action="<?=$r->getCurrentActionUrl(true, array('form', 'submit'));?>"> 
	<div class='Field'>
			<label for="reference" class='title required'><?=ucfirst(Localizer::getText('reference'));?>:</label>
			<input type="text" id="reference" name="form[reference]" value="<?=$object->getTrackableReference();?>" />
	</div>
	<div class='Field'>
			<label for="fromaddress" class='title required'><?=ucfirst(Localizer::getText('From address'));?>:</label>
			<select id="fromaddress" name="form[fromaddress]">
			<? foreach ($addresses as $address) { ?>
				<option value="<?=$address->getAddressId();?>">
					<?=$address->getName() . ' ' . $address->getAddress1() . ' ' . $address->getAddress2() . ' ' . $address->getPostalCode() . ' ' . $address->getCity();?>
				</option>
			<?}?>
			</select>
	</div>
	<div class='Field'>
			<label for="toaddress" class='title required'><?=ucfirst(Localizer::getText('To address'));?>:</label>
			<select id="toaddress" name="form[toaddress]">
			<? foreach ($addresses as $address) { ?>
				<option value="<?=$address->getAddressId();?>">
					<?=$address->getName() . ' ' . $address->getAddress1() . ' ' . $address->getAddress2() . ' ' . $address->getPostalCode() . ' ' . $address->getCity();?>
				</option>
			<?}?>
			</select>	
	</div>
	<p>
		<input type="button" class="button" value="<?=ucfirst(Localizer::getText('back'));?>" title="<?=ucfirst(Localizer::getText('back to browser'));?>"
       	       onclick="location='<?=$r->getActionUrl('BrowseController', 'Trackable', false); ?>'" />	
		<input class="button" type="submit" name="submit" value="<?=ucfirst(Localizer::getText('save'));?>">
		<? if ($browserConfig['allowView'] && $object->getPrimaryKey()): ?>
			<input type="button" class="button" value="<?=ucfirst(Localizer::getText('view'));?>" title="<?=ucfirst(Localizer::getText('view'));?>"
       	       onclick="location='<?=$r->getActionUrl('ViewController', 'Trackable', false); ?>?id=<?=$object->getPrimaryKey();?>'" />
		<? endif; ?>
		<? if ($browserConfig['allowDelete'] && $object->getPrimaryKey()): ?>
			<input type="button" class="button" value="<?=ucfirst(Localizer::getText('delete'));?>" title="<?=ucfirst(Localizer::getText('delete'));?>"
       	       onclick="location='<?=$r->getActionUrl('EditController', 'Trackable', false); ?>?delete=<?=$object->getPrimaryKey();?>&id=<?=$object->getPrimaryKey();?>'" />
		<? endif; ?>
	</p>
</form>
</div>
