<?php


/**
 * Base class that represents a row from the 'countryname' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseCountryname extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'CountrynamePeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        CountrynamePeer
	 */
	protected static $peer;

	/**
	 * The value for the countrynameid field.
	 * @var        int
	 */
	protected $countrynameid;

	/**
	 * The value for the countryid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $countryid;

	/**
	 * The value for the languageid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $languageid;

	/**
	 * The value for the countryname field.
	 * @var        string
	 */
	protected $countryname;

	/**
	 * @var        Country
	 */
	protected $aCountry;

	/**
	 * @var        Language
	 */
	protected $aLanguage;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->countryid = 1;
		$this->languageid = 1;
	}

	/**
	 * Initializes internal state of BaseCountryname object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [countrynameid] column value.
	 * 
	 * @return     int
	 */
	public function getCountrynameid()
	{
		return $this->countrynameid;
	}

	/**
	 * Get the [countryid] column value.
	 * 
	 * @return     int
	 */
	public function getCountryid()
	{
		return $this->countryid;
	}

	/**
	 * Get the [languageid] column value.
	 * 
	 * @return     int
	 */
	public function getLanguageid()
	{
		return $this->languageid;
	}

	/**
	 * Get the [countryname] column value.
	 * 
	 * @return     string
	 */
	public function getCountryname()
	{
		return $this->countryname;
	}

	/**
	 * Set the value of [countrynameid] column.
	 * 
	 * @param      int $v new value
	 * @return     Countryname The current object (for fluent API support)
	 */
	public function setCountrynameid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->countrynameid !== $v) {
			$this->countrynameid = $v;
			$this->modifiedColumns[] = CountrynamePeer::COUNTRYNAMEID;
		}

		return $this;
	} // setCountrynameid()

	/**
	 * Set the value of [countryid] column.
	 * 
	 * @param      int $v new value
	 * @return     Countryname The current object (for fluent API support)
	 */
	public function setCountryid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->countryid !== $v) {
			$this->countryid = $v;
			$this->modifiedColumns[] = CountrynamePeer::COUNTRYID;
		}

		if ($this->aCountry !== null && $this->aCountry->getCountryid() !== $v) {
			$this->aCountry = null;
		}

		return $this;
	} // setCountryid()

	/**
	 * Set the value of [languageid] column.
	 * 
	 * @param      int $v new value
	 * @return     Countryname The current object (for fluent API support)
	 */
	public function setLanguageid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->languageid !== $v) {
			$this->languageid = $v;
			$this->modifiedColumns[] = CountrynamePeer::LANGUAGEID;
		}

		if ($this->aLanguage !== null && $this->aLanguage->getLanguageid() !== $v) {
			$this->aLanguage = null;
		}

		return $this;
	} // setLanguageid()

	/**
	 * Set the value of [countryname] column.
	 * 
	 * @param      string $v new value
	 * @return     Countryname The current object (for fluent API support)
	 */
	public function setCountryname($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->countryname !== $v) {
			$this->countryname = $v;
			$this->modifiedColumns[] = CountrynamePeer::COUNTRYNAME;
		}

		return $this;
	} // setCountryname()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->countryid !== 1) {
				return false;
			}

			if ($this->languageid !== 1) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->countrynameid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->countryid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->languageid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->countryname = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 4; // 4 = CountrynamePeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Countryname object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCountry !== null && $this->countryid !== $this->aCountry->getCountryid()) {
			$this->aCountry = null;
		}
		if ($this->aLanguage !== null && $this->languageid !== $this->aLanguage->getLanguageid()) {
			$this->aLanguage = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CountrynamePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = CountrynamePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aCountry = null;
			$this->aLanguage = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CountrynamePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = CountrynameQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CountrynamePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				CountrynamePeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCountry !== null) {
				if ($this->aCountry->isModified() || $this->aCountry->isNew()) {
					$affectedRows += $this->aCountry->save($con);
				}
				$this->setCountry($this->aCountry);
			}

			if ($this->aLanguage !== null) {
				if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
					$affectedRows += $this->aLanguage->save($con);
				}
				$this->setLanguage($this->aLanguage);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = CountrynamePeer::COUNTRYNAMEID;
		if (null !== $this->countrynameid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . CountrynamePeer::COUNTRYNAMEID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(CountrynamePeer::COUNTRYNAMEID)) {
			$modifiedColumns[':p' . $index++]  = '`COUNTRYNAMEID`';
		}
		if ($this->isColumnModified(CountrynamePeer::COUNTRYID)) {
			$modifiedColumns[':p' . $index++]  = '`COUNTRYID`';
		}
		if ($this->isColumnModified(CountrynamePeer::LANGUAGEID)) {
			$modifiedColumns[':p' . $index++]  = '`LANGUAGEID`';
		}
		if ($this->isColumnModified(CountrynamePeer::COUNTRYNAME)) {
			$modifiedColumns[':p' . $index++]  = '`COUNTRYNAME`';
		}

		$sql = sprintf(
			'INSERT INTO `countryname` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`COUNTRYNAMEID`':
						$stmt->bindValue($identifier, $this->countrynameid, PDO::PARAM_INT);
						break;
					case '`COUNTRYID`':
						$stmt->bindValue($identifier, $this->countryid, PDO::PARAM_INT);
						break;
					case '`LANGUAGEID`':
						$stmt->bindValue($identifier, $this->languageid, PDO::PARAM_INT);
						break;
					case '`COUNTRYNAME`':
						$stmt->bindValue($identifier, $this->countryname, PDO::PARAM_STR);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setCountrynameid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aCountry !== null) {
				if (!$this->aCountry->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCountry->getValidationFailures());
				}
			}

			if ($this->aLanguage !== null) {
				if (!$this->aLanguage->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aLanguage->getValidationFailures());
				}
			}


			if (($retval = CountrynamePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CountrynamePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCountrynameid();
				break;
			case 1:
				return $this->getCountryid();
				break;
			case 2:
				return $this->getLanguageid();
				break;
			case 3:
				return $this->getCountryname();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Countryname'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Countryname'][$this->getPrimaryKey()] = true;
		$keys = CountrynamePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCountrynameid(),
			$keys[1] => $this->getCountryid(),
			$keys[2] => $this->getLanguageid(),
			$keys[3] => $this->getCountryname(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aCountry) {
				$result['Country'] = $this->aCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aLanguage) {
				$result['Language'] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CountrynamePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCountrynameid($value);
				break;
			case 1:
				$this->setCountryid($value);
				break;
			case 2:
				$this->setLanguageid($value);
				break;
			case 3:
				$this->setCountryname($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CountrynamePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCountrynameid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCountryid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLanguageid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCountryname($arr[$keys[3]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(CountrynamePeer::DATABASE_NAME);

		if ($this->isColumnModified(CountrynamePeer::COUNTRYNAMEID)) $criteria->add(CountrynamePeer::COUNTRYNAMEID, $this->countrynameid);
		if ($this->isColumnModified(CountrynamePeer::COUNTRYID)) $criteria->add(CountrynamePeer::COUNTRYID, $this->countryid);
		if ($this->isColumnModified(CountrynamePeer::LANGUAGEID)) $criteria->add(CountrynamePeer::LANGUAGEID, $this->languageid);
		if ($this->isColumnModified(CountrynamePeer::COUNTRYNAME)) $criteria->add(CountrynamePeer::COUNTRYNAME, $this->countryname);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CountrynamePeer::DATABASE_NAME);
		$criteria->add(CountrynamePeer::COUNTRYNAMEID, $this->countrynameid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getCountrynameid();
	}

	/**
	 * Generic method to set the primary key (countrynameid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setCountrynameid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getCountrynameid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Countryname (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCountryid($this->getCountryid());
		$copyObj->setLanguageid($this->getLanguageid());
		$copyObj->setCountryname($this->getCountryname());
		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setCountrynameid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Countryname Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     CountrynamePeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CountrynamePeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Country object.
	 *
	 * @param      Country $v
	 * @return     Countryname The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCountry(Country $v = null)
	{
		if ($v === null) {
			$this->setCountryid(1);
		} else {
			$this->setCountryid($v->getCountryid());
		}

		$this->aCountry = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Country object, it will not be re-added.
		if ($v !== null) {
			$v->addCountryname($this);
		}

		return $this;
	}


	/**
	 * Get the associated Country object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Country The associated Country object.
	 * @throws     PropelException
	 */
	public function getCountry(PropelPDO $con = null)
	{
		if ($this->aCountry === null && ($this->countryid !== null)) {
			$this->aCountry = CountryQuery::create()->findPk($this->countryid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCountry->addCountrynames($this);
			 */
		}
		return $this->aCountry;
	}

	/**
	 * Declares an association between this object and a Language object.
	 *
	 * @param      Language $v
	 * @return     Countryname The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setLanguage(Language $v = null)
	{
		if ($v === null) {
			$this->setLanguageid(1);
		} else {
			$this->setLanguageid($v->getLanguageid());
		}

		$this->aLanguage = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Language object, it will not be re-added.
		if ($v !== null) {
			$v->addCountryname($this);
		}

		return $this;
	}


	/**
	 * Get the associated Language object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Language The associated Language object.
	 * @throws     PropelException
	 */
	public function getLanguage(PropelPDO $con = null)
	{
		if ($this->aLanguage === null && ($this->languageid !== null)) {
			$this->aLanguage = LanguageQuery::create()->findPk($this->languageid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aLanguage->addCountrynames($this);
			 */
		}
		return $this->aLanguage;
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->countrynameid = null;
		$this->countryid = null;
		$this->languageid = null;
		$this->countryname = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

		$this->aCountry = null;
		$this->aLanguage = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(CountrynamePeer::DEFAULT_STRING_FORMAT);
	}

} // BaseCountryname
