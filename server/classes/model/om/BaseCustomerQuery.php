<?php


/**
 * Base class that represents a query for the 'customer' table.
 *
 * 
 *
 * @method     CustomerQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     CustomerQuery orderByCustomer($order = Criteria::ASC) Order by the customer column
 * @method     CustomerQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     CustomerQuery groupByCustomerid() Group by the customerid column
 * @method     CustomerQuery groupByCustomer() Group by the customer column
 * @method     CustomerQuery groupByValid() Group by the valid column
 *
 * @method     CustomerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     CustomerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     CustomerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     CustomerQuery leftJoinAddress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Address relation
 * @method     CustomerQuery rightJoinAddress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Address relation
 * @method     CustomerQuery innerJoinAddress($relationAlias = null) Adds a INNER JOIN clause to the query using the Address relation
 *
 * @method     CustomerQuery leftJoinDetailtype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Detailtype relation
 * @method     CustomerQuery rightJoinDetailtype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Detailtype relation
 * @method     CustomerQuery innerJoinDetailtype($relationAlias = null) Adds a INNER JOIN clause to the query using the Detailtype relation
 *
 * @method     CustomerQuery leftJoinGroupreference($relationAlias = null) Adds a LEFT JOIN clause to the query using the Groupreference relation
 * @method     CustomerQuery rightJoinGroupreference($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Groupreference relation
 * @method     CustomerQuery innerJoinGroupreference($relationAlias = null) Adds a INNER JOIN clause to the query using the Groupreference relation
 *
 * @method     CustomerQuery leftJoinLogentry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Logentry relation
 * @method     CustomerQuery rightJoinLogentry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Logentry relation
 * @method     CustomerQuery innerJoinLogentry($relationAlias = null) Adds a INNER JOIN clause to the query using the Logentry relation
 *
 * @method     CustomerQuery leftJoinNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the Notification relation
 * @method     CustomerQuery rightJoinNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Notification relation
 * @method     CustomerQuery innerJoinNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the Notification relation
 *
 * @method     CustomerQuery leftJoinStatustype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Statustype relation
 * @method     CustomerQuery rightJoinStatustype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Statustype relation
 * @method     CustomerQuery innerJoinStatustype($relationAlias = null) Adds a INNER JOIN clause to the query using the Statustype relation
 *
 * @method     CustomerQuery leftJoinTrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackable relation
 * @method     CustomerQuery rightJoinTrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackable relation
 * @method     CustomerQuery innerJoinTrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackable relation
 *
 * @method     CustomerQuery leftJoinTrip($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trip relation
 * @method     CustomerQuery rightJoinTrip($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trip relation
 * @method     CustomerQuery innerJoinTrip($relationAlias = null) Adds a INNER JOIN clause to the query using the Trip relation
 *
 * @method     CustomerQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     CustomerQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     CustomerQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     CustomerQuery leftJoinVehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vehicle relation
 * @method     CustomerQuery rightJoinVehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vehicle relation
 * @method     CustomerQuery innerJoinVehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Vehicle relation
 *
 * @method     Customer findOne(PropelPDO $con = null) Return the first Customer matching the query
 * @method     Customer findOneOrCreate(PropelPDO $con = null) Return the first Customer matching the query, or a new Customer object populated from the query conditions when no match is found
 *
 * @method     Customer findOneByCustomerid(int $customerid) Return the first Customer filtered by the customerid column
 * @method     Customer findOneByCustomer(string $customer) Return the first Customer filtered by the customer column
 * @method     Customer findOneByValid(boolean $valid) Return the first Customer filtered by the valid column
 *
 * @method     array findByCustomerid(int $customerid) Return Customer objects filtered by the customerid column
 * @method     array findByCustomer(string $customer) Return Customer objects filtered by the customer column
 * @method     array findByValid(boolean $valid) Return Customer objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseCustomerQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseCustomerQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Customer', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new CustomerQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    CustomerQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof CustomerQuery) {
			return $criteria;
		}
		$query = new CustomerQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Customer|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = CustomerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(CustomerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Customer A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `CUSTOMERID`, `CUSTOMER`, `VALID` FROM `customer` WHERE `CUSTOMERID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Customer();
			$obj->hydrate($row);
			CustomerPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Customer|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(CustomerPeer::CUSTOMERID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(CustomerPeer::CUSTOMERID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(CustomerPeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the customer column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomer('fooValue');   // WHERE customer = 'fooValue'
	 * $query->filterByCustomer('%fooValue%'); // WHERE customer LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $customer The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($customer)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $customer)) {
				$customer = str_replace('*', '%', $customer);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CustomerPeer::CUSTOMER, $customer, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(CustomerPeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Address object
	 *
	 * @param     Address $address  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByAddress($address, $comparison = null)
	{
		if ($address instanceof Address) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $address->getCustomerid(), $comparison);
		} elseif ($address instanceof PropelCollection) {
			return $this
				->useAddressQuery()
				->filterByPrimaryKeys($address->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByAddress() only accepts arguments of type Address or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Address relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinAddress($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Address');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Address');
		}

		return $this;
	}

	/**
	 * Use the Address relation Address object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery A secondary query class using the current class as primary query
	 */
	public function useAddressQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAddress($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Address', 'AddressQuery');
	}

	/**
	 * Filter the query by a related Detailtype object
	 *
	 * @param     Detailtype $detailtype  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByDetailtype($detailtype, $comparison = null)
	{
		if ($detailtype instanceof Detailtype) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $detailtype->getCustomerid(), $comparison);
		} elseif ($detailtype instanceof PropelCollection) {
			return $this
				->useDetailtypeQuery()
				->filterByPrimaryKeys($detailtype->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByDetailtype() only accepts arguments of type Detailtype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Detailtype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinDetailtype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Detailtype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Detailtype');
		}

		return $this;
	}

	/**
	 * Use the Detailtype relation Detailtype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DetailtypeQuery A secondary query class using the current class as primary query
	 */
	public function useDetailtypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDetailtype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Detailtype', 'DetailtypeQuery');
	}

	/**
	 * Filter the query by a related Groupreference object
	 *
	 * @param     Groupreference $groupreference  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByGroupreference($groupreference, $comparison = null)
	{
		if ($groupreference instanceof Groupreference) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $groupreference->getCustomerid(), $comparison);
		} elseif ($groupreference instanceof PropelCollection) {
			return $this
				->useGroupreferenceQuery()
				->filterByPrimaryKeys($groupreference->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByGroupreference() only accepts arguments of type Groupreference or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Groupreference relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinGroupreference($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Groupreference');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Groupreference');
		}

		return $this;
	}

	/**
	 * Use the Groupreference relation Groupreference object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    GroupreferenceQuery A secondary query class using the current class as primary query
	 */
	public function useGroupreferenceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinGroupreference($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Groupreference', 'GroupreferenceQuery');
	}

	/**
	 * Filter the query by a related Logentry object
	 *
	 * @param     Logentry $logentry  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByLogentry($logentry, $comparison = null)
	{
		if ($logentry instanceof Logentry) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $logentry->getCustomerid(), $comparison);
		} elseif ($logentry instanceof PropelCollection) {
			return $this
				->useLogentryQuery()
				->filterByPrimaryKeys($logentry->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByLogentry() only accepts arguments of type Logentry or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Logentry relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinLogentry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Logentry');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Logentry');
		}

		return $this;
	}

	/**
	 * Use the Logentry relation Logentry object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    LogentryQuery A secondary query class using the current class as primary query
	 */
	public function useLogentryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinLogentry($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Logentry', 'LogentryQuery');
	}

	/**
	 * Filter the query by a related Notification object
	 *
	 * @param     Notification $notification  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByNotification($notification, $comparison = null)
	{
		if ($notification instanceof Notification) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $notification->getCustomerid(), $comparison);
		} elseif ($notification instanceof PropelCollection) {
			return $this
				->useNotificationQuery()
				->filterByPrimaryKeys($notification->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByNotification() only accepts arguments of type Notification or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Notification relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinNotification($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Notification');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Notification');
		}

		return $this;
	}

	/**
	 * Use the Notification relation Notification object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery A secondary query class using the current class as primary query
	 */
	public function useNotificationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinNotification($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Notification', 'NotificationQuery');
	}

	/**
	 * Filter the query by a related Statustype object
	 *
	 * @param     Statustype $statustype  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByStatustype($statustype, $comparison = null)
	{
		if ($statustype instanceof Statustype) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $statustype->getCustomerid(), $comparison);
		} elseif ($statustype instanceof PropelCollection) {
			return $this
				->useStatustypeQuery()
				->filterByPrimaryKeys($statustype->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByStatustype() only accepts arguments of type Statustype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Statustype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinStatustype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Statustype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Statustype');
		}

		return $this;
	}

	/**
	 * Use the Statustype relation Statustype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    StatustypeQuery A secondary query class using the current class as primary query
	 */
	public function useStatustypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinStatustype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Statustype', 'StatustypeQuery');
	}

	/**
	 * Filter the query by a related Trackable object
	 *
	 * @param     Trackable $trackable  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByTrackable($trackable, $comparison = null)
	{
		if ($trackable instanceof Trackable) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $trackable->getCustomerid(), $comparison);
		} elseif ($trackable instanceof PropelCollection) {
			return $this
				->useTrackableQuery()
				->filterByPrimaryKeys($trackable->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTrackable() only accepts arguments of type Trackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinTrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackable');
		}

		return $this;
	}

	/**
	 * Use the Trackable relation Trackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackable', 'TrackableQuery');
	}

	/**
	 * Filter the query by a related Trip object
	 *
	 * @param     Trip $trip  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByTrip($trip, $comparison = null)
	{
		if ($trip instanceof Trip) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $trip->getCustomerid(), $comparison);
		} elseif ($trip instanceof PropelCollection) {
			return $this
				->useTripQuery()
				->filterByPrimaryKeys($trip->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTrip() only accepts arguments of type Trip or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trip relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinTrip($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trip');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trip');
		}

		return $this;
	}

	/**
	 * Use the Trip relation Trip object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery A secondary query class using the current class as primary query
	 */
	public function useTripQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrip($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trip', 'TripQuery');
	}

	/**
	 * Filter the query by a related User object
	 *
	 * @param     User $user  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByUser($user, $comparison = null)
	{
		if ($user instanceof User) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $user->getCustomerid(), $comparison);
		} elseif ($user instanceof PropelCollection) {
			return $this
				->useUserQuery()
				->filterByPrimaryKeys($user->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the User relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('User');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'User');
		}

		return $this;
	}

	/**
	 * Use the User relation User object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    UserQuery A secondary query class using the current class as primary query
	 */
	public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinUser($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'User', 'UserQuery');
	}

	/**
	 * Filter the query by a related Vehicle object
	 *
	 * @param     Vehicle $vehicle  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function filterByVehicle($vehicle, $comparison = null)
	{
		if ($vehicle instanceof Vehicle) {
			return $this
				->addUsingAlias(CustomerPeer::CUSTOMERID, $vehicle->getCustomerid(), $comparison);
		} elseif ($vehicle instanceof PropelCollection) {
			return $this
				->useVehicleQuery()
				->filterByPrimaryKeys($vehicle->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByVehicle() only accepts arguments of type Vehicle or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Vehicle relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function joinVehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Vehicle');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Vehicle');
		}

		return $this;
	}

	/**
	 * Use the Vehicle relation Vehicle object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery A secondary query class using the current class as primary query
	 */
	public function useVehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinVehicle($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Vehicle', 'VehicleQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Customer $customer Object to remove from the list of results
	 *
	 * @return    CustomerQuery The current query, for fluid interface
	 */
	public function prune($customer = null)
	{
		if ($customer) {
			$this->addUsingAlias(CustomerPeer::CUSTOMERID, $customer->getCustomerid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseCustomerQuery