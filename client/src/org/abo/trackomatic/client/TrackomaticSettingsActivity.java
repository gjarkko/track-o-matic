package org.abo.trackomatic.client;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

public class TrackomaticSettingsActivity extends PreferenceActivity implements OnPreferenceChangeListener {
	private static final String TAG = "TrackomaticSettingsActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		
		// Register handlers for validating the input value of user settings
		getPreferenceScreen().findPreference("settingsApiKey").setOnPreferenceChangeListener(this);
		getPreferenceScreen().findPreference("settingsServer").setOnPreferenceChangeListener(this);
	}
	
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String key = preference.getKey();
		
		if (key.compareTo("settingsApiKey") == 0) {
			String apiKey = (String) newValue;
			
			// The API key should not contain any whitespace, tabs, newlines, etc
			if (apiKey.matches("[^\\s]*")) {
				return true;
			} else {
				Log.d(TAG, "The API key should not contain any illegal characters");
				showMessageInUI("The API key should not contain any illegal characters");
			}
		} else if (key.compareTo("settingsServer") == 0) {
			String serverAddress = (String) newValue;
			
			// Check if the new server address is a valid URL and is terminated
			// with a slash
			if (URLUtil.isHttpUrl(serverAddress)) {
				return true;
			} else {
				Log.d(TAG, "The server address is invalid");
				showMessageInUI("The server address is invalid");
			}
		}
		// Do not update
		return false;
	}
	
	private void showMessageInUI(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
}
