<?

class Localizer {
		
	private function __construct() {}
	
	public static function getText($localizationkey, $languageid = false) {
		
		$originalKey = $localizationkey;
		
		$localizationkey = lcfirst($localizationkey);
		
		if(!$languageid) {
			if(Session::getInstance()->getUser()) {
				$languageid = Session::getInstance()->getUser()->getLanguageid();
			}
			else {
				$languageid = LanguageQuery::create()->findOneByLanguage(Registry::get('default_language'))->getLanguageid();
			}
		}
		$localization = LocalizationQuery::create()
			->findByArray(array(
				'localizationkey' => $localizationkey,
				'languageid' => $languageid,
			));
		if(count($localization)) {
			return $localization[0]->getLocalizationvalue();
		}
		else {
			return $originalKey;
		}
		
		
	}
	
}