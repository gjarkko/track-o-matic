#!/usr/bin/php -q
<?php

$speed = 500; // Speed in per cent (default 200%). 
$sendInterval = 0.5; // Default is 0.5, two requests a second.
$dataPath = __DIR__ . '/feedData';
$baseUrl = 'http://absx.dyndns.org/tom/api/locationUpdate?apikey=%KEY%&lat=%LAT%&lng=%LNG%';

$counters = array(
	'trip1' => array(
		'vehicle' => 'FWD8-M39M-BKZZ-AQU28',
		'pointsFile' => $dataPath . '/trip1.csv',
		'counter' => 0,
	),
	'trip2' => array(
		'vehicle' => 'Y8WS-SRSM-9EA6-KQ0K8',
		'pointsFile' => $dataPath . '/trip2.csv',
		'counter' => 0,
	),
	'trip3' => array(
		'vehicle' => '3GW5-7G8E-C7HZ-YB9B9',
		'pointsFile' => $dataPath . '/trip3.csv',
		'counter' => 0,
	),
);
$data = array();
foreach($counters as $tripName => $tripConfig) {
	$fp = fopen($tripConfig['pointsFile'], 'r');
	while (false !== ($line = fgetcsv($fp, 1024))) {
		$data[$tripName][$line[0]] = array('lat' => $line[1], 'lng' => $line[2]);
		$max[$tripName] = $line[0];
	}
	fclose($fp);
}

$startTime = microtime(true);
while (1) {
	foreach ($counters as $tripName => $tripConfig) {
		if (isset($data[$tripName][$tripConfig['counter']])) {
			$point = $data[$tripName][$tripConfig['counter']];
			$url = str_replace(array('%KEY%', '%LAT%', '%LNG%'), array($tripConfig['vehicle'], $point["lat"], $point["lng"]), $baseUrl);
			
			$result = '';
			$code = http_response($url, $result);
			
			if ($code != 201) {
				echo "\nERROR with url $url: got $code\nRESULT:\n$result\n\n";
			}
			else {
				echo ".";
			}
			
		}
		if ($tripConfig['counter'] < $max[$tripName]) {
			$counters[$tripName]['counter'] += floor(($speed/100) * 1);
		} 
		else {
			$counters[$tripName]['counter'] = 0;
		}
	}
	
	usleep($sendInterval * 1000000);
}


function http_response($url, &$result) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, FALSE); // remove header
	curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$result = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	return $httpCode;
}
