<?

class View {
	
	const DEFAULT_VIEWNAME = 'default';
	const VIEW_FILE_EXTENSION = '.php';
	
	const VIEW_TYPE_XML = 1;
	const VIEW_TYPE_JSON = 2;
	
	const DEFAULT_HEADER_JSON = '200';
	const DEFAULT_FOOTER_JSON = 'empty';
		
	const DEFAULT_HEADER_XML = 'header';
	const DEFAULT_FOOTER_XML = 'footer';
		
	const VIEW_DIRECTORY = 'view';
	const TEMPLATE_DIRECTORY = 'templates';
	
	const JS_DIRECTORY = 'js';
	const CSS_DIRECTORY = 'css';
		
	protected $header;
	protected $footer;
	protected $viewName;
	
	protected $includes;
	protected $menuLinks;
	
	protected $values;
	protected $type;
	
	protected $mimeType = 'text/html';
	protected $headersSent = false;

	
	public function __construct($type = NULL, $viewName = NULL, $header = NULL, $footer = NULL) {
		$this->values = array();
		$this->includes = array();
		$this->init($type, $viewName, $header, $footer);
	}
	
	public static function create($type = NULL, $viewName = NULL, $header = NULL, $footer = NULL) {
		return new self($type, $viewName, $header, $footer);
	}
	
	protected function init($type = NULL, $viewName = NULL, $header = NULL, $footer = NULL) {
		$this->type = $type;
		if (empty($this->type) || !in_array($this->type, array(self::VIEW_TYPE_XML, self::VIEW_TYPE_JSON))) {
			$this->type = self::VIEW_TYPE_XML;
		}
		$this->viewName = $viewName;
		if (empty($this->viewName)) {
			$this->viewName = self::DEFAULT_VIEWNAME;
		}
		$this->header = $header;
		$this->footer = $header;
		if (empty($this->header)) {
			switch ($this->type) {
				case self::VIEW_TYPE_JSON:
					$this->header = self::DEFAULT_HEADER_JSON;
					break;
				default:
				case self::VIEW_TYPE_XML:
					$this->header = self::DEFAULT_HEADER_XML;
					break;
			}
		}
		if (empty($this->footer)) {
			switch ($this->type) {
				case self::VIEW_TYPE_JSON:
					$this->footer = self::DEFAULT_FOOTER_JSON;
					break;
				default:
				case self::VIEW_TYPE_XML:
					$this->footer = self::DEFAULT_FOOTER_XML;
				break;
			}
		}
		return $this;
	}
	
	public function renderHeader() {
		$this->sendHeaders();			
		if(!$this->header || !file_exists(self::TEMPLATE_DIRECTORY . $this->header . self::VIEW_FILE_EXTENSION)) {
				
			if($this->type == self::VIEW_TYPE_JSON) {
				$this->header = self::DEFAULT_HEADER_JSON;
			} else {
				$this->header = self::DEFAULT_HEADER_XML;
			}
		}
	
		include ROOT_PATH . DIRECTORY_SEPARATOR . self::TEMPLATE_DIRECTORY . DIRECTORY_SEPARATOR . $this->header . self::VIEW_FILE_EXTENSION;
		return $this;
	}
	
	public function renderFooter() {
		$this->sendHeaders();
	
		if(!$this->footer || !file_exists(self::TEMPLATE_DIRECTORY . $this->footer . self::VIEW_FILE_EXTENSION)) {
			if($this->type == self::VIEW_TYPE_JSON) {
				$this->footer = self::DEFAULT_FOOTER_JSON;
			} else {
				$this->footer = self::DEFAULT_FOOTER_XML;
			}
		}
		
		include ROOT_PATH . DIRECTORY_SEPARATOR . self::TEMPLATE_DIRECTORY . DIRECTORY_SEPARATOR . $this->footer . self::VIEW_FILE_EXTENSION;
		return $this;	
	}
	
	public function renderView() {
		$this->sendHeaders();
		include CLASS_PATH . self::VIEW_DIRECTORY . DIRECTORY_SEPARATOR . $this->viewName . self::VIEW_FILE_EXTENSION;
		return $this;
	}
	
	public function render() {
		$this->sendHeaders();
		
		if(!$this->viewName) {
			$this->viewName = self::DEFAULT_VIEWNAME;
		}
		$this->renderHeader()->renderView()->renderFooter();
		return $this;
	}
	
	public function setType($type) {
		$this->type = $type;
		$this->init($this->type, $this->viewName, $this->header, $this->footer);
		return $this;
	}
	
	public function getType() {
		return $this->type;
	}
	
	protected function sendHeaders() {
		if ($this->headersSent) {
			return;
		}
		header('Content-type: ' . $this->mimeType);
		$this->headersSent = true;
	}
	
	public function setMimeType($mimeType) {
		$this->mimeType = $mimeType;
		return $this;
	}
	
	public function getMimeType() {
		return $this->mimeType;
	}
	
	public function setViewName($viewName) {
		$this->viewName = $viewName;
		$this->init($this->type, $this->viewName, $this->header, $this->footer);
		return $this;
	}
	
	public function getViewName() {
		return $this->viewName;
	}
	
	public function setHeader($header) {
		$this->header = $header;
		$this->init($this->type, $this->viewName, $this->header, $this->footer);
		return $this;
	}
	
	public function getHeader() {
		return $this->header;
	}
	
	public function setFooter($footer) {
		$this->footer = $footer;
		$this->init($this->type, $this->viewName, $this->header, $this->footer);
		return $this;
	}
	
	public function getFooter() {
		return $this->footer;
	}

	public function setValue($key, $value) {
		$this->values[$key] = $value;
		return $this;
	}
	
	public function getValue($key) {
		if (isset($this->values[$key])) {
			return $this->values[$key];			
		}
	}
	
	public function setValues($values) {
		$this->values = $values;
	}
	
	public function getValues() {
		return $this->values;
	}
	
	public function addInclude($file) {
		if(!isset($this->includes[$file])) {
			$extension = substr($file, strrpos($file, '.') + 1);
			if ($extension == 'js') {
				$path = PUBLIC_PATH . DIRECTORY_SEPARATOR . self::JS_DIRECTORY . DIRECTORY_SEPARATOR . $file;
			}
			elseif ($extension == 'css') {
				$path = PUBLIC_PATH . DIRECTORY_SEPARATOR . self::CSS_DIRECTORY . DIRECTORY_SEPARATOR . $file;
			}
			$path = Tools::sanitizePath($path);
				
			if (file_exists($path)) {
				if ($extension == 'js') {
					$this->includes[$file] = '<script type="text/javascript" src="' . BASEURL . '/' . self::JS_DIRECTORY . '/' . $file . '"></script>';
				}
				elseif ($extension == 'css') {
					$this->includes[$file] = '<link rel="stylesheet" type="text/css" href="' . BASEURL . '/' . self::CSS_DIRECTORY . '/' . $file . '" />';
				}
			}
		}
		return $this;
	}
	
	public function addMenuLink() {
		$args = func_get_args(); 
	
		if (is_object($args[0]) && get_class($args[0]) == 'MenuLink') {
			$this->menuLinks[] = $args[0];
		} 
		elseif (count($args) == 3) {
			$this->menuLinks[] = new MenuLink($args[0], $args[1], $args[2]);
		}
		elseif (count($args) == 2) {
			$this->menuLinks[] = new MenuLink($args[0], $args[1]);
		}
		else {
			throw new Exception('Incorrect amount of arguments or argument of wrong type.');
		}
		return $this;
	}
	
	public function appendMenuLink() {
		$args = func_get_args();
		if (!isset($this->menuLinks)) {
			$this->menuLinks = array();
		}
	
		if (is_object($args[0]) && get_class($args[0]) == 'MenuLink') {
			$this->menuLinks = array_merge(array($args[0]), $this->menuLinks);
		}
		elseif (count($args) == 3) {
			$this->menuLinks = array_merge(array(new MenuLink($args[0], $args[1], $args[2])), $this->menuLinks);
		}
		elseif (count($args) == 2) {
			$this->menuLinks = array_merge(array(new MenuLink($args[0], $args[1])), $this->menuLinks);
		}
		else {
			throw new Exception('Incorrect amount of arguments or argument of wrong type.');
		}
		return $this;
	}
	
	public function getMenuLinks() {
		if (!isset($this->menuLinks)) {
			$this->menuLinks = array();
		}
		return $this->menuLinks;
	}
}