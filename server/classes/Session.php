<?

class Session {
	
	public static $instance;
	private $sessionId;
	private $user;
	private $loginTime;
	private $lastRequestTime;
	private $ip;
	private $usePhpSessions;
	private $messages;
	private $values;
	
	private function __construct($usePhpSessions) {
		$this->usePhpSessions = (bool)$usePhpSessions;
		if ($this->usePhpSessions) {
			session_name(Registry::get('system'));
			session_start();
			$this->sessionid = session_id();
		}
		else {
			$this->values = array();
		}
		$this->ip = Tools::get_client_ip();
		
		if (
			$this->usePhpSessions 
			&& isset($_SESSION['user'], $_SESSION['lastRequestTime']) 
			&& time() - $_SESSION['lastRequestTime'] <= Registry::get('session_time_limit')*60
		) {
			$this->user = $_SESSION['user'];
			$_SESSION['lastRequestTime'] = time();	
		}
		elseif (
			$this->usePhpSessions 
			&& isset($_SESSION['user'], $_SESSION['lastRequestTime']) 
			&& time() - $_SESSION['lastRequestTime'] > Registry::get('session_time_limit')*60
		) {
			$this->messages[] = new FlashMessage(
				ucfirst(Localizer::getText('your session has timed out', 1) . '. ') 
				. ucfirst(Localizer::getText('please re-login', 1) . '.')
			);
			session_destroy();
		}
	}
	
	public static function getInstance($usePhpSessions = true) {
		if (!isset(self::$instance)) {
			self::$instance = new self($usePhpSessions);
		}
		return self::$instance;
	}
	
	
	public function setUser($user) {
		if (get_class($user) == 'User') {
			$this->user = $user;
			$this->loginTime = time();
			$this->lastRequestTime = $this->loginTime;
			if ($this->usePhpSessions) {
				$_SESSION['user'] = $this->user;
				$_SESSION['loginTime'] =$this->loginTime;
				$_SESSION['lastRequestTime'] =$this->loginTime;
			}
		}
	}
	
	public function getSessionId() {
		return $this->sessionId;
	}
	
	public function getUser() {
		if (isset($this->user)) {
			return $this->user;
		}
	}
	
	public function getLoginTime() {
		if (isset($this->loginTime)) {
			return $this->loginTime;
		}
	}
	
	public function getIp() {
		if (isset($this->ip)) {
			return $this->ip;
		}
	}
	
	public function addFlashMessage($message, $type = NULL) {
		if (!is_object($message)) {
			$message = new FlashMessage($message, $type);
		}
		if ($this->usePhpSessions) {
			$_SESSION['messages'][] = $message;
		} else {
			$this->messages[] = $message;
		}
	}
	
	public function getFlashMessages() {
		$messages = array();
		if ($this->usePhpSessions && isset($_SESSION['messages'])) {
			$messages = array_merge($messages, $_SESSION['messages']);
		}
		if (isset($this->messages)) {
			$messages = array_merge($messages, $this->messages);
		}
		return $messages;
	}
	
	public function clearFlashMessages() {
		if ($this->usePhpSessions) {
			unset($_SESSION['messages']);
		} 
		$this->messages = array();
	}
	
	public function destroy() {
		$usePhpSessions = $this->usePhpSessions;
		session_destroy();
		//self::$instance = new self($usePhpSessions);
	}
	
	public function usingPhpSessions() {
		return $this->usePhpSessions;
	}
	
	public function setValue($key, $value) {
		if ($this->usePhpSessions) {
			$_SESSION['values'][$key] = $value;
		}
		else {
			$this->values[$key] = $value;
		}
	}
	
	public function hasValue($key) {
		if ($this->usePhpSessions) {
			return isset($_SESSION['values'][$key]);
		}
		else {
			return isset($this->values[$key]);
		}
	}
	
	public function getValue($key) {
		if (!$this->hasValue($key)) {
			throw new Exception('Unknown value ' . $key . ' requested.');
		}
		
		if ($this->usePhpSessions) {
			return $_SESSION['values'][$key];
		}
		else {
			return $this->values[$key];
		}
	}
	
	public function clearValue($key) {
		if (!$this->hasValue($key)) {
			throw new Exception('Unknown value ' . $key . ' requested.');
		}
		if ($this->usePhpSessions) {
			unset($_SESSION['values'][$key]);
		}
		else {
			unset($this->values[$key]);
		}
	}
}
