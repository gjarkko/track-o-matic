<?
$r = RequestHandler::getInstance();
$class = $this->values['className'];
$object = $this->values['object'];
$browserConfig = array(
	'allowView' => false,
	'allowEdit' => false,
	'allowCreate' => false,
	'allowDelete' => false,
);
if (is_callable($class . '::getBrowserConfig')) {
	$browserConfig = array_merge($browserConfig, $class::getBrowserConfig());
}
$crudMap = $this->getValue('objectCrudMap');

?>

<div class="object">
	<table class="viewObject">
	<? foreach ($this->values['viewValues'] as $title => $value): ?>
	 	<? $inputtype = 'text'; 
	 	foreach($crudMap as $entry) {
			if(strtolower($entry['label']) == strtolower($title)) {
				$inputtype = $entry['inputType'];
			}
		}
		?>
		<tr>
			<td class="title"><?=$title;?></td>
			<td class="value"><?=($inputtype == 'checkbox') ? ($value == 1) ? '<img src="/tom/image/check_small.png" />' : '' : $value;?></td>
		</tr>
	<? endforeach; ?>
	</table>
</div>
<p>
<input type="button" value="<?=ucfirst(Localizer::getText('back'));?>" class="button" onclick="location = '<?=$r->getActionUrl('BrowseController', $class, false); ?>'" />
<? if ($browserConfig['allowEdit'] && $object->getPrimaryKey()): ?>
    <input type="button" value="<?=ucfirst(Localizer::getText('edit'));?>" class="button" onclick="location = '<?=$r->getActionUrl('EditController', $class, false); ?>?id=<?=$object->getPrimaryKey();?>'" />
<? endif; ?>
<? if ($browserConfig['allowDelete'] && $object->getPrimaryKey()): ?>
	<input type="button" value="<?=ucfirst(Localizer::getText('delete'));?>" class="button" onclick="location = '<?=$r->getActionUrl('EditController', $class, false); ?>?delete=<?=$object->getPrimaryKey();?>&id=<?=$object->getPrimaryKey();?>'" />
<? endif; ?>
</p>