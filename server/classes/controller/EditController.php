<?php

class EditController extends CrudController {
	
	protected $className;
	
	public function __construct() {
		parent::__construct();
		$this->view->setViewName('edit');
	}
	
	protected function validate(BaseObject $object, $formValues) {
		
		$class = $this->className;
		if (!is_callable($class . '::getCrudMap')) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'undefinedobject'));
			return;
		}
		
		try {
			$objectCrudMap = $class::getCrudMap();
			foreach ($formValues as $key => $value) {
				foreach ($objectCrudMap as $field) {
					if ($field['column'] == $key) {
						
						if (!empty($field['setMethod'])) {
							$method = $field['setMethod'];
						}
						elseif (!empty($field['foreign']) || !empty($field['foreignChild'])) {
							$method = 'set' . ucfirst($field['column']) . 'id';
						}
						else {
							$method = 'set' . ucfirst($field['column']);
						}
						
						$object->$method($value);
						
					}
				}				
			}
			foreach ($objectCrudMap as $field) {
				if ($field['inputType'] == 'checkbox' && !isset($formValues[$field['column']])) {
					$method = 'set' . ucfirst($field['column']);
					$object->$method(false);
				}
			}
			
			if ($object->validate()) {
				$object->save();
				Session::getInstance()->addFlashMessage(ucfirst(Localizer::getText('saved successfully')));
				return true;
			}
			else {
				$search = array();
				$replace = array();
				foreach ($objectCrudMap as $field) {
					$search[] = $field['column'];
					$search[] = $field['column'] . 'id';
					$replace[] = $field['label'];
					$replace[] = $field['label'];
				}
				foreach ($object->getValidationFailures() as $failure) {
					$message = $failure->getMessage();
					
					$message = ucfirst(str_replace($search, $replace, $message));
					Session::getInstance()->addFlashMessage($message, FlashMessage::ERROR);
				}
				return false;
			}
			
		} catch (Exception $e) {
		
			$message = $e->getMessage();  	
			preg_match('/((?<=SQLSTATE).+\: (.*)\]$)/U', $message, $match);
			if (!empty($match[2])) {
				$message = $match[2];	
			}
				
			Session::getInstance()->addFlashMessage($message, FlashMessage::ERROR);
			return false;
		}
		
		
	}
	
	protected function buildEditData(BaseObject $object) {
		
		
		$this->view->setValue('className', $this->className);
		$this->view->setValue('object', $object);
		
		// Fetch ordered data
		$class = $this->className;
				
		if (!is_callable($class . '::getCrudMap')) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'undefinedobject'));
			return;
		}
		
		$user = Session::getInstance()->getUser();
		$objectCrudMap = $class::getCrudMap($user);
		$editFields = array();
		foreach ($objectCrudMap as $field) {
			
			$editFields[$field['label']] = $field;
			
			
			if (isset($field['getMethod'])) {
				$methodName = $field['getMethod'];
			}
			else {
				$methodName = 'get' . ucfirst(strtolower($field['column']));
			}
			
			
			if (isset($field['getIdMethod'])) {
				$idMethodName = $field['getIdMethod'];
			}
			else {
				$idMethodName = $methodName . 'id';
			}
			
			
			if (!empty($field['foreign'])) {
				
				$editFields[$field['label']]['currentValue'] = $object->$idMethodName();
					
				if (empty($field['getMethod'])) {
					if (empty($field['foreignGetMethod'])) {
						$foreignMethodName = $methodName;
					}
					else {
						$foreignMethodName = $field['foreignGetMethod'];
					}
					if (is_object($object->$methodName())) {
						$editFields[$field['label']]['displayValue'] = $object->$methodName()->$foreignMethodName();
					}
					else {
						$editFields[$field['label']]['displayValue'] = NULL;
					}
				}
				else {
					$editFields[$field['label']]['displayValue'] = $object->$methodName();
				}
			}
			else {
				$editFields[$field['label']]['currentValue'] = $object->$methodName();
			}
			
			if (!empty($field['inputType']) && $field['inputType'] == 'select') {
				if (!empty($field['optionsMethod'])) {
					$methodName = $field['optionsMethod'];
					$editFields[$field['label']]['selectOptions'] = $object->$methodName(); 
				}
				else {
					if (empty($field['foreignClass'])) {
						$foreignClass = ucfirst(strtolower($field['column']));
					}
					else {
						$foreignClass = ucfirst(strtolower($field['foreignClass']));
					}
					$foreignClassQuery = $foreignClass . 'Query';
					$foreignClassKey = get_class($object) . 'id';
					if (!empty($field['foreignGetMethod'])) {
						$foreignMethod = $field['foreignGetMethod'];
					}
					elseif (!empty($field['getMethod'])) {
						$foreignMethod = $field['getMethod'];
					}
					else {
						$foreignMethod = 'get' . ucfirst(strtolower($field['column']));
					}
					$method = 'get' . $foreignClassKey;
					$query = $foreignClassQuery::create();
					if (!empty($field['joinArgs'])) {
						foreach ($field['joinArgs'] as $arg) {
							$query->filterBy(ucfirst(strtolower($arg[0])), $arg[1], $arg[2]);
						}
					}
					$foreignObjects = $query->find();
					foreach ($foreignObjects as $foreignObject) {
						$editFields[$field['label']]['selectOptions'][$foreignObject->getPrimaryKey()] = $foreignObject->$foreignMethod();
					}
				}
				asort($editFields[$field['label']]['selectOptions']);
			}
			
		}		
		$this->view->setValue('editFields', $editFields);
	}
	
	protected function editAction($callback = 'buildEditData', $validateCallback = 'validate', $okToEditCallback = false) {
		if (!$this->isAuthorized()) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unauthorized'));
			return;
		}
		
		$queryClassName = $this->className . 'Query';
		$query = $queryClassName::create();

		// Creating: on the first time we generate the form
		if (
			!$this->requestHandler->getParam('id')
			&& (
				!Session::getInstance()->hasValue('tempObject')			 
				|| get_class(Session::getInstance()->getValue('tempObject')) != $this->className
			)
		) {
			$object = new $this->className();
		}
		// Creating: the form has been submitted
		elseif (
			!$this->requestHandler->getParam('id') 
			&& Session::getInstance()->hasValue('tempObject') 
			&& get_class(Session::getInstance()->getValue('tempObject')) == $this->className
		) {
			$object = Session::getInstance()->getValue('tempObject');
			Session::getInstance()->clearValue('tempObject');
		}
		// Editing: just display the old object
		else {
			if (is_callable($this->className . '::getValid')) {
				$query->filterByValid(1);
			}
			
			$object = $query->findPk($this->requestHandler->getParam('id'));
			
			if ($okToEditCallback) {
				if (! call_user_func($okToEditCallback, $object)) {
					Session::getInstance()->addFlashMessage(ucfirst(Localizer::getText('this item can not be edited').'.'), FlashMessage::ERROR);
					$this->requestHandler->redirect($this->requestHandler->getActionUrl('browse', strtolower($this->className)));
				}
			}
		}
		
		
		// Deleting
		if ($this->requestHandler->getParam('delete')) {
			$this->view->setViewName('delete');
			$this->view->setValue('type', $this->className);
			$this->view->setValue('id', $object->getPrimaryKey());
			return;
		}
		
		// Deletion cancelled
		if ($this->requestHandler->getParam('cancel')) {
			Session::getInstance()->addFlashMessage(ucfirst(Localizer::getText('action cancelled').'.'), FlashMessage::INFO);
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('browse', strtolower($this->className)));
				
		}
		
		// Deletion confirmed
		if ($this->requestHandler->getParam('confirmdelete')) {
			$object->delete();
			Session::getInstance()->addFlashMessage(ucfirst(Localizer::getText('deleted successfully')));
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('browse', strtolower($this->className)));
			return;
		}
		
		if (!is_object($object)) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('main', 'unknownobject'));
			return;
		}
		
		if (!is_array($validateCallback)) {
			$validateCallback = array($this, $validateCallback);
		}
		
		if (
			$this->requestHandler->getParam('submit')
			&& call_user_func($validateCallback, $object, $this->requestHandler->getParam('form'))
		) {
			$this->requestHandler->redirect($this->requestHandler->getActionUrl('browse', strtolower($this->className)));
			return;
		}
		
		// Nothing submitted or form validation failed - show form
		if (!is_array($callback)) {
			$callback = array($this, $callback);
		}
		call_user_func($callback, $object);
		
		if (!$this->requestHandler->getParam('id')) {// && !Session::getInstance()->hasValue('tempObject')) {
			Session::getInstance()->setValue('tempObject', $object);
		}
	}
	
	public function userAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit user';
		$this->className = 'User';
		$this->editAction();
	}
	
	public function customerAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit customer';
		$this->className = 'Customer';
		$this->editAction();
	}

	public function languageAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit language';
		$this->className = 'Language';
		$this->editAction();
	}
	
	public function localizationAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit translation';
		$this->className = 'Localization';
		$this->editAction();
	}
	
	public function countryAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit country';
		$this->className = 'Country';
		$this->editAction();
	}
	
	public function addressAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit address';
		$this->className = 'Address';
		$this->editAction('buildEditData', 'validate', array($this, 'isAddressOkToEdit'));
	}
	
	private function isAddressOkToEdit(Address $address) {
		// It's ok to edit unsaved addresses
		if (!$address->getPrimaryKey()) {
			return true;
		}
		// It's not ok to edit addresses that are connected to a trackable.
		if (
			TrackableQuery::create()->filterByStartAddressid($address->getPrimaryKey())->count()
			+ TrackableQuery::create()->filterByEndAddressid($address->getPrimaryKey())->count() 
			> 0
		) {
			return false;
		}
		return true;
	}
	
	public function trackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit trackable';
		$this->className = 'Trackable';
		$this->editAction('buildEditData', 'validate', array($this, 'isTrackableOkToEdit'));
	}
	
	private function isTrackableOkToEdit(Trackable $trackable) {
		// It's ok to edit unsaved addresses
		if (!$trackable->getPrimaryKey()) {
			return true;
		}
		// It's not ok to edit addresses that are connected to a trackable.
		if (
			TriptrackableQuery::create()->filterByTrackableid($trackable->getPrimaryKey())->count()
			> 0
		) {
			return false;
		}
		return true;
	}
	public function groupreferenceAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit group reference';
		$this->className = 'Groupreference';
		$this->editAction();
	}

	public function tripAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit trip';
		$this->className = 'Trip';
		$this->editAction();
	}
	
	public function vehicleAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit vehicle';
		$this->className = 'Vehicle';
		$this->editAction();
	}
	
	public function tripvehicleAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit trip on vehicle';
		$this->className = 'Tripvehicle';
		$this->editAction();
	}
	
	public function detailtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit detail type';
		$this->className = 'Detailtype';
		$this->editAction();
	}

	public function detailAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit detail';
		$this->className = 'Detail';
		$this->editAction();
	}
	
	public function triptrackableAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit trackable on trip';
		$this->className = 'Triptrackable';
		$this->editAction();
	}

	public function statustypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit status type';
		$this->className = 'Statustype';
		$this->editAction();
	}
	
	public function vehiclestatusAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit status';
		$this->className = 'Vehiclestatus';
		$this->editAction();
	}
	
	public function notificationAction() {
		$this->requiredUserType = Controller::USERTYPE_USER;
		$this->values['title'] = 'edit notification';
		$this->className = 'Notification';
		$this->editAction();
	}
	
	public function notificationtypeAction() {
		$this->requiredUserType = Controller::USERTYPE_ADMIN;
		$this->values['title'] = 'edit notification type';
		$this->className = 'Notificationtype';
		$this->editAction();
	}
	/*
	public function buildTrackableEditData(Trackable $trackable) {
		$this->view->setViewName('edittrackable');
		$this->view->setValue('trackable', $trackable);		
		$addresses = AddressQuery::create()->findByCustomerid(
			Session::getInstance()->getUser()->getCustomerId()
		);
		$this->view->setValue('addresses', $addresses);
	}
	
	public function validateTrackable(Trackable $trackable, $formValues) {
	
	}
	*/
}