<?php



/**
 * This class defines the structure of the 'user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class UserTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.UserTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('user');
		$this->setPhpName('User');
		$this->setClassname('User');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('USERID', 'Userid', 'INTEGER', true, 10, null);
		$this->addForeignKey('USERTYPEID', 'Usertypeid', 'INTEGER', 'usertype', 'USERTYPEID', true, 10, 1);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addForeignKey('LANGUAGEID', 'Languageid', 'INTEGER', 'language', 'LANGUAGEID', true, 10, 1);
		$this->addColumn('USERNAME', 'Username', 'VARCHAR', true, 80, null);
		$this->addColumn('FIRSTNAME', 'Firstname', 'VARCHAR', true, 60, null);
		$this->addColumn('LASTNAME', 'Lastname', 'VARCHAR', false, 60, null);
		$this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 80, null);
		$this->addColumn('PASSWORD', 'Password', 'VARCHAR', false, 160, null);
		$this->addColumn('APIKEY', 'Apikey', 'VARCHAR', false, 44, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('USERNAME', 'minLength', 'propel.validator.MinLengthValidator', '3', 'user name must be at least 3 characters');
		$this->addValidator('USERNAME', 'maxLength', 'propel.validator.MaxLengthValidator', '80', 'user name must be at most 80 characters');
		$this->addValidator('USERNAME', 'required', 'propel.validator.RequiredValidator', '', 'user name is required');
		$this->addValidator('FIRSTNAME', 'minLength', 'propel.validator.MinLengthValidator', '3', 'first name must be at least 3 characters');
		$this->addValidator('FIRSTNAME', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'first name must be at most 60 characters');
		$this->addValidator('FIRSTNAME', 'required', 'propel.validator.RequiredValidator', '', 'first name is required');
		$this->addValidator('LASTNAME', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'last name must be at most 60 characters');
		$this->addValidator('EMAIL', 'minLength', 'propel.validator.MinLengthValidator', '3', 'last name must be at least 4 characters');
		$this->addValidator('EMAIL', 'maxLength', 'propel.validator.MaxLengthValidator', '80', 'last name must be at most 80 characters');
		$this->addValidator('EMAIL', 'required', 'propel.validator.RequiredValidator', '', 'last name is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Usertype', 'Usertype', RelationMap::MANY_TO_ONE, array('usertypeid' => 'usertypeid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Language', 'Language', RelationMap::MANY_TO_ONE, array('languageid' => 'languageid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Logentry', 'Logentry', RelationMap::ONE_TO_MANY, array('userid' => 'userid', ), 'CASCADE', 'CASCADE', 'Logentrys');
	} // buildRelations()

} // UserTableMap
