<?php



/**
 * This class defines the structure of the 'triptrackable' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class TriptrackableTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.TriptrackableTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('triptrackable');
		$this->setPhpName('Triptrackable');
		$this->setClassname('Triptrackable');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('TRIPTRACKABLEID', 'Triptrackableid', 'INTEGER', true, 10, null);
		$this->addForeignKey('TRIPID', 'Tripid', 'INTEGER', 'trip', 'TRIPID', true, 10, 1);
		$this->addForeignKey('TRACKABLEID', 'Trackableid', 'INTEGER', 'trackable', 'TRACKABLEID', true, 10, 1);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Trip', 'Trip', RelationMap::MANY_TO_ONE, array('tripid' => 'tripid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Trackable', 'Trackable', RelationMap::MANY_TO_ONE, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // TriptrackableTableMap
