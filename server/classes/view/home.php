<?
	$defaultLat = 60.449249;
	$defaultLng = 22.259239;
	$delay = 200;
	$lat = 0.0;
	$lng = 0.0;
	$zoom = 3;
	$vehicles = $this->getValue('vehicles');
	foreach($vehicles as $vehicle) {
		$lat = $lat + abs($vehicle['lat']);
		$lng = $lng + abs($vehicle['lng']);
	}
	if(count($vehicles) > 0) {
		$lat = $lat / count($vehicles);
		$lng = $lng / count($vehicles);
		$zoom = 6;
	} else {
		$lat = $defaultLat;
		$lng = $defaultLng;
	}
?>
<script type="text/javascript">
	var apikey = '<?=$this->getValue('apikey')?>';
	var lat = <?=$lat?>;
	var lng = <?=$lng?>;
	var latlng = null;
	var mapOptions = null;
	var map = null;
	var delay = <?=$delay?>;
	var timeout = 0;
	var userIsActive = 0;
	var markers = {};
	
	function refresh() {
		
		$.ajax({
		  url: '/tom/api/getVehicles?apikey=' + apikey,
		  dataType: 'json',
		  success: function(response) {
		   	for(var i = 0;i < response['vehicles'].length;i++) {
			   	var vehicleName = response['vehicles'][i]['reference'];
			   	if (markers[vehicleName] == null) {
				   	markers[vehicleName] = new google.maps.Marker({
			              map: map,
			              title: response['vehicles'][i]['reference']
			       });
				   console.log('Created a new marker for ' + vehicleName);
			   	}	
			   	markers[vehicleName].setPosition(new google.maps.LatLng(response['vehicles'][i]['lat'], response['vehicles'][i]['lng']));
			   	
		   	}
			var _lat = 0.0;
			var _lng = 0.0;
			var zoom = 3;
			for(var i = 0;i < response['vehicles'].length;i++) {
				_lat = _lat + (1 * response['vehicles'][i]['lat']);
				_lng = _lng + (1 * response['vehicles'][i]['lng']);
			}
			if(response['vehicles'].length > 0) {
				_lat = _lat / response['vehicles'].length;
				_lng = _lng / response['vehicles'].length;
				zoom = 6;
			} else {
				_lat = <?=$defaultLat?>;
				_lng = <?=$defaultLng?>;
			} 
			
			if(_lat != lat || _lng != lng) {
				if (!userIsActive) {
					map.setZoom(zoom);
					map.setCenter(latlng);
					userIsActive = 0;
				}
			}

			timeout = setTimeout("refresh()", delay);
		  },
		  error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.status + ' ' + thrownError);
              clearTimeout(timeout);
          }
		});
	}
</script>
<?if(Session::getInstance()->getUser()->getUsertypeid() == Controller::USERTYPE_USER) { ?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<?=(count($vehicles) > 0 || true) ? ucfirst(Localizer::getText('latest known locations of vehicles on the move')) . lcfirst(Localizer::getText(' (the map refreshes automatically in ')) . ($delay / 1000) . Localizer::getText(' seconds)') : ucfirst(Localizer::getText('There are no vehicles on the move'));?>
<div id="map_canvas" class="map"></div>
<script type="text/javascript">

$(document).ready(function() {
	overlays = [];
	lat = <?=$lat?>;
	lng = <?=$lng?>;
	latlng = new google.maps.LatLng(lat, lng);
	mapOptions = {zoom: <?=$zoom?>, center: latlng, mapTypeId: google.maps.MapTypeId.TERRAIN};
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

	google.maps.event.addListener(map, 'zoom_changed', function() {
		userIsActive = 1;
	  });
	  
	map.setCenter(latlng);
	while(overlays[0]) {
	    overlays.pop().setMap(null);
	}	
	<? foreach($vehicles as $vehicle) { ?>
       markers['<?=$vehicle['reference'];?>'] = new google.maps.Marker({
              position: new google.maps.LatLng(<?=$vehicle['lat']?>, <?=$vehicle['lng']?>),
              map: map,
              title: '<?=$vehicle['reference']?>'
       });
    <? } ?>
    timeout = setTimeout("refresh()", delay);
});
</script>
<? } else {	
	echo ucfirst(Localizer::getText('Welcome to the administrative UI. You can use the the above functions to add, modify and delete master data.'));
} ?>
