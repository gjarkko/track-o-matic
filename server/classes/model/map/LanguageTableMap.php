<?php



/**
 * This class defines the structure of the 'language' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class LanguageTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.LanguageTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('language');
		$this->setPhpName('Language');
		$this->setClassname('Language');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('LANGUAGEID', 'Languageid', 'INTEGER', true, 10, null);
		$this->addColumn('LANGUAGE', 'Language', 'VARCHAR', true, 20, null);
		$this->addColumn('LANGUAGELOCAL', 'Languagelocal', 'VARCHAR', true, 20, null);
		// validators
		$this->addValidator('LANGUAGE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'language must be at least 1 character');
		$this->addValidator('LANGUAGE', 'maxLength', 'propel.validator.MaxLengthValidator', '20', 'language must be at most 20 characters');
		$this->addValidator('LANGUAGE', 'required', 'propel.validator.RequiredValidator', '', 'language is required');
		$this->addValidator('LANGUAGELOCAL', 'minLength', 'propel.validator.MinLengthValidator', '1', 'language (local) must be at least 1 characters');
		$this->addValidator('LANGUAGELOCAL', 'maxLength', 'propel.validator.MaxLengthValidator', '20', 'language (local) must be at most 20 characters');
		$this->addValidator('LANGUAGELOCAL', 'required', 'propel.validator.RequiredValidator', '', 'language (local) is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Localization', 'Localization', RelationMap::ONE_TO_MANY, array('languageid' => 'languageid', ), 'CASCADE', 'CASCADE', 'Localizations');
		$this->addRelation('User', 'User', RelationMap::ONE_TO_MANY, array('languageid' => 'languageid', ), 'CASCADE', 'CASCADE', 'Users');
	} // buildRelations()

} // LanguageTableMap
