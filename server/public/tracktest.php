<?

// Guess the application installation path, set some path constants
define('ROOT_PATH', implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -1)) . DIRECTORY_SEPARATOR);
define('CLASS_PATH', ROOT_PATH . 'classes' . DIRECTORY_SEPARATOR);
define('VENDOR_PATH', ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR);
define('CONFIG_PATH', ROOT_PATH . 'config' . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', ROOT_PATH . 'public' . DIRECTORY_SEPARATOR);
define('BASEURL', '/tom');

// Set include paths
set_include_path(CLASS_PATH . ':' . ROOT_PATH . ':' . VENDOR_PATH . ':' . get_include_path());

ini_set("display_errors", "on");
error_reporting(E_ALL);


// Include the bare necessities & register autoloader
include 'AutoLoader.php';
include 'Tools.php';
spl_autoload_register('AutoLoader::autoload');

// Init Propel
Propel::init(CONFIG_PATH . "propel-conf.php");

// Bootstrap & run
$app = new Application();
$app->bootstrap();

var_dump(NotificationQuery::create()->findSendable());
