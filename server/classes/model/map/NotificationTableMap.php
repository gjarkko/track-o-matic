<?php



/**
 * This class defines the structure of the 'notification' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class NotificationTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.NotificationTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('notification');
		$this->setPhpName('Notification');
		$this->setClassname('Notification');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('NOTIFICATIONID', 'Notificationid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addForeignKey('NOTIFICATIONTYPEID', 'Notificationtypeid', 'INTEGER', 'notificationtype', 'NOTIFICATIONTYPEID', true, 10, 1);
		$this->addForeignKey('TRACKABLEID', 'Trackableid', 'INTEGER', 'trackable', 'TRACKABLEID', true, 10, 1);
		$this->addColumn('DELIVERYADDRESS', 'Deliveryaddress', 'VARCHAR', true, 60, null);
		$this->addColumn('PROXIMITY', 'Proximity', 'INTEGER', true, 10, null);
		$this->addColumn('TIMECREATED', 'Timecreated', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
		$this->addColumn('TIMESENT', 'Timesent', 'TIMESTAMP', false, null, null);
		$this->addColumn('CREATIONSENT', 'Creationsent', 'BOOLEAN', true, 1, false);
		$this->addColumn('NOTIFICATIONSENT', 'Notificationsent', 'BOOLEAN', true, 1, false);
		// validators
		$this->addValidator('DELIVERYADDRESS', 'minLength', 'propel.validator.MinLengthValidator', '1', 'delivery address must be at least 1 character');
		$this->addValidator('DELIVERYADDRESS', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'delivery address must be at most 60 characters');
		$this->addValidator('DELIVERYADDRESS', 'required', 'propel.validator.RequiredValidator', '', 'delivery address is required');
		$this->addValidator('DELIVERYADDRESS', 'match', 'propel.validator.MatchValidator', '/(^[a-zA-Z]+(([\\\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$|^\+{0,1}[\d- ]{7,16}$)/', 'delivery address must be a valid email address or a mobile telephone number');
		$this->addValidator('PROXIMITY', 'required', 'propel.validator.RequiredValidator', '', 'proximity is required');
		$this->addValidator('PROXIMITY', 'match', 'propel.validator.MatchValidator', '/^([0-9])+$/', 'proximity must be numeric');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Notificationtype', 'Notificationtype', RelationMap::MANY_TO_ONE, array('notificationtypeid' => 'notificationtypeid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Trackable', 'Trackable', RelationMap::MANY_TO_ONE, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // NotificationTableMap
