<?php



/**
 * Skeleton subclass for representing a row from the 'notificationtype' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Notificationtype extends BaseNotificationtype {
	public static function getBrowserConfig() {
		return array(
				'allowView' => true,
				'allowEdit' => true,
				'allowCreate' => true,
				'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
					'column' => 'notificationtype',
					'label' => ucfirst(Localizer::getText('notification type')),
			),
		);
	}
	
	public static function getCrudMap($user = NULL) {
		return array(
		array(
					'column' => 'notificationtype',
					'label' => ucfirst(Localizer::getText('notification type')),
					'required' => true,
					'inputType' => 'text',
					'default' => '',
		),
		);
	}

} // Notificationtype
