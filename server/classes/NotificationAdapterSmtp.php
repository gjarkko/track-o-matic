<?php
class NotificationAdapterSmtp implements NotificationAdapterInterface {
	
	private $transport;
	private $mailer;
	
	private function init() {
		
		require_once 'swift/lib/swift_required.php';
		$this->transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
		$this->mailer = Swift_Mailer::newInstance($this->transport);
	}
	
	public function sendNotification($receiver, $message, $subject = NULL,  $mime = 'text/plain') {
			
		if (empty($this->transport)) {
			$this->init();
		}
		
		/*
		 * Receiver can be one of
		 * array('address' => 'name')
		 * (string)'address'
		 * array('address')
		 */
		
		$emailSender = Registry::get('email_sender');
		
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom($emailSender)
			->setTo($receiver)
			->setBody($message, $mime);
		
		if (!$this->mailer->send($message)) {
			throw new Exception(ucfirst(Localizer::getText('unable to send email')));
		}
	}
	
	public function isHtml() {
		return true;
	}
}