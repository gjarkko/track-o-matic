<?
$r = RequestHandler::getInstance();
$order = $this->getValue('order');
$direction = $this->getValue('direction');
$baseUrl = $r->getCurrentActionUrl(true, array('page'));
$pagingLinks = Pager::getPagingLinks($this->getValue('page'), $this->getValue('pages'));
?>
<table class="pagingLinks">
	<tr>
		<th colspan="3">
			<?=ucfirst(sprintf(Localizer::getText('showing page %ld of %ld'), $this->getValue('page'), $this->getValue('pages')));?>.<br />
		</th>
	</tr>
	<tr>
		<td class="backward">
			<? foreach ($pagingLinks['backward'] as $pagingLink): ?>
				<? if ($pagingLink['link']): ?>
					<span class="pagingLink"><a href="<?=$baseUrl;?>&page=<?=$pagingLink['pageNo'];?>"><img src="/tom/image/<?=$pagingLink['image']?>" border="0" title="<?=$pagingLink['text']?>"/></a></span> 
				<? endif; ?>
			<? endforeach; ?>		
		</td>
		<td class="pages">
			<? foreach ($pagingLinks['pages'] as $pagingLink): ?>
				<? if ($pagingLink['link']): ?>
					<span class="pagingLink<?=$pagingLink['current'] ? ' pagingLink_active' : '';?>"><a href="<?=$baseUrl;?>&page=<?=$pagingLink['pageNo'];?>"><?=$pagingLink['text']?></a></span>
				<? else: ?>
					<span class="pagingLink<?=$pagingLink['current'] ? ' pagingLink_active' : '';?>"><?=$pagingLink['text'];?></span>
				<? endif; ?>
			<? endforeach; ?>		
		</td>
		<td class="forward">
			<? foreach ($pagingLinks['forward'] as $pagingLink): ?>
				<? if ($pagingLink['link']): ?>
					<span class="pagingLink"><a href="<?=$baseUrl;?>&page=<?=$pagingLink['pageNo'];?>"><img src="/tom/image/<?=$pagingLink['image']?>" border="0" title="<?=$pagingLink['text']?>" /></a></span>
				<? endif; ?>
			<? endforeach; ?>		
		</td>
	</tr>
</table>


<? 
// Use UTF8 arrow characters
$arrow = array('ASC' => '<img src="/tom/image/sortasc.gif" border="0" />', 'DESC' => '<img src="/tom/image/sortdesc.gif" border="0" />');

/* @var $data PropelObjectCollection */
$data = $this->getValue('browserData');
$crudMap = $this->getValue('objectCrudMap');

$baseUrl = $r->getCurrentActionUrl(true, array('order', 'direction'));
$className = $this->getValue('className');

$browserConfig = array(
	'allowView' => false,
	'allowEdit' => false,
	'allowCreate' => false,
	'allowDelete' => false,
);
if (is_callable($className . '::getBrowserConfig')) {
	$browserConfig = array_merge($browserConfig, $className::getBrowserConfig());
}

$actionsColumn = false;
if ($browserConfig['allowEdit'] || $browserConfig['allowView'] || $browserConfig['allowDelete']) {
	$actionsColumn = true;
}

$peerClass = $className . 'Peer';
$columnMaps = $peerClass::getTableMap()->getColumns();
if (is_callable($className . '::getBrowserColumnMap')) {
	$objectColumnMap =  $className::getBrowserColumnMap();
}
else {
	$objectColumnMap = false;
} ?>

<? // Object class has a browser column map - list defined columns
if ($objectColumnMap): ?>
	<div class="tableTop"></div>
	<table class="objectList">
	<tr>
	<? foreach ($objectColumnMap as $i => $col): ?>
		<?
			if ($order !== $i || ($order === $i && $direction == 'DESC')) {
				$linkDirection = 'ASC';
			}
			else {
				$linkDirection = 'DESC';
			}
		
		?>
		<th>
			<a href="<?=$baseUrl;?>&order=<?=$i;?>&direction=<?=$linkDirection;?>"><?=$col['label'];?></a> 
			<?=($order === $i) ? $arrow[$linkDirection] : ''?>
		</th>
	<? endforeach; ?>
	
	<? if ($actionsColumn): ?>
		<th class="action">
		<? if ($browserConfig['allowCreate']): ?>
			<a href="<?=$r->getActionUrl('EditController', $className, false); ?>"><img src="/tom/image/add.png" title="Create new" border="0" /></a>
		<? endif; ?>
		</th>
	<? endif; ?>
	</tr>
	<? foreach ($data as $i => $object): ?>
	    <? $inputtype = 'text'; ?>
	    <? $class = ($i % 2 == 0) ? 'even' : 'odd';?>
	    <? $class .= ($i == count($data) - 1) ? ' last' : '';?>
	    <tr>
		<? foreach ($objectColumnMap as $i => $col): 
			if (!empty($col['foreign'])):
				if (empty($col['display_method'])) {
					$method = 'get' . ucfirst(strtolower($col['column'])); 
					$value = $object->$method()->$method();
				}
				else {
					$method = $col['display_method'];
					$value = $object->$method();
				}
			elseif (!empty($col['foreignChild'])):
				$foreignClass = ucfirst(strtolower($col['column']));
				$foreignClassQuery = $foreignClass . 'Query';
				$foreignClassKey = get_class($object) . 'id';
				$foreignMethod = 'get' . $col['column'];
				$method = 'get' . $foreignClassKey;
				$query = $foreignClassQuery::create()->filterBy($foreignClassKey, $object->getPrimaryKey());
				if (!empty($col['joinArgs'])) {
					foreach ($col['joinArgs'] as $arg) {
						$query->filterBy(ucfirst(strtolower($arg[0])), $arg[1], $arg[2]);
					}
				}
				$foreignObject = $query->findOne();
				$value = $foreignObject->$foreignMethod();
			else:
				$value = $object->getByName(ucfirst(strtolower($col['column'])));
				foreach($crudMap as $entry) {
					if(strtolower($entry['column']) == strtolower($col['column'])) {
						$inputtype = $entry['inputType'];
					}
				}
			endif; ?>
			<td class="<?=$class?>"><?=($inputtype == 'checkbox') ? ($value == 1) ? '<img src="/tom/image/check.png" />' : '' : $value;?></td>
		<? endforeach; ?>
		
		<? if ($actionsColumn): ?>
			<td class="action <?=$class?>">
			<? if ($browserConfig['allowView']): ?>
				<a href="<?=$r->getActionUrl('ViewController', $className, false); ?>?id=<?=$object->getPrimaryKey();?>"><img src="/tom/image/view.gif" border="0" title="View" /></a>
			<? endif; ?>
			<? if ($browserConfig['allowEdit']): ?>
				<a href="<?=$r->getActionUrl('EditController', $className, false); ?>?id=<?=$object->getPrimaryKey();?>"><img src="/tom/image/edit.gif" border="0" title="Edit" /></a>
			<? endif; ?>
			<? if ($browserConfig['allowDelete']): ?>
				<a href="<?=$r->getActionUrl('EditController', $className, false); ?>?delete=<?=$object->getPrimaryKey();?>&id=<?=$object->getPrimaryKey();?>"><img src="/tom/image/delete.png" border="0" title="Delete" /></a>
			<? endif; ?>
			</td>
		<? endif; ?>
		</tr>
	<? endforeach; ?>
	</table><div class="tableBottom"></div>
<? endif; ?>
