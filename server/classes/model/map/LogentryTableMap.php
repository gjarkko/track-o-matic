<?php



/**
 * This class defines the structure of the 'logentry' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class LogentryTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.LogentryTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('logentry');
		$this->setPhpName('Logentry');
		$this->setClassname('Logentry');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('LOGENTRYID', 'Logentryid', 'INTEGER', true, 10, null);
		$this->addForeignKey('LOGENTRYTYPEID', 'Logentrytypeid', 'INTEGER', 'logentrytype', 'LOGENTRYTYPEID', true, 10, 1);
		$this->addForeignKey('USERID', 'Userid', 'INTEGER', 'user', 'USERID', true, 10, 1);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('TIME', 'Time', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
		$this->addColumn('IP', 'Ip', 'VARCHAR', false, 15, null);
		$this->addColumn('SESSIONID', 'Sessionid', 'VARCHAR', false, 30, null);
		$this->addColumn('SESSIONAGE', 'Sessionage', 'FLOAT', false, null, null);
		$this->addColumn('MESSAGE', 'Message', 'VARCHAR', true, 128, null);
		$this->addColumn('REFERENCETYPE', 'Referencetype', 'VARCHAR', false, 128, null);
		$this->addColumn('REFERENCEID', 'Referenceid', 'INTEGER', false, 10, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Logentrytype', 'Logentrytype', RelationMap::MANY_TO_ONE, array('logentrytypeid' => 'logentrytypeid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('User', 'User', RelationMap::MANY_TO_ONE, array('userid' => 'userid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // LogentryTableMap
