<?php



/**
 * This class defines the structure of the 'statustype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class StatustypeTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.StatustypeTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('statustype');
		$this->setPhpName('Statustype');
		$this->setClassname('Statustype');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('STATUSTYPEID', 'Statustypeid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('STATUSTYPE', 'Statustype', 'VARCHAR', true, 60, null);
		$this->addColumn('ISMOVING', 'Ismoving', 'BOOLEAN', true, 1, false);
		$this->addColumn('HASLOCATION', 'Haslocation', 'BOOLEAN', true, 1, false);
		$this->addColumn('ISFINAL', 'Isfinal', 'BOOLEAN', true, 1, false);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('STATUSTYPE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'status type must be at least 1 character');
		$this->addValidator('STATUSTYPE', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'status type must be at most 60 characters');
		$this->addValidator('STATUSTYPE', 'required', 'propel.validator.RequiredValidator', '', 'status type is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Vehiclestatus', 'Vehiclestatus', RelationMap::ONE_TO_MANY, array('statustypeid' => 'statustypeid', ), 'CASCADE', 'CASCADE', 'Vehiclestatuss');
		$this->addRelation('Trackablestatus', 'Trackablestatus', RelationMap::ONE_TO_MANY, array('statustypeid' => 'statustypeid', ), 'CASCADE', 'CASCADE', 'Trackablestatuss');
	} // buildRelations()

} // StatustypeTableMap
