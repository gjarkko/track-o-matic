<?
abstract class Controller {
	
	const USERTYPE_SYSTEM = 1;
	const USERTYPE_ADMIN = 2;
	const USERTYPE_USER = 3;
	const USERTYPE_DRIVER = 4;
	
	protected $requiredUserType;
	protected $requestHandler;
	protected $renderStarted;
	protected $publicActionNames;
	
	protected $view;
	
	public function __construct() {
		if (!isset($this->view)) {
			$this->view = new View();
		}
		
		$this->requestHandler = RequestHandler::getInstance();
		$this->renderStarted = false;
		$this->continueExecution = true;
		$this->publicActionNames = array(
			'MainController::unsupportedactionAction', 
			'MainController::loginAction', 
			'MainController::logoutAction'
		);
	}
	
	public function indexAction() {
		echo "you have reached the index action of the base controller and are therefore doing it wrong.";
	}
		
	public function getRenderStarted() {
		return $this->renderStarted;
	}
	
	public function render() {
		if (!isset($this->view) || !is_object($this->view) || get_class($this->view) != 'View') {
			$this->view = new View();
		}
		$this->view->render();
	}
	
	public function getView() {
		if (isset($this->view) && get_class($this->view) == 'View') {
			return $this->view;
		}
	}
	
	protected function isAuthorized() {
		return Session::getInstance(true)->getUser() != NULL && Session::getInstance()->getUser()->getUsertypeid() <= $this->requiredUserType;
	}
	
	protected function hasCustomerRelation($customerid = NULL) {
		if ($customerid === NULL) {
			return Session::getInstance(true)->getUser() != NULL && Session::getInstance()->getUser()->getCustomerid() != 1;
		}
		else {
			return Session::getInstance(true)->getUser() != NULL && Session::getInstance()->getUser()->getCustomerid() == $customerid;
		}
	}
	public function unsupportedactionAction() { } 
}