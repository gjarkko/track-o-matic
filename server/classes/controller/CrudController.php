<?
/**
 *
 * Generic object editor
 * Makes HTML views and forms of given object
 */
class CrudController extends XmlController {
	
	protected $className;
	protected $keys;
	
	public function __construct() {
		parent::__construct();
		$this->view->setViewName('view');
	}
}
