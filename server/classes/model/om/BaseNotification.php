<?php


/**
 * Base class that represents a row from the 'notification' table.
 *
 * 
 *
 * @package    propel.generator.model.om
 */
abstract class BaseNotification extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'NotificationPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        NotificationPeer
	 */
	protected static $peer;

	/**
	 * The value for the notificationid field.
	 * @var        int
	 */
	protected $notificationid;

	/**
	 * The value for the customerid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $customerid;

	/**
	 * The value for the notificationtypeid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $notificationtypeid;

	/**
	 * The value for the trackableid field.
	 * Note: this column has a database default value of: 1
	 * @var        int
	 */
	protected $trackableid;

	/**
	 * The value for the deliveryaddress field.
	 * @var        string
	 */
	protected $deliveryaddress;

	/**
	 * The value for the proximity field.
	 * @var        int
	 */
	protected $proximity;

	/**
	 * The value for the timecreated field.
	 * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
	 * @var        string
	 */
	protected $timecreated;

	/**
	 * The value for the timesent field.
	 * @var        string
	 */
	protected $timesent;

	/**
	 * The value for the creationsent field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $creationsent;

	/**
	 * The value for the notificationsent field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $notificationsent;

	/**
	 * @var        Notificationtype
	 */
	protected $aNotificationtype;

	/**
	 * @var        Trackable
	 */
	protected $aTrackable;

	/**
	 * @var        Customer
	 */
	protected $aCustomer;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->customerid = 1;
		$this->notificationtypeid = 1;
		$this->trackableid = 1;
		$this->creationsent = false;
		$this->notificationsent = false;
	}

	/**
	 * Initializes internal state of BaseNotification object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [notificationid] column value.
	 * 
	 * @return     int
	 */
	public function getNotificationid()
	{
		return $this->notificationid;
	}

	/**
	 * Get the [customerid] column value.
	 * 
	 * @return     int
	 */
	public function getCustomerid()
	{
		return $this->customerid;
	}

	/**
	 * Get the [notificationtypeid] column value.
	 * 
	 * @return     int
	 */
	public function getNotificationtypeid()
	{
		return $this->notificationtypeid;
	}

	/**
	 * Get the [trackableid] column value.
	 * 
	 * @return     int
	 */
	public function getTrackableid()
	{
		return $this->trackableid;
	}

	/**
	 * Get the [deliveryaddress] column value.
	 * 
	 * @return     string
	 */
	public function getDeliveryaddress()
	{
		return $this->deliveryaddress;
	}

	/**
	 * Get the [proximity] column value.
	 * 
	 * @return     int
	 */
	public function getProximity()
	{
		return $this->proximity;
	}

	/**
	 * Get the [optionally formatted] temporal [timecreated] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getTimecreated($format = 'Y-m-d H:i:s')
	{
		if ($this->timecreated === null) {
			return null;
		}


		if ($this->timecreated === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->timecreated);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->timecreated, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [timesent] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getTimesent($format = 'Y-m-d H:i:s')
	{
		if ($this->timesent === null) {
			return null;
		}


		if ($this->timesent === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->timesent);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->timesent, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [creationsent] column value.
	 * 
	 * @return     boolean
	 */
	public function getCreationsent()
	{
		return $this->creationsent;
	}

	/**
	 * Get the [notificationsent] column value.
	 * 
	 * @return     boolean
	 */
	public function getNotificationsent()
	{
		return $this->notificationsent;
	}

	/**
	 * Set the value of [notificationid] column.
	 * 
	 * @param      int $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setNotificationid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->notificationid !== $v) {
			$this->notificationid = $v;
			$this->modifiedColumns[] = NotificationPeer::NOTIFICATIONID;
		}

		return $this;
	} // setNotificationid()

	/**
	 * Set the value of [customerid] column.
	 * 
	 * @param      int $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setCustomerid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->customerid !== $v) {
			$this->customerid = $v;
			$this->modifiedColumns[] = NotificationPeer::CUSTOMERID;
		}

		if ($this->aCustomer !== null && $this->aCustomer->getCustomerid() !== $v) {
			$this->aCustomer = null;
		}

		return $this;
	} // setCustomerid()

	/**
	 * Set the value of [notificationtypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setNotificationtypeid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->notificationtypeid !== $v) {
			$this->notificationtypeid = $v;
			$this->modifiedColumns[] = NotificationPeer::NOTIFICATIONTYPEID;
		}

		if ($this->aNotificationtype !== null && $this->aNotificationtype->getNotificationtypeid() !== $v) {
			$this->aNotificationtype = null;
		}

		return $this;
	} // setNotificationtypeid()

	/**
	 * Set the value of [trackableid] column.
	 * 
	 * @param      int $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setTrackableid($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->trackableid !== $v) {
			$this->trackableid = $v;
			$this->modifiedColumns[] = NotificationPeer::TRACKABLEID;
		}

		if ($this->aTrackable !== null && $this->aTrackable->getTrackableid() !== $v) {
			$this->aTrackable = null;
		}

		return $this;
	} // setTrackableid()

	/**
	 * Set the value of [deliveryaddress] column.
	 * 
	 * @param      string $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setDeliveryaddress($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->deliveryaddress !== $v) {
			$this->deliveryaddress = $v;
			$this->modifiedColumns[] = NotificationPeer::DELIVERYADDRESS;
		}

		return $this;
	} // setDeliveryaddress()

	/**
	 * Set the value of [proximity] column.
	 * 
	 * @param      int $v new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setProximity($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->proximity !== $v) {
			$this->proximity = $v;
			$this->modifiedColumns[] = NotificationPeer::PROXIMITY;
		}

		return $this;
	} // setProximity()

	/**
	 * Sets the value of [timecreated] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setTimecreated($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->timecreated !== null || $dt !== null) {
			$currentDateAsString = ($this->timecreated !== null && $tmpDt = new DateTime($this->timecreated)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->timecreated = $newDateAsString;
				$this->modifiedColumns[] = NotificationPeer::TIMECREATED;
			}
		} // if either are not null

		return $this;
	} // setTimecreated()

	/**
	 * Sets the value of [timesent] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.
	 *               Empty strings are treated as NULL.
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setTimesent($v)
	{
		$dt = PropelDateTime::newInstance($v, null, 'DateTime');
		if ($this->timesent !== null || $dt !== null) {
			$currentDateAsString = ($this->timesent !== null && $tmpDt = new DateTime($this->timesent)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
			if ($currentDateAsString !== $newDateAsString) {
				$this->timesent = $newDateAsString;
				$this->modifiedColumns[] = NotificationPeer::TIMESENT;
			}
		} // if either are not null

		return $this;
	} // setTimesent()

	/**
	 * Sets the value of the [creationsent] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setCreationsent($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->creationsent !== $v) {
			$this->creationsent = $v;
			$this->modifiedColumns[] = NotificationPeer::CREATIONSENT;
		}

		return $this;
	} // setCreationsent()

	/**
	 * Sets the value of the [notificationsent] column.
	 * Non-boolean arguments are converted using the following rules:
	 *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * 
	 * @param      boolean|integer|string $v The new value
	 * @return     Notification The current object (for fluent API support)
	 */
	public function setNotificationsent($v)
	{
		if ($v !== null) {
			if (is_string($v)) {
				$v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
			} else {
				$v = (boolean) $v;
			}
		}

		if ($this->notificationsent !== $v) {
			$this->notificationsent = $v;
			$this->modifiedColumns[] = NotificationPeer::NOTIFICATIONSENT;
		}

		return $this;
	} // setNotificationsent()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->customerid !== 1) {
				return false;
			}

			if ($this->notificationtypeid !== 1) {
				return false;
			}

			if ($this->trackableid !== 1) {
				return false;
			}

			if ($this->creationsent !== false) {
				return false;
			}

			if ($this->notificationsent !== false) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->notificationid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->customerid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->notificationtypeid = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->trackableid = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->deliveryaddress = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->proximity = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
			$this->timecreated = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->timesent = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->creationsent = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
			$this->notificationsent = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 10; // 10 = NotificationPeer::NUM_HYDRATE_COLUMNS.

		} catch (Exception $e) {
			throw new PropelException("Error populating Notification object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aCustomer !== null && $this->customerid !== $this->aCustomer->getCustomerid()) {
			$this->aCustomer = null;
		}
		if ($this->aNotificationtype !== null && $this->notificationtypeid !== $this->aNotificationtype->getNotificationtypeid()) {
			$this->aNotificationtype = null;
		}
		if ($this->aTrackable !== null && $this->trackableid !== $this->aTrackable->getTrackableid()) {
			$this->aTrackable = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(NotificationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = NotificationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aNotificationtype = null;
			$this->aTrackable = null;
			$this->aCustomer = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(NotificationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$deleteQuery = NotificationQuery::create()
				->filterByPrimaryKey($this->getPrimaryKey());
			$ret = $this->preDelete($con);
			if ($ret) {
				$deleteQuery->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(NotificationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				NotificationPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (Exception $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aNotificationtype !== null) {
				if ($this->aNotificationtype->isModified() || $this->aNotificationtype->isNew()) {
					$affectedRows += $this->aNotificationtype->save($con);
				}
				$this->setNotificationtype($this->aNotificationtype);
			}

			if ($this->aTrackable !== null) {
				if ($this->aTrackable->isModified() || $this->aTrackable->isNew()) {
					$affectedRows += $this->aTrackable->save($con);
				}
				$this->setTrackable($this->aTrackable);
			}

			if ($this->aCustomer !== null) {
				if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
					$affectedRows += $this->aCustomer->save($con);
				}
				$this->setCustomer($this->aCustomer);
			}

			if ($this->isNew() || $this->isModified()) {
				// persist changes
				if ($this->isNew()) {
					$this->doInsert($con);
				} else {
					$this->doUpdate($con);
				}
				$affectedRows += 1;
				$this->resetModified();
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Insert the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @throws     PropelException
	 * @see        doSave()
	 */
	protected function doInsert(PropelPDO $con)
	{
		$modifiedColumns = array();
		$index = 0;

		$this->modifiedColumns[] = NotificationPeer::NOTIFICATIONID;
		if (null !== $this->notificationid) {
			throw new PropelException('Cannot insert a value for auto-increment primary key (' . NotificationPeer::NOTIFICATIONID . ')');
		}

		 // check the columns in natural order for more readable SQL queries
		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONID)) {
			$modifiedColumns[':p' . $index++]  = '`NOTIFICATIONID`';
		}
		if ($this->isColumnModified(NotificationPeer::CUSTOMERID)) {
			$modifiedColumns[':p' . $index++]  = '`CUSTOMERID`';
		}
		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONTYPEID)) {
			$modifiedColumns[':p' . $index++]  = '`NOTIFICATIONTYPEID`';
		}
		if ($this->isColumnModified(NotificationPeer::TRACKABLEID)) {
			$modifiedColumns[':p' . $index++]  = '`TRACKABLEID`';
		}
		if ($this->isColumnModified(NotificationPeer::DELIVERYADDRESS)) {
			$modifiedColumns[':p' . $index++]  = '`DELIVERYADDRESS`';
		}
		if ($this->isColumnModified(NotificationPeer::PROXIMITY)) {
			$modifiedColumns[':p' . $index++]  = '`PROXIMITY`';
		}
		if ($this->isColumnModified(NotificationPeer::TIMECREATED)) {
			$modifiedColumns[':p' . $index++]  = '`TIMECREATED`';
		}
		if ($this->isColumnModified(NotificationPeer::TIMESENT)) {
			$modifiedColumns[':p' . $index++]  = '`TIMESENT`';
		}
		if ($this->isColumnModified(NotificationPeer::CREATIONSENT)) {
			$modifiedColumns[':p' . $index++]  = '`CREATIONSENT`';
		}
		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONSENT)) {
			$modifiedColumns[':p' . $index++]  = '`NOTIFICATIONSENT`';
		}

		$sql = sprintf(
			'INSERT INTO `notification` (%s) VALUES (%s)',
			implode(', ', $modifiedColumns),
			implode(', ', array_keys($modifiedColumns))
		);

		try {
			$stmt = $con->prepare($sql);
			foreach ($modifiedColumns as $identifier => $columnName) {
				switch ($columnName) {
					case '`NOTIFICATIONID`':
						$stmt->bindValue($identifier, $this->notificationid, PDO::PARAM_INT);
						break;
					case '`CUSTOMERID`':
						$stmt->bindValue($identifier, $this->customerid, PDO::PARAM_INT);
						break;
					case '`NOTIFICATIONTYPEID`':
						$stmt->bindValue($identifier, $this->notificationtypeid, PDO::PARAM_INT);
						break;
					case '`TRACKABLEID`':
						$stmt->bindValue($identifier, $this->trackableid, PDO::PARAM_INT);
						break;
					case '`DELIVERYADDRESS`':
						$stmt->bindValue($identifier, $this->deliveryaddress, PDO::PARAM_STR);
						break;
					case '`PROXIMITY`':
						$stmt->bindValue($identifier, $this->proximity, PDO::PARAM_INT);
						break;
					case '`TIMECREATED`':
						$stmt->bindValue($identifier, $this->timecreated, PDO::PARAM_STR);
						break;
					case '`TIMESENT`':
						$stmt->bindValue($identifier, $this->timesent, PDO::PARAM_STR);
						break;
					case '`CREATIONSENT`':
						$stmt->bindValue($identifier, (int) $this->creationsent, PDO::PARAM_INT);
						break;
					case '`NOTIFICATIONSENT`':
						$stmt->bindValue($identifier, (int) $this->notificationsent, PDO::PARAM_INT);
						break;
				}
			}
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
		}

		try {
			$pk = $con->lastInsertId();
		} catch (Exception $e) {
			throw new PropelException('Unable to get autoincrement id.', $e);
		}
		$this->setNotificationid($pk);

		$this->setNew(false);
	}

	/**
	 * Update the row in the database.
	 *
	 * @param      PropelPDO $con
	 *
	 * @see        doSave()
	 */
	protected function doUpdate(PropelPDO $con)
	{
		$selectCriteria = $this->buildPkeyCriteria();
		$valuesCriteria = $this->buildCriteria();
		BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
	}

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aNotificationtype !== null) {
				if (!$this->aNotificationtype->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aNotificationtype->getValidationFailures());
				}
			}

			if ($this->aTrackable !== null) {
				if (!$this->aTrackable->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTrackable->getValidationFailures());
				}
			}

			if ($this->aCustomer !== null) {
				if (!$this->aCustomer->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCustomer->getValidationFailures());
				}
			}


			if (($retval = NotificationPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = NotificationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getNotificationid();
				break;
			case 1:
				return $this->getCustomerid();
				break;
			case 2:
				return $this->getNotificationtypeid();
				break;
			case 3:
				return $this->getTrackableid();
				break;
			case 4:
				return $this->getDeliveryaddress();
				break;
			case 5:
				return $this->getProximity();
				break;
			case 6:
				return $this->getTimecreated();
				break;
			case 7:
				return $this->getTimesent();
				break;
			case 8:
				return $this->getCreationsent();
				break;
			case 9:
				return $this->getNotificationsent();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
	{
		if (isset($alreadyDumpedObjects['Notification'][$this->getPrimaryKey()])) {
			return '*RECURSION*';
		}
		$alreadyDumpedObjects['Notification'][$this->getPrimaryKey()] = true;
		$keys = NotificationPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getNotificationid(),
			$keys[1] => $this->getCustomerid(),
			$keys[2] => $this->getNotificationtypeid(),
			$keys[3] => $this->getTrackableid(),
			$keys[4] => $this->getDeliveryaddress(),
			$keys[5] => $this->getProximity(),
			$keys[6] => $this->getTimecreated(),
			$keys[7] => $this->getTimesent(),
			$keys[8] => $this->getCreationsent(),
			$keys[9] => $this->getNotificationsent(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aNotificationtype) {
				$result['Notificationtype'] = $this->aNotificationtype->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aTrackable) {
				$result['Trackable'] = $this->aTrackable->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
			if (null !== $this->aCustomer) {
				$result['Customer'] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = NotificationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setNotificationid($value);
				break;
			case 1:
				$this->setCustomerid($value);
				break;
			case 2:
				$this->setNotificationtypeid($value);
				break;
			case 3:
				$this->setTrackableid($value);
				break;
			case 4:
				$this->setDeliveryaddress($value);
				break;
			case 5:
				$this->setProximity($value);
				break;
			case 6:
				$this->setTimecreated($value);
				break;
			case 7:
				$this->setTimesent($value);
				break;
			case 8:
				$this->setCreationsent($value);
				break;
			case 9:
				$this->setNotificationsent($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = NotificationPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setNotificationid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCustomerid($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNotificationtypeid($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTrackableid($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDeliveryaddress($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setProximity($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTimecreated($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTimesent($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setCreationsent($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setNotificationsent($arr[$keys[9]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(NotificationPeer::DATABASE_NAME);

		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONID)) $criteria->add(NotificationPeer::NOTIFICATIONID, $this->notificationid);
		if ($this->isColumnModified(NotificationPeer::CUSTOMERID)) $criteria->add(NotificationPeer::CUSTOMERID, $this->customerid);
		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONTYPEID)) $criteria->add(NotificationPeer::NOTIFICATIONTYPEID, $this->notificationtypeid);
		if ($this->isColumnModified(NotificationPeer::TRACKABLEID)) $criteria->add(NotificationPeer::TRACKABLEID, $this->trackableid);
		if ($this->isColumnModified(NotificationPeer::DELIVERYADDRESS)) $criteria->add(NotificationPeer::DELIVERYADDRESS, $this->deliveryaddress);
		if ($this->isColumnModified(NotificationPeer::PROXIMITY)) $criteria->add(NotificationPeer::PROXIMITY, $this->proximity);
		if ($this->isColumnModified(NotificationPeer::TIMECREATED)) $criteria->add(NotificationPeer::TIMECREATED, $this->timecreated);
		if ($this->isColumnModified(NotificationPeer::TIMESENT)) $criteria->add(NotificationPeer::TIMESENT, $this->timesent);
		if ($this->isColumnModified(NotificationPeer::CREATIONSENT)) $criteria->add(NotificationPeer::CREATIONSENT, $this->creationsent);
		if ($this->isColumnModified(NotificationPeer::NOTIFICATIONSENT)) $criteria->add(NotificationPeer::NOTIFICATIONSENT, $this->notificationsent);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(NotificationPeer::DATABASE_NAME);
		$criteria->add(NotificationPeer::NOTIFICATIONID, $this->notificationid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getNotificationid();
	}

	/**
	 * Generic method to set the primary key (notificationid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setNotificationid($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getNotificationid();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Notification (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
	{
		$copyObj->setCustomerid($this->getCustomerid());
		$copyObj->setNotificationtypeid($this->getNotificationtypeid());
		$copyObj->setTrackableid($this->getTrackableid());
		$copyObj->setDeliveryaddress($this->getDeliveryaddress());
		$copyObj->setProximity($this->getProximity());
		$copyObj->setTimecreated($this->getTimecreated());
		$copyObj->setTimesent($this->getTimesent());
		$copyObj->setCreationsent($this->getCreationsent());
		$copyObj->setNotificationsent($this->getNotificationsent());
		if ($makeNew) {
			$copyObj->setNew(true);
			$copyObj->setNotificationid(NULL); // this is a auto-increment column, so set to default value
		}
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Notification Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     NotificationPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new NotificationPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Notificationtype object.
	 *
	 * @param      Notificationtype $v
	 * @return     Notification The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setNotificationtype(Notificationtype $v = null)
	{
		if ($v === null) {
			$this->setNotificationtypeid(1);
		} else {
			$this->setNotificationtypeid($v->getNotificationtypeid());
		}

		$this->aNotificationtype = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Notificationtype object, it will not be re-added.
		if ($v !== null) {
			$v->addNotification($this);
		}

		return $this;
	}


	/**
	 * Get the associated Notificationtype object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Notificationtype The associated Notificationtype object.
	 * @throws     PropelException
	 */
	public function getNotificationtype(PropelPDO $con = null)
	{
		if ($this->aNotificationtype === null && ($this->notificationtypeid !== null)) {
			$this->aNotificationtype = NotificationtypeQuery::create()->findPk($this->notificationtypeid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aNotificationtype->addNotifications($this);
			 */
		}
		return $this->aNotificationtype;
	}

	/**
	 * Declares an association between this object and a Trackable object.
	 *
	 * @param      Trackable $v
	 * @return     Notification The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setTrackable(Trackable $v = null)
	{
		if ($v === null) {
			$this->setTrackableid(1);
		} else {
			$this->setTrackableid($v->getTrackableid());
		}

		$this->aTrackable = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Trackable object, it will not be re-added.
		if ($v !== null) {
			$v->addNotification($this);
		}

		return $this;
	}


	/**
	 * Get the associated Trackable object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Trackable The associated Trackable object.
	 * @throws     PropelException
	 */
	public function getTrackable(PropelPDO $con = null)
	{
		if ($this->aTrackable === null && ($this->trackableid !== null)) {
			$this->aTrackable = TrackableQuery::create()->findPk($this->trackableid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aTrackable->addNotifications($this);
			 */
		}
		return $this->aTrackable;
	}

	/**
	 * Declares an association between this object and a Customer object.
	 *
	 * @param      Customer $v
	 * @return     Notification The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setCustomer(Customer $v = null)
	{
		if ($v === null) {
			$this->setCustomerid(1);
		} else {
			$this->setCustomerid($v->getCustomerid());
		}

		$this->aCustomer = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Customer object, it will not be re-added.
		if ($v !== null) {
			$v->addNotification($this);
		}

		return $this;
	}


	/**
	 * Get the associated Customer object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Customer The associated Customer object.
	 * @throws     PropelException
	 */
	public function getCustomer(PropelPDO $con = null)
	{
		if ($this->aCustomer === null && ($this->customerid !== null)) {
			$this->aCustomer = CustomerQuery::create()->findPk($this->customerid, $con);
			/* The following can be used additionally to
				guarantee the related object contains a reference
				to this object.  This level of coupling may, however, be
				undesirable since it could result in an only partially populated collection
				in the referenced object.
				$this->aCustomer->addNotifications($this);
			 */
		}
		return $this->aCustomer;
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->notificationid = null;
		$this->customerid = null;
		$this->notificationtypeid = null;
		$this->trackableid = null;
		$this->deliveryaddress = null;
		$this->proximity = null;
		$this->timecreated = null;
		$this->timesent = null;
		$this->creationsent = null;
		$this->notificationsent = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->applyDefaultValues();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all references to other model objects or collections of model objects.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect
	 * objects with circular references (even in PHP 5.3). This is currently necessary
	 * when using Propel in certain daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all referrer objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

		$this->aNotificationtype = null;
		$this->aTrackable = null;
		$this->aCustomer = null;
	}

	/**
	 * Return the string representation of this object
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->exportTo(NotificationPeer::DEFAULT_STRING_FORMAT);
	}

} // BaseNotification
