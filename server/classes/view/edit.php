<?
$r = RequestHandler::getInstance();
$class = $this->values['className'];
$object = $this->values['object'];
$browserConfig = array(
	'allowView' => false,
	'allowEdit' => false,
	'allowCreate' => false,
	'allowDelete' => false,
);
if (is_callable($class . '::getBrowserConfig')) {
	$browserConfig = array_merge($browserConfig, $class::getBrowserConfig());
}
?>

<div class="objectForm">
	<?=ucfirst(Localizer::getText('fields marked with the symbol'));?> <label style="float: none; width: 100%; display: inline; height: 20px; font-size: 1.2em;" class="title required">&nbsp;</label> <?=Localizer::getText('are mandatory');?>.
</div><br />
<div class="objectForm">
<form method="post" action="<?=$r->getCurrentActionUrl(true, array('form', 'submit'));?>"> 
	<? foreach ($this->values['editFields'] as $title => $details): ?>
	<div class='Field'>
		<? if (!empty($details['required'])): ?>
			<label for="<?=$details['column'];?>" class='title required'><?=$title;?>:</label>
		<? else: ?>
			<label for="<?=$details['column'];?>" class='title'><?=$title;?>:</label>
		<? endif; ?>
		
		<? if ($details['inputType'] == 'text'): 
			if (!empty($details['displayValue'])) {
				$value = $details['displayValue'];
			}
			elseif (!empty($details['currentValue'])) {
				$value = $details['currentValue'];
			}
			elseif (!empty($details['defaultValue'])) {
				$value = $details['defaultValue'];
			}
			else {
				$value = '';
			}
			?>
			<input type="text" id="<?=$details['column'];?>" name="form[<?=$details['column'];?>]" value="<?=$value;?>" />
		
		<? elseif ($details['inputType'] == 'password'): ?>
			<input type="password" id="<?=$details['column'];?>" name="form[<?=$details['column'];?>]" />
		
		<? elseif ($details['inputType'] == 'select'): 
			if (!empty($details['currentValue'])) {
				$value = $details['currentValue'];
			}
			elseif (!empty($details['defaultValue'])) {
				$value = $details['defaultValue'];
			}
			else {
				$value = '';
			}
			?>
			<select id="<?=$details['column'];?>" name="form[<?=$details['column'];?>]">
			<? foreach ($details['selectOptions'] as $optionId => $description): ?>
				<? if ($optionId == $value): ?>
					<option selected="selected" value="<?=$optionId;?>"><?=$description;?></option>
				<? else: ?>
					<option value="<?=$optionId;?>"><?=$description;?></option>
				<? endif; ?>
			<? endforeach; ?>
			</select>
			
		<? elseif ($details['inputType'] == 'checkbox'): 
			if (!empty($details['currentValue'])) {
				$checked = 'checked="checked"';
			}
			elseif (!empty($details['defaultValue'])) {
				$checked = 'checked="checked"';
			}
			else {
				$checked = '';
			}
			?>
		
			<input type="checkbox" id="<?=$details['column'];?>" name="form[<?=$details['column'];?>]" <?=$checked;?>/>
		<? elseif ($details['inputType'] == 'show'): ?>
			<div><?=$details['currentValue'];?></div>
		<? endif; ?>
	</div>
	<? endforeach; ?>
	<p>
		<input type="button" class="button" value="<?=ucfirst(Localizer::getText('back'));?>" title="<?=ucfirst(Localizer::getText('back to browser'));?>"
       	       onclick="location='<?=$r->getActionUrl('BrowseController', $class, false); ?>'" />	
		<input class="button" type="submit" name="submit" value="<?=ucfirst(Localizer::getText('save'));?>">
		<? if ($browserConfig['allowView'] && $object->getPrimaryKey()): ?>
			<input type="button" class="button" value="<?=ucfirst(Localizer::getText('view'));?>" title="<?=ucfirst(Localizer::getText('view'));?>"
       	       onclick="location='<?=$r->getActionUrl('ViewController', $class, false); ?>?id=<?=$object->getPrimaryKey();?>'" />
		<? endif; ?>
		<? if ($browserConfig['allowDelete'] && $object->getPrimaryKey()): ?>
			<input type="button" class="button" value="<?=ucfirst(Localizer::getText('delete'));?>" title="<?=ucfirst(Localizer::getText('delete'));?>"
       	       onclick="location='<?=$r->getActionUrl('EditController', $class, false); ?>?delete=<?=$object->getPrimaryKey();?>&id=<?=$object->getPrimaryKey();?>'" />
		<? endif; ?>
	</p>
</form>
</div>