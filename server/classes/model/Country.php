<?php



/**
 * Skeleton subclass for representing a row from the 'country' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Country extends BaseCountry {
	
	public static function getBrowserConfig() {
		return array(
				'allowView' => true,
				'allowEdit' => true,
				'allowCreate' => true,
				'allowDelete' => true,
		);
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'isocode',
				'label' => ucfirst(Localizer::getText('ISO code')),
			),
			array(
				'column' => 'countryname',
				'label' => ucfirst(Localizer::getText('name')),
			),
		);
	}
	
	public static function getCrudMap() {
		return array(
			array(
				'column' => 'isocode',
				'label' => ucfirst(Localizer::getText('ISO code')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'countryname',
				'label' => ucfirst(Localizer::getText('country name')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
	
} // Country
