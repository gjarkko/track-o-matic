<?

class Registry {
	
	public static $registry;
	
	private function __construct() { }
	
	public static function get($key) {
		if(!isset(self::$registry)) {
			self::$registry = array();
		}
		
		if (isset(self::$registry[$key])) {
			return self::$registry[$key];
		}
		else {
			return NULL;
		}
	}
	
	public static function set($key, $value) {
		if(!isset(self::$registry)) {
			self::$registry = array();
		}
		
		self::$registry[$key] = $value;
	}
}