<?
abstract class XmlController extends Controller {

	public function __construct() {		
		parent::__construct();
		
		$this->view = View::create(View::VIEW_TYPE_XML)
			->addInclude('default.css')
			->addInclude('jquery-1.6.4.min.js');
		
		// If not logged in (Session->user doesn't exist), redirect to login action
		$currentControllerName = $this->requestHandler->getCurrentControllerName();
		$currentActionName = $this->requestHandler->getCurrentActionName();
		$loginControllerName = $this->requestHandler->getControllerClass(RequestHandler::LOGIN_CONTROLLER);
		$loginActionName = $this->requestHandler->getActionMethod($loginControllerName, RequestHandler::LOGIN_ACTION);
		
		$user = Session::getInstance(true)->getUser();
		if(
			$user == NULL 
			&& ($currentControllerName != $loginControllerName || $currentActionName != $loginActionName)
			&& !in_array($currentControllerName . '::' . lcfirst($currentActionName), $this->publicActionNames)
		) {
			
			$this->requestHandler->addAction(RequestHandler::LOGIN_CONTROLLER, RequestHandler::LOGIN_ACTION);
			$this->requestHandler->setContinueExecution(false);
			return;
		} 
		
		// If logged in as a an administrator, show administrative menu
		if (
			$user != NULL 
			&& $user->getUsertypeid() <= Controller::USERTYPE_ADMIN
		) {
			$this->view
				//->addMenuLink(new MenuLink('main', 'index', ucfirst(Localizer::getText('home'))))
				->addMenuLink('browse', 'customer', ucfirst(Localizer::getText('customers')))
				->addMenuLink('browse', 'user', ucfirst(Localizer::getText('users')))
				->addMenuLink('browse', 'vehicle', ucfirst(Localizer::getText('vehicles')))
				->addMenuLink('browse', 'statustype', ucfirst(Localizer::getText('status types')))
				->addMenuLink('browse', 'detailtype', ucfirst(Localizer::getText('detail types')))				
				->addMenuLink('browse', 'language', ucfirst(Localizer::getText('languages')))
				->addMenuLink('browse', 'localization', ucfirst(Localizer::getText('translations')))
				->addMenuLink('browse', 'country', ucfirst(Localizer::getText('countries')))
				//->addMenuLink('browse', 'notificationtype', ucfirst(Localizer::getText('notification types')))
				->addMenuLink(new MenuLink('main', 'logout', ucfirst(Localizer::getText('log out'))));
		} 
		
		// If logged in as a a user, show administrative menu
		elseif (
			$user != NULL 
			&& $user->getUsertypeid() <= Controller::USERTYPE_USER
		) {
			$this->view
				->addMenuLink(new MenuLink('main', 'index', ucfirst(Localizer::getText('home'))))
				->addMenuLink('browse', 'address', ucfirst(Localizer::getText('addresses')))
				->addMenuLink('browse', 'trackable', ucfirst(Localizer::getText('trackables')))
				->addMenuLink('browse', 'groupreference', ucfirst(Localizer::getText('group references')))
				->addMenuLink('browse', 'trip', ucfirst(Localizer::getText('trips')))
				->addMenuLink('browse', 'triptrackable', ucfirst(Localizer::getText('trackables on trip')))
				->addMenuLink('browse', 'tripvehicle', ucfirst(Localizer::getText('trips on vehicle')))
				->addMenuLink('browse', 'detail', ucfirst(Localizer::getText('details')))
				->addMenuLink('browse', 'vehiclestatus', ucfirst(Localizer::getText('statuses')))
				->addMenuLink('browse', 'notification', ucfirst(Localizer::getText('notifications')))
				->addMenuLink(new MenuLink('main', 'logout', ucfirst(Localizer::getText('log out'))));
		} 
		else {
			$this->view
				->addMenuLink(new MenuLink('main', 'login', ucfirst(Localizer::getText('log in'))));
		}
		
	}
	
	public function unsupportedactionAction() {
		$this->view
			->setViewName('error')
			->setValue('error', ucfirst(Localizer::getText('the requested function is not available')) . '.');
	}
	public function unauthorizedAction() {
		$this->view
			->setViewName('error')
			->setValue('error', ucfirst(Localizer::getText('insufficient authority for this function')) . '.');
	}
	public function unknownobjectAction() {
		$this->view
			->setViewName('error')
			->setValue('error', ucfirst(Localizer::getText('unknown object reference')) . '.');
	}
	public function undefinedobjectAction() {
		$this->view
			->setViewName('error')
			->setValue('error', ucfirst(Localizer::getText('undefined object type')) . '.');
	}
}