<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="/tom/css/default.css" />	<script type="text/javascript" src="/tom/js/jquery-1.6.4.min.js"></script>	<title></title>
</head>
<body class="gradient">
	<div id="content">
	
		<div class="logo">
	    	<img src="/tom/image/trackomatic.png" alt="Trackomatic" title="Trackomatic" />
	    </div>
	    <div class="version">
	    	Prototype version 0.1
	    </div>
	    <div id="userdetails">
	    	    Hello Test Customer	    		</div>
		<div id="topmenu">
			<ul>
					<li class="active"><a href="/tom/main/index">Home</a></li>
						<li><a href="/tom/browse/address">Addresses</a></li>	
						<li><a href="/tom/browse/trackable">Trackables</a></li>	
						<li><a href="/tom/browse/groupreference">Group references</a></li>	
						<li><a href="/tom/browse/trip">Trips</a></li>	
						<li><a href="/tom/browse/triptrackable">Trackables on trip</a></li>	
						<li><a href="/tom/browse/tripvehicle">Trips on vehicle</a></li>	
						<li><a href="/tom/browse/detail">Details</a></li>	
						<li><a href="/tom/browse/vehiclestatus">Statuses</a></li>	
						<li><a href="/tom/browse/notification">Notifications</a></li>	
						<li><a href="/tom/main/logout">Log out</a></li>	
	
			</ul>
		</div>
		<div id="main">
<script type="text/javascript">
	var apikey = 999;
	var overlays = [];
	var lat = 60.309862969682;
	var lng = 22.321831993315;
	var latlng = null;
	var mapOptions = null;
	var map = null;
	var delay = 1000;
	var timeout = 0;
	var userIsActive = 0;
	
	function refresh() {
		while(overlays[0]) {
		    overlays.pop().setMap(null);
		}
		$.ajax({
		  url: '/tom/api/getVehicles?apikey=' + apikey,
		  dataType: 'json',
		  success: function(response) {
		   	for(var i = 0;i < response['vehicles'].length;i++) {
		   		marker = new google.maps.Marker({
		              position: new google.maps.LatLng(response['vehicles'][i]['lat'], response['vehicles'][i]['lng']),
		              map: map,
		              title: response['vehicles'][i]['reference']
		       });
		       overlays.push(marker);
		   	}
			var _lat = 0.0;
			var _lng = 0.0;
			var zoom = 3;
			for(var i = 0;i < response['vehicles'].length;i++) {
				_lat = _lat + (1 * response['vehicles'][i]['lat']);
				_lng = _lng + (1 * response['vehicles'][i]['lng']);
			}
			if(response['vehicles'].length > 0) {
				_lat = _lat / response['vehicles'].length;
				_lng = _lng / response['vehicles'].length;
				zoom = 6;
			} else {
				_lat = 60.449249;
				_lng = 22.259239;
			} 
			
			if(_lat != lat || _lng != lng) {
				if (!userIsActive) {
					map.setZoom(zoom);
					map.setCenter(latlng);
					userIsActive = 0;
				}
			}

			timeout = setTimeout("refresh()", delay);
		  },
		  error:function (xhr, ajaxOptions, thrownError){
              alert(xhr.status + ' ' + thrownError);
              clearTimeout(timeout);
          }
		});
	}
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false">
</script>
Latest known locations of vehicles on the move (the map refreshes automatically in 1 seconds)<div id="map_canvas" class="map"></div>
<script type="text/javascript">

$(document).ready(function() {
	overlays = [];
	lat = 60.309862969682;
	lng = 22.321831993315;
	latlng = new google.maps.LatLng(lat, lng);
	mapOptions = {zoom: 6, center: latlng, mapTypeId: google.maps.MapTypeId.TERRAIN};
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

	google.maps.event.addListener(map, 'zoom_changed', function() {
		userIsActive = 1;
	  });
	  
	map.setCenter(latlng);
	while(overlays[0]) {
	    overlays.pop().setMap(null);
	}	
	       marker = new google.maps.Marker({
              position: new google.maps.LatLng(60.309862969682, 22.321831993315),
              map: map,
              title: 'MKVEHICLE1'
       });
       overlays.push(marker);
        timeout = setTimeout("refresh()", delay);
});
</script>
		</div>
	</div>
</div>
</body>
</html>