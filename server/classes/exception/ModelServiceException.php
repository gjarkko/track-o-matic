<?

class ModelServiceException extends Exception {
	
	private $httpCode;
	
	public function setHttpCode($code) {
		$this->httpCode = $code;
	}
	
	public function getHttpCode() {
		if (!isset($this->httpCode)) {
			$this->httpCode = 200;
		}
		return $this->httpCode;
	}
}