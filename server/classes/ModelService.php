<?

class ModelService {
	
public static function getCountryByIsocode($isocode) {
		$country = CountryQuery::create()->findOneByIsocode($isocode);
		if (empty($country)) {
			$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("country '%s' not found")), $isocode));
			$e->setHttpCode(412);
			throw $e;
		}
		return $country;
	}
	
	public static function getObjectByConstraints($objectType, $constraint, $value) {
		$objects = self::getObjectsByConstraints($objectType, $constraint, $value);
		if (is_array($objects) && isset($objects[0])) {
			return $objects[0];
		}
	}
	
	public static function getObjectsByConstraints($objectType, $constraints, $params) {
		$queryClass = ucfirst($objectType) . 'Query';
		$query = $queryClass::create();
		foreach ($constraints as $constraint) {
			$methodName = 'filterBy' . ucfirst($constraint['constraint']);
			if (isset($constraint['constraintEqualsParam'])) {
				$value = self::getParamObject($constraint['constraintEqualsParam'], $params);
				
				/**
				 *  KLUDGE AHEAD
				 *  Does not support scenarios with more than one multi-valued constraint.
				 *  Does not recognize *which* referenced object was missing.
				 */ 
				if (is_array($value)) {
					$expectedCount = count($value);
				}
				else {
					$expectedCount = 1;
				}
				$query->$methodName($value);
			}
			elseif (isset($constraint['constraintEqualsValue'])) {
				$query->$methodName($constraint['constraintEqualsValue']);
			}
		}
		
		$objects = (array)$query->find();
		if (count($objects) == 0 || (isset($expectedCount) && count($objects) < $expectedCount)) {
			$e = new ModelServiceException(sprintf(ucfirst(sprintf(Localizer::getText("one or more references of %s not valid"), $objectType)), $value));
			$e->setHttpCode(412);
			throw $e;
		}
				
		return $objects;
	}
	
	public static function getObjectByParams($className, $params, $paramMap, $createOnly = false, &$valid = NULL, $save = true) {
		$objects = self::getObjectsByParams($className, $params, $paramMap, $createOnly, $valid, $save);
		if (is_array($objects) && isset($objects[0])) {
			return $objects[0];
		}
	}
	
	
	public static function getParamObject($key, $params) {
		// Detect & decode JSON objects to arrays
		$paramValueObject = $params[strtolower($key)];
		if (preg_match("/^[\s]*[\{\[\"].*[\}\]\"][\s]*$/", $paramValueObject)) {
			$paramValueObject = json_decode($paramValueObject);
			if (is_object($paramValueObject)) {
				$paramValueObject = (array) $paramValueObject;
			}
		}
		return $paramValueObject;
	}
	
	public static function hasParam($key, $params) {
		return isset($params[strtolower($key)]);
	}
	
	public static function multiplyValueSets($valueSets, $key, $paramValueObject) {
		if (is_array($paramValueObject)) {
			$newValueSets = array();		
			// Multiply the size of valuesets by the size of the param array
			foreach ($valueSets as $values) {
				foreach ($paramValueObject as $pvo) {
					$newValueSets[] = $values;
				}
			}
			$valueSets = $newValueSets;
			
			// Assign each new valueset the new values
			foreach ($valueSets as $i => $values) {
				$valueSets[$i][$key] = $paramValueObject[$i%count($paramValueObject)];
			}
		}
		else {
			foreach ($valueSets as $i => $values) {
				$valueSets[$i][$key] = $paramValueObject;
			}	
		}
		return $valueSets;
	}
	
	public static function getObjectsByParams($className, $params, $paramMap, $createOnly = false, &$valid = NULL, $save = true) {
	
		// Populate arrays of values to define new objects
		$valueSets = array();
		
		$valueSets[0] = array();
		foreach ($paramMap as $key => $paramInfo) {
			if (!empty($paramInfo['value'])) {
				
				// Split value set to multiple objects
				if (!empty($paramInfo['multiple'])) {
					$valueSets = self::multiplyValueSets($valueSets, $key, $paramInfo['value']);
				}
				else {
					if (is_array($paramInfo['value'])) {
						$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('parameter %s can not be an object or array'), $key)));
						$mse->setHttpCode(412);
						throw $mse;
					}
					foreach ($valueSets as $i => $values) {
						$valueSets[$i][$key] = $paramInfo['value'];
					}
				}
				
			}
			elseif (!empty($paramInfo['param'])) {
				if (empty($paramInfo['optional'])) {
					if (!self::hasParam($paramInfo['param'], $params)) {
						
						$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), $paramInfo['param'])));
						$mse->setHttpCode(412);
						throw $mse;
					}
				}
				elseif (!self::hasParam($paramInfo['param'], $params)) {
					continue;
				}
				
				$paramValueObject = self::getParamObject($paramInfo['param'], $params);
				
				// Split value set to multiple objects
				if (!empty($paramInfo['multiple'])) {
					$valueSets = self::multiplyValueSets($valueSets, $key, $paramValueObject);
				}
				else {
					if (is_array($paramValueObject)) {
						$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('parameter %s can not be an object or array'), $paramInfo['param'])));
						$mse->setHttpCode(412);
						throw $mse;						
					}
					foreach ($valueSets as $i => $values) {
						$valueSets[$i][$key] = $paramValueObject;
					}	
				}
			}
			elseif (
				!empty($paramInfo['objectType']) 
				&& !empty($paramInfo['constraints']) 
			) {
				
				if (empty($paramInfo['optional'])) {
					
					foreach ($paramInfo['constraints'] as $constraint) {
						if (isset($constraint['constraintEqualsParam']) && !self::hasParam($constraint['constraintEqualsParam'], $params)) {
							$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), $constraint['constraintEqualsParam'])));
							$mse->setHttpCode(412);
							throw $mse;
						}
					}
				}
				
				// Split value set to multiple objects
				if (!empty($paramInfo['multiple'])) {
					
					$objects = ModelService::getObjectsByConstraints($paramInfo['objectType'], $paramInfo['constraints'], $params);
					$objectkeys = array();
					foreach ($objects as $object) {
						$objectkeys[] = $object->getPrimaryKey();
					}
	
					$valueSets = self::multiplyValueSets($valueSets, $key, $objectkeys);
					
				}
				else {
					
					$object = ModelService::getObjectByConstraints($paramInfo['objectType'], $paramInfo['constraints'], $params);
					if (is_array($object)) {
						$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('parameter %s can not be an object or array'), $key)));
						$mse->setHttpCode(412);
						throw $mse;
					}
					
					foreach ($valueSets as $i => $values) {
						$valueSets[$i][$key] = $object->getPrimaryKey();
					}
				}
				
			}
		}
		
		// Create objects
		$objects = array();
		$newObjects = array();
		$valid = true;
		
		foreach ($valueSets as $i => $values) {
			if(!$createOnly) { 
				$queryClassName = $className . 'Query';
				$objects[$i] = $queryClassName::create()->findOneByArray($values);
			}
			
			if(empty($objects[$i])) {
				$objects[$i] = new $className();
				
				$objects[$i]->fromArray($values);
				$newObjects[] = $i;
				$valid = $valid && $objects[$i]->validate();
				
			}
		}
		
		if ($valid) {
			foreach ($newObjects as $i) {
				try {
					if ($save) {
						$objects[$i]->save();
					}
				} catch (Exception $e) {
					/**
					 * @todo Log exception
					 */
					$mse = new ModelServiceException(ucfirst(Localizer::getText('internal service error')) . ' - ' . $e->getMessage());
					$mse->setHttpCode(500);
					throw $mse;
				}
			}
		}
		
		return $objects;
	}
	
}