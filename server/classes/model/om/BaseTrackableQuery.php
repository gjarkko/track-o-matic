<?php


/**
 * Base class that represents a query for the 'trackable' table.
 *
 * 
 *
 * @method     TrackableQuery orderByTrackableid($order = Criteria::ASC) Order by the trackableid column
 * @method     TrackableQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     TrackableQuery orderByStartAddressid($order = Criteria::ASC) Order by the start_addressid column
 * @method     TrackableQuery orderByEndAddressid($order = Criteria::ASC) Order by the end_addressid column
 * @method     TrackableQuery orderByTrackablereference($order = Criteria::ASC) Order by the trackablereference column
 * @method     TrackableQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method     TrackableQuery orderByEdited($order = Criteria::ASC) Order by the edited column
 * @method     TrackableQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     TrackableQuery groupByTrackableid() Group by the trackableid column
 * @method     TrackableQuery groupByCustomerid() Group by the customerid column
 * @method     TrackableQuery groupByStartAddressid() Group by the start_addressid column
 * @method     TrackableQuery groupByEndAddressid() Group by the end_addressid column
 * @method     TrackableQuery groupByTrackablereference() Group by the trackablereference column
 * @method     TrackableQuery groupByCreated() Group by the created column
 * @method     TrackableQuery groupByEdited() Group by the edited column
 * @method     TrackableQuery groupByValid() Group by the valid column
 *
 * @method     TrackableQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TrackableQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TrackableQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TrackableQuery leftJoinAddressRelatedByStartAddressid($relationAlias = null) Adds a LEFT JOIN clause to the query using the AddressRelatedByStartAddressid relation
 * @method     TrackableQuery rightJoinAddressRelatedByStartAddressid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AddressRelatedByStartAddressid relation
 * @method     TrackableQuery innerJoinAddressRelatedByStartAddressid($relationAlias = null) Adds a INNER JOIN clause to the query using the AddressRelatedByStartAddressid relation
 *
 * @method     TrackableQuery leftJoinAddressRelatedByEndAddressid($relationAlias = null) Adds a LEFT JOIN clause to the query using the AddressRelatedByEndAddressid relation
 * @method     TrackableQuery rightJoinAddressRelatedByEndAddressid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AddressRelatedByEndAddressid relation
 * @method     TrackableQuery innerJoinAddressRelatedByEndAddressid($relationAlias = null) Adds a INNER JOIN clause to the query using the AddressRelatedByEndAddressid relation
 *
 * @method     TrackableQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     TrackableQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     TrackableQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     TrackableQuery leftJoinDetail($relationAlias = null) Adds a LEFT JOIN clause to the query using the Detail relation
 * @method     TrackableQuery rightJoinDetail($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Detail relation
 * @method     TrackableQuery innerJoinDetail($relationAlias = null) Adds a INNER JOIN clause to the query using the Detail relation
 *
 * @method     TrackableQuery leftJoinGroupreference($relationAlias = null) Adds a LEFT JOIN clause to the query using the Groupreference relation
 * @method     TrackableQuery rightJoinGroupreference($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Groupreference relation
 * @method     TrackableQuery innerJoinGroupreference($relationAlias = null) Adds a INNER JOIN clause to the query using the Groupreference relation
 *
 * @method     TrackableQuery leftJoinNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the Notification relation
 * @method     TrackableQuery rightJoinNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Notification relation
 * @method     TrackableQuery innerJoinNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the Notification relation
 *
 * @method     TrackableQuery leftJoinTrackablestatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the Trackablestatus relation
 * @method     TrackableQuery rightJoinTrackablestatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Trackablestatus relation
 * @method     TrackableQuery innerJoinTrackablestatus($relationAlias = null) Adds a INNER JOIN clause to the query using the Trackablestatus relation
 *
 * @method     TrackableQuery leftJoinTriptrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Triptrackable relation
 * @method     TrackableQuery rightJoinTriptrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Triptrackable relation
 * @method     TrackableQuery innerJoinTriptrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Triptrackable relation
 *
 * @method     Trackable findOne(PropelPDO $con = null) Return the first Trackable matching the query
 * @method     Trackable findOneOrCreate(PropelPDO $con = null) Return the first Trackable matching the query, or a new Trackable object populated from the query conditions when no match is found
 *
 * @method     Trackable findOneByTrackableid(int $trackableid) Return the first Trackable filtered by the trackableid column
 * @method     Trackable findOneByCustomerid(int $customerid) Return the first Trackable filtered by the customerid column
 * @method     Trackable findOneByStartAddressid(int $start_addressid) Return the first Trackable filtered by the start_addressid column
 * @method     Trackable findOneByEndAddressid(int $end_addressid) Return the first Trackable filtered by the end_addressid column
 * @method     Trackable findOneByTrackablereference(string $trackablereference) Return the first Trackable filtered by the trackablereference column
 * @method     Trackable findOneByCreated(string $created) Return the first Trackable filtered by the created column
 * @method     Trackable findOneByEdited(string $edited) Return the first Trackable filtered by the edited column
 * @method     Trackable findOneByValid(boolean $valid) Return the first Trackable filtered by the valid column
 *
 * @method     array findByTrackableid(int $trackableid) Return Trackable objects filtered by the trackableid column
 * @method     array findByCustomerid(int $customerid) Return Trackable objects filtered by the customerid column
 * @method     array findByStartAddressid(int $start_addressid) Return Trackable objects filtered by the start_addressid column
 * @method     array findByEndAddressid(int $end_addressid) Return Trackable objects filtered by the end_addressid column
 * @method     array findByTrackablereference(string $trackablereference) Return Trackable objects filtered by the trackablereference column
 * @method     array findByCreated(string $created) Return Trackable objects filtered by the created column
 * @method     array findByEdited(string $edited) Return Trackable objects filtered by the edited column
 * @method     array findByValid(boolean $valid) Return Trackable objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTrackableQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseTrackableQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Trackable', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TrackableQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TrackableQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TrackableQuery) {
			return $criteria;
		}
		$query = new TrackableQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Trackable|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = TrackablePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(TrackablePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trackable A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `TRACKABLEID`, `CUSTOMERID`, `START_ADDRESSID`, `END_ADDRESSID`, `TRACKABLEREFERENCE`, `CREATED`, `EDITED`, `VALID` FROM `trackable` WHERE `TRACKABLEID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Trackable();
			$obj->hydrate($row);
			TrackablePeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trackable|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TrackablePeer::TRACKABLEID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TrackablePeer::TRACKABLEID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the trackableid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackableid(1234); // WHERE trackableid = 1234
	 * $query->filterByTrackableid(array(12, 34)); // WHERE trackableid IN (12, 34)
	 * $query->filterByTrackableid(array('min' => 12)); // WHERE trackableid > 12
	 * </code>
	 *
	 * @param     mixed $trackableid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByTrackableid($trackableid = null, $comparison = null)
	{
		if (is_array($trackableid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TrackablePeer::TRACKABLEID, $trackableid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(TrackablePeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(TrackablePeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablePeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the start_addressid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByStartAddressid(1234); // WHERE start_addressid = 1234
	 * $query->filterByStartAddressid(array(12, 34)); // WHERE start_addressid IN (12, 34)
	 * $query->filterByStartAddressid(array('min' => 12)); // WHERE start_addressid > 12
	 * </code>
	 *
	 * @see       filterByAddressRelatedByStartAddressid()
	 *
	 * @param     mixed $startAddressid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByStartAddressid($startAddressid = null, $comparison = null)
	{
		if (is_array($startAddressid)) {
			$useMinMax = false;
			if (isset($startAddressid['min'])) {
				$this->addUsingAlias(TrackablePeer::START_ADDRESSID, $startAddressid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($startAddressid['max'])) {
				$this->addUsingAlias(TrackablePeer::START_ADDRESSID, $startAddressid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablePeer::START_ADDRESSID, $startAddressid, $comparison);
	}

	/**
	 * Filter the query on the end_addressid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByEndAddressid(1234); // WHERE end_addressid = 1234
	 * $query->filterByEndAddressid(array(12, 34)); // WHERE end_addressid IN (12, 34)
	 * $query->filterByEndAddressid(array('min' => 12)); // WHERE end_addressid > 12
	 * </code>
	 *
	 * @see       filterByAddressRelatedByEndAddressid()
	 *
	 * @param     mixed $endAddressid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByEndAddressid($endAddressid = null, $comparison = null)
	{
		if (is_array($endAddressid)) {
			$useMinMax = false;
			if (isset($endAddressid['min'])) {
				$this->addUsingAlias(TrackablePeer::END_ADDRESSID, $endAddressid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($endAddressid['max'])) {
				$this->addUsingAlias(TrackablePeer::END_ADDRESSID, $endAddressid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablePeer::END_ADDRESSID, $endAddressid, $comparison);
	}

	/**
	 * Filter the query on the trackablereference column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTrackablereference('fooValue');   // WHERE trackablereference = 'fooValue'
	 * $query->filterByTrackablereference('%fooValue%'); // WHERE trackablereference LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $trackablereference The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByTrackablereference($trackablereference = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($trackablereference)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $trackablereference)) {
				$trackablereference = str_replace('*', '%', $trackablereference);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TrackablePeer::TRACKABLEREFERENCE, $trackablereference, $comparison);
	}

	/**
	 * Filter the query on the created column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
	 * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
	 * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $created The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByCreated($created = null, $comparison = null)
	{
		if (is_array($created)) {
			$useMinMax = false;
			if (isset($created['min'])) {
				$this->addUsingAlias(TrackablePeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($created['max'])) {
				$this->addUsingAlias(TrackablePeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablePeer::CREATED, $created, $comparison);
	}

	/**
	 * Filter the query on the edited column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByEdited('2011-03-14'); // WHERE edited = '2011-03-14'
	 * $query->filterByEdited('now'); // WHERE edited = '2011-03-14'
	 * $query->filterByEdited(array('max' => 'yesterday')); // WHERE edited > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $edited The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByEdited($edited = null, $comparison = null)
	{
		if (is_array($edited)) {
			$useMinMax = false;
			if (isset($edited['min'])) {
				$this->addUsingAlias(TrackablePeer::EDITED, $edited['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($edited['max'])) {
				$this->addUsingAlias(TrackablePeer::EDITED, $edited['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TrackablePeer::EDITED, $edited, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(TrackablePeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Address object
	 *
	 * @param     Address|PropelCollection $address The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByAddressRelatedByStartAddressid($address, $comparison = null)
	{
		if ($address instanceof Address) {
			return $this
				->addUsingAlias(TrackablePeer::START_ADDRESSID, $address->getAddressid(), $comparison);
		} elseif ($address instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TrackablePeer::START_ADDRESSID, $address->toKeyValue('PrimaryKey', 'Addressid'), $comparison);
		} else {
			throw new PropelException('filterByAddressRelatedByStartAddressid() only accepts arguments of type Address or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the AddressRelatedByStartAddressid relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinAddressRelatedByStartAddressid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('AddressRelatedByStartAddressid');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'AddressRelatedByStartAddressid');
		}

		return $this;
	}

	/**
	 * Use the AddressRelatedByStartAddressid relation Address object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery A secondary query class using the current class as primary query
	 */
	public function useAddressRelatedByStartAddressidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAddressRelatedByStartAddressid($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'AddressRelatedByStartAddressid', 'AddressQuery');
	}

	/**
	 * Filter the query by a related Address object
	 *
	 * @param     Address|PropelCollection $address The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByAddressRelatedByEndAddressid($address, $comparison = null)
	{
		if ($address instanceof Address) {
			return $this
				->addUsingAlias(TrackablePeer::END_ADDRESSID, $address->getAddressid(), $comparison);
		} elseif ($address instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TrackablePeer::END_ADDRESSID, $address->toKeyValue('PrimaryKey', 'Addressid'), $comparison);
		} else {
			throw new PropelException('filterByAddressRelatedByEndAddressid() only accepts arguments of type Address or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the AddressRelatedByEndAddressid relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinAddressRelatedByEndAddressid($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('AddressRelatedByEndAddressid');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'AddressRelatedByEndAddressid');
		}

		return $this;
	}

	/**
	 * Use the AddressRelatedByEndAddressid relation Address object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery A secondary query class using the current class as primary query
	 */
	public function useAddressRelatedByEndAddressidQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAddressRelatedByEndAddressid($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'AddressRelatedByEndAddressid', 'AddressQuery');
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(TrackablePeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TrackablePeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Filter the query by a related Detail object
	 *
	 * @param     Detail $detail  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByDetail($detail, $comparison = null)
	{
		if ($detail instanceof Detail) {
			return $this
				->addUsingAlias(TrackablePeer::TRACKABLEID, $detail->getTrackableid(), $comparison);
		} elseif ($detail instanceof PropelCollection) {
			return $this
				->useDetailQuery()
				->filterByPrimaryKeys($detail->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByDetail() only accepts arguments of type Detail or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Detail relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinDetail($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Detail');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Detail');
		}

		return $this;
	}

	/**
	 * Use the Detail relation Detail object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DetailQuery A secondary query class using the current class as primary query
	 */
	public function useDetailQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDetail($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Detail', 'DetailQuery');
	}

	/**
	 * Filter the query by a related Groupreference object
	 *
	 * @param     Groupreference $groupreference  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByGroupreference($groupreference, $comparison = null)
	{
		if ($groupreference instanceof Groupreference) {
			return $this
				->addUsingAlias(TrackablePeer::TRACKABLEID, $groupreference->getTrackableid(), $comparison);
		} elseif ($groupreference instanceof PropelCollection) {
			return $this
				->useGroupreferenceQuery()
				->filterByPrimaryKeys($groupreference->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByGroupreference() only accepts arguments of type Groupreference or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Groupreference relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinGroupreference($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Groupreference');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Groupreference');
		}

		return $this;
	}

	/**
	 * Use the Groupreference relation Groupreference object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    GroupreferenceQuery A secondary query class using the current class as primary query
	 */
	public function useGroupreferenceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinGroupreference($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Groupreference', 'GroupreferenceQuery');
	}

	/**
	 * Filter the query by a related Notification object
	 *
	 * @param     Notification $notification  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByNotification($notification, $comparison = null)
	{
		if ($notification instanceof Notification) {
			return $this
				->addUsingAlias(TrackablePeer::TRACKABLEID, $notification->getTrackableid(), $comparison);
		} elseif ($notification instanceof PropelCollection) {
			return $this
				->useNotificationQuery()
				->filterByPrimaryKeys($notification->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByNotification() only accepts arguments of type Notification or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Notification relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinNotification($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Notification');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Notification');
		}

		return $this;
	}

	/**
	 * Use the Notification relation Notification object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    NotificationQuery A secondary query class using the current class as primary query
	 */
	public function useNotificationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinNotification($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Notification', 'NotificationQuery');
	}

	/**
	 * Filter the query by a related Trackablestatus object
	 *
	 * @param     Trackablestatus $trackablestatus  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByTrackablestatus($trackablestatus, $comparison = null)
	{
		if ($trackablestatus instanceof Trackablestatus) {
			return $this
				->addUsingAlias(TrackablePeer::TRACKABLEID, $trackablestatus->getTrackableid(), $comparison);
		} elseif ($trackablestatus instanceof PropelCollection) {
			return $this
				->useTrackablestatusQuery()
				->filterByPrimaryKeys($trackablestatus->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTrackablestatus() only accepts arguments of type Trackablestatus or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Trackablestatus relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinTrackablestatus($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Trackablestatus');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Trackablestatus');
		}

		return $this;
	}

	/**
	 * Use the Trackablestatus relation Trackablestatus object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackablestatusQuery A secondary query class using the current class as primary query
	 */
	public function useTrackablestatusQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTrackablestatus($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Trackablestatus', 'TrackablestatusQuery');
	}

	/**
	 * Filter the query by a related Triptrackable object
	 *
	 * @param     Triptrackable $triptrackable  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function filterByTriptrackable($triptrackable, $comparison = null)
	{
		if ($triptrackable instanceof Triptrackable) {
			return $this
				->addUsingAlias(TrackablePeer::TRACKABLEID, $triptrackable->getTrackableid(), $comparison);
		} elseif ($triptrackable instanceof PropelCollection) {
			return $this
				->useTriptrackableQuery()
				->filterByPrimaryKeys($triptrackable->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTriptrackable() only accepts arguments of type Triptrackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Triptrackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function joinTriptrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Triptrackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Triptrackable');
		}

		return $this;
	}

	/**
	 * Use the Triptrackable relation Triptrackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TriptrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTriptrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTriptrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Triptrackable', 'TriptrackableQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Trackable $trackable Object to remove from the list of results
	 *
	 * @return    TrackableQuery The current query, for fluid interface
	 */
	public function prune($trackable = null)
	{
		if ($trackable) {
			$this->addUsingAlias(TrackablePeer::TRACKABLEID, $trackable->getTrackableid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseTrackableQuery