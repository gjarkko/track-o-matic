<?php

/**
 * 
 * Enter description here ...
 * @todo Add methods for API Calls in https://docs.google.com/document/d/1lZPgHNJuOa_dPqqIF4rbxF5OC_Ch7C7Z_lF8NWnaNm0/edit?hl=en_US 
 *
 */

class ApiController extends Controller {
	
	const JSON_VIEWNAME = 'json';
	
	public function __construct() {
		parent::__construct();
		
		$this->view = new View(View::VIEW_TYPE_JSON, self::JSON_VIEWNAME);
		$params = $this->requestHandler->getParams();
		
		$session = Session::getInstance(false);
				
		if (
			in_array($this->requestHandler->getCurrentActionName(), $this->publicActionNames) 
			|| ($this->requestHandler->getParam('apikey') && Authentication::Authenticate($this->requestHandler->getParam('apikey')))
		) {
			$this->requestHandler->setContinueExecution(true);
			$this->view->setMimeType('text/javascript');
			return;
		}
		else {
			$e = new ModelServiceException(ucfirst(Localizer::getText('unknown API key')));
			$e->setHttpCode(401);
			$this->handleModelServiceException($e);
			return;
		}
		
	}
	
	public function indexAction() {
		echo CryptographyProvider::encrypt(uniqid('', true)) . '<br />';
		echo strlen(CryptographyProvider::encrypt(uniqid('', true)))  . '<br />';
		echo CryptographyProvider::decrypt(CryptographyProvider::encrypt(uniqid('', true)));	
	}
	
	private function checkAccess($required) {
		$this->requiredUserType = $required;

		if (!$this->isAuthorized()) {	
			$e = new ModelServiceException(ucfirst(Localizer::getText('insufficient privileges')));
			$e->setHttpCode(401);
			$this->handleModelServiceException($e);
			return false;
		}
		
		return true;
	}
	
	public function locationUpdateAction() {
		if(!$this->checkAccess(Controller::USERTYPE_DRIVER)) 
			return;
		
		$vehicle = VehicleQuery::create()->findPk(Registry::get('vehicleid'));
		
		$statusTypeId = StatustypeQuery::create()
					->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerid())
					->filterByIsmoving(1)
					->filterByValid(1)
					->findOne()
					->getPrimaryKey();
		
		if (!$statusTypeId) {
			$e = new ModelServiceException(ucfirst(Localizer::getText("status type \"en route\" not supported")));
			$e->setHttpCode(417);
			$this->handleModelServiceException($e);
		} 
		
		$vehicleStatusParamMap = array(
			'Statustypeid' => array(
				'value' => $statusTypeId,
			),
			'Vehicleid' => array(
				'value' => $vehicle->getPrimaryKey(),
			),
			'Lat' => array(
				'param' => 'lat',
			),
			'Lng' => array(
				'param' => 'lng',
			),
		);
		
		$trackableStatusParamMaps = array();
		
		foreach ($vehicle->getCurrentTrackables() as $trackable) {
			$trackableStatusParamMaps[] = array(
				'Statustypeid' => array(
					'value' => $statusTypeId,
				),
				'Trackableid' => array(
					'value' => $trackable->getPrimaryKey(),
				),
				'Lat' => array(
					'param' => 'lat',
				),
				'Lng' => array(
					'param' => 'lng',
				),
			);
		}
		
		try {
				
			$valid = true;
			$objects = array(ModelService::getObjectByParams('Vehiclestatus', $this->requestHandler->getParams(), $vehicleStatusParamMap, true, $valid));
			
			foreach ($trackableStatusParamMaps as $trackableStatusParamMap) {
				$objects = array_merge($objects, ModelService::getObjectsByParams('Trackablestatus', $this->requestHandler->getParams(), $trackableStatusParamMap, true, $valid));
			}
			
			foreach ($objects as $object) {
				if (!$valid) {
					$this->validationErrorAction($object);
					return;
				}
			}
			
			if ($valid) {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}	 			
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function registerDropAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_DRIVER)) 
			return;
		
		try {
			$user = Session::getInstance()->getUser();
			$vehicle = VehicleQuery::create()->findPk(Registry::get('vehicleid'));
			/* @var $vehicle Vehicle  */
			$r = $this->requestHandler;
			$tolerance = 100; // Drop tolerance radius in meters
			if (!$r->getParam('reference') && !$r->getParam('groupReference')) {
				
				$dropCandidateTrackables = array();
				$unsetAddresses = 0;
				$currentTrackables = $vehicle->getCurrentTrackables();
								
				if (count($currentTrackables) == 0) {
					$e = new ModelServiceException(ucfirst(Localizer::getText("no trackables on vehicle")));
					$e->setHttpCode(417);
					throw $e;
				}
				
				foreach ($currentTrackables as $trackable) {
					/* @var $trackable Trackable */
					
					// Candidate if no current location supplied
					if(!$r->getParam('lat') || !$r->getParam('lng')) {
						$dropCandidateTrackables[] = $trackable;
						continue;
					}
					
					// Candidate if no end address defined
					if ($trackable->getEndAddressid() == 1) {
						$dropCandidateTrackables[] = $trackable;
						$unsetAddresses++;
						continue;
					}
					
					// Candidate if no end point defined
					$endAddress = $trackable->getAddressRelatedByEndAddressid();
					if (!$endAddress->getLat() || !$endAddress->getLng()) {
						$dropCandidateTrackables[] = $trackable;
						continue;
					}
					
					// Candidate if trackable's end point is within 100 meter radius of the current location
					if (Tools::haversineDistance($endAddress->getLat(), $endAddress->getLng(), $r->getParam('lat'), $r->getParam('lng')) <= $tolerance) {
						$dropCandidateTrackables[] = $trackable;
					}
				}
				
				// No matches - all are candidates
				if (count($dropCandidateTrackables) == 0) {
					$dropCandidateTrackables = $vehicle->getCurrentTrackables(); 
				}
							
				// If all candidates share the end address, they'll all be dropped
				if ($unsetAddresses == 0) {
					$compareAddresses = array();
					$compareAddressCounts = array();
					$compareAddressIndex = 0;
					$j = 0;
					foreach ($dropCandidateTrackables as $trackable) {
						$endAddress = $trackable->getAddressRelatedByEndAddressid();
						$compareAddress = array(
							'address1' => $endAddress->getAddress1(),
							'address2' => $endAddress->getAddress2(),
							'postalcode' => $endAddress->getPostalcode(),
							'city' => $endAddress->getCity(),
							'countryid' => $endAddress->getCountryid(),
						);
						$i = array_search($compareAddress, $compareAddresses);
						if (false !== $i) {
							$compareAddressCounts[$i]++;
						} else {
							$compareAddresses[$j] = $compareAddress;
							$compareAddressCounts[$j] = 1;
							$j++;					
						}
					}
					
					if (count($compareAddresses) == 1) {
						// The candidates share a single non-empty address and can all be dropped at this location.
						$trackables = $dropCandidateTrackables;
					}
				}
							
				
				// If more than one candidate, return list of trackables with address
				if (count($dropCandidateTrackables) == 1) {
					$trackables = $dropCandidateTrackables;
				}
				elseif (empty($trackables)) {
					foreach ($dropCandidateTrackables as $trackable) {
						/* @var $trackable Trackable */
						$reference = $trackable->getTrackablereference();
						$endAddress = $trackable->getAddressRelatedByEndAddressid();
						
						$trackableObject = new stdClass();
						$trackableObject->reference = $trackable->getTrackablereference();
						$trackableObject->name = $endAddress->getName();
						$trackableObject->address1 = $endAddress->getAddress1();
						$trackableObject->address2 = $endAddress->getAddress2();
						$trackableObject->postalcode = $endAddress->getPostalcode();
						$trackableObject->city = $endAddress->getCity();
						$trackableObject->country = $endAddress->getCountry()->getIsocode();
						$trackableList[] = $trackableObject;
						
					}
					$this->view->setHeader(300);
					$this->view->setValue('status', 'multiple candidates');
					$this->view->setValue('dropCandidates', $trackableList);
					return;
				}
					
			} elseif ($r->getParam('reference')) {
				$trackable = TrackableQuery::create()->filterByCustomerid($user->getCustomerid())->findOneByTrackablereference($r->getParam('reference'));
				if (!$trackable) {
					$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'trackable'));
					$e->setHttpCode(412);
					throw $e;
				}
				else {
					if ($trackable->getLastStatus()->getStatustype()->getIsfinal() == 0) {
						$trackables[] = $trackable;
					}
					else {
						$e = new ModelServiceException(ucfirst(Localizer::getText("trackable already delivered")));
						$e->setHttpCode(417);
						throw $e;
					}
				}
			} elseif ($r->getParam('groupReference')) {
				$groupreferences = GroupreferenceQuery::create()->filterByCustomerid($user->getCustomerid())->findByGroupreference($r->getParam('groupreference'));
				if (!$groupreferences) {
					$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'groupreference'));
					$e->setHttpCode(412);
					throw $e;
				}
				else {
					foreach ($groupreferences as $groupreference) {
						$trackable = $groupreference->getTrackable();
						if ($trackable->getLastStatus()->getStatustype()->getIsfinal() == 0) {
							$trackables[] = $trackable;
						}
						else {
							$e = new ModelServiceException(ucfirst(Localizer::getText("one or more of the trackables already delivered")));
							$e->setHttpCode(417);
							throw $e;
						}
					}
				}
			}
			
			$statusTypeId = StatustypeQuery::create()->filterByCustomerid($user->getCustomerid())->findOneByIsfinal(1)->getPrimaryKey();
			
			if (isset($trackables) && count($trackables) > 0) {
				$vehicleStatusParamMap = array(
					'Statustypeid' => array(
						'value' => $statusTypeId,
					),
					'Vehicleid' => array(
						'value' => $vehicle->getPrimaryKey(),
					),
					'Lat' => array(
						'optional' => true,
						'param' => 'lat',
					),
					'Lng' => array(
						'optional' => true,
						'param' => 'lng',
					),
				);
				
				$trackableStatusParamMaps = array();
				foreach ($trackables as $trackable) {
					$trackableStatusParamMaps[] = array(
						'Statustypeid' => array(
							'value' => $statusTypeId,
						),
						'Trackableid' => array(
							'value' => $trackable->getPrimaryKey(),
						),
						'Lat' => array(
							'optional' => true,
							'param' => 'lat',
						),
						'Lng' => array(
							'optional' => true,
							'param' => 'lng',
						),
					);
				}
				
				try {
						
					$valid = true;
					$objects = array(ModelService::getObjectByParams('Vehiclestatus', $this->requestHandler->getParams(), $vehicleStatusParamMap, true, $valid));
					foreach ($trackableStatusParamMaps as $trackableStatusParamMap) {
						$objects = array_merge($objects, ModelService::getObjectsByParams('Trackablestatus', $this->requestHandler->getParams(), $trackableStatusParamMap, true, $valid));
					}
					
					foreach ($objects as $object) {
						if (!$valid) {
							$this->validationErrorAction($object);
							return;
						}
					}
					if ($valid) {
						$this->view->setHeader(201);
						$this->view->setValue('status', 'ok');
					}	 			
				} catch (ModelServiceException $e) {
					$this->handleModelServiceException($e);
					return;
				}
			} else {
				$e = new ModelServiceException(ucfirst(Localizer::getText("nothing to drop")));
				$e->setHttpCode(417);
				throw $e;
			}
		
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function registerGroupAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		$paramMap = array(
			'Customerid' => array(
				'value' => Session::getInstance(false)->getUser()->getCustomerId()
			),
			'Groupreference' => array(
				'param' => 'groupReference',
			),
			'Trackableid' => array(
				'multiple' => true,
				'objectType' => 'Trackable',
				'constraints' => array(
					array( 
						'constraint' => 'trackablereference',
						'constraintEqualsParam' => 'reference',
					),
					array(
						'constraint' => 'customerid',
						'constraintEqualsValue' => Session::getInstance(false)->getUser()->getCustomerId(),						
					),
				),
			),
		);
		try {
			$valid = true;
			$objects = ModelService::getObjectsByParams('Groupreference', $this->requestHandler->getParams(), $paramMap, false, $valid);
		
			if (!$valid) {
				$this->validationErrorAction($objects);
				return;
			}
			else {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function registerTripAction() {
		$paramMap = array(
			'Customerid' => array(
				'value' => Session::getInstance(false)->getUser()->getCustomerId()
			),
			'Tripreference' => array(
				'param' => 'tripReference',
			),
		);
		try {
			$valid = true;
			$objects = ModelService::getObjectsByParams('Trip', $this->requestHandler->getParams(), $paramMap, false, $valid);
		
			if (!$valid) {
				$this->validationErrorAction($objects);
				return;
			}
			else {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function registerTripTrackableAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		$paramMap = array(
			'Tripid' => array(
				'objectType' => 'Trip',
				'constraints' => array(
					array(
						'constraint' => 'tripreference',
						'constraintEqualsParam' => 'tripReference',
					),
					array(
						'constraint' => 'customerid',
						'constraintEqualsValue' => Session::getInstance(false)->getUser()->getCustomerId(),						
					),
				),
			),
			'Trackableid' => array(
				'multiple' => true,
				'objectType' => 'Trackable',
				'constraints' => array(
					array(
						'constraint' => 'trackablereference',
						'constraintEqualsParam' => 'reference',
					),
					array(
						'constraint' => 'customerid',
						'constraintEqualsValue' => Session::getInstance(false)->getUser()->getCustomerId(),						
					),
				),
			),
		);
		try {
			$valid = true;
			$objects = ModelService::getObjectsByParams('Triptrackable', $this->requestHandler->getParams(), $paramMap, false, $valid);
		
			if (!$valid) {
				$this->validationErrorAction($objects);
				return;
			}
			else {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	} 
	
	public function registerTripGroupAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			
			if (!ModelService::hasParam('groupreference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'groupreference')));
				$mse->setHttpCode(412);
				throw $mse;
			}

			$groupParams = ModelService::getParamObject('groupreference', $this->requestHandler->getParams());
			$groups = GroupreferenceQuery::create()
				->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
				->findByGroupreference($groupParams);
			
			if (count($groups) == 0) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'groupreference'));
				$e->setHttpCode(412);
				throw $e;
			}
			
			$trackableIds = array();
			foreach($groups as $group) {
				if (!in_array($group->getTrackableid(), $trackableIds)) {
					$trackableIds[] = $group->getTrackableid();
				}
			}
			
			$paramMap = array(
				'Tripid' => array(
					'objectType' => 'Trip',
					'constraints' => array(
						array(
							'constraint' => 'tripreference',
							'constraintEqualsParam' => 'tripReference',
						),
						array(
							'constraint' => 'customerid',
							'constraintEqualsValue' => Session::getInstance(false)->getUser()->getCustomerId(),						
						),
					),
				),
				'Trackableid' => array(
					'multiple' => true,
					'value' => $trackableIds,
				),
			);
			$valid = true;
			$objects = ModelService::getObjectsByParams('Triptrackable', $this->requestHandler->getParams(), $paramMap, false, $valid);
		
			if (!$valid) {
				$this->validationErrorAction($objects);
				return;
			}
			else {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	} 
	
	public function registerTripVehicleAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
			
		try {	

			if (!ModelService::hasParam('tripReference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'tripreference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
	
			if (!ModelService::hasParam('vehicleReference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'vehiclereference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
			
			$tripParam = ModelService::getParamObject('tripReference', $this->requestHandler->getParams());
			$trip = TripQuery::create()
				->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
				->findOneByTripreference($tripParam);
				
			if (count($trip) == 0) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'tripreference'));
				$e->setHttpCode(412);
				throw $e;
			}
			$tripId = $trip->getPrimaryKey();
			
			
			$vehicleParam = ModelService::getParamObject('vehicleReference', $this->requestHandler->getParams());
			$vehicle = VehicleQuery::create()
				->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
				->findOneByVehiclereference($vehicleParam);
			
			if (empty($vehicle)) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'vehiclereference'));
				$e->setHttpCode(412);
				throw $e;
			}
			
			$paramMap = array(
				'Tripid' => array(
					'value' => $tripId,
				),
				'Vehicleid' => array(
					'multiple' => true,
					'value' => $vehicle->getPrimaryKey(),
				),
			);
			$valid = true;
			$object = ModelService::getObjectByParams('Tripvehicle', $this->requestHandler->getParams(), $paramMap, true, $valid, false);
	
			if (!$valid) {
				$this->validationErrorAction($object);
				return;
			}
			else {
				$vehicle->removeTripvehicles();
				$object->save();
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function registerTrackableAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
					
		$startParamMap = array(
			'Name' => array(
				'param' => 'fromName',
			),
			'Address1' => array(
				'param' => 'fromAddress1',
			),
			'Address2' => array(
				'optional' => true,
				'param' => 'fromAddress2',
			),
			'Postalcode' => array(
				'param' => 'fromPostalCode',
			),
			'City' => array(
				'param' => 'fromCity',
			),
			'Countryid' => array(
				'objectType' => 'Country',
				'constraints' => array(
					array(
						'constraint' => 'Isocode',
						'constraintEqualsParam' => 'fromCountry',
					),
				),
			),
		);
						
		$endParamMap = array(
			'Name' => array(
				'param' => 'toName',
			),
			'Address1' => array(
				'param' => 'toAddress1',
			),
			'Address2' => array(
				'optional' => true,
				'param' => 'toAddress2',
			),
			'Postalcode' => array(
				'param' => 'toPostalCode',
			),
			'City' => array(
				'param' => 'toCity',
			),
			'Countryid' => array(
				'objectType' => 'Country',
				'constraints' => array(
					array(
						'constraint' => 'Isocode',
						'constraintEqualsParam' => 'toCountry',
					),
				),
			),
		);
		
		try {
			
			$valid = true;
			if($this->requestHandler->hasParamMap($startParamMap)) {
				$startAddress = ModelService::getObjectByParams('Address', $this->requestHandler->getParams(), $startParamMap, false, $valid);
				if(!$valid) {
					$this->validationErrorAction($startAddress);
					return;
				}
				$startAddress->geoCode();
				$startAddress->save();
			} else {
				$startAddress = AddressQuery::create()->findPk(1);
			}
			
			if($this->requestHandler->hasParamMap($endParamMap)) {
				$endAddress = ModelService::getObjectByParams('Address', $this->requestHandler->getParams(), $endParamMap, false, $valid);
				if(!$valid) {
					$this->validationErrorAction($endAddress);
					return;
				}
				$endAddress->geoCode();
				$endAddress->save();
			} else {
				$endAddress = AddressQuery::create()->findPk(1);
			}
				
			$trackableMap = array(
				'Customerid' => array('value' => Session::getInstance(false)->getUser()->getCustomerId()),
				'StartAddressid' => array('value' => $startAddress->getAddressid()),
				'EndAddressid' => array('value' => $endAddress->getAddressid()),
				'Trackablereference' => array('value' => $this->requestHandler->getParam('reference'))
			);
			
			$trackable = ModelService::getObjectByParams('Trackable', $this->requestHandler->getParams(), $trackableMap, true, $valid);
			
			if(!$valid) {
				$this->validationErrorAction($trackable);
				return;
			} else {
				$this->view->setHeader(201);
				$this->view->setValue('status', 'ok');
			}
			
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getTrackableLocationAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			if (!ModelService::hasParam('reference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'reference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
			
			$referenceParam = ModelService::getParamObject('reference', $this->requestHandler->getParams());
			if (is_array($referenceParam)) {
				$expectedCount = count($referenceParam);
			}
			else {
				$expectedCount = 1;
			}
			
			$trackables = TrackableQuery::create()
				->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
				->findByTrackablereference($referenceParam);
				
			if (count($trackables) == 0 || (isset($expectedCount) && count($trackables) < $expectedCount)) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'reference'));
				$e->setHttpCode(412);
				throw $e;
			}
			
			$locations = array();
			foreach ($trackables as $i => $trackable) {
				
				$status = $trackable->getLastStatus();
				if (
					is_object($status)
					&& $status->getStatustype()->getHasLocation()
				) {
					$lat = $status->getLat();
					$lng = $status->getLng();
				}
				else {
					list($lat, $lng) = array($trackable->getAddressRelatedByStartAddressid()->getLat(),$trackable->getAddressRelatedByStartAddressid()->getLng());
				}
				$locations[$trackable->getTrackablereference()] = array(
					'lat' => $lat,
					'lng' => $lng,
				);
			}
			
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('locations', $locations);
			
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getGroupLocationAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			if (!ModelService::hasParam('groupReference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'groupReference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
				
			$groupReferenceParam = ModelService::getParamObject('groupReference', $this->requestHandler->getParams());
			if (!is_array($groupReferenceParam)) {
				$groupReferenceParam = array($groupReferenceParam);
			}
			
			$trackables = array();
			foreach($groupReferenceParam as $groupReference) {
				$group = GroupreferenceQuery::create()
					->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
					->findByGroupreference($groupReference);
				if (count($group) == 0) {
					$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'reference'));
					$e->setHttpCode(412);
					throw $e;
				}
				foreach ($group as $groupdetail) {
					$trackables[$groupdetail->getGroupreference()][] = $groupdetail->getTrackable();
				}
			}
			
			$locations = array();
			foreach ($trackables as $groupReference => $groupTrackables) {
				if (!isset($locations[$groupReference])) {
					$locations[$groupReference] = array();
				}
				foreach($groupTrackables as $trackable) {
					$status = $trackable->getLastStatus();
					if (
						is_object($status)
						&& $status->getStatustype()->getHasLocation()
					) {
						$lat = $status->getLat();
						$lng = $status->getLng();
					}
					else {
						list($lat, $lng) = array($trackable->getAddressRelatedByStartAddressid()->getLat(),$trackable->getAddressRelatedByStartAddressid()->getLng());
			
					}
					$location = array(
							'lat' => $lat,
							'lng' => $lng,
					);
					if (!in_array($location, $locations[$groupReference])) {
						$locations[$groupReference][] = $location;
					}
				}
			}
				
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('locations', $locations);
				
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getTrackableStatusAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			if (!ModelService::hasParam('reference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'reference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
				
			$referenceParam = ModelService::getParamObject('reference', $this->requestHandler->getParams());
			if (is_array($referenceParam)) {
				$expectedCount = count($referenceParam);
			}
			else {
				$expectedCount = 1;
			}
				
			$trackables = TrackableQuery::create()
				->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
				->findByTrackablereference($referenceParam);
			
			if (count($trackables) == 0 || (isset($expectedCount) && count($trackables) < $expectedCount)) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'reference'));
				$e->setHttpCode(412);
				throw $e;
			}
			
			$statuses = array();
			foreach ($trackables as $i => $trackable) {
				$status = $trackable->getLastStatus();
				if (is_object($status)) {
					$statusArray = array(
						'status' => $status->getStatustype()->getStatustype(),
						'time' => $status->getTime(),
						'lat' => $status->getLat(),
						'lng' => $status->getLng(),
					);
					if (empty($statusArray['lat']) || empty($statusArray['lng'])) {
						$statusArray['lat'] = $trackable->getAddressRelatedByEndaddressid()->getLat();
						$statusArray['lng'] = $trackable->getAddressRelatedByEndaddressid()->getLng();
					}
				}
				else {
					$statusArray = array(
						'status' => 'Unknown',
						'time' => $trackable->getCreated(),
						'lat' => $trackable->getAddressRelatedByStartAddressid()->getLat(),
						'lng' => $trackable->getAddressRelatedByStartAddressid()->getLng(),  
					);
				}
				$statuses[$trackable->getTrackablereference()][] = $statusArray;
			}
			
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('statuses', $statuses);
				
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getVehiclesAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			
			$vehicles = array();
			$trackables = array();
			
			$vehicle = VehicleQuery::create()->findByCustomerid(Session::getInstance()->getUser()->getCustomerid());
			
			foreach ($vehicle as $v) {
				$status = $v->getLastStatus();
				if($status != null) {
					 array_push($vehicles, 
						array(
							'reference' => $v->getVehiclereference(),
							'lng' => $status->getLng(),
							'lat' => $status->getLat()
						)
					);		
				}
			}
			
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('vehicles', $vehicles);
			
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getGroupStatusAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			if (!ModelService::hasParam('groupReference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'groupReference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
				
			$groupReferenceParam = ModelService::getParamObject('groupReference', $this->requestHandler->getParams());
			if (!is_array($groupReferenceParam)) {
				$groupReferenceParam = array($groupReferenceParam);
			}
			
			$trackables = array();
			foreach($groupReferenceParam as $groupReference) {
				$group = GroupreferenceQuery::create()
					->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
					->findByGroupreference($groupReference);
				if (count($group) == 0) {
					$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'reference'));
					$e->setHttpCode(412);
					throw $e;
				}
				foreach ($group as $groupdetail) {
					$trackables[$groupdetail->getGroupreference()][] = $groupdetail->getTrackable();
				}
			}
			
			$statuses = array();
			foreach ($trackables as $groupReference => $groupTrackables) {
				if (!isset($statuses[$groupReference])) {
					$statuses[$groupReference] = array();
				}
				foreach($groupTrackables as $trackable) {
					$status = $trackable->getLastStatus();
					if (is_object($status)) {
						$statusArray = array(
							'status' => $status->getStatustype()->getStatustype(),
							'time' => $status->getTime(),
							'lat' => $status->getLat(),
							'lng' => $status->getLng(),
						);
					}
					else {
						$statusArray = array(
							'status' => 'Unknown',
							'time' => $trackable->getCreated(),
							'lat' => $trackable->getAddressRelatedByStartAddressid()->getLat(),
							'lng' => $trackable->getAddressRelatedByStartAddressid()->getLng(),  
						);
					}
					if (!in_array($statusArray, $statuses[$groupReference])) {
						$statuses[$groupReference][] = $statusArray;
					}
				}
			}
				
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('statuses', $statuses);
				
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
	
	public function getTrackableDistanceAction() {
		
		if(!$this->checkAccess(Controller::USERTYPE_USER)) 
			return;
		
		try {
			
			if (!ModelService::hasParam('reference', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'reference')));
				$mse->setHttpCode(412);
				throw $mse;
			}
			
			if (!ModelService::hasParam('lat', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'lat')));
				$mse->setHttpCode(412);
				throw $mse;
			}
			else {
				$lat = ModelService::getParamObject('lat', $this->requestHandler->getParams());
				if ($lat <= -90)  {
					$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('%s cannot be less than -90 degrees'), 'lat')));;
					$mse->setHttpCode(412);
					throw $mse;
				}
				elseif ($lat > 90) {
					$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('%s cannot be greater than 90 degrees'), 'lat')));
					$mse->setHttpCode(412);
					throw $mse;
				}
			}
			
			if (!ModelService::hasParam('lng', $this->requestHandler->getParams())) {
				$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('missing parameter %s'), 'lng')));
				$mse->setHttpCode(412);
				throw $mse;
			}
			else {
				$lng = ModelService::getParamObject('lng', $this->requestHandler->getParams());
				if ($lng <= -90)  {
					$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('%s cannot be less than -90 degrees'), 'lng')));;
					$mse->setHttpCode(412);
					throw $mse;
				}
				elseif ($lng > 90) {
					$mse = new ModelServiceException(ucfirst(sprintf(Localizer::getText('%s cannot be greater than 90 degrees'), 'lng')));
					$mse->setHttpCode(412);
					throw $mse;
				}
			}
	
			$referenceParam = ModelService::getParamObject('reference', $this->requestHandler->getParams());
			if (is_array($referenceParam)) {
				$expectedCount = count($referenceParam);
			}
			else {
				$expectedCount = 1;
			}
	
			$trackables = TrackableQuery::create()
			->filterByCustomerid(Session::getInstance(false)->getUser()->getCustomerId())
			->findByTrackablereference($referenceParam);
				
			if (count($trackables) == 0 || (isset($expectedCount) && count($trackables) < $expectedCount)) {
				$e = new ModelServiceException(sprintf(ucfirst(Localizer::getText("one or more references of %s not valid")), 'reference'));
				$e->setHttpCode(412);
				throw $e;
			}
				
			$distances = array();
			foreach ($trackables as $i => $trackable) {
				$status = $trackable->getLastStatus();
				if (
					is_object($status)
					&& $status->getStatustype()->getHasLocation()
				) {
					$Tlat = $status->getLat();
					$Tlng = $status->getLng();
					$distance = Tools::haversineDistance($Tlat, $Tlng, $lat, $lng);
				}
				else {
					$distance = NULL;
				}
				$distances[$trackable->getTrackablereference()][] = $distance;
			}
				
			$this->view->setHeader(200);
			$this->view->setValue('status', 'ok');
			$this->view->setValue('distances', $distances);
	
		} catch (ModelServiceException $e) {
			$this->handleModelServiceException($e);
			return;
		}
	}
		
	
	public function validationErrorAction($ormObjects) {
		if (!is_array($ormObjects)) {
			$ormObject0 = $ormObjects;
			$ormObjects = array();
			$ormObjects[] = $ormObject0;
		}
		$errors = array();
		foreach ($ormObjects as $i => $ormObject) {
			foreach ($ormObject->getValidationFailures() as $failure) {
				$errors[] = ucfirst(sprintf(Localizer::getText('on item %s'), $i+1)) . ': ' . lcfirst(Localizer::getText($failure->getMessage())) . '.';	
			}
		}
		$e = new ModelServiceException(implode('\n', $errors));
		$e->setHttpCode(412);
		$this->handleModelServiceException($e);
	}
	
	public function unsupportedactionAction() {
		$e = new ModelServiceException(ucfirst(sprintf(Localizer::getText("unsupported action '%s'"), $this->requestHandler->getParam('unsupportedActionName'))));
		$e->setHttpCode(501);
		$this->handleModelServiceException($e);
	}
	
	
	public function render() {
		if (!isset($this->view) || !is_object($this->view) || get_class($this->view) != 'View') {
			$this->view = new View();
			$this->view->setViewName(self::JSON_VIEWNAME);
		}

		// If callback parameter defined, only the HTTP 200 code is used
		if (
			!$this->requestHandler->getParam('callback') 
			&& stristr($_SERVER['SERVER_PROTOCOL'], 'http')
		) {
			$httpCode = Tools::getHttpCode($this->view->getHeader());
			header($_SERVER["SERVER_PROTOCOL"] . ' ' . $httpCode['code'] . ' ' . $httpCode['description'], true, $httpCode['code']);
		}	
		$this->view->renderView();
		
	}
	
	private function handleModelServiceException(ModelServiceException $e) {
		$this->view->setHeader($e->getHttpCode());
		$this->view->setValues(array());
		$this->view->setValue('error', $e->getMessage());
		$this->requestHandler->setContinueExecution(false);
	}
}
