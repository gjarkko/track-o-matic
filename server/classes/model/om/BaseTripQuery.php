<?php


/**
 * Base class that represents a query for the 'trip' table.
 *
 * 
 *
 * @method     TripQuery orderByTripid($order = Criteria::ASC) Order by the tripid column
 * @method     TripQuery orderByCustomerid($order = Criteria::ASC) Order by the customerid column
 * @method     TripQuery orderByTripreference($order = Criteria::ASC) Order by the tripreference column
 * @method     TripQuery orderByValid($order = Criteria::ASC) Order by the valid column
 *
 * @method     TripQuery groupByTripid() Group by the tripid column
 * @method     TripQuery groupByCustomerid() Group by the customerid column
 * @method     TripQuery groupByTripreference() Group by the tripreference column
 * @method     TripQuery groupByValid() Group by the valid column
 *
 * @method     TripQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TripQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TripQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TripQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     TripQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     TripQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     TripQuery leftJoinTriptrackable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Triptrackable relation
 * @method     TripQuery rightJoinTriptrackable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Triptrackable relation
 * @method     TripQuery innerJoinTriptrackable($relationAlias = null) Adds a INNER JOIN clause to the query using the Triptrackable relation
 *
 * @method     TripQuery leftJoinTripvehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tripvehicle relation
 * @method     TripQuery rightJoinTripvehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tripvehicle relation
 * @method     TripQuery innerJoinTripvehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Tripvehicle relation
 *
 * @method     Trip findOne(PropelPDO $con = null) Return the first Trip matching the query
 * @method     Trip findOneOrCreate(PropelPDO $con = null) Return the first Trip matching the query, or a new Trip object populated from the query conditions when no match is found
 *
 * @method     Trip findOneByTripid(int $tripid) Return the first Trip filtered by the tripid column
 * @method     Trip findOneByCustomerid(int $customerid) Return the first Trip filtered by the customerid column
 * @method     Trip findOneByTripreference(string $tripreference) Return the first Trip filtered by the tripreference column
 * @method     Trip findOneByValid(boolean $valid) Return the first Trip filtered by the valid column
 *
 * @method     array findByTripid(int $tripid) Return Trip objects filtered by the tripid column
 * @method     array findByCustomerid(int $customerid) Return Trip objects filtered by the customerid column
 * @method     array findByTripreference(string $tripreference) Return Trip objects filtered by the tripreference column
 * @method     array findByValid(boolean $valid) Return Trip objects filtered by the valid column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseTripQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseTripQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Trip', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TripQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TripQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TripQuery) {
			return $criteria;
		}
		$query = new TripQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Trip|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = TripPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(TripPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trip A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `TRIPID`, `CUSTOMERID`, `TRIPREFERENCE`, `VALID` FROM `trip` WHERE `TRIPID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Trip();
			$obj->hydrate($row);
			TripPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Trip|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TripPeer::TRIPID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TripPeer::TRIPID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the tripid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTripid(1234); // WHERE tripid = 1234
	 * $query->filterByTripid(array(12, 34)); // WHERE tripid IN (12, 34)
	 * $query->filterByTripid(array('min' => 12)); // WHERE tripid > 12
	 * </code>
	 *
	 * @param     mixed $tripid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByTripid($tripid = null, $comparison = null)
	{
		if (is_array($tripid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TripPeer::TRIPID, $tripid, $comparison);
	}

	/**
	 * Filter the query on the customerid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCustomerid(1234); // WHERE customerid = 1234
	 * $query->filterByCustomerid(array(12, 34)); // WHERE customerid IN (12, 34)
	 * $query->filterByCustomerid(array('min' => 12)); // WHERE customerid > 12
	 * </code>
	 *
	 * @see       filterByCustomer()
	 *
	 * @param     mixed $customerid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByCustomerid($customerid = null, $comparison = null)
	{
		if (is_array($customerid)) {
			$useMinMax = false;
			if (isset($customerid['min'])) {
				$this->addUsingAlias(TripPeer::CUSTOMERID, $customerid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($customerid['max'])) {
				$this->addUsingAlias(TripPeer::CUSTOMERID, $customerid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TripPeer::CUSTOMERID, $customerid, $comparison);
	}

	/**
	 * Filter the query on the tripreference column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTripreference('fooValue');   // WHERE tripreference = 'fooValue'
	 * $query->filterByTripreference('%fooValue%'); // WHERE tripreference LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $tripreference The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByTripreference($tripreference = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tripreference)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tripreference)) {
				$tripreference = str_replace('*', '%', $tripreference);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TripPeer::TRIPREFERENCE, $tripreference, $comparison);
	}

	/**
	 * Filter the query on the valid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByValid(true); // WHERE valid = true
	 * $query->filterByValid('yes'); // WHERE valid = true
	 * </code>
	 *
	 * @param     boolean|string $valid The value to use as filter.
	 *              Non-boolean arguments are converted using the following rules:
	 *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
	 *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
	 *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByValid($valid = null, $comparison = null)
	{
		if (is_string($valid)) {
			$valid = in_array(strtolower($valid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
		}
		return $this->addUsingAlias(TripPeer::VALID, $valid, $comparison);
	}

	/**
	 * Filter the query by a related Customer object
	 *
	 * @param     Customer|PropelCollection $customer The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByCustomer($customer, $comparison = null)
	{
		if ($customer instanceof Customer) {
			return $this
				->addUsingAlias(TripPeer::CUSTOMERID, $customer->getCustomerid(), $comparison);
		} elseif ($customer instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(TripPeer::CUSTOMERID, $customer->toKeyValue('PrimaryKey', 'Customerid'), $comparison);
		} else {
			throw new PropelException('filterByCustomer() only accepts arguments of type Customer or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Customer relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Customer');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Customer');
		}

		return $this;
	}

	/**
	 * Use the Customer relation Customer object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CustomerQuery A secondary query class using the current class as primary query
	 */
	public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinCustomer($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Customer', 'CustomerQuery');
	}

	/**
	 * Filter the query by a related Triptrackable object
	 *
	 * @param     Triptrackable $triptrackable  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByTriptrackable($triptrackable, $comparison = null)
	{
		if ($triptrackable instanceof Triptrackable) {
			return $this
				->addUsingAlias(TripPeer::TRIPID, $triptrackable->getTripid(), $comparison);
		} elseif ($triptrackable instanceof PropelCollection) {
			return $this
				->useTriptrackableQuery()
				->filterByPrimaryKeys($triptrackable->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTriptrackable() only accepts arguments of type Triptrackable or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Triptrackable relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function joinTriptrackable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Triptrackable');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Triptrackable');
		}

		return $this;
	}

	/**
	 * Use the Triptrackable relation Triptrackable object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TriptrackableQuery A secondary query class using the current class as primary query
	 */
	public function useTriptrackableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTriptrackable($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Triptrackable', 'TriptrackableQuery');
	}

	/**
	 * Filter the query by a related Tripvehicle object
	 *
	 * @param     Tripvehicle $tripvehicle  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function filterByTripvehicle($tripvehicle, $comparison = null)
	{
		if ($tripvehicle instanceof Tripvehicle) {
			return $this
				->addUsingAlias(TripPeer::TRIPID, $tripvehicle->getTripid(), $comparison);
		} elseif ($tripvehicle instanceof PropelCollection) {
			return $this
				->useTripvehicleQuery()
				->filterByPrimaryKeys($tripvehicle->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByTripvehicle() only accepts arguments of type Tripvehicle or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Tripvehicle relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function joinTripvehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tripvehicle');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tripvehicle');
		}

		return $this;
	}

	/**
	 * Use the Tripvehicle relation Tripvehicle object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TripvehicleQuery A secondary query class using the current class as primary query
	 */
	public function useTripvehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTripvehicle($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tripvehicle', 'TripvehicleQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Trip $trip Object to remove from the list of results
	 *
	 * @return    TripQuery The current query, for fluid interface
	 */
	public function prune($trip = null)
	{
		if ($trip) {
			$this->addUsingAlias(TripPeer::TRIPID, $trip->getTripid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseTripQuery