<?php



/**
 * This class defines the structure of the 'postalcode' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class PostalcodeTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.PostalcodeTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('postalcode');
		$this->setPhpName('Postalcode');
		$this->setClassname('Postalcode');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('POSTALCODEID', 'Postalcodeid', 'INTEGER', true, 10, null);
		$this->addForeignKey('COUNTRYID', 'Countryid', 'INTEGER', 'country', 'COUNTRYID', true, 10, 1);
		$this->addColumn('POSTALCODE', 'Postalcode', 'VARCHAR', true, 10, null);
		$this->addColumn('POSTALAREA', 'Postalarea', 'VARCHAR', true, 60, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Country', 'Country', RelationMap::MANY_TO_ONE, array('countryid' => 'countryid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Address', 'Address', RelationMap::ONE_TO_MANY, array('postalcodeid' => 'postalcodeid', ), 'CASCADE', 'CASCADE', 'Addresss');
	} // buildRelations()

} // PostalcodeTableMap
