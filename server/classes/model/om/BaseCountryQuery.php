<?php


/**
 * Base class that represents a query for the 'country' table.
 *
 * 
 *
 * @method     CountryQuery orderByCountryid($order = Criteria::ASC) Order by the countryid column
 * @method     CountryQuery orderByIsocode($order = Criteria::ASC) Order by the isocode column
 * @method     CountryQuery orderByCountryname($order = Criteria::ASC) Order by the countryname column
 *
 * @method     CountryQuery groupByCountryid() Group by the countryid column
 * @method     CountryQuery groupByIsocode() Group by the isocode column
 * @method     CountryQuery groupByCountryname() Group by the countryname column
 *
 * @method     CountryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     CountryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     CountryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     CountryQuery leftJoinAddress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Address relation
 * @method     CountryQuery rightJoinAddress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Address relation
 * @method     CountryQuery innerJoinAddress($relationAlias = null) Adds a INNER JOIN clause to the query using the Address relation
 *
 * @method     Country findOne(PropelPDO $con = null) Return the first Country matching the query
 * @method     Country findOneOrCreate(PropelPDO $con = null) Return the first Country matching the query, or a new Country object populated from the query conditions when no match is found
 *
 * @method     Country findOneByCountryid(int $countryid) Return the first Country filtered by the countryid column
 * @method     Country findOneByIsocode(string $isocode) Return the first Country filtered by the isocode column
 * @method     Country findOneByCountryname(string $countryname) Return the first Country filtered by the countryname column
 *
 * @method     array findByCountryid(int $countryid) Return Country objects filtered by the countryid column
 * @method     array findByIsocode(string $isocode) Return Country objects filtered by the isocode column
 * @method     array findByCountryname(string $countryname) Return Country objects filtered by the countryname column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseCountryQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseCountryQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Country', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new CountryQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    CountryQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof CountryQuery) {
			return $criteria;
		}
		$query = new CountryQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Country|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = CountryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(CountryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Country A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `COUNTRYID`, `ISOCODE`, `COUNTRYNAME` FROM `country` WHERE `COUNTRYID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Country();
			$obj->hydrate($row);
			CountryPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Country|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(CountryPeer::COUNTRYID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(CountryPeer::COUNTRYID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the countryid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryid(1234); // WHERE countryid = 1234
	 * $query->filterByCountryid(array(12, 34)); // WHERE countryid IN (12, 34)
	 * $query->filterByCountryid(array('min' => 12)); // WHERE countryid > 12
	 * </code>
	 *
	 * @param     mixed $countryid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByCountryid($countryid = null, $comparison = null)
	{
		if (is_array($countryid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(CountryPeer::COUNTRYID, $countryid, $comparison);
	}

	/**
	 * Filter the query on the isocode column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByIsocode('fooValue');   // WHERE isocode = 'fooValue'
	 * $query->filterByIsocode('%fooValue%'); // WHERE isocode LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $isocode The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByIsocode($isocode = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($isocode)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $isocode)) {
				$isocode = str_replace('*', '%', $isocode);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CountryPeer::ISOCODE, $isocode, $comparison);
	}

	/**
	 * Filter the query on the countryname column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByCountryname('fooValue');   // WHERE countryname = 'fooValue'
	 * $query->filterByCountryname('%fooValue%'); // WHERE countryname LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $countryname The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByCountryname($countryname = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($countryname)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $countryname)) {
				$countryname = str_replace('*', '%', $countryname);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CountryPeer::COUNTRYNAME, $countryname, $comparison);
	}

	/**
	 * Filter the query by a related Address object
	 *
	 * @param     Address $address  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function filterByAddress($address, $comparison = null)
	{
		if ($address instanceof Address) {
			return $this
				->addUsingAlias(CountryPeer::COUNTRYID, $address->getCountryid(), $comparison);
		} elseif ($address instanceof PropelCollection) {
			return $this
				->useAddressQuery()
				->filterByPrimaryKeys($address->getPrimaryKeys())
				->endUse();
		} else {
			throw new PropelException('filterByAddress() only accepts arguments of type Address or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Address relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function joinAddress($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Address');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Address');
		}

		return $this;
	}

	/**
	 * Use the Address relation Address object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AddressQuery A secondary query class using the current class as primary query
	 */
	public function useAddressQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAddress($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Address', 'AddressQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Country $country Object to remove from the list of results
	 *
	 * @return    CountryQuery The current query, for fluid interface
	 */
	public function prune($country = null)
	{
		if ($country) {
			$this->addUsingAlias(CountryPeer::COUNTRYID, $country->getCountryid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseCountryQuery