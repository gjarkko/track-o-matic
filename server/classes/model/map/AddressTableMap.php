<?php



/**
 * This class defines the structure of the 'address' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class AddressTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.AddressTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('address');
		$this->setPhpName('Address');
		$this->setClassname('Address');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('ADDRESSID', 'Addressid', 'INTEGER', true, 10, null);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('NAME', 'Name', 'VARCHAR', true, 120, null);
		$this->addColumn('ADDRESS1', 'Address1', 'VARCHAR', true, 60, null);
		$this->addColumn('ADDRESS2', 'Address2', 'VARCHAR', false, 60, null);
		$this->addColumn('POSTALCODE', 'Postalcode', 'VARCHAR', true, 10, null);
		$this->addColumn('CITY', 'City', 'VARCHAR', true, 60, '');
		$this->addForeignKey('COUNTRYID', 'Countryid', 'INTEGER', 'country', 'COUNTRYID', true, 10, 1);
		$this->addColumn('LAT', 'Lat', 'DECIMAL', false, 18, null);
		$this->addColumn('LNG', 'Lng', 'DECIMAL', false, 18, null);
		// validators
		$this->addValidator('NAME', 'minLength', 'propel.validator.MinLengthValidator', '5', 'name must be at least 5 characters');
		$this->addValidator('NAME', 'maxLength', 'propel.validator.MaxLengthValidator', '120', 'name must be at most 120 characters');
		$this->addValidator('NAME', 'required', 'propel.validator.RequiredValidator', '', 'name is required');
		$this->addValidator('ADDRESS1', 'minLength', 'propel.validator.MinLengthValidator', '5', 'address 1 must be at least 5 characters');
		$this->addValidator('ADDRESS1', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'address 1 must be at most 60 characters');
		$this->addValidator('ADDRESS1', 'required', 'propel.validator.RequiredValidator', '', 'address 1 is required');
		$this->addValidator('ADDRESS2', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'address 2 must be at most 60 characters');
		$this->addValidator('POSTALCODE', 'minLength', 'propel.validator.MinLengthValidator', '2', 'postal code must be at least 2 characters');
		$this->addValidator('POSTALCODE', 'maxLength', 'propel.validator.MaxLengthValidator', '10', 'postal code must be at most 10 characters');
		$this->addValidator('POSTALCODE', 'required', 'propel.validator.RequiredValidator', '', 'postal code is required');
		$this->addValidator('CITY', 'minLength', 'propel.validator.MinLengthValidator', '2', 'city must be at least 2 characters');
		$this->addValidator('CITY', 'maxLength', 'propel.validator.MaxLengthValidator', '60', 'city must be at most 60 characters');
		$this->addValidator('CITY', 'required', 'propel.validator.RequiredValidator', '', 'city is required');
		$this->addValidator('LAT', 'minValue', 'propel.validator.MinValueValidator', '-90', 'latitude cannot be less than -90 degrees');
		$this->addValidator('LAT', 'maxValue', 'propel.validator.MaxValueValidator', '90', 'latitude cannot be greater than 90 degrees');
		$this->addValidator('LNG', 'minValue', 'propel.validator.MinValueValidator', '-90', 'longitude cannot be less than -90 degrees');
		$this->addValidator('LNG', 'maxValue', 'propel.validator.MaxValueValidator', '90', 'longitude cannot be greater than 90 degrees');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Country', 'Country', RelationMap::MANY_TO_ONE, array('countryid' => 'countryid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('TrackableRelatedByStartAddressid', 'Trackable', RelationMap::ONE_TO_MANY, array('addressid' => 'start_addressid', ), 'CASCADE', 'CASCADE', 'TrackablesRelatedByStartAddressid');
		$this->addRelation('TrackableRelatedByEndAddressid', 'Trackable', RelationMap::ONE_TO_MANY, array('addressid' => 'end_addressid', ), 'CASCADE', 'CASCADE', 'TrackablesRelatedByEndAddressid');
	} // buildRelations()

} // AddressTableMap
