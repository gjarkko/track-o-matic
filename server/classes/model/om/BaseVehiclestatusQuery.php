<?php


/**
 * Base class that represents a query for the 'vehiclestatus' table.
 *
 * 
 *
 * @method     VehiclestatusQuery orderByVehiclestatusid($order = Criteria::ASC) Order by the vehiclestatusid column
 * @method     VehiclestatusQuery orderByStatustypeid($order = Criteria::ASC) Order by the statustypeid column
 * @method     VehiclestatusQuery orderByVehicleid($order = Criteria::ASC) Order by the vehicleid column
 * @method     VehiclestatusQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     VehiclestatusQuery orderByLng($order = Criteria::ASC) Order by the lng column
 * @method     VehiclestatusQuery orderByTime($order = Criteria::ASC) Order by the time column
 * @method     VehiclestatusQuery orderByMessage($order = Criteria::ASC) Order by the message column
 *
 * @method     VehiclestatusQuery groupByVehiclestatusid() Group by the vehiclestatusid column
 * @method     VehiclestatusQuery groupByStatustypeid() Group by the statustypeid column
 * @method     VehiclestatusQuery groupByVehicleid() Group by the vehicleid column
 * @method     VehiclestatusQuery groupByLat() Group by the lat column
 * @method     VehiclestatusQuery groupByLng() Group by the lng column
 * @method     VehiclestatusQuery groupByTime() Group by the time column
 * @method     VehiclestatusQuery groupByMessage() Group by the message column
 *
 * @method     VehiclestatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     VehiclestatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     VehiclestatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     VehiclestatusQuery leftJoinStatustype($relationAlias = null) Adds a LEFT JOIN clause to the query using the Statustype relation
 * @method     VehiclestatusQuery rightJoinStatustype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Statustype relation
 * @method     VehiclestatusQuery innerJoinStatustype($relationAlias = null) Adds a INNER JOIN clause to the query using the Statustype relation
 *
 * @method     VehiclestatusQuery leftJoinVehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vehicle relation
 * @method     VehiclestatusQuery rightJoinVehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vehicle relation
 * @method     VehiclestatusQuery innerJoinVehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Vehicle relation
 *
 * @method     Vehiclestatus findOne(PropelPDO $con = null) Return the first Vehiclestatus matching the query
 * @method     Vehiclestatus findOneOrCreate(PropelPDO $con = null) Return the first Vehiclestatus matching the query, or a new Vehiclestatus object populated from the query conditions when no match is found
 *
 * @method     Vehiclestatus findOneByVehiclestatusid(int $vehiclestatusid) Return the first Vehiclestatus filtered by the vehiclestatusid column
 * @method     Vehiclestatus findOneByStatustypeid(int $statustypeid) Return the first Vehiclestatus filtered by the statustypeid column
 * @method     Vehiclestatus findOneByVehicleid(int $vehicleid) Return the first Vehiclestatus filtered by the vehicleid column
 * @method     Vehiclestatus findOneByLat(string $lat) Return the first Vehiclestatus filtered by the lat column
 * @method     Vehiclestatus findOneByLng(string $lng) Return the first Vehiclestatus filtered by the lng column
 * @method     Vehiclestatus findOneByTime(string $time) Return the first Vehiclestatus filtered by the time column
 * @method     Vehiclestatus findOneByMessage(string $message) Return the first Vehiclestatus filtered by the message column
 *
 * @method     array findByVehiclestatusid(int $vehiclestatusid) Return Vehiclestatus objects filtered by the vehiclestatusid column
 * @method     array findByStatustypeid(int $statustypeid) Return Vehiclestatus objects filtered by the statustypeid column
 * @method     array findByVehicleid(int $vehicleid) Return Vehiclestatus objects filtered by the vehicleid column
 * @method     array findByLat(string $lat) Return Vehiclestatus objects filtered by the lat column
 * @method     array findByLng(string $lng) Return Vehiclestatus objects filtered by the lng column
 * @method     array findByTime(string $time) Return Vehiclestatus objects filtered by the time column
 * @method     array findByMessage(string $message) Return Vehiclestatus objects filtered by the message column
 *
 * @package    propel.generator.model.om
 */
abstract class BaseVehiclestatusQuery extends ModelCriteria
{
	
	/**
	 * Initializes internal state of BaseVehiclestatusQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'server', $modelName = 'Vehiclestatus', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new VehiclestatusQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    VehiclestatusQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof VehiclestatusQuery) {
			return $criteria;
		}
		$query = new VehiclestatusQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key.
	 * Propel uses the instance pool to skip the database if the object exists.
	 * Go fast if the query is untouched.
	 *
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Vehiclestatus|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ($key === null) {
			return null;
		}
		if ((null !== ($obj = VehiclestatusPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
			// the object is alredy in the instance pool
			return $obj;
		}
		if ($con === null) {
			$con = Propel::getConnection(VehiclestatusPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		if ($this->formatter || $this->modelAlias || $this->with || $this->select
		 || $this->selectColumns || $this->asColumns || $this->selectModifiers
		 || $this->map || $this->having || $this->joins) {
			return $this->findPkComplex($key, $con);
		} else {
			return $this->findPkSimple($key, $con);
		}
	}

	/**
	 * Find object by primary key using raw SQL to go fast.
	 * Bypass doSelect() and the object formatter by using generated code.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Vehiclestatus A model object, or null if the key is not found
	 */
	protected function findPkSimple($key, $con)
	{
		$sql = 'SELECT `VEHICLESTATUSID`, `STATUSTYPEID`, `VEHICLEID`, `LAT`, `LNG`, `TIME`, `MESSAGE` FROM `vehiclestatus` WHERE `VEHICLESTATUSID` = :p0';
		try {
			$stmt = $con->prepare($sql);
			$stmt->bindValue(':p0', $key, PDO::PARAM_INT);
			$stmt->execute();
		} catch (Exception $e) {
			Propel::log($e->getMessage(), Propel::LOG_ERR);
			throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
		}
		$obj = null;
		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$obj = new Vehiclestatus();
			$obj->hydrate($row);
			VehiclestatusPeer::addInstanceToPool($obj, (string) $row[0]);
		}
		$stmt->closeCursor();

		return $obj;
	}

	/**
	 * Find object by primary key.
	 *
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con A connection object
	 *
	 * @return    Vehiclestatus|array|mixed the result, formatted by the current formatter
	 */
	protected function findPkComplex($key, $con)
	{
		// As the query uses a PK condition, no limit(1) is necessary.
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKey($key)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
		}
		$this->basePreSelect($con);
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		$stmt = $criteria
			->filterByPrimaryKeys($keys)
			->doSelect($con);
		return $criteria->getFormatter()->init($criteria)->format($stmt);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(VehiclestatusPeer::VEHICLESTATUSID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(VehiclestatusPeer::VEHICLESTATUSID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the vehiclestatusid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByVehiclestatusid(1234); // WHERE vehiclestatusid = 1234
	 * $query->filterByVehiclestatusid(array(12, 34)); // WHERE vehiclestatusid IN (12, 34)
	 * $query->filterByVehiclestatusid(array('min' => 12)); // WHERE vehiclestatusid > 12
	 * </code>
	 *
	 * @param     mixed $vehiclestatusid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByVehiclestatusid($vehiclestatusid = null, $comparison = null)
	{
		if (is_array($vehiclestatusid) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(VehiclestatusPeer::VEHICLESTATUSID, $vehiclestatusid, $comparison);
	}

	/**
	 * Filter the query on the statustypeid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByStatustypeid(1234); // WHERE statustypeid = 1234
	 * $query->filterByStatustypeid(array(12, 34)); // WHERE statustypeid IN (12, 34)
	 * $query->filterByStatustypeid(array('min' => 12)); // WHERE statustypeid > 12
	 * </code>
	 *
	 * @see       filterByStatustype()
	 *
	 * @param     mixed $statustypeid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByStatustypeid($statustypeid = null, $comparison = null)
	{
		if (is_array($statustypeid)) {
			$useMinMax = false;
			if (isset($statustypeid['min'])) {
				$this->addUsingAlias(VehiclestatusPeer::STATUSTYPEID, $statustypeid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($statustypeid['max'])) {
				$this->addUsingAlias(VehiclestatusPeer::STATUSTYPEID, $statustypeid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::STATUSTYPEID, $statustypeid, $comparison);
	}

	/**
	 * Filter the query on the vehicleid column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByVehicleid(1234); // WHERE vehicleid = 1234
	 * $query->filterByVehicleid(array(12, 34)); // WHERE vehicleid IN (12, 34)
	 * $query->filterByVehicleid(array('min' => 12)); // WHERE vehicleid > 12
	 * </code>
	 *
	 * @see       filterByVehicle()
	 *
	 * @param     mixed $vehicleid The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByVehicleid($vehicleid = null, $comparison = null)
	{
		if (is_array($vehicleid)) {
			$useMinMax = false;
			if (isset($vehicleid['min'])) {
				$this->addUsingAlias(VehiclestatusPeer::VEHICLEID, $vehicleid['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($vehicleid['max'])) {
				$this->addUsingAlias(VehiclestatusPeer::VEHICLEID, $vehicleid['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::VEHICLEID, $vehicleid, $comparison);
	}

	/**
	 * Filter the query on the lat column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLat(1234); // WHERE lat = 1234
	 * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
	 * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
	 * </code>
	 *
	 * @param     mixed $lat The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByLat($lat = null, $comparison = null)
	{
		if (is_array($lat)) {
			$useMinMax = false;
			if (isset($lat['min'])) {
				$this->addUsingAlias(VehiclestatusPeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lat['max'])) {
				$this->addUsingAlias(VehiclestatusPeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::LAT, $lat, $comparison);
	}

	/**
	 * Filter the query on the lng column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByLng(1234); // WHERE lng = 1234
	 * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
	 * $query->filterByLng(array('min' => 12)); // WHERE lng > 12
	 * </code>
	 *
	 * @param     mixed $lng The value to use as filter.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByLng($lng = null, $comparison = null)
	{
		if (is_array($lng)) {
			$useMinMax = false;
			if (isset($lng['min'])) {
				$this->addUsingAlias(VehiclestatusPeer::LNG, $lng['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($lng['max'])) {
				$this->addUsingAlias(VehiclestatusPeer::LNG, $lng['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::LNG, $lng, $comparison);
	}

	/**
	 * Filter the query on the time column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByTime('2011-03-14'); // WHERE time = '2011-03-14'
	 * $query->filterByTime('now'); // WHERE time = '2011-03-14'
	 * $query->filterByTime(array('max' => 'yesterday')); // WHERE time > '2011-03-13'
	 * </code>
	 *
	 * @param     mixed $time The value to use as filter.
	 *              Values can be integers (unix timestamps), DateTime objects, or strings.
	 *              Empty strings are treated as NULL.
	 *              Use scalar values for equality.
	 *              Use array values for in_array() equivalent.
	 *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByTime($time = null, $comparison = null)
	{
		if (is_array($time)) {
			$useMinMax = false;
			if (isset($time['min'])) {
				$this->addUsingAlias(VehiclestatusPeer::TIME, $time['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($time['max'])) {
				$this->addUsingAlias(VehiclestatusPeer::TIME, $time['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::TIME, $time, $comparison);
	}

	/**
	 * Filter the query on the message column
	 *
	 * Example usage:
	 * <code>
	 * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
	 * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
	 * </code>
	 *
	 * @param     string $message The value to use as filter.
	 *              Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByMessage($message = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($message)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $message)) {
				$message = str_replace('*', '%', $message);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(VehiclestatusPeer::MESSAGE, $message, $comparison);
	}

	/**
	 * Filter the query by a related Statustype object
	 *
	 * @param     Statustype|PropelCollection $statustype The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByStatustype($statustype, $comparison = null)
	{
		if ($statustype instanceof Statustype) {
			return $this
				->addUsingAlias(VehiclestatusPeer::STATUSTYPEID, $statustype->getStatustypeid(), $comparison);
		} elseif ($statustype instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(VehiclestatusPeer::STATUSTYPEID, $statustype->toKeyValue('PrimaryKey', 'Statustypeid'), $comparison);
		} else {
			throw new PropelException('filterByStatustype() only accepts arguments of type Statustype or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Statustype relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function joinStatustype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Statustype');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Statustype');
		}

		return $this;
	}

	/**
	 * Use the Statustype relation Statustype object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    StatustypeQuery A secondary query class using the current class as primary query
	 */
	public function useStatustypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinStatustype($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Statustype', 'StatustypeQuery');
	}

	/**
	 * Filter the query by a related Vehicle object
	 *
	 * @param     Vehicle|PropelCollection $vehicle The related object(s) to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function filterByVehicle($vehicle, $comparison = null)
	{
		if ($vehicle instanceof Vehicle) {
			return $this
				->addUsingAlias(VehiclestatusPeer::VEHICLEID, $vehicle->getVehicleid(), $comparison);
		} elseif ($vehicle instanceof PropelCollection) {
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
			return $this
				->addUsingAlias(VehiclestatusPeer::VEHICLEID, $vehicle->toKeyValue('PrimaryKey', 'Vehicleid'), $comparison);
		} else {
			throw new PropelException('filterByVehicle() only accepts arguments of type Vehicle or PropelCollection');
		}
	}

	/**
	 * Adds a JOIN clause to the query using the Vehicle relation
	 *
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function joinVehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Vehicle');

		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}

		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Vehicle');
		}

		return $this;
	}

	/**
	 * Use the Vehicle relation Vehicle object
	 *
	 * @see       useQuery()
	 *
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    VehicleQuery A secondary query class using the current class as primary query
	 */
	public function useVehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinVehicle($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Vehicle', 'VehicleQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Vehiclestatus $vehiclestatus Object to remove from the list of results
	 *
	 * @return    VehiclestatusQuery The current query, for fluid interface
	 */
	public function prune($vehiclestatus = null)
	{
		if ($vehiclestatus) {
			$this->addUsingAlias(VehiclestatusPeer::VEHICLESTATUSID, $vehiclestatus->getVehiclestatusid(), Criteria::NOT_EQUAL);
		}

		return $this;
	}

} // BaseVehiclestatusQuery