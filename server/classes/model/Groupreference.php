<?php



/**
 * Skeleton subclass for representing a row from the 'groupreference' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Groupreference extends BaseGroupreference {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}
	
	public function getTrackableReference() {
		if (is_object($this->getTrackable())) {
			return $this->getTrackable()->getTrackablereference();
		}
		else {
			return NULL;
		}
	}
	
	public static function getBrowserColumnMap() {
		return array(
			array(
				'column' => 'groupreference',
				'label' => ucfirst(Localizer::getText('reference')),
			),
		
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackables')),
				'foreign' => true,
				'display_method' => 'getTrackableReference',
				'display_SQL' => 'trackablereference',
		
				'join' => array(
					'relation' => 'Trackable',
					'table' => 'trackable',
					'own_key' => 'trackableid',
					'foreign_key' => 'trackableid',
				),
			),
		);
	}
		
	
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'groupreference',
				'label' => ucfirst(Localizer::getText('group reference')),
				'required' => true,
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'trackable',
				'label' => ucfirst(Localizer::getText('trackable')),
				'foreign' => true,
				'getMethod' => 'getTrackableReference',
				'getIdMethod' => 'getTrackableid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
		);
	}

	public function save(PropelPDO $con = null) {
	
		// If creating a new address, use the customer the user has been bound to.
		if (!$this->getPrimaryKey()) {
			if (Session::getInstance()->getUser()) {
				$this->setCustomerId(Session::getInstance()->getUser()->getCustomerid());
			}
		}
	
		parent::save($con);
	}
	
} // Groupreference
