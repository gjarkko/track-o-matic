<?php



/**
 * This class defines the structure of the 'localization' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class LocalizationTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.LocalizationTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('localization');
		$this->setPhpName('Localization');
		$this->setClassname('Localization');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('LOCALIZATIONID', 'Localizationid', 'INTEGER', true, 10, null);
		$this->addForeignKey('LANGUAGEID', 'Languageid', 'INTEGER', 'language', 'LANGUAGEID', true, 10, 1);
		$this->addColumn('LOCALIZATIONKEY', 'Localizationkey', 'VARCHAR', true, 60, null);
		$this->addColumn('LOCALIZATIONVALUE', 'Localizationvalue', 'VARCHAR', true, 120, null);
		// validators
		$this->addValidator('LOCALIZATIONVALUE', 'minLength', 'propel.validator.MinLengthValidator', '1', 'localization value must be at least 1 character');
		$this->addValidator('LOCALIZATIONVALUE', 'maxLength', 'propel.validator.MaxLengthValidator', '120', 'localization value must be at most 120 characters');
		$this->addValidator('LOCALIZATIONVALUE', 'required', 'propel.validator.RequiredValidator', '', 'localization value is required');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Language', 'Language', RelationMap::MANY_TO_ONE, array('languageid' => 'languageid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // LocalizationTableMap
