<?

class AutoLoader {
	
	const CACHE_FILE = 'classcache.dat';
	const CACHE_DIR = 'cache';
	const EXCEPTION_DIR = 'exception';
	
	public static $cache;
	
	/**
	*
	* Automatically load source for called class.
	*
	* Classes are looked for in these locations:
	*   - CLASS_PATH/* (recursive lookup)
	*   - EXTERNAL_PATH/* (recursive lookup)
	*
	* Case sensitive file system is assumed, class files should be named
	* exactly like the contained class. One class per file.
	*
	* First matching file wins - avoid duplicate naming like plaque.
	*/
	public static function autoload($class) {
	
		if (!isset(self::$cache)) {
			self::populateCache();
		}
				
		if (isset(self::$cache[$class])) {
			include self::$cache[$class];
		} else {
			
			// Try to rebuild cache once, then fail
			self::buildCache();
			if (isset(self::$cache[$class])) {
				include self::$cache[$class];
			//} else {
			//	throw new Exception('Class ' . $class . ' not found.');
			}
		}
	
	}
	
	/**
	 * 
	 * Populate the static class-file mapping 
	 * either from a pre-generated cache file 
	 * or by generating a new map.
	 */	
	public static function populateCache() {
		
		$cacheDirPath = ROOT_PATH . self::CACHE_DIR;
		$cacheFilePath = $cacheDirPath . DIRECTORY_SEPARATOR . self::CACHE_FILE;
				
		if (!file_exists($cacheFilePath) || !($cacheData = file_get_contents($cacheFilePath)) || !(self::$cache = @unserialize($cacheData))) {
			self::buildCache();
		}
	}
	
	/**
	 * Build cache by scanning the directory tree
	 */
	public static function buildCache() {
		$cacheDirPath = ROOT_PATH . self::CACHE_DIR;
		$cacheFilePath = $cacheDirPath . DIRECTORY_SEPARATOR . self::CACHE_FILE;
		// Build cache
		$sources = array(CLASS_PATH, VENDOR_PATH);
		$exclude = array(CLASS_PATH . DIRECTORY_SEPARATOR . 'view');
					
		$cache = array();
		foreach($sources as $source) {
			$cache = array_merge($cache, self::findClasses($source, $exclude));
		}
			
		self::$cache = $cache;
			
		// Store cache
		if (!file_exists($cacheDirPath) || !is_dir($cacheDirPath) || !is_writable($cacheDirPath)) {
			throw new Exception('Unable to write class cache to ' . $cacheDirPath);
		}
		else {
			if (file_exists($cacheFilePath) && !is_writable($cacheFilePath)) {
				throw new Exception('Unable to write class cache file to ' . $cacheFilePath);
			}
			file_put_contents($cacheFilePath, serialize(self::$cache));
		}
	}
	
	/**
	*
	* Recursively walk the directory tree for the predefined
	* include directories and store the result in a static variable.
	*/
	public static function findClasses($path, $exclude) {
		$classList = array();
		if (file_exists($path) && is_dir($path)) {
			$files = scandir($path);
			foreach ($files as $file) {
				if ($file != '.' && $file != '..') {
					$filePath = $path . DIRECTORY_SEPARATOR . $file;
					$filePath = preg_replace('/\/{2,}/', '/', $filePath);
					if (substr($file,-4) == '.php') {
						$classList[substr(basename($file),0,-4)] = $filePath;
					}
					if (is_dir($filePath) && !in_array($filePath, $exclude)) {
						$classList = array_merge($classList, self::findClasses($filePath, $exclude));
					}
				}
			}
		}
		return $classList;
	}
}
