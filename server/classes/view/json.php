<? 
$r = RequestHandler::getInstance();
if ($r->getParam('callback')) {
	$callbackFunction = lcfirst(str_replace('Action', '', $r->getCurrentActionName()));
	echo $callbackFunction . '(' . json_encode($this->values) . ');';
}
else {
	echo json_encode($this->values);
}