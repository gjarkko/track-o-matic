<?php



/**
 * This class defines the structure of the 'groupreference' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.model.map
 */
class GroupreferenceTableMap extends TableMap
{

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'model.map.GroupreferenceTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('groupreference');
		$this->setPhpName('Groupreference');
		$this->setClassname('Groupreference');
		$this->setPackage('model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('GROUPREFERENCEID', 'Groupreferenceid', 'INTEGER', true, 10, null);
		$this->addForeignKey('TRACKABLEID', 'Trackableid', 'INTEGER', 'trackable', 'TRACKABLEID', true, 10, 1);
		$this->addForeignKey('CUSTOMERID', 'Customerid', 'INTEGER', 'customer', 'CUSTOMERID', true, 10, 1);
		$this->addColumn('GROUPREFERENCE', 'Groupreference', 'VARCHAR', true, 120, null);
		$this->addColumn('VALID', 'Valid', 'BOOLEAN', true, 1, true);
		// validators
		$this->addValidator('GROUPREFERENCE', 'minLength', 'propel.validator.MinLengthValidator', '5', 'group reference must be at least 6 characters');
		$this->addValidator('GROUPREFERENCE', 'maxLength', 'propel.validator.MaxLengthValidator', '120', 'group reference must be at most 120 characters');
		$this->addValidator('GROUPREFERENCE', 'required', 'propel.validator.RequiredValidator', '', 'group reference is required');
		$this->addValidator('GROUPREFERENCE', 'match', 'propel.validator.MatchValidator', '/^([a-zA-Z0-9-\/])+$/', 'group reference must consist of letters, numbers or characters \'-\' and \'/\'');
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Trackable', 'Trackable', RelationMap::MANY_TO_ONE, array('trackableid' => 'trackableid', ), 'CASCADE', 'CASCADE');
		$this->addRelation('Customer', 'Customer', RelationMap::MANY_TO_ONE, array('customerid' => 'customerid', ), 'CASCADE', 'CASCADE');
	} // buildRelations()

} // GroupreferenceTableMap
