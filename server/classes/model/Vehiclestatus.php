<?php



/**
 * Skeleton subclass for representing a row from the 'status' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.model
 */
class Vehiclestatus extends BaseVehiclestatus {
	
	public static function getBrowserConfig() {
		return array(
			'allowView' => true,
			'allowEdit' => true,
			'allowCreate' => true,
			'allowDelete' => true,
		);
	}

	public function getVehicleReference() {
		if (is_object($this->getVehicle())) {
			return $this->getVehicle()->getVehiclereference();
		}
		else {
			return NULL;
		}
	}
	
	public static function getBrowserColumnMap($user = NULL) {
		return array(
			array(
				'column' => 'statustype',
				'label' => ucfirst(Localizer::getText('status type')),
				'foreign' => true,
				
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'vehicle',
				'label' => ucfirst(Localizer::getText('vehicle')),
				'foreign' => true,
				'display_method' => 'getVehicleReference',
				'display_SQL' => 'vehiclereference',
				
				'join' => array(
					'relation' => 'Vehicle',
					'table' => 'vehicle',
					'own_key' => 'vehicleid',
					'foreign_key' => 'vehicleid',
				),
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'lat',
				'label' => ucfirst(Localizer::getText('latitude')),
			),
			array(
				'column' => 'lng',
				'label' => ucfirst(Localizer::getText('longitude')),
			),
			array(
				'column' => 'time',
				'label' => ucfirst(Localizer::getText('time')),
			),
			array(
				'column' => 'message',
				'label' => ucfirst(Localizer::getText('message')),
			),
		);
	}
	
		
	public static function getCrudMap($user = NULL) {
		return array(
			array(
				'column' => 'statustype',
				'label' => ucfirst(Localizer::getText('status type')),
				'foreign' => true,
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'vehicle',
				'label' => ucfirst(Localizer::getText('vehicle')),
				'foreign' => true,
				'getMethod' => 'getVehicleReference',
				'getIdMethod' => 'getVehicleid',
				'required' => true,
				'inputType' => 'select',
				'default' => 1,
				'joinArgs' => array(
					array('customerid', (!empty($user) ? $user->getCustomerid() : NULL), Criteria::EQUAL)
				),
			),
			array(
				'column' => 'lat',
				'label' => ucfirst(Localizer::getText('latitude')),
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'lng',
				'label' => ucfirst(Localizer::getText('longitude')),
				'inputType' => 'text',
				'default' => '',
			),
			array(
				'column' => 'time',
				'label' => ucfirst(Localizer::getText('time')),
				'required' => true,
				'inputType' => 'text',
				'default' => date('Y-m-d H:i:s'),
			),
			array(
				'column' => 'message',
				'label' => ucfirst(Localizer::getText('message')),
				'inputType' => 'text',
				'default' => '',
			),
		);
	}
		
} // Status
