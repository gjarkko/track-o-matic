<?

class APITest extends PHPUnit_Framework_TestCase {
	
	protected function setUp() {
		require_once implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -2)) . DIRECTORY_SEPARATOR . 'testBootstrap.php';

		$this->app = new Application();
		$this->app->bootstrap();
		
		$this->customer1 = new Customer();
		$this->customer1
			->setCustomer('Asiakas 1')
			->save();
		
		$this->user1 = new User();
		$this->user1
			->setCustomer($this->customer1)
			->setUsername('Customer 1 driver')
			->setFirstname('Customer 1 driver')
			->setUsertype(UsertypeQuery::create()->findOneByUsertype('Driver'))
			->save();
		
		$this->vehicle1 = new Vehicle();
		$this->vehicle1
			->setCustomer($this->customer1)
			->setApikey(CryptographyProvider::encrypt('ABC'))
			->setVehiclereference('TRUCK1')
			->save();
			
		$this->statustype1 = new Statustype();
		$this->statustype1->setStatustype('Waiting')
			->setCustomer($this->customer1)
			->setHaslocation(0)
			->setIsmoving(0)
			->save();
			
		
		$this->statustype2 = new Statustype();
		$this->statustype2->setStatustype('En route')
			->setCustomer($this->customer1)
			->setHaslocation(1)
			->setIsmoving(1)
			->save();
		
		$this->statustype3 = new Statustype();
		$this->statustype3->setStatustype('Delivered')
			->setCustomer($this->customer1)
			->setHaslocation(1)
			->setIsmoving(0)
			->setIsfinal(1)
			->save();
		
		// Country c1 is Finland.
		$this->c1 = CountryQuery::create()->findOneByIsocode('FI');

		// Addresses a1 and a2 are the same. Only the receiver changes.
		$this->a1 = new Address();
		$this->a1
			->setName('Veikko Vastaanottaja')
			->setAddress1('Kellokukantie 2')
			->setPostalcode('01300')
			->setCity('Vantaa')
			->setCountry($this->c1)
			->save();
		
		$this->a2 = new Address();
		$this->a2
			->setName('Vieno Vastaanottaja')
			->setAddress1('Kellokukantie 2')
			->setPostalcode('01300')
			->setCity('Vantaa')
			->setCountry($this->c1)
			->save();

		// Address a3 is within 100 meters of addresses a1 and a2
		$this->a3 = new Address();
		$this->a3
			->setAddress1('Kellokukantie 3')
			->setPostalcode('01300')
			->setCity('Vantaa')
			->setCountry($this->c1)
			->save();
		
		// Address a4 is further than 100 meters of address a1
		$this->a4 = new Address();
		$this->a4
			->setAddress1('Ruusutie 3')
			->setPostalcode('01300')
			->setCity('Vantaa')
			->setCountry($this->c1)
			->save();
		
		// Trackables t1-t4 are destined to addresses a1-a4
		$this->trackable1 = new Trackable();
		$this->trackable1->setTrackablereference('T1')
			->setCustomer($this->customer1)
			->setAddressRelatedByStartAddressid($this->a1)
			->setAddressRelatedByEndAddressid($this->a1)
			->save();
		
		$this->trackable2 = new Trackable();
		$this->trackable2->setTrackablereference('T2')
			->setCustomer($this->customer1)
			->setAddressRelatedByStartAddressid($this->a1)
			->setAddressRelatedByEndAddressid($this->a2)
			->save();
		
		$this->trackable3 = new Trackable();
		$this->trackable3->setTrackablereference('T3')
			->setCustomer($this->customer1)
			->setAddressRelatedByStartAddressid($this->a1)
			->setAddressRelatedByEndAddressid($this->a3)
			->save();
		
		$this->trackable4 = new Trackable();
		$this->trackable4->setTrackablereference('T4')
			->setCustomer($this->customer1)
			->setAddressRelatedByStartAddressid($this->a1)
			->setAddressRelatedByEndAddressid($this->a4)
			->save();
		
		// Trackable t5 has no defined destination
		$this->trackable5 = new Trackable();
		$this->trackable5->setTrackablereference('T5')
			->setCustomer($this->customer1)
			->setAddressRelatedByStartAddressid($this->a1)
			->save();
		
		// Group 1 contains trackables t1-t3
		$this->groupreference1_1 = new Groupreference();
		$this->groupreference1_1
			->setCustomer($this->customer1)
			->setGroupreference('GROUP1')
			->setTrackable($this->trackable1)
			->save();
		
		$this->groupreference1_2 = new Groupreference();
		$this->groupreference1_2
			->setCustomer($this->customer1)
			->setGroupreference('GROUP1')
			->setTrackable($this->trackable2)
			->save();
		
		$this->groupreference1_3 = new Groupreference();
		$this->groupreference1_3
			->setCustomer($this->customer1)
			->setGroupreference('GROUP1')
			->setTrackable($this->trackable3)
			->save();
			
		
		// Group 2 contains trackables t1-t5
		$this->groupreference2_1 = new Groupreference();
		$this->groupreference2_1
			->setCustomer($this->customer1)
			->setGroupreference('GROUP2')
			->setTrackable($this->trackable1)
			->save();
		
		$this->groupreference2_2 = new Groupreference();
		$this->groupreference2_2
			->setCustomer($this->customer1)
			->setGroupreference('GROUP2')
			->setTrackable($this->trackable2)
			->save();
		
		$this->groupreference2_3 = new Groupreference();
		$this->groupreference2_3
			->setCustomer($this->customer1)
			->setGroupreference('GROUP2')
			->setTrackable($this->trackable3)
			->save();
		
		$this->groupreference2_4 = new Groupreference();
		$this->groupreference2_4
			->setCustomer($this->customer1)
			->setGroupreference('GROUP2')
			->setTrackable($this->trackable4)
			->save();
		
		$this->groupreference2_5 = new Groupreference();
		$this->groupreference2_5
			->setCustomer($this->customer1)
			->setGroupreference('GROUP2')
			->setTrackable($this->trackable5)
			->save();
		
		$this->trip1 = new Trip();
		$this->trip1
			->setCustomer($this->customer1)
			->setTripreference('TRIP1')
			->save();
		
		$this->trip2 = new Trip();
		$this->trip2
			->setCustomer($this->customer1)
			->setTripreference('TRIP2')
			->save();
		
		$this->trip3 = new Trip();
		$this->trip3
			->setCustomer($this->customer1)
			->setTripreference('TRIP3')
			->save();
	}
	
	protected function tearDown() {
		if (!$this->hasFailed()) {
			require implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)), 0, -2)) . DIRECTORY_SEPARATOR . 'populateTestDb.php';
		}
		
	}
	
	public function testAddressesWorks() {
		$this->assertInstanceOf('Country', $this->c1);
		$this->assertInstanceOf('Address', $this->a1);
		$this->assertInstanceOf('Address', $this->a2);
		$this->assertInstanceOf('Address', $this->a3);
		$this->assertInstanceOf('Address', $this->a4);
	}
	
	public function testCustomerWorks() {
		$this->assertInstanceOf('Customer', $this->customer1);
	}
	
	public function testRegisterDropWorksTrip1() {
		
		// Add trackables t1 and t2 to trip1
		$tt1 = new Triptrackable();
		$tt1
			->setTrip($this->trip1)
			->setTrackable($this->trackable1)
			->save();
		
		$tt2 = new Triptrackable();
		$tt2
			->setTrip($this->trip1)
			->setTrackable($this->trackable2)
			->save();
		
		// Register trip1 to vehicle1
		$tv1 = new Tripvehicle();
		$tv1
			->setTrip($this->trip1)
			->setVehicle($this->vehicle1)
			->save();
		
		// Add location update nearby
		$geocode = Geocoder::getInstance()->geocodeAddress('Kellokukantie 6, 01300, Vantaa, FI');
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'locationUpdate',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Trackable 1 should now have a location update of status type 'en route' with the last coordinates
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype2->getStatustypeid(), $lastStatus->getStatustypeid());
		
		// Attempt to drop without trackable reference at a location.
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'registerDrop',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Status should now have changed, since a single address trackable was used on both trackables.
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype3->getStatustypeid(), $lastStatus->getStatustypeid());
		
	}
	
	public function testRegisterDropTrip2Works() {
		
		// Add trackables t1 and t5 to trip2
		$tt1 = new Triptrackable();
		$tt1
			->setTrip($this->trip2)
			->setTrackable($this->trackable1)
			->save();
		
		$tt2 = new Triptrackable();
		$tt2
			->setTrip($this->trip2)
			->setTrackable($this->trackable5)
			->save();
		
		// Register trip1 to vehicle1
		$tv1 = new Tripvehicle();
		$tv1
			->setTrip($this->trip2)
			->setVehicle($this->vehicle1)
			->save();
		
		// Add location update nearby
		$geocode = Geocoder::getInstance()->geocodeAddress('Kellokukantie 6, 01300, Vantaa, FI');
		
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'locationUpdate',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Trackable 1 should now have a location update of status type 'en route' with the last coordinates
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype2->getStatustypeid(), $lastStatus->getStatustypeid());
		
		// Attempt to drop without trackable reference at a location.
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'registerDrop',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Status should not have changed, since there are several trackables within radius.
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype2->getStatustypeid(), $lastStatus->getStatustypeid());
		
		
		// Attempt to drop with a trackable reference at a location.
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'registerDrop',
			'apikey' => 'ABC',
			'reference' => 'T1',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Status should now have changed, since a single trackable was defined.
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype3->getStatustypeid(), $lastStatus->getStatustypeid());
	}
	
	public function testRegisterDropTrip3Works() {
	
		// Add trackables t1 and t4 to trip3
		$tt1 = new Triptrackable();
		$tt1
			->setTrip($this->trip2)
			->setTrackable($this->trackable1)
			->save();
		
		$tt2 = new Triptrackable();
		$tt2
			->setTrip($this->trip2)
			->setTrackable($this->trackable4)
			->save();
		
		// Register trip1 to vehicle1
		$tv1 = new Tripvehicle();
		$tv1
			->setTrip($this->trip2)
			->setVehicle($this->vehicle1)
			->save();
		
		// Add location update nearby
		$geocode = Geocoder::getInstance()->geocodeAddress('Kellokukantie 6, 01300, Vantaa, FI');
		
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'locationUpdate',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Trackable 1 should now have a location update of status type 'en route' with the last coordinates
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype2->getStatustypeid(), $lastStatus->getStatustypeid());
		
		// Attempt to drop without trackable reference at a location.
		$_REQUEST = array(
			'controller' => 'api',
			'action' => 'registerDrop',
			'apikey' => 'ABC',
			'lat' => $geocode['lat'],
			'lng' => $geocode['lng'],
		);
				
		$_SERVER['SERVER_PROTOCOL'] = 'test';
				
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
				
		// Status should now have changed, since there is only one trackable going nearby.
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype3->getStatustypeid(), $lastStatus->getStatustypeid());
	
	}
	
	public function testRegisterDropTrip4Works() {
	
		// Add trackables t1 and t4 to trip4
		$tt1 = new Triptrackable();
		$tt1
		->setTrip($this->trip2)
		->setTrackable($this->trackable1)
		->save();
		
		
		$tt2 = new Triptrackable();
		$tt2
		->setTrip($this->trip2)
		->setTrackable($this->trackable2)
		->save();
	
		$tt3 = new Triptrackable();
		$tt3
		->setTrip($this->trip2)
		->setTrackable($this->trackable4)
		->save();
	
		// Register trip1 to vehicle1
		$tv1 = new Tripvehicle();
		$tv1
		->setTrip($this->trip2)
		->setVehicle($this->vehicle1)
		->save();
	
		// Add location update nearby
		$geocode = Geocoder::getInstance()->geocodeAddress('Kellokukantie 6, 01300, Vantaa, FI');
	
		$_REQUEST = array(
				'controller' => 'api',
				'action' => 'locationUpdate',
				'apikey' => 'ABC',
				'lat' => $geocode['lat'],
				'lng' => $geocode['lng'],
		);
		$_SERVER['SERVER_PROTOCOL'] = 'test';
		
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
	
		
		// Trackable 1 should now have a location update of status type 'en route' with the last coordinates
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype2->getStatustypeid(), $lastStatus->getStatustypeid());
	
		// Attempt to drop without trackable reference at a location.
		$_REQUEST = array(
				'controller' => 'api',
				'action' => 'registerDrop',
				'apikey' => 'ABC',
				'lat' => $geocode['lat'],
				'lng' => $geocode['lng'],
		);
	
		$_SERVER['SERVER_PROTOCOL'] = 'test';
	
		ob_start();
		$this->app->run();
		$output = ob_get_contents();
		ob_end_clean();
		
		// Status should now have changed, since there is only one trackable going nearby.
		$lastStatus = $this->trackable1->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype3->getStatustypeid(), $lastStatus->getStatustypeid());
		
		
		// Status should now have changed, since there is only one trackable going nearby.
		$lastStatus = $this->trackable2->getLastStatus();
		$this->assertInstanceOf('Trackablestatus', $lastStatus);
		$this->assertEquals($geocode['lat'], $lastStatus->getLat());
		$this->assertEquals($geocode['lng'], $lastStatus->getLng());
		$this->assertEquals($this->statustype3->getStatustypeid(), $lastStatus->getStatustypeid());
		
	}
}